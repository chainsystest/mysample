import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class sessionValidator {
    constructor(private http: HttpClient) { }

    validateSession(isMobile) {
        if (isMobile) {
            return Promise.resolve(true)
        } else {            
            const url = '/apps/AuthGuardAuth';

            return this.http.get(url)
                .toPromise()
                .then(res => {                    
                    if (res === 'success') {
                        return true
                    } else {
                        return false
                    }
                }).catch(error => {
                    console.log("error===>", error)
                    return false
                })
        }
    }

    validateSessionWithOrgidandUserId() {

        const userId = localStorage['userId'];
        const orgId = localStorage['orgId'];
        if (userId && orgId) {
            
            const url = '/apps/sessionValidWithId';

            return this.http.post(url, { userId: userId, orgId: orgId })
                .toPromise()
                .then(res => {
            
                    return res
                }).catch(error => {
                    console.log("error===>", error)
                    const errmsg = error.message || 'Server connection failed';
                    return { 'status': 'failed', 'message': errmsg }
                })
        } else {
            
            const url = '/apps/sessionValid';
            return this.http.get(url)
                .toPromise()
                .then(res => {            
                    return res
                }).catch(error => {
                    console.log("error===>", error)
                    const errmsg = error.message || 'Server connection failed';
                    return { 'status': 'failed', 'message': errmsg }
                })
        }
    }
}


