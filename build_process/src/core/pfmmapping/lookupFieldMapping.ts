import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class lookupFieldMapping {

  public mappingDetail = {"pfm143053":{"customer":"pfm141093_287308","salesperson":"pfm141095_287312"},"pfm144293":{"salesperson":"pfm140973_289121","customer":"pfm141093_289113","opportunity":"pfm143053_289115"},"pfm144373":{"customername":"pfm147213_301933"},"pfm144393":{"product":"pfm142973_289353"}};

}
