import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class objectTableMapping {

  public mappingDetail = {"saleperson":"pfm140973","crmcustomer":"pfm141093","crmuser":"pfm141095","crmproduct":"pfm142973","crmopportunity":"pfm143053","crmquote":"pfm144293","crmsaleorders":"pfm144373","crmsaleorderlines":"pfm144393","customermaster":"pfm147213","addressdetails":"pfm147233","contactdetails":"pfm147274","inventorymaster":"pfm151994"};

}
