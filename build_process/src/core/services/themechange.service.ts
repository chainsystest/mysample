import { Injectable,Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import * as Color from 'color';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})



export class themechange {

 public themenewColor;
  
  constructor(
    @Inject(DOCUMENT) private document: Document,private storage:Storage) {
      storage.get('theme').then(cssText => {
        this.setGlobalCSS(cssText);
      });

   }  
  changeToStyleOne(){
    var style=document.querySelector('ion-router-outlet');
     style.setAttribute('class','hydrated');
     var toolbar=document.querySelector('ion-toolbar');
     toolbar.setAttribute('color','primary');
     var headerColor=document.querySelectorAll('h2');
     var hcolor=headerColor.length;
     for( var i=0; i<hcolor;i++){
          headerColor[i].removeAttribute('style');
        }
        var menuList=document.querySelectorAll('ion-content.menu-content .menu-content-list');
        var menuListLen=menuList.length;
        for( var i=0; i<menuListLen;i++){
            menuList[i].removeAttribute('style');
            }       
        var menuItem=document.querySelectorAll('ion-content.menu-content .menu-content-item');
        var menuItemLen=menuItem.length;
        for( var i=0; i<menuItemLen;i++){
            menuItem[i].removeAttribute('color');
            }
            var hldividerAttrColor=document.querySelectorAll('.styleLight ion-item-divider ion-row io-col ion-row');
            var hldividerAttrColorLen=hldividerAttrColor.length;
            for( var i=0; i<hldividerAttrColorLen;i++){  
              hldividerAttrColor[i].setAttribute('color',`var(--ion-color-primary, #3880ff)`);
            }
            var menuContentItem=document.querySelectorAll('ion-content.menu-content');
            var menuContentItemLen=menuContentItem.length;
            for( var i=0; i<menuContentItemLen;i++){
                menuContentItem[i].removeAttribute('color');
            }
            var holor=document.querySelectorAll('.styleLight .hl-scroll-list-text p');
            var hcolor=headerColor.length;
            for( var i=0; i<hcolor;i++){
              headerColor[i].removeAttribute('color');
            }
            var heColor=document.querySelectorAll('.styleLight .hl-card-content-item ion-label');
            var labelcolor=heColor.length;
            for( var i=0; i<labelcolor;i++){   
               heColor[i].removeAttribute('color');
             }
             var hlHeaderColor=document.querySelectorAll('.styleLight .hl-header-text');
             var hlHeaderColorLen=hlHeaderColor.length;
             for( var i=0; i<hlHeaderColorLen;i++){  
               hlHeaderColor[i].removeAttribute('color');
             }
             var hldividerIconColor=document.querySelectorAll('.styleLight ion-item-divider ion-icon');
             var hldividerIconColorLen=hlHeaderColor.length;
             for( var i=0; i<hldividerIconColorLen;i++){  
               hldividerIconColor[i].removeAttribute('backgroundcolor');
             }
             var hldividerAttrColor=document.querySelectorAll('ion-item-divider ion-icon');
             var hldividerAttrColorLen=hldividerAttrColor.length;
             for( var i=0; i<hldividerAttrColorLen;i++){  
               hldividerAttrColor[i].removeAttribute('backgroundcolor');
             } 
             this.themeChanged();

  }
   setColor(theme){
    this.themenewColor = theme;
       console.log(this.themenewColor);
  }
themeChanged() {
  var menuList = document.querySelectorAll('.styleLight ion-content.menu-content .menu-content-list');
  var menuListLen = menuList.length;
  for (var i = 0; i < menuListLen; i++) {
      menuList[i].setAttribute('style', `background:var(--ion-color-primary, #3880ff)`);
  }
  var menuItem = document.querySelectorAll('.styleLight ion-content.menu-content .menu-content-item');
  var menuItemLen = menuItem.length;
  for (var i = 0; i < menuItemLen; i++) {
      menuItem[i].setAttribute('color', `var(--ion-color-primary, #3880ff)`);
  }
  var menuItem = document.querySelectorAll('.styleLight ion-content.menu-content .menu-content-item');
  var menuItemLen = menuItem.length;
  for (var i = 0; i < menuItemLen; i++) {
      var menuValue = menuItem[i].classList.remove('class', 'active');
  }
  var menuContent = document.querySelectorAll('.styleLight ion-content.menu-content');
  var menuContentLen = menuContent.length;
  for (var i = 0; i < menuContentLen; i++) {
      menuContent[i].setAttribute('color', `var(--ion-color-primary, #3880ff)`);
  }
}
  setTheme(theme){
    const cssText=CSSTextGenerator(theme);
    this.setGlobalCSS(cssText);
    this.storage.set('theme', cssText);
   }
   setVariable(name,value){
     this.document.documentElement.style.setProperty(name,value);
   }
   private setGlobalCSS(css:string){
    this.document.documentElement.style.cssText= css;
   }
  changeToStyleTwo(){
    var style=document.querySelector('ion-router-outlet');
    style.setAttribute('class','styleLight hydrated');

    var header=document.querySelector('ion-toolbar');
    header.removeAttribute('color');
    this.themeChanged();
  }
  }
  const defaults = {
    primary: '',
    secondary: '',
    tertiary: '',
  };
  function CSSTextGenerator(colors) {
    colors = { ...defaults, ...colors };
  
    const {
      primary,
      secondary,
      tertiary,
    } = colors;
  
     const shadeRatio = 0.1;
     const tintRatio = 0.1;
  
     return `
       --ion-color-primary: ${primary};
       --ion-color-primary-rgb: 56,128,255;
       --ion-color-primary-contrast: ${contrast(primary)};
       --ion-color-primary-contrast-rgb: 255,255,255;
       --ion-color-primary-shade:  ${Color(primary).darken(shadeRatio)}; 
   `;
   }
   function contrast(color, ratio = 1){
    color = Color(color);
    return color.isDark() ? color.lighten(ratio) : color.darken(ratio);
  }


