import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class attachmentDbConfiguration {

  // Note : After the session_id handled in jwt. We need to remove databaseName,remoteDbUrl,user name ,user password keys.
  public configuration = {
    databaseName: '',
    remoteDbUrl: '',
    user: {
      name: '',
      password: ''
    },
    schema: [],
    dataFilters: [],
    tableStructure: {},
    pouchDBSyncEnabledObjectSelectors: [],
    couchDBSyncEnabledObjectSelectors: [],
    attachmentIndexingObject: []
  };

  private _proxyPassURL: string;
  private _dataBaseName:  string;
  private _userName: string;
  private _password: string;

  get remoteDbUrl(): string {
    return this._proxyPassURL;
  }
  set remoteDbUrl(proxyPass: string) {
    this._proxyPassURL = proxyPass;
  }
  get databaseName(): string {
    return this._dataBaseName;
  }
  set databaseName(dBaseName: string) {
    this._dataBaseName = dBaseName;
  }
  get username(): string {
    return this._userName;
  }
  set username(userName: string) {
    this._userName = userName;
  }
  get password(): string {
    return this._password;
  }
  set password(passWord: string) {
    this._password = passWord;
  }
}
