import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class cspfmExecutionPouchDbConfiguration {
  public workFlowUserApprovalStatusObject = 'WorkFlowUserApprovalStatus';
  // Note : After the session_id handled in jwt. We need to remove databaseName,remoteDbUrl,user name ,user password keys.
  public configuration = {
    databaseName: "pfm_3_executions",
    remoteDbUrl: "https://dev.chainsys.com/couchdb/",
    user: {
      name: "qadatauser",
      password: "welcome1"
    },
    schema: [
      {
        "singular": "WorkFlowUserApprovalStatus",
        "plural": "WorkFlowUserApprovalStatuss"
      }
      ,
      {
        "singular": "WorkFlowUserApprovalStatus_only",
        "plural": "WorkFlowUserApprovalStatuss",
        "documentType": "WorkFlowUserApprovalStatus"
      },
    ],
    tableStructure: {}
  }
}
