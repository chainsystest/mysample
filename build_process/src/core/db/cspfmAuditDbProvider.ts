import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import PouchDB from 'pouchdb';
import { dbConfiguration } from './dbConfiguration';
import { appUtility } from '../utils/appUtility';
import { appConfiguration } from '../utils/appConfiguration';
import PouchDBAllDB from 'pouchdb-all-dbs';
import { cspfmFieldTrackingMapping } from '../pfmmapping/cspfmFieldTrackingMapping';

PouchDB.plugin(PouchDBAllDB);



@Injectable()

export class cspfmAuditDbProvider {
  private failed = "FAILED"
  private success = 'SUCCESS';

  constructor(public http: Http, public dbConfigurationObj: dbConfiguration,
    public appUtilityObj: appUtility, public appconfig: appConfiguration, public fieldTrackMapping: cspfmFieldTrackingMapping) {
  }

  initializeAuditDB(type, newDoc, previousDoc, newDocId, newDocRevId) {
    const stringToSplit = type;
    const splitValue = stringToSplit.split("pfm");
    console.log(splitValue[1]);
    const objectId = splitValue[1]

    const auditdatabseName = "pfm" + '_' + this.appUtilityObj.orgId + '_' +
      objectId + '_' + "audit";
    let db;
    if (this.appUtilityObj.isMobile) {
      db = new PouchDB(auditdatabseName + '.db', { adapter: 'cordova-sqlite', location: 'default' });
    } else {
      db = new PouchDB(auditdatabseName, { size: 50 });
    }
    const fieldTrackObject = this.fieldTrackMapping.mappingDetail[type]
    const trackingFields = fieldTrackObject["tracking_fields"]
    return this.saveAuditRecord(trackingFields, newDoc, previousDoc, db, newDocId, newDocRevId).then(res => {
      if (res["status"] === this.success) {
        const allresult = res["records"]
        const resultObject = allresult[0];
        const pouchdb = resultObject["db"];
        pouchdb.close();
        return Promise.resolve(this.success);
      } else {
        return Promise.resolve(this.failed);
      }
    }).catch(function (err) {
      return Promise.resolve(err.message);
    });
  }

  saveAuditRecord(trackingFields, newDoc, previousDoc, pouchDB, newDocId, newDocRevId) {
    const saveTrackingRecords = [];
    if (previousDoc !== undefined) {
      previousDoc = JSON.parse(previousDoc)
    } else {
      previousDoc = {}
    }
    trackingFields.forEach(fieldName => {
      const previousDocFieldValue = previousDoc[fieldName] ? previousDoc[fieldName] : "";
      const newDocFieldValue = newDoc[fieldName];
      const saveRecord = {
        "tracking_field_name": fieldName,
        "current_value": newDocFieldValue,
        "prev_value": previousDocFieldValue ? previousDocFieldValue : "",
        "maindoc_id": newDocId,
        "maindoc_rev": newDocRevId,
        "modifiedon": new Date().getTime(),
        "modifiedby": this.appUtilityObj.userId,
        "fromoffline": true,
        "conflict": false
      }
      saveTrackingRecords.push(pouchDB.post(saveRecord).then(res => {
        return { reposnse: res, db: pouchDB };
      }));
    });
    return Promise.all(saveTrackingRecords).then(allresult => {
      return { status: this.success, message: '', records: allresult };
    }).catch(error => {
      return { status: this.failed, message: error.message };
    });
  }

  startSyncOperation() {
    const auditDBSyncStatus = []
    return PouchDB.allDbs().then(dbs => {
      console.log("startSyncOperation dbs = ", dbs);
        dbs.forEach(element => {
          if (element.includes("audit")) {
            let db;
            if (this.appUtilityObj.isMobile) {
              db = new PouchDB(element, { adapter: 'cordova-sqlite', location: 'default' });
            } else {
              db = new PouchDB(element, { size: 50 });
            }
            const splitValue = element.split(".db");
            console.log(splitValue[0]);
            const dbName = splitValue[0]
            auditDBSyncStatus.push(this.oneTimeReplicationToServer(db, dbName).then(res => {
              console.log('DBReplicationToServer completed =>', res);
              if (res.status === 'complete') {
                return db.destroy().then(response => {
                  console.log("destory response => ", response);
                  return this.success;
                }).catch(err => {
                  return this.failed;
                })
              } else {
                return this.failed;
              }
            }).catch(error => {
              return this.failed;
            }));
          }
        });
        return Promise.all(auditDBSyncStatus).then(allresult => {
          console.log("auditDBSyncStatus allresult  = ", allresult);
          return { status: this.success, message: '', records: allresult };
        }).catch(error => {
          return { status: this.failed, message: error.message };
        });
    }).catch(function (err) {
      return Promise.resolve(err.message);
    });
  }
  // One time replication to server
  oneTimeReplicationToServer(pouchDB, dbName) {
    const remote = this.dbConfigurationObj.remoteDbUrl + dbName;
    const options = this.getSyncOption();
    return pouchDB.replicate.to(remote, options)
  }
  // Get sync option
  private getSyncOption() {
    return {
      live: false,
      retry: true,
      auth: { "username": this.dbConfigurationObj.username, "password": this.dbConfigurationObj.password }
    };
  }

  public getAuditIsAvailable() {
    let auditDBAvailable = false;
    return PouchDB.allDbs().then(dbs => {
      dbs.forEach(element => {
        if (element.includes('audit')) {
          auditDBAvailable = true
        }
      })
      return auditDBAvailable;
    })
  }
}
