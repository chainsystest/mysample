import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Http } from '@angular/http';
import PouchDB from 'pouchdb';
import { dbConfiguration } from './dbConfiguration';
import { appUtility } from '../utils/appUtility';
import { appConfiguration } from '../utils/appConfiguration';
import { Events } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
import { File } from '@ionic-native/file/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { attachmentDbProvider } from 'src/core/db/attachmentDbProvider';
import { attachmentCouchDbProvider } from 'src/core/db/attachmentCouchDbProvider';
import { Observable, throwError } from 'rxjs';
import {timeout,catchError} from 'rxjs/operators'
import { stringify } from 'querystring';

@Injectable()

export class cspfmAttachementUploadDbProvider {
  private isUploadProgress = false;
  private attachUploadDB;
  private dbChanges;
  private docToUpload;
  private pouchSaveDoc;
  private localNotifIdVal = 0;
  // private localNotifications: LocalNotifications
  constructor(public http: Http, public dbConfigurationObj: dbConfiguration,
    public appUtilityObj: appUtility, public appconfig: appConfiguration, public events: Events,
    public file: File, public attachmentDbProviderObj: attachmentDbProvider, public localNotifcation: LocalNotifications,
    public attachmentCouchDbProviderObj: attachmentCouchDbProvider, public toastCtrl: ToastController) {
    if (this.appUtilityObj.isMobile) {
      this.initializeAttachUploadDB();
    }
  }

  initializeAttachUploadDB() {
    const attachmentdatabaseName = "attachment" + '_' + this.appUtilityObj.orgId + '_' + "upload";
    this.attachUploadDB = new PouchDB(attachmentdatabaseName + '.db', { adapter: 'cordova-sqlite', location: 'default' });
    this.startChangeListner();
  }

  public startChangeListner() {
    if (this.dbChanges === undefined) {
      this.dbChanges = this.attachUploadDB.changes({ live: true, since: 'now', include_docs: true, attachments: true })
        .on('change', this.onDatabaseChange);
    }
  }

  // Pouch Database change listener callback
  private onDatabaseChange = (change) => {
    console.log('change', change);
    if (this.isUploadProgress === false && change.doc.status === 'initiated') {
      this.docToUpload = change.doc;
      this.pouchSaveDoc = this.docToUpload.pouchSaveObject;
      this.fileManageDBSave();
      }
  }
  saveAttachmentInfo(tempSaveRecord) {
    this.pouchSaveDoc = tempSaveRecord.pouchSaveObject;
    this.attachUploadDB.post(tempSaveRecord).catch(err => {
      console.log('temp record save error', err);
    });
  }

  saveAttachmentInfoWithContentForWeb(tempSaveRecord, imgUpload) {
    this.docToUpload = tempSaveRecord;
    this.pouchSaveDoc = tempSaveRecord.pouchSaveObject;
    this.isUploadProgress = false;
    this.fileManageDBSave(imgUpload);
  }

  fileManageDBSave(imageToUpload?) {
    let serviceObject;
    if (this.docToUpload.pouchServiceObj === "PouchDB") {
      serviceObject = this.attachmentDbProviderObj;
    } else {
      serviceObject = this.attachmentCouchDbProviderObj;
    }
    serviceObject.save(this.docToUpload.pouchParentType, this.pouchSaveDoc).then(result => {
      this.pouchSaveDoc.id = result.id;
      this.pouchSaveDoc.rev = result.rev;
      console.log('save obj', result);
      if (result["status"] !== "SUCCESS") {
        // local notification... and retry when user click it...
        console.log(JSON.stringify(result["message"]));
        return;
      } else if (this.isUploadProgress === false && this.docToUpload.status === 'initiated') {
        if (this.appUtilityObj.isMobile) {
        this.uploadToServer(this.docToUpload, result.id);
        } else {
          const url = this.appUtilityObj.appBuilderURL;
          const path = url + '/filemanage/upload';
          console.log('path', path);
          const formData = new FormData();
          formData.append("image", imageToUpload);
          console.log('formData', formData);
          this.postRequest(path, formData, this.docToUpload, result.id)
        }
      }
    })
  }
  fetchDataFromDB() {
    const selector = {}
    selector['status'] = 'initiated';
    const options = {};
    options['selector'] = selector;
    this.attachUploadDB.find(options).then(function (doc) {
      console.log('doc fetched', doc);
      if (doc['docs'].length === 0) {
        return;
      } else {
        const docsArray = doc['docs'];
        const firstObj = docsArray[0];
        this.docToUpload = firstObj
        this.pouchSaveDoc = this.docToUpload.pouchSaveObject;
        this.fileManageDBSave();
      }
    }).then(function (response) {
      // handle response
    }).catch(function (err) {
      console.log(err);
    });
  }

  uploadToServer(doc, id) {
    this.presentLocalNotification('Upload process has been started');
    const url = this.appUtilityObj.appBuilderURL;
    // const docId = doc._id;
    const filePath = doc.file_path;
    this.file.readAsDataURL(filePath, doc.file_name)
      .then(async entry => {
        this.isUploadProgress = true;
        const blobData = this.convertBase64ToBlob(entry);
        const formData = new FormData();
        formData.append("image", blobData, doc.file_name);
        console.log('formData', formData);
        const path = url + '/filemanage/upload';
        console.log('path', path);
        this.postRequest(path, formData, doc, id)
      }).catch(err => {
        console.log('Error while reading file.', err);
        this.isUploadProgress = false;
        this.pouchSaveDoc.status = 'failed';
        this.docToUpload.status = 'failed';
        this.fileManageDBSave();
        if (this.appUtilityObj.isMobile) {
          this.saveAttachmentInfo(document);
          this.fetchDataFromDB();
        }
      });
  }

  private convertBase64ToBlob(base64: string) {
    const info = this.getInfoFromBase64(base64);
    const sliceSize = 512;
    const byteCharacters = window.atob(info.rawBase64);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
      const byteNumbers = new Array(slice.length);

      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      byteArrays.push(new Uint8Array(byteNumbers));
    }
    return new Blob(byteArrays, { type: info.mime });
  }
  private getInfoFromBase64(base64: string) {
    const meta = base64.split(',')[0];
    const rawBase64 = base64.split(',')[1].replace(/\s/g, '');
    const mime = /:([^;]+);/.exec(meta)[1];
    const extension = /\/([^;]+);/.exec(meta)[1];

    return {
      mime,
      extension,
      meta,
      rawBase64
    };
  }

  postRequest(path, postParams, document , idVal) {
    let sessionTokenStr;
    let sessionType = '';

    if (this.appUtilityObj.isMobile) {
      sessionTokenStr = this.appUtilityObj.accessToken;
      sessionType = "OAUTH";
    } else {
      sessionTokenStr = this.appUtilityObj.sessionId;
      sessionType = "NODEJS";
    }
    let headers = new Headers({
      'input_json': JSON.stringify({
        "orgId": this.appUtilityObj.orgId,
        "couch_id": "pfm" + document.objectId + "att_2_" + idVal,
        "fileType": "ATTACHMENTS",
        "fileSaveAs": document.fileSaveAs,
        "recordId": document.recordId,
        "objectId": document.objectId,
        "configId": document.configId,
        "userId": this.appUtilityObj.userId,
        "sessionType": sessionType,
        "sessionToken": sessionTokenStr
      })
    });
    const options = new RequestOptions({ headers: headers });
    return this.http.post(path, postParams, options)
    .pipe(timeout(250000))
    .pipe(catchError((error: any) => {
      this.presentLocalNotification('Upload has been failed');
      console.log('error=======>' + error);
      this.isUploadProgress = false;
      this.pouchSaveDoc.status = 'failed';
      this.docToUpload.status = 'failed';
      this.fileManageDBSave();
      if (this.appUtilityObj.isMobile) {
        this.saveAttachmentInfo(document);
        this.fetchDataFromDB();
      } else {
        this.presentToast('upload process interputed');
      }
      return Observable.throw(error.statusText);
    }))
      .subscribe(data => {
        this.presentLocalNotification('Upload process Completed');
        console.log('response data', data);
        this.isUploadProgress = false;
        let toastStr = '';
        if (data['_body'] === "Success") {
          this.pouchSaveDoc.status = 'finished';
          this.docToUpload.status = 'finished';
          toastStr = 'upload has been completed';
          } else {
          console.log(data['_body']);
          this.pouchSaveDoc.status = 'failed';
          this.docToUpload.status = 'failed';
          toastStr = 'upload has been failed';
        }
        this.fileManageDBSave();
        if (this.appUtilityObj.isMobile) {
          this.saveAttachmentInfo(document);
          this.fetchDataFromDB();
        } else {
          this.presentToast(toastStr);
        }
        return true;
      });
    }
    presentLocalNotification(displayStr){
      if (this.appUtilityObj.isMobile){
        this.localNotifIdVal = this.localNotifIdVal + 1;
        try {
          this.localNotifcation.schedule({
            id: this.localNotifIdVal,
            text: displayStr,
            foreground: true
            });
        } catch (error) {
          console.log(error);
        }
      }
    }
    async presentToast(message) {
      const toast = await this.toastCtrl.create({
        message: message,
        duration: 2000,
        position: 'bottom'
      });
      toast.dismiss(() => {
        console.log('Dismissed toast');
      });
      toast.present();
    }
    async presentNoInternetToast() {
      const toast = await this.toastCtrl.create({
        message: 'No internet connection. Please check your internet connection and try again.',
        duration: 2000,
        position: 'bottom'
      });
      toast.dismiss(() => {
        console.log('Dismissed toast');
      });
      toast.present();
    }
}
