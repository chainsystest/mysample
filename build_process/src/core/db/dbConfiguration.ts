import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class dbConfiguration {

  // Note : After the session_id handled in jwt. We need to remove databaseName,remoteDbUrl,user name ,user password keys.
  public configuration = {
    databaseName:'pfm_3_mobile_platform',
    remoteDbUrl: 'https://dev.chainsys.com/couchdb/',
    user: {
      name: 'qadatauser',
      password: 'welcome1'
    },
    schema : [{"singular":"pfm140973_only","plural":"pfm140973s","documentType":"pfm140973"},{"singular":"pfm140973","plural":"pfm140973s"},{"singular":"pfm141093_only","plural":"pfm141093s","documentType":"pfm141093"},{"singular":"pfm141093","plural":"pfm141093s"},{"singular":"pfm141095_only","plural":"pfm141095s","documentType":"pfm141095"},{"singular":"pfm141095","plural":"pfm141095s"},{"singular":"pfm142973_only","plural":"pfm142973s","documentType":"pfm142973"},{"singular":"pfm142973","plural":"pfm142973s"},{"singular":"pfm143053_only","plural":"pfm143053s","documentType":"pfm143053"},{"singular":"pfm143053","plural":"pfm143053s","relations":{"pfm141095":{"belongsTo":{"type":"pfm141095","options":{"async":true}}},"pfm141093":{"belongsTo":{"type":"pfm141093","options":{"async":true}}}}},{"singular":"pfm144293_only","plural":"pfm144293s","documentType":"pfm144293"},{"singular":"pfm144293","plural":"pfm144293s","relations":{"pfm140973":{"belongsTo":{"type":"pfm140973","options":{"async":true}}},"pfm143053":{"belongsTo":{"type":"pfm143053","options":{"async":true}}},"pfm141093":{"belongsTo":{"type":"pfm141093","options":{"async":true}}}}},{"singular":"pfm144373_only","plural":"pfm144373s","documentType":"pfm144373"},{"singular":"pfm144373","plural":"pfm144373s","relations":{"pfm147213":{"belongsTo":{"type":"pfm147213","options":{"async":true}}},"pfm144393s":{"hasMany":{"type":"pfm144393","options":{"queryInverse":"pfm144373"}}}}},{"singular":"pfm144393_only","plural":"pfm144393s","documentType":"pfm144393"},{"singular":"pfm144393","plural":"pfm144393s","relations":{"pfm144373":{"belongsTo":{"type":"pfm144373","options":{"async":true}}},"pfm142973":{"belongsTo":{"type":"pfm142973","options":{"async":true}}}}},{"singular":"pfm147213_only","plural":"pfm147213s","documentType":"pfm147213"},{"singular":"pfm147213","plural":"pfm147213s","relations":{"pfm147233s":{"hasMany":{"type":"pfm147233","options":{"queryInverse":"pfm147213"}}}}},{"singular":"pfm147233_only","plural":"pfm147233s","documentType":"pfm147233"},{"singular":"pfm147233","plural":"pfm147233s","relations":{"pfm147213":{"belongsTo":{"type":"pfm147213","options":{"async":true}}},"pfm147274s":{"hasMany":{"type":"pfm147274","options":{"queryInverse":"pfm147233"}}}}},{"singular":"pfm147274_only","plural":"pfm147274s","documentType":"pfm147274"},{"singular":"pfm147274","plural":"pfm147274s","relations":{"pfm147233":{"belongsTo":{"type":"pfm147233","options":{"async":true}}}}},{"singular":"pfm151994_only","plural":"pfm151994s","documentType":"pfm151994"},{"singular":"pfm151994","plural":"pfm151994s"}],
    tableStructure : {"pfm140973":{"display_name":null,"couch_rev_id":null,"couch_id":null,"salepersonid":null,"guid":null,"lastmodifiedon":null,"lastmodifiedby":null,"createdon":null,"createdby":null,"pfm_140973_id":null,"salepersondescription":null,"salepersonname":null},"pfm141093":{"lastmodifiedby":null,"guid":null,"name":null,"display_name":null,"customerstatus":"None","customercontact":null,"customernumber":null,"customername":null,"createdon":null,"createdby":null,"pfm_141093_id":null,"billtolocation":null,"phone":null,"annualrevenue":null,"userid":null,"couch_id":null,"couch_rev_id":null,"customerrisktype":"None","email":null,"website":null,"shiptolocation":null,"lastmodifiedon":null},"pfm141095":{"lastmodifiedon":null,"guid":null,"crmuserid":null,"pfm_141095_id":null,"email":null,"firstname":null,"lastname":null,"display_name":null,"createdby":null,"createdon":null,"couch_id":null,"couch_rev_id":null,"lastmodifiedby":null},"pfm142973":{"listprice":null,"productdescription":null,"productname":null,"manufacturerorigin":"None","couch_rev_id":null,"couch_id":null,"display_name":null,"name":null,"guid":null,"lastmodifiedon":null,"lastmodifiedby":null,"createdon":null,"createdby":null,"pfm_142973_id":null,"productcategory":"None","itemno":null},"pfm143053":{"closedate":null,"display_name":null,"opportunityid":null,"guid":null,"lastmodifiedon":null,"lastmodifiedby":null,"createdon":null,"opportunityname":null,"createdby":null,"pfm_143053_id":null,"opportunityamount":null,"couch_id":null,"couch_rev_id":null,"stage":"Prospective","lponumber":null,"pfm141093_287308":null,"pfm_141093_287308_id":null,"grossmargin":null,"pfm141095_287312":null,"probability":"10","opportunitydescription":null,"pfm_141095_287312_id":null,"closedlostreason":"None","orderwinningdate":null},"pfm144293":{"quoteenddate":null,"pfm_140973_289121_id":null,"pfm_144293_id":null,"createdby":null,"createdon":null,"expirydate":null,"pfm140973_289121":null,"lastmodifiedby":null,"lastmodifiedon":null,"guid":null,"quotemargin":null,"paymenttype":"None","financecharge":null,"revisequote":"No","quoteid":null,"display_name":null,"couch_id":null,"couch_rev_id":null,"status":"Draft","paymentterm":"None","quotedescription":null,"pfm141093_289113":null,"pfm_141093_289113_id":null,"pfm143053_289115":null,"pfm_143053_289115_id":null,"quotestartdate":null},"pfm144373":{"pfm147213_301933":null,"pfm_147213_301933_id":null,"shiptoaddress":null,"lastmodifiedon":null,"guid":null,"orderid":null,"display_name":null,"billtoaddress":null,"couch_id":null,"pfm_144373_id":null,"couch_rev_id":null,"customernumber":null,"customercontact":null,"orderstatus":"Draft","ordercompleteddate":null,"lastmodifiedby":null,"createdon":null,"createdby":null,"orderconfirmeddate":null},"pfm144393":{"display_name":null,"guid":null,"olineid":null,"lastmodifiedon":null,"lastmodifiedby":null,"createdon":null,"pfm142973_289353":null,"couch_id":null,"couch_rev_id":null,"taxpercentage":null,"pfm144373":null,"discountpercentage":null,"pfm_144393_id":null,"quantity":null,"listprice":null,"productdescription":null,"partno":null,"createdby":null,"totalamount":null,"pfm_144373_id":null,"pfm_142973_289353_id":null},"pfm147213":{"display_name":null,"paymentterms":"None","status":"New","cusstatus":"Draft","couch_rev_id":null,"accounttype":"None","region":"None","dunsnumber":null,"businessunits":"None","createdon":null,"lastmodifiedby":null,"lastmodifiedon":null,"guid":null,"accountno":null,"creditlimitrequested":null,"couch_id":null,"customernumber":null,"customercontact":null,"createdby":null,"pfm_147213_id":null,"accountdescription":null,"accountname":null,"taxregistrationnumber":null,"taxexempted":null,"accountcurrency":"USD"},"pfm147233":{"pfm_147213_id":null,"postalcode":null,"createdon":null,"sitenumber":null,"createdby":null,"pfm_147233_id":null,"country":"None","stateprovince":null,"lastmodifiedon":null,"guid":null,"addressid":null,"display_name":null,"address1":null,"address2":null,"pfm147213":null,"addresstype":["None"],"couch_rev_id":null,"couch_id":null,"city":null,"lastmodifiedby":null},"pfm147274":{"pfm_147233_id":null,"pfm_147274_id":null,"createdby":null,"createdon":null,"lastmodifiedby":null,"lastmodifiedon":null,"guid":null,"name":null,"display_name":null,"couch_id":null,"couch_rev_id":null,"pfm147233":null,"responsibility":null,"contactname":null,"phonenumber":null,"email":null},"pfm151994":{"couch_rev_id":null,"product":null,"guid":null,"name":null,"display_name":null,"itemcreateddate":null,"lastmodifiedby":null,"createdon":null,"createdby":null,"pfm_151994_id":null,"totalquantity":null,"availablestatus":null,"inventorydescription":null,"availablequantity":null,"lastmodifiedon":null,"couch_id":null}},
    dataFilters : [{"filterName":"39113_live_filter","filterType":"liveSync","params":""}],
    pouchDBSyncEnabledObjectSelectors : [{
                    "data.type" : "pfm141093"
                },{
                    "data.type" : "pfm144373"
                },{
                    "data.type" : "pfm147213"
                },{
                    "data.type" : "pfm142973"
                },{
                    "data.type" : "pfm144393"
                },{
                    "data.type" : "pfm147274"
                },{
                    "data.type" : "pfm147233"
                },{ 
                    "_id": "_design/validation"
                    },
                    {
                        "_id": '_design/type_createdby_docid_view'
                      },
                      {
                        "_id": '_design/masterdetailview'
                      },
                      {
                        "_id": '_design/masterdetail_createdby_view'
                      },
                      {
                        "_id": '_design/masterdetail_createdby_docid_view'
                      }],
    couchDBSyncEnabledObjectSelectors : [],
    indexingObject: ["pfm144373","pfm144393","pfm142973_289353","pfm147213_301933","pfm147233","pfm147213"]
  };

  private _proxyPassURL: string;
  private _dataBaseName:  string;
  private _userName: string;
  private _password: string;

  get remoteDbUrl(): string {
    return this._proxyPassURL;
  }
  set remoteDbUrl(proxyPass: string) {
    this._proxyPassURL = proxyPass;
  }
  get databaseName(): string {
    return this._dataBaseName;
  }
  set databaseName(dBaseName: string) {
    this._dataBaseName = dBaseName;
  }
  get username(): string {
    return this._userName;
  }
  set username(userName: string) {
    this._userName = userName;
  }
  get password(): string {
    return this._password;
  }
  set password(passWord: string) {
    this._password = passWord;
  }
}
