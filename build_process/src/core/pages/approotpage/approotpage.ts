import { Broadcaster } from '@ionic-native/broadcaster/ngx';
import { Component, OnInit, ViewChild } from '@angular/core';
import { appUtility } from 'src/core/utils/appUtility';
import { Router, ActivatedRoute } from '@angular/router';
import { appConfiguration } from 'src/core/utils/appConfiguration';
import { TranslateService } from '@ngx-translate/core';
import { Events, Platform, IonRouterOutlet, AlertController, NavController } from '@ionic/angular';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
// import { StatusBar } from '@ionic-native/status-bar';
import { networkHandler } from 'src/core/utils/networkHandler';

@Component({
  selector: 'app-approotpage',
  templateUrl: './approotpage.html',
  styleUrls: ['./approotpage.scss'],
})
export class approotpage implements OnInit {
  @ViewChild(IonRouterOutlet) routerOutlet: IonRouterOutlet;
  isConfigurationFailed: Boolean = false;
  failureMessage: String;
  failureDescription: String;
  rootPage: any = 'syncpage';
  embedingRoute : any;
  embedingRouteParams: any;
  constructor(private appUtilityObj: appUtility, public router: Router, public appConfig: appConfiguration,
    private broadcaster: Broadcaster, public translate: TranslateService, public events: Events, private appPreferences: AppPreferences,
    public platform: Platform, public networkHandlerObject: networkHandler, public alertCtrl: AlertController,
    public navCtrl: NavController, private route: ActivatedRoute, ) {

    this.route.queryParams.subscribe(params => {
      this.embedingRoute = params['menu']
      if (this.embedingRoute) {
        this.rootPage = `menu/${this.embedingRoute}`
        this.embedingRouteParams = params['params']
      }
    })

    platform.backButton.subscribeWithPriority(0, () => {
      let childNode = document.getElementsByTagName('ion-router-outlet')[1]
      if (this.routerOutlet && this.routerOutlet.canGoBack()) {
        this.routerOutlet.pop();
      } else if (childNode.childNodes.length == 1 && childNode.children[0].tagName.toLowerCase() != 'app-homepage') {
        this.navCtrl.navigateBack(['menu/'])
      } else if (childNode.childNodes.length == 1 && childNode.children[0].tagName.toLowerCase() == 'app-homepage') {
        this.showExitAlertView({ title: 'Exit' })
      } else if (router.url === '/syncpage') {

      } else {
        this.navCtrl.pop()
      }
    })

    platform.ready().then(() => {
      this.initialSetup();
      //  meta data failure page trigger
      this.failurePageEventSubscribe()
    });

  }

  ngOnInit() {
  }
  initialSetup() {
    //   alert('initialSetup in comp page');
    this.appUtilityObj.initialSetup().then(res => {
      if (res === 'Success') {
        console.log('initialSetup approot page');
        //Show Sync Page, based on Network		
        this.syncPageSubscribe();
        // Handle Network		
        this.networkHandlerObject.networkHandlerForSync();
        //   this.appRootPage = this.rootPage;
        if (this.embedingRoute) {
          let embedingparam = { 'params': this.embedingRouteParams }
          this.router.navigate([this.rootPage], { skipLocationChange: true, queryParams: embedingparam });
        } else {
          this.router.navigate([this.rootPage], { skipLocationChange: true });
        }
        this.setApplicationLanguage();
      } else {
        this.isConfigurationFailed = true;
        this.failureMessage = res;
        if (res === 'Invalid session') {
          this.failureDescription = 'Please login again';
        } else if (res === "Session Expired") {
          this.failureDescription = '';
        } else {
          this.failureDescription = 'Please reach application administrator';
        }
      }
    });
  }
  gotoApplist() {
    this.broadcaster.fireNativeEvent('ionicNativeBroadcast', { action: 'Exit' })
      .then(() => console.log('Success'))
      .catch(() => console.log('Error'));
  }

  setApplicationLanguage() {
    this.translate.setDefaultLang('en');
    if (this.appConfig.configuration['defaultLanguage']) {
      if (this.appConfig.configuration['defaultLanguage']['code']) {
        this.translate.use(this.appConfig.configuration['defaultLanguage']['code']);
      } else {
        this.translate.use('en');
      }
    } else {
      this.translate.use('en');
    }

    if (this.appUtilityObj.isMobile) {
      this.appPreferences.fetch('language_' + this.appConfig.configuration.appId).then((val) => {
        if (val && val != null && val !== 'null') {
          this.translate.use(val);
        }
      });
    } else {
      const code = localStorage.getItem('language_' + this.appConfig.configuration.appId);
      if (code != null && code !== 'null') {
        this.translate.use(code);
      }
    }
  }
  failurePageEventSubscribe() {
    this.events.subscribe('events', (modified) => {
      const editNavigationParams = {

        'modifiedRecord': JSON.stringify(modified)
      };
      this.router.navigate(['/metadatafailure'], { queryParams: editNavigationParams, skipLocationChange: true });
    });
  }
  syncPageSubscribe() {
    this.events.subscribe('syncPageSubscribe', () => {
      this.router.navigate([this.rootPage], { skipLocationChange: true });
    })
  }
  async showExitAlertView(page) {
    const confirm = await this.alertCtrl.create({
      header: 'Exit',
      message: 'Are you sure want to exit ?',
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'OK',
          handler: () => {
            if (this.appUtilityObj.isMobile) {
              this.callNotificationBroadcastPlugin(page);
            }
          }
        }
      ]
    });
    confirm.present();
  }
  callNotificationBroadcastPlugin(page) {
    this.broadcaster.fireNativeEvent('ionicNativeBroadcast', { action: page.title })
      .then(() => console.log('Success'))
      .catch(() => console.log('Error'));
  }

}