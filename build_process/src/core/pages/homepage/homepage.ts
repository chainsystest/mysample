import { Component, OnInit } from '@angular/core';
import { appUtility } from 'src/core/utils/appUtility';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import { appConfiguration } from 'src/core/utils/appConfiguration';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.html',
  styleUrls: ['./homepage.scss'],
})
export class homepage implements OnInit {
  version: number;
  currentObject;
  public appIcon = 'appicon.png';
  constructor(private apputilityObject: appUtility, private appPreferences: AppPreferences, private appConfig: appConfiguration) {

    if (this.apputilityObject.isMobile) {
      this.appPreferences.fetch('login_response').then((res) => {
        const assignedAppsDetail = JSON.parse(res);
        const currentAppId = this.appConfig.configuration.appId;
        this.currentObject = assignedAppsDetail.filter(app => app.appId == currentAppId);
        this.apputilityObject.applicationName = this.currentObject[0]['appDisplayName'];
      });
    } else {
      if (localStorage['assignedApps']) {
        const assignedAppsDetail = JSON.parse(localStorage['assignedApps']);
        const currentAppId = this.appConfig.configuration.appId;
        this.currentObject = assignedAppsDetail.filter(app => app.appId == currentAppId);
        this.apputilityObject.applicationName = this.currentObject[0]['appDisplayName'];
        // this.appIcon = 'appicon.png';
      }
    }
  }

  ngOnInit() {
    this.apputilityObject.setHomePageNode()
  }

}
