import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';

@Component({
  selector: 'app-popover',
  templateUrl: './popoverpage.html',
  styleUrls: ['./popoverpage.scss'],
})

export class popoverpage {

  items;

  constructor(public navParams: NavParams) {
    this.items = this.navParams['data']['popoverController']['records']
  }
}
