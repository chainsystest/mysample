import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule, PopoverController } from '@ionic/angular';

import { popoverpage } from './popoverpage';

const routes: Routes = [
  {
    path: '',
    component: popoverpage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [popoverpage]
})
export class popoverpageModule {
  constructor(public popoverController: PopoverController) { }
}
