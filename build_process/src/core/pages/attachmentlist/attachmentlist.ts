import { Component, ApplicationRef, OnInit, ViewChild } from '@angular/core';
import { NavController, LoadingController, ToastController, IonSlides, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { attachmentDbProvider } from 'src/core/db/attachmentDbProvider';
import { attachmentCouchDbProvider } from 'src/core/db/attachmentCouchDbProvider';
import { appUtility } from 'src/core/utils/appUtility';
import * as lodash from 'lodash';
import { File } from '@ionic-native/file/ngx';
import { Http, ResponseContentType } from '@angular/http';
import { saveAs } from 'file-saver';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Zip } from '@ionic-native/zip/ngx';
import { promise } from 'selenium-webdriver';
import { Broadcaster } from '@ionic-native/broadcaster/ngx';
// import { TouchSequence, promise } from 'selenium-webdriver';
// import { multicast } from 'rxjs/operators';
// declare const Buffer

@Component({
  selector: 'page-attachmentlist',
  templateUrl: 'attachmentlist.html',
})
export class attachmentlist implements OnInit {

  @ViewChild('sliderView') sliderView: IonSlides;
  // Variable to store list of data to display
  public resultList: Array<any> = [];

  // Parent Id from navigation parameter
  private parentId: any;

  // Parent Type from navigation parameter
  private parentType: any;

  // variable for errorMessage
  public errorMessageToDisplay: String = "No Records";

  // Selected Object Display Name
  public selectedObjDisplayName = "";

  // Navigation Parameter
  public navParameters;

  // Selected Object Holding Variable
  public selectedObj = {};

  public documentDataArray = [];
  // Selected Segment For default selection
  public selectedSegment = "";

  public searchText = '';

  public isSelectOption: Boolean = false;
  public isFilterOption: Boolean = false;
  public isMultiselect: Boolean = false;
  public searchTerm: String = '';
  public filteredResultList: Array<any> = [];
  public frequencyArray: Array<any> = ['AnyDate', 'Daily', 'Weekly', 'Monthly', 'Quaterly', 'Yearly'];
  public documentTypeArray: Array<any> = ['Multiple', 'Single'];
  public documentToExpand = -1;
  public filteredDocObjectArray: Array<any> = [];
  public attachmentInfoDict: {};
  public selectedFrequencyVal = -1;
  public selectedDocTypeVal = -1;
  loading;
  private redirectUrl = "/";
  public groupedResultList: Array<any> = [];
  public keyGroupedResult = [];
  constructor(public navCtrl: NavController, public activatedRoute: ActivatedRoute,public broadcaster: Broadcaster,
    public loadingCtrl: LoadingController, public applicationRef: ApplicationRef, public router: Router,
    public toastCtrl: ToastController, public attachmentDbObject: attachmentDbProvider,
    public attachmentCouchDbObject: attachmentCouchDbProvider, public appUtilityConfig: appUtility,
    public file: File, public http: Http, public fileOpener: FileOpener, private zip: Zip, private alertCtrl: AlertController
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.navParameters = JSON.parse(params['params']);
      this.selectedObj = this.navParameters[0];
      this.parentType = this.selectedObj['parentType'];
      this.parentId = this.selectedObj['parentId'];
      console.log('selectedObj', this.selectedObj, this.parentId, this.parentType);
      this.redirectUrl = params['redirectUrl'];
      this.selectedObjDisplayName = this.selectedObj['parentDisplayName'];
      this.documentDataArray = this.selectedObj['documentInfo'];
      this.filteredDocObjectArray = this.selectedObj['documentInfo'];
      this.attachmentInfoDict = this.selectedObj['attachmentInfo'];
      console.log('attachmentInfoDict', this.attachmentInfoDict);
      console.log('documentDataArray', this.documentDataArray);
      console.log('data', this.documentDataArray[0])

      if (this.selectedObj['isAttachmentShow']) {
        this.selectedSegment = "Attachment"
        this.fetchAllData(this.selectedObj['attachmentType'])
      } else if (this.selectedObj['isDocumentShow']) {
        this.selectedSegment = "Document"
      }
    });
  }
  ngOnInit() {
  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter in list")
  }
  // Un subscribe event listener
  ionViewWillUnload() {

  }
  backButtonOnclick() {
    this.router.navigate([this.redirectUrl], { skipLocationChange: true });
  }
  async fetchAllData(dataType) {
    this.loading = await this.loadingCtrl.create({ message: 'Data sync ...' });
    this.loading.present();
    const selector = {};
    selector['data.type'] = dataType;
    selector['data.' + this.parentType] = this.parentId;
    const options = {};
    options['selector'] = selector;
    this.resultList = []
    let serviceObject;
    if (this.selectedObj['serviceObjectAttachment'] === "PouchDB") {
      serviceObject = this.attachmentDbObject;
    } else {
      serviceObject = this.attachmentCouchDbObject;
    }
    serviceObject.fetchDocsWithRelationshipUsingFindOption(options, true).then(res => {
      if (res['status'] === 'SUCCESS') {
        if (res['records'].length > 0) {
          const data = res['records'];
          this.resultList = data;
          if (this.selectedSegment === "Attachment") {
            this.filteredResultList = data;
          } else {
            this.groupRecordsBasedOnName(data);
          }
        } else {
          this.resultList = [];
          this.filteredResultList = [];
          this.errorMessageToDisplay = "No Records";
        }
      } else {
        this.resultList = [];
        this.filteredResultList = [];
        this.errorMessageToDisplay = res['message'];
        if (this.errorMessageToDisplay === 'No internet') {
          this.presentNoInternetToast();
        }
      }
      this.loading.dismiss();
    })
  }
  uploadButtonAction() {
    const navigationParams = JSON.stringify([
      {
      "documentInfo" : this.documentDataArray,
      "attachmentInfo" :  this.attachmentInfoDict,
      "selectedObj" : this.selectedObj
      }
    ])

    const infoParams = {
      "params" : navigationParams,
      redirectUrl: "/menu/attachmentlist"
    }
    this.router.navigate(["/menu/cspfmAttachmentUpload"], {queryParams: infoParams, skipLocationChange: true});
  }
  groupRecordsBasedOnName(data) {
    let groupaganistname = [];
    let tempResultGroup = [];
    this.groupedResultList = lodash.groupBy(data, function (arrayObj) {
      return arrayObj.obj_document_config_id;
    })
    this.keyGroupedResult = Object.keys(this.groupedResultList);
    tempResultGroup = this.groupedResultList;
    for (const i of this.keyGroupedResult) {
      const keyArray = tempResultGroup[i];
      groupaganistname = lodash.groupBy(keyArray, function (groupedelement) {
        return groupedelement.file_name;
      })
      const groupedValues = Object.values(groupaganistname);
      this.groupedResultList[i] = groupedValues;
      console.log('this.groupedResultList[i]', this.groupedResultList)
    }
  }

  downloadClickAction(indexVal, event) {
    event.stopPropagation();
    this.downloadSingleFile(indexVal);
  }


  downloadSingleFileRequstBuilder(indexVal) {
    let objectIdVal = '';
    let configIdVal = '';
    let fileSaveVal = '';
    let recordIdStr = '';
    let fileTypeVal = '';
    let fileIdStr = '';
    let downloadObj = [];
    if (this.selectedSegment === "Attachment") {
      objectIdVal = this.attachmentInfoDict[0]['objectId'];
      configIdVal = this.attachmentInfoDict[0]['objAttachmentConfigId'];
      fileSaveVal = this.attachmentInfoDict[0]['fileSaveAs'];
      recordIdStr = 'pfm_' + objectIdVal + '_id';
      fileIdStr = 'csp_' + objectIdVal + '_att_id';
      fileTypeVal = 'ATTACHMENTS';
      downloadObj = this.filteredResultList[indexVal];
    } else {
      objectIdVal = this.documentDataArray[indexVal]['objectId'];
      configIdVal = this.documentDataArray[indexVal]["objDocumentConfigId"];
      fileSaveVal = this.documentDataArray[indexVal]["fileSaveAs"]
      recordIdStr = 'pfm_' + objectIdVal + '_id';
      fileIdStr = 'csp_' + objectIdVal + '_doc_id';
      fileTypeVal = 'DOCUMENTS';
      const downloadArray = this.groupedResultList[configIdVal];
      const downloadObject = downloadArray[0];
      downloadObj = downloadObject[0];
    }

    const requestInput = {
      "inputparams": {
        "orgId": this.appUtilityConfig.orgId,
        "userId": this.appUtilityConfig.userId,
        "recordId": downloadObj[recordIdStr],
        "objectId": objectIdVal,
        "fileType": fileTypeVal,
        "configId": configIdVal,
        "fileSaveAs": fileSaveVal,
        "pfmFileVOSet": [
          {
            "fileId": downloadObj[fileIdStr],
            "GUID": downloadObj["guid"],
            "filePath": downloadObj["file_path"],
            "fileName": downloadObj["file_name"],
            "fileExtension": downloadObj["file_extension"],
            "fileSize": downloadObj["file_size"],
            "fileContent": []
          }
        ]
      }
    }
    if (this.appUtilityConfig.isMobile) {
        requestInput.inputparams['sessionToken'] = this.appUtilityConfig.accessToken
        requestInput.inputparams['sessionType'] = "OAUTH"
      } else {
      requestInput.inputparams['sessionToken'] = this.appUtilityConfig.sessionId
      requestInput.inputparams['sessionType'] = "NODEJS"
    }

    return requestInput;
  }

  downloadMultiFileRequstBuilder(downloadArray, indexVal) {
    let objectIdVal = '';
    let configIdVal = '';
    let fileSaveVal = '';
    let recordIdStr = '';
    let fileTypeVal = '';
    let fileIdStr = '';
    if (this.selectedSegment === 'Attachment') {
      objectIdVal = this.attachmentInfoDict[0]['objectId'];
      configIdVal = this.attachmentInfoDict[0]['objAttachmentConfigId']
      fileSaveVal = this.attachmentInfoDict[0]['fileSaveAs'];
      recordIdStr = 'pfm_' + objectIdVal + '_id';
      fileIdStr = 'csp_' + objectIdVal + '_att_id'
      fileTypeVal = 'ATTACHMENTS';
    } else {
      objectIdVal = this.documentDataArray[indexVal]['objectId'];
      configIdVal = this.documentDataArray[indexVal]["objDocumentConfigId"];
      fileSaveVal = this.documentDataArray[indexVal]["fileSaveAs"]
      recordIdStr = 'pfm_' + objectIdVal + '_id';
      fileIdStr = 'csp_' + objectIdVal + '_doc_id';
      fileTypeVal = 'DOCUMENTS';
    }
    const fileSetArray = [];
    const recordIdData = downloadArray[0][recordIdStr];
    for (let i = 0; i < downloadArray.length; i++) {
      const dataDict = {
        "fileId": downloadArray[i][fileIdStr],
        "GUID": downloadArray[i]["guid"],
        "filePath": downloadArray[i]["file_path"],
        "fileName": downloadArray[i]["file_name"],
        "fileExtension": downloadArray[i]["file_extension"],
        "fileSize": downloadArray[i]["file_size"],
        "fileContent": []
      }
      fileSetArray.push(dataDict);
    }
    const requestInput = {
      "inputparams": {
        "orgId": this.appUtilityConfig.orgId,
        "userId": this.appUtilityConfig.userId,
        "recordId": recordIdData,
        "objectId": objectIdVal,
        "fileType": fileTypeVal,
        "configId": configIdVal,
        "fileSaveAs": fileSaveVal,
        "pfmFileVOSet": fileSetArray
      }
    }

    if (this.appUtilityConfig.isMobile) {
      requestInput.inputparams['sessionToken'] = this.appUtilityConfig.accessToken
      requestInput.inputparams['sessionType'] = "OAUTH"
    } else {
      requestInput.inputparams['sessionToken'] = this.appUtilityConfig.sessionId
      requestInput.inputparams['sessionType'] = "NODEJS"
      }
    return requestInput;
  }
  downloadWebserviceCall(requestInput) {
    const appBuilderUrl = this.appUtilityConfig.appBuilderURL;
    const downloadUrlStr = '/filemanage/download';
    const serviceURl = appBuilderUrl + downloadUrlStr;
    return this.http
      // tslint:disable-next-line: deprecation
      .post(serviceURl, requestInput, { responseType: ResponseContentType.ArrayBuffer })
      .toPromise().then(res => {
        console.log('response', res);
        const response = res['_body'];
        return Promise.resolve(response);
      }).catch(error => {
        return Promise.reject(error);
      });
  }
  async downloadSingleFile(indexVal) {
    // this.presentToast('Download action');
    this.loading = await this.loadingCtrl.create({ message: 'downloading ...' });
    this.loading.present();
    let folderName = '';
    if (this.selectedSegment === "Attachment") {
      folderName = 'Attachment';
    } else {
      folderName = 'Document';
    }
    let recordIdFolder = '';
    const requestInput = this.downloadSingleFileRequstBuilder(indexVal)
    const inputParamsData = requestInput['inputparams']
    const objectIdStr = JSON.stringify(inputParamsData['objectId']);
    const configIdStr = JSON.stringify(inputParamsData['configId']);
    if (this.selectedSegment === "Attachment") {
      recordIdFolder = JSON.stringify(inputParamsData['recordId']);
    } else {
      recordIdFolder = inputParamsData['recordId'];
    }
    const fileInfo = inputParamsData['pfmFileVOSet'];
    const fileNameStr = fileInfo[0]["fileName"];
    const fileName = fileNameStr + '.' + fileInfo[0]["fileExtension"]
    let blobData;
    this.downloadWebserviceCall(requestInput).then((response) => {
      try {
        const decodedString = String.fromCharCode.apply(null, new Uint8Array(response));
        const obj = JSON.parse(decodedString);
        this.presentToast(obj.message);
        this.loading.dismiss();
        if (obj.message === "Invalid sessiontoken" || obj.message === "Session expired") {
          if (this.appUtilityConfig.isMobile) {
            this.broadcaster.fireNativeEvent('ionicNativeBroadcast', { action: 'Logout' });
          } else {
            window.location.replace('/apps/applist?metaFailureAction=Logout');
          }
        }
        return;
      } catch (error) {
        blobData = new Blob([response], { type: 'application/octet-stream' });
        if (!this.appUtilityConfig.isMobile) {
          saveAs(blobData, fileName);
          this.loading.dismiss();
          return;
        }
        const writeDirectory = this.file.dataDirectory;
        this.checkAndCreateDirectory(writeDirectory, folderName).then(res1 => {
          this.checkAndCreateDirectory(writeDirectory + folderName + '/', objectIdStr).then(res2 => {
            this.checkAndCreateDirectory(writeDirectory + folderName + '/' + objectIdStr + '/', configIdStr).then(res3 => {
              this.checkAndCreateDirectory(writeDirectory + folderName + '/' + objectIdStr + '/' + configIdStr + '/',
                recordIdFolder).then(res4 => {
                  if (this.selectedSegment === 'Attachment') {
                    this.file.writeFile(writeDirectory + folderName + '/' + objectIdStr + '/' + configIdStr + '/' + recordIdFolder,
                      fileName, blobData, { replace: true }).then(() => {
                        this.loading.dismiss();
                        console.log('Sucessfully downloaded');
                      }).catch(error => {
                        this.loading.dismiss();
                        console.log('error msg', error);
                      });
                  } else {
                    this.checkAndCreateDirectory(writeDirectory + folderName + '/' + objectIdStr + '/' + configIdStr + '/' +
                      recordIdFolder + '/', fileNameStr).then(res5 => {
                        this.file.writeFile(writeDirectory + folderName + '/' + objectIdStr + '/' + configIdStr + '/' + recordIdFolder
                          + '/' + fileNameStr, fileName, blobData, { replace: true }).then(() => {
                            this.loading.dismiss();
                            console.log('Sucessfully downloaded');
                          }).catch(error => {
                            this.loading.dismiss();
                            console.log('error msg', error);
                          });
                      })
                  }
                })
            })
          })
        })
      }
    })
      .catch(error => {
        console.log('An error occurred', error);
        this.loading.dismiss();
        this.presentToast(error.message);
      });
  }

  async multiselectAttachementdownloadAction() {

    this.isMultiselect = false;
    const downloadArray = [];
    let indexValueForSingleObj = 0;
    for (let iVal = 0; iVal < this.filteredResultList.length; iVal++) {
      if (this.filteredResultList[iVal]['isSelected'] === true) {
        downloadArray.push(this.filteredResultList[iVal])
        indexValueForSingleObj = iVal;
      }
    }
    if (downloadArray.length === 1) {
      this.downloadSingleFile(indexValueForSingleObj);
      return;
    } else if (downloadArray.length === 0) {
      alert('select file to download');
      return;
    } else {
      this.loading = await this.loadingCtrl.create({ message: 'downloading ...' });
      this.loading.present();
      const requestInput = this.downloadMultiFileRequstBuilder(downloadArray, 0)
      const inputParamsData = requestInput['inputparams']
      const folderName = 'Attachment';
      const zipFolderName = 'attachmentTemp.zip';
      const unzipFolderName = 'attachmentTemp';
      const objectIdStr = JSON.stringify(inputParamsData['objectId']);
      const configIdStr = JSON.stringify(inputParamsData['configId']);
      const recordIdData = JSON.stringify(inputParamsData['recordId']);
      let blobData;
      this.downloadWebserviceCall(requestInput).then((response) => {
        try {
          const decodedString = String.fromCharCode.apply(null, new Uint8Array(response));
          const obj = JSON.parse(decodedString);
          this.presentToast(obj.message);
          this.loading.dismiss();
          return;
        } catch (error) {
          blobData = new Blob([response], { type: 'application/octet-stream' });
          if (!this.appUtilityConfig.isMobile) {
            saveAs(blobData, zipFolderName);
            this.loading.dismiss();
            return;
          }
          const writeDirectory = this.file.dataDirectory;
          this.file.writeFile(writeDirectory, zipFolderName, blobData, { replace: true }).then(() => {
            this.zip.unzip(writeDirectory + zipFolderName, writeDirectory + unzipFolderName)
              .then((result) => {
                if (result === 0) {
                  console.log('SUCCESS');
                  this.file.removeFile(writeDirectory, zipFolderName);
                  this.file.listDir(writeDirectory, unzipFolderName).then(data => {
                    data.forEach(element => {
                      this.checkAndCreateDirectory(writeDirectory, folderName).then(res1 => {
                        this.checkAndCreateDirectory(writeDirectory + folderName + '/', objectIdStr).then(res2 => {
                          this.checkAndCreateDirectory(writeDirectory + folderName + '/' + objectIdStr + '/', configIdStr).then(res3 => {
                            this.checkAndCreateDirectory(writeDirectory + folderName + '/' + objectIdStr + '/' + configIdStr + '/',
                              recordIdData).then(res4 => {
                                this.loading.dismiss();
                                this.file.checkFile(writeDirectory + folderName + '/' + objectIdStr + '/' + configIdStr + '/' +
                                  recordIdData + '/', element.name).then(chkfile => {
                                    console.log('chkfile exist', chkfile);
                                    this.file.removeFile(writeDirectory + folderName + '/' + objectIdStr + '/' + configIdStr + '/' +
                                      recordIdData + '/', element.name).then(removeFile => {
                                        console.log('remove under process');
                                        this.file.moveFile(writeDirectory + unzipFolderName, element.name, writeDirectory + folderName
                                          + '/' + objectIdStr + '/' + configIdStr + '/' + recordIdData, element.name)
                                          .then((moveRes) => {
                                            console.log('move sucess');
                                          })
                                      }).catch(() => {
                                        this.file.moveFile(writeDirectory + unzipFolderName, element.name, writeDirectory + folderName
                                          + '/' + objectIdStr + '/' + configIdStr + '/' + recordIdData, element.name)
                                          .then((moveRes) => {
                                            console.log('move sucess in catch');
                                          })
                                      })
                                  }).catch(() => {
                                    this.file.moveFile(writeDirectory + unzipFolderName, element.name, writeDirectory + folderName + '/' +
                                      objectIdStr + '/' + configIdStr + '/' + recordIdData, element.name).then((moveRes) => {
                                        console.log('move sucess');
                                      })
                                  })
                              })
                          })
                        })
                      })
                    });
                  });
                }
                if (result === -1) {
                  this.loading.dismiss();
                  this.presentToast('Download Failed');
                }
              });
          })
        }
      }).catch(error => {
        console.log('An error occurred', error);
        this.loading.dismiss();
        this.presentToast(error.message);
      });
    }
  }

  async multiselectDocumentClickAction(indexVal) {
    // this.presentToast('Download action');
    this.isMultiselect = false;
    const downloadArray = [];
    const folderName = 'Document';
    const zipFolderName = 'documentTemp.zip';
    const unzipFolderName = 'documentTemp';
    const configIdVal = this.documentDataArray[indexVal]["objDocumentConfigId"];
    const hasIndex = lodash.has(this.groupedResultList, [configIdVal])
    if (hasIndex === false) {
      alert('select file to download');
      return;
    }
    const downloadObj = this.groupedResultList[configIdVal];
    for (let i = 0; i < downloadObj.length; i++) {
      const innerArray = downloadObj[i];
      for (let j = 0; j < innerArray.length; j++) {
        downloadArray.push(innerArray[j]);
      }
    }
    if (downloadArray.length === 1) {
      this.downloadSingleFile(indexVal);
      return;
    } else {
      this.loading = await this.loadingCtrl.create({ message: 'downloading ...' });
      this.loading.present();
      const requestInput = this.downloadMultiFileRequstBuilder(downloadArray, indexVal)
      const inputParamsData = requestInput['inputparams']
      const recordIdFolder = JSON.stringify(inputParamsData['recordId']);
      const objectIdStr = JSON.stringify(inputParamsData['objectId']);
      const configIdStr = JSON.stringify(inputParamsData['configId']);
      let blobData;
      this.downloadWebserviceCall(requestInput).then((response) => {
        try {
          const decodedString = String.fromCharCode.apply(null, new Uint8Array(response));
          const obj = JSON.parse(decodedString);
          this.presentToast(obj.message);
          this.loading.dismiss();
          return;
        } catch (error) {
          blobData = new Blob([response], { type: 'application/octet-stream' });
          if (!this.appUtilityConfig.isMobile) {
            saveAs(blobData, zipFolderName);
            this.loading.dismiss();
            return;
          }
          const writeDirectory = this.file.dataDirectory;
          this.file.writeFile(writeDirectory, zipFolderName, blobData, { replace: true }).then(() => {
            this.zip.unzip(writeDirectory + zipFolderName, writeDirectory + unzipFolderName)
              .then((result) => {
                if (result === 0) {
                  console.log('SUCCESS');
                  this.file.removeFile(writeDirectory, zipFolderName);
                  this.file.listDir(writeDirectory, unzipFolderName).then(data => {
                    data.forEach(element => {
                      this.checkAndCreateDirectory(writeDirectory, folderName).then(res1 => {
                        this.checkAndCreateDirectory(writeDirectory + folderName + '/', objectIdStr).then(res2 => {
                          this.checkAndCreateDirectory(writeDirectory + folderName + '/' + objectIdStr + '/', configIdStr).then(res3 => {
                            this.checkAndCreateDirectory(writeDirectory + folderName + '/' + objectIdStr + '/' + configIdStr + '/',
                              recordIdFolder).then(res4 => {
                                this.loading.dismiss();
                                this.file.checkDir(writeDirectory + folderName + '/' + objectIdStr + '/' + configIdStr + '/'
                                  + recordIdFolder + '/', element.name).then(chkfile => {
                                    this.file.removeRecursively(writeDirectory + folderName + '/' + objectIdStr + '/' + configIdStr
                                      + '/' + recordIdFolder + '/', element.name).then(removeFile => {
                                        console.log('remove under process');
                                        this.file.moveDir(writeDirectory + unzipFolderName, element.name, writeDirectory + folderName
                                          + '/' + objectIdStr + '/' + configIdStr + '/' + recordIdFolder, element.name)
                                          .then((moveRes) => {
                                            console.log('move sucess');
                                          })
                                      }).catch(() => {
                                        this.file.moveDir(writeDirectory + unzipFolderName, element.name, writeDirectory + folderName
                                          + '/' + objectIdStr + '/' + configIdStr + '/' + recordIdFolder, element.name)
                                          .then((moveRes) => {
                                            console.log('move sucess in catch');
                                          })
                                      })
                                  }).catch(() => {
                                    this.file.moveDir(writeDirectory + unzipFolderName, element.name, writeDirectory + folderName + '/' +
                                      objectIdStr + '/' + configIdStr + '/' + recordIdFolder, element.name).then((moveRes) => {
                                        console.log('move sucess');
                                      })
                                  })
                              })
                          })
                        })
                      })
                    });
                  });
                }
                if (result === -1) {
                  console.log('FAILED');
                  this.presentToast('Download Failed');
                  this.loading.dismiss();
                }
              });
          })
        }
      }).catch(error => {
        console.log('An error occurred', error);
        this.loading.dismiss();
        this.presentToast(error.message);
      });
    }
  }

  checkAndCreateDirectory(pathStr, newDirectory) {
    console.log('pathStr', pathStr, newDirectory)
    return this.file.checkDir(pathStr, newDirectory).then(responseCheck => {
      return Promise.resolve(true);
    }).catch(errorCreate => {
      return this.file.createDir(pathStr, newDirectory, false).then(responseCreate => {
        return Promise.resolve(true);
      }).catch(error => {
        return Promise.resolve(false);
      })
    })
  }

  showAttachmentDownloadedFile(indexVal) {
    if (this.isMultiselect === true) {
      return;
    }
    const downloadObj = this.filteredResultList[indexVal];
    const folderName = 'Attachment';
    const objectId = JSON.stringify(this.attachmentInfoDict[0]['objectId']);
    const configId = JSON.stringify(this.attachmentInfoDict[0]['objAttachmentConfigId']);
    const fileIdStr = 'pfm_' + objectId + '_id';
    const fileId = JSON.stringify(downloadObj[fileIdStr]);
    const fileName = downloadObj["file_name"] + '.' + downloadObj["file_extension"]
    const writeDirectory = this.file.dataDirectory;
    const pathStr = writeDirectory + folderName + '/' + objectId + '/' + configId + '/' + fileId + '/';
    console.log('str path', pathStr);
    this.file.checkFile(pathStr, fileName).then(responseCheck => {
      const fileExtn = fileName.split('.').reverse()[0];
      const fileMIMEType = this.getMIMEtype(fileExtn);
      this.fileOpener.open(pathStr + fileName, fileMIMEType)
        .then(() => console.log('File is opened'))
        .catch(e => {
          alert('file is not in correct format');
        });
    }).catch(async () => {
      const alert = await this.alertCtrl.create({
        header: '',
        subHeader: '',
        message: 'Do you need to download this file?',
        buttons: [{
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Yes',
          handler: () => {
            console.log('Confirm Ok');
            this.downloadSingleFile(indexVal);
          }
        }]
      });
      await alert.present();
    })
  }
  getMIMEtype(extn) {
    const ext = extn.toLowerCase();
    const MIMETypes = {
      'txt': 'text/plain',
      'docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      'doc': 'application/msword',
      'pdf': 'application/pdf',
      'jpg': 'image/jpeg',
      'bmp': 'image/bmp',
      'png': 'image/png',
      'xls': 'application/vnd.ms-excel',
      'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'rtf': 'application/rtf',
      'ppt': 'application/vnd.ms-powerpoint',
      'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
    }
    return MIMETypes[ext];
  }
  showDocumentDownloadedFile(indexrow, indexFordata, indexItem) {
    const folderName = 'Document';
    const objectIdVal = JSON.stringify(this.documentDataArray[indexrow]['objectId']);
    const configIdVal = JSON.stringify(this.documentDataArray[indexrow]["objDocumentConfigId"]);
    const fileIdStr = 'pfm_' + objectIdVal + '_id';
    const viewArray = this.groupedResultList[configIdVal];
    const downloadObject = viewArray[indexFordata];
    const downloadObj = downloadObject[indexItem];
    const fileId = downloadObj[fileIdStr];
    const fileNameFolder = downloadObj["file_name"];
    const fileCount = indexItem + 1;
    let fileName = '';
    if (downloadObject.length === 1 && viewArray.length === 1) {
      fileName = downloadObj["file_name"] + '.' + downloadObj["file_extension"]
    } else {
      fileName = downloadObj["file_name"] + '_' + fileCount + '.' + downloadObj["file_extension"]
    }
    const writeDirectory = this.file.dataDirectory;
    const pathStr = writeDirectory + folderName + '/' + objectIdVal + '/' + configIdVal + '/' + fileId + '/' + fileNameFolder + '/';
    console.log('str path', pathStr, fileName);
    this.file.checkFile(pathStr, fileName).then(responseCheck => {
      const fileExtn = fileName.split('.').reverse()[0];
      const fileMIMEType = this.getMIMEtype(fileExtn);
      this.fileOpener.open(pathStr + fileName, fileMIMEType)
        .then(() => console.log('File is opened'))
        .catch(e => alert('file is not in correct format'));
    }).catch(async () => {
      const alert = await this.alertCtrl.create({
        header: '',
        subHeader: '',
        message: 'Do you need to download this file?',
        buttons: [{
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Yes',
          handler: () => {
            console.log('Confirm Ok');
            this.multiselectDocumentClickAction(indexrow);
          }
        }]
      });
      await alert.present();
    })

  }
  // Method to present toast
  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    });
    toast.dismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }
  async presentNoInternetToast() {
    const toast = await this.toastCtrl.create({
      message: 'No internet connection. Please check your internet connection and try again.',
      duration: 2000,
      position: 'bottom'
    });
    toast.dismiss(() => {
      this.retryButtonPressed();
    });
    toast.present();
  }
  retryButtonPressed() {
    if (this.selectedSegment === "Attachment") {
      this.fetchAllData(this.selectedObj['attachmentType'])
    }
  }

  // Navigation button click options
  selectOptionButtonAction() {
    this.multiselectcancelPressed();
    this.isFilterOption = false;
    if (this.isSelectOption) {
      this.isSelectOption = false;
    } else {
      this.isSelectOption = true;
    }
  }
  filterButtonAction() {
    console.log('filterAction');
    this.isSelectOption = false;
    if (this.isFilterOption) {
      this.isFilterOption = false;
    } else {
      this.isFilterOption = true;
    }
  }

  // document or attachement select events
  documentSelected() {
    this.isFilterOption = false;
    this.isSelectOption = false;
    this.isMultiselect = false;
    this.onCancel();
    this.selectedFrequencyVal = -1;
    this.selectedDocTypeVal = -1;
    console.log('document selected');
    this.selectedSegment = "Document";
    this.documentDataArray = this.selectedObj['documentInfo'];
    this.filteredDocObjectArray = this.selectedObj['documentInfo'];
    this.filteredResultList = [];
    var actTab = document.querySelector(".cs-am-att");
    actTab.classList.remove("active");
    var actTab = document.querySelector(".cs-am-doc");
    actTab.classList.add("active");
    this.fetchAllData(this.selectedObj['documentType']);

  }
  attachementSelected() {
    this.isFilterOption = false;
    this.isSelectOption = false;
    this.onCancel()
    this.selectedFrequencyVal = -1;
    this.selectedDocTypeVal = -1;
    console.log('attachement selected');
    this.selectedSegment = "Attachment"
    this.fetchAllData(this.selectedObj['attachmentType']);

    var actTab = document.querySelector(".cs-am-doc");
    actTab.classList.remove("active");
    var actTab = document.querySelector(".cs-am-att");
    actTab.classList.add("active");
  }

  // Method to filter searched Items
  getSearchedItems(searchText) {
    this.isSelectOption = false;
    this.isFilterOption = false;
    this.searchTerm = searchText
    if (this.selectedSegment === 'Attachment') {
      const params = ["file_name", "file_extension"]; // define the data which need to be filter aganist the text
      this.filteredResultList = this.resultList.filter((item) => {
        for (const param of params) {
          if (item[param] && item[param].toString().toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
            console.log('filteredResultList', this.filteredResultList)
            return true;
          }
        }
        console.log('filteredResultList', this.filteredResultList)
        return false;
      });
    } else {
      const params = ['documentName', 'fileSaveAs'];
      const documentObj = this.filteredDocObjectArray;
      this.documentDataArray = documentObj.filter((item) => {
        for (const param of params) {
          if (item[param] && item[param].toString().toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
            return true;
          }
        }
        console.log('filteredResultList', this.filteredResultList)
        return false;
      });
    }
  }

  // cancel search action
  multiselectAction() {
    this.isSelectOption = false;
    this.isMultiselect = true;
  }
  multiselectcancelPressed() {
    this.isSelectOption = false;
    this.isMultiselect = false;
    this.filteredResultList.forEach(element => {
      element['isSelected'] = false;
    });
  }
  onCancel() {
    this.searchText = '';
    console.log('onCancel');
  }
  // fab button action

  multiselectdeleteAction() {
    console.log('multiselectdeleteAction')
  }
  allSelectAction() {
    console.log('allSelectAction');
    this.filteredResultList.forEach(element => {
      element['isSelected'] = true;
    });
  }
  allDeSelectAction() {
    console.log('allDeSelectAction')
    this.filteredResultList.forEach(element => {
      element['isSelected'] = false;
    });
  }
  cancelAction() {
    this.isMultiselect = false;
  }
  // radio button actions for select and non-select
  multiSelectRadioButton(indexVal) {
    const selectedObject = this.filteredResultList[indexVal];
    if (selectedObject['isSelected'] === true) {
      selectedObject['isSelected'] = false;
    } else {
      selectedObject['isSelected'] = true;
    }
    this.filteredResultList[indexVal] = selectedObject;
    this.applicationRef.tick()
  }

  // Navigation button select action for attachement
  attachementSelectOption(selectedObject) {
    this.isSelectOption = false;
    this.selectedObj = selectedObject;
    this.parentType = this.selectedObj['parentType'];
    this.parentId = this.selectedObj['parentId'];
    this.selectedObjDisplayName = this.selectedObj['parentDisplayName'];
    console.log('this.selectedObj', this.selectedObj);
    if (this.selectedObj['isAttachmentShow']) {
      this.selectedSegment = "Attachment"
      this.fetchAllData(this.selectedObj['attachmentType']);
      return
    }
    if (this.selectedObj['isDocumentShow']) {
      this.selectedSegment = "Document"
    }
  }

  frequencyCloseButton() {
    this.isFilterOption = false;
  }
  // Navigation button select action for document
  selectedFrequency(indexVal) {
    this.onCancel()
    // this.isFilterOption = false;
    this.documentDataArray = [];
    if (this.selectedFrequencyVal === indexVal) {
      this.selectedFrequencyVal = -1;
      if (this.selectedDocTypeVal === -1) {
        this.documentDataArray = this.selectedObj['documentInfo'];
      } else {
        const tempArrayToFilter = this.selectedObj['documentInfo'];
        const item = this.documentTypeArray[this.selectedDocTypeVal];
        const tempFilterArray = [];
        if (item === 'Multiple') {
          tempArrayToFilter.forEach(element => {
            if (element['canUploadMultipleFile'] === 'Y') {
              tempFilterArray.push(element)
            }
          });
        } else {
          tempArrayToFilter.forEach(element => {
            if (element['canUploadMultipleFile'] === 'N') {
              tempFilterArray.push(element)
            }
          });
        }
        this.documentDataArray = tempFilterArray;
      }
    } else {
      this.selectedFrequencyVal = indexVal;
      let firstArrayToFilter = [];
      if (this.selectedDocTypeVal === -1) {
        firstArrayToFilter = this.selectedObj['documentInfo'];
        console.log('firstArrayToFilter', firstArrayToFilter)
      } else {
        const arrayToFilter = this.selectedObj['documentInfo'];
        const item1 = this.documentTypeArray[this.selectedDocTypeVal];
        if (item1 === 'Multiple') {
          arrayToFilter.forEach(element => {
            if (element['canUploadMultipleFile'] === 'Y') {
              firstArrayToFilter.push(element)
            }
          });
        } else {
          arrayToFilter.forEach(element => {
            if (element['canUploadMultipleFile'] === 'N') {
              firstArrayToFilter.push(element)
            }
          });
        }
      }
      const item = this.frequencyArray[indexVal];
      const tempFilterArray = [];
      firstArrayToFilter.forEach(element => {
        if (element['frequency'] === item) {
          tempFilterArray.push(element)
        }
      });
      this.documentDataArray = tempFilterArray;
    }
  }
  multipleDocSelect(indexVal) {
    this.onCancel()
    // this.isFilterOption = false;
    this.documentDataArray = [];
    if (this.selectedDocTypeVal === indexVal) {
      this.selectedDocTypeVal = -1;
      if (this.selectedFrequencyVal === -1) {
        this.documentDataArray = this.selectedObj['documentInfo'];
      } else {
        const tempArrayToFilter = this.selectedObj['documentInfo'];
        const item = this.frequencyArray[this.selectedFrequencyVal];
        const tempFilterArray = [];
        tempArrayToFilter.forEach(element => {
          if (element['frequency'] === item) {
            tempFilterArray.push(element)
          }
        });
        this.documentDataArray = tempFilterArray;
      }
    } else {
      this.selectedDocTypeVal = indexVal;
      let firstArrayToFilter: any = [];
      if (this.selectedFrequencyVal === -1) {
        firstArrayToFilter = this.selectedObj['documentInfo'];
      } else {
        const tempArrayToFilter = this.selectedObj['documentInfo'];
        const itemtext = this.frequencyArray[this.selectedFrequencyVal];
        const tempFilterArray = [];
        tempArrayToFilter.forEach(element => {
          if (element['frequency'] === itemtext) {
            tempFilterArray.push(element)
          }
        });
        firstArrayToFilter = tempFilterArray;
      }
      const item = this.documentTypeArray[indexVal];
      const tempArrayTobeFilter = [];
      if (item === 'Multiple') {
        firstArrayToFilter.forEach(element => {
          if (element['canUploadMultipleFile'] === 'Y') {
            tempArrayTobeFilter.push(element)
          }
        });
      } else {
        firstArrayToFilter.forEach(element => {
          if (element['canUploadMultipleFile'] === 'N') {
            tempArrayTobeFilter.push(element)
          }
        });
      }
      this.documentDataArray = tempArrayTobeFilter;
    }
  }

  nextButtonPressed(selectedSlides: IonSlides, event) {
    event.stopPropagation();
    selectedSlides.slideNext();
  }
  prevButtonPressed(selectedSlides: IonSlides, event) {
    event.stopPropagation();
    selectedSlides.slidePrev();
  }
  slideChanged() {
    console.log('slideChanged');
  }
}

