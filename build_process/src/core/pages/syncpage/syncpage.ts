import { Component, OnInit } from '@angular/core';
import { Broadcaster } from '@ionic-native/broadcaster/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { ToastController, AlertController, NavController, LoadingController, Platform, Events } from '@ionic/angular';
import { appUtility } from 'src/core/utils/appUtility';
import { dbProvider } from 'src/core/db/dbProvider';
import { dbConfiguration } from 'src/core/db/dbConfiguration';
import { metaDbValidation } from 'src/core/utils/metaDbValidation';
import { metaDataDbProvider } from 'src/core/db/metaDataDbProvider';
import { metaDbConfiguration } from 'src/core/db/metaDbConfiguration';
import { appConfiguration } from 'src/core/utils/appConfiguration';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
// import { MenuComponent } from 'src/core/menu/menu.component';
import { Router } from '@angular/router';
import { attachmentDbConfiguration } from 'src/core/db/attachmentDbConfiguration';
import { attachmentDbProvider } from 'src/core/db/attachmentDbProvider';
import { offlineDbIndexCreation } from 'src/core/utils/offlineDbIndexCreation';
import { couchdbProvider } from 'src/core/db/couchdbProvider';
import { attachmentCouchDbProvider } from 'src/core/db/attachmentCouchDbProvider';
import { cspfmExecutionPouchDbProvider } from 'src/core/db/cspfmExecutionPouchDbProvider';
import { cspfmExecutionPouchDbConfiguration } from 'src/core/db/cspfmExecutionPouchDbConfiguration';
import { cspfmAuditDbProvider } from 'src/core/db/cspfmAuditDbProvider';
import { appConstant } from 'src/core/utils/appConstant';
import { Title } from "@angular/platform-browser";

@Component({
    selector: 'app-syncpage',
    templateUrl: './syncpage.html',
    styleUrls: ['./syncpage.scss'],
})

export class syncpage implements OnInit {

    appFilterArray;
    isSyncFailed: Boolean = false;
    syncCompletedStatus: Boolean = false;
    syncFailureMessage = 'Sync failed';
    metaDbfilterParams = {};
    metaValidationResponse = '';
    syncStatusArray: Array<any> = [];
    isAuditDBAvailable: Boolean = false;
    private syncStartTime;
    applicationDetail;
    private aplicationAssignmentObjectHierarchyJSON = {
        "objectId": this.metaDbConfigurationObj.applicationAssignmentObject,
        "objectName": this.metaDbConfigurationObj.applicationAssignmentObject,
        "fieldId": 0,
        "objectType": "PRIMARY",
        "relationShipType": null,
        "childObject": [

        ]
    };
    constructor(private broadcaster: Broadcaster,
        public emailComposer: EmailComposer, public indexObj: offlineDbIndexCreation,
        public toastCtrl: ToastController,
        public alertCtrl: AlertController,
        private apputilityObject: appUtility, public dbprovider: dbProvider,
        public navCtrl: NavController, public loadingCtrl: LoadingController,
        public dbConfigurationObj: dbConfiguration,
        public metaDbValidation: metaDbValidation, public attachmentConfig: attachmentDbConfiguration,
        public metaDbProvider: metaDataDbProvider, public attachdbprovider: attachmentDbProvider,
        public metaDbConfigurationObj: metaDbConfiguration,
        private appConfig: appConfiguration,
        private appPreferences: AppPreferences,
        public platform: Platform, public router: Router, public events: Events,
        public couchdbprovider: couchdbProvider,
        public attachementCouchDBProvider: attachmentCouchDbProvider,
        public cspfmexecutionPouchDbProvider: cspfmExecutionPouchDbProvider,
        public cspfmExecutionDBConfigurationObject: cspfmExecutionPouchDbConfiguration,
        public auditDBProvider: cspfmAuditDbProvider,
        public title: Title) {

        if (this.apputilityObject.isMobile) {
            this.appPreferences.fetch('login_response').then((res) => {
                const assignedAppsDetail = JSON.parse(res);
                const currentAppId = this.appConfig.configuration.appId;
                this.applicationDetail = assignedAppsDetail.filter(app => app.appId === currentAppId);
            });
        } else {
            if (localStorage['assignedApps']) {
                const assignedAppsDetail = JSON.parse(localStorage['assignedApps']);
                const currentAppId = this.appConfig.configuration.appId;
                this.applicationDetail = assignedAppsDetail.filter(app => app.appId === currentAppId);
                // this.appIcon = 'appicon.png';
                this.title.setTitle(this.applicationDetail[0]['appDisplayName']);
            }
        }

    }
    ionViewDidEnter() {
        setTimeout(() => {
            document.querySelector('.cs-l-main').classList.remove('cs-l-start');
            document.querySelector('.cs-l-main').classList.add('cs-l-load');
            this.isAuditDBAvailable = false;
            // meta data sync with mobile platform sync
            this.startMetaDataSync()
            // Each app SyncCompted Status - First Set as False.In navigationMenuPage(), set status as True.
            const syncCompletedStatus = this.appConfig.configuration.appId + "synCompletedStatus";
            if (this.apputilityObject.isMobile) {
                this.appPreferences.store("", syncCompletedStatus, false);
            } else {
                localStorage.setItem(syncCompletedStatus, "false");
            }
            this.syncCompletedStatus = false;
            // While Network Change, this subscribe is called
            this.events.subscribe('startFullSync', (oneTimeSync) => {
                const networkAvailable = oneTimeSync["network"]
                if (networkAvailable) {
                    this.startSync()
                } else {
                    this.stopSync()
                }
            });

            // While stage 5 completed, this subscribe is called
            this.events.subscribe('stage5Completed', (completed) => {
                const isCompleted = completed["stage5Completed"]
                if (isCompleted) {
                    // setTimeout(() => {
                    this.getStartedAction();
                    //  }, 3000);
                }
            });

        }, 2500);
    }
    // Stop Sync - Show Internet Alert		
    stopSync() {
        this.isSyncFailed = true;
        this.syncFailureMessage =
            "No internet connection. You should have network connectivity for initial sync";
        this.showInfoAlert("No internet connection. You should have network connectivity for initial sync");
    }
    // Start Sync - Restart Sync Process		
    startSync() {
        this.alertCtrl.dismiss();
        this.isSyncFailed = false;
        this.syncFailureMessage = "";
        this.reStartSync();
    }

    async startSyncOperation(querySelector?, objectPresentInCurrentObjectSet?) {
        // Live replication using couch server
        const stage2 = this.syncStatusArray[1]
        stage2.status = "running"

        var finalSelector;
        if (querySelector && querySelector.length > 0) {
            finalSelector = querySelector
        } else {
            finalSelector = this.dbConfigurationObj.configuration.pouchDBSyncEnabledObjectSelectors;
        }
        this.dbprovider.oneTimeReplicationFromServer(finalSelector).then(res => {
            console.log(res);
            if (res === "Success") {
                if (objectPresentInCurrentObjectSet !== undefined && objectPresentInCurrentObjectSet.length > 0) {
                    this.dbprovider.oneTimeReplicationFromServerWithSpecificObject(objectPresentInCurrentObjectSet).then(result => {
                        this.pfmObjectSyncStarted(stage2);
                    }).catch(err => {
                        this.pfmObjectSyncStarted(stage2);
                    });
                } else {
                    this.pfmObjectSyncStarted(stage2);
                }
            } else {
                this.syncFailureMessage = "Sync Failed";
                this.isSyncFailed = true;
            }
        }).catch(error => {
            this.syncFailureMessage = error.message;
            this.isSyncFailed = true;
        });
    }

    pfmObjectSyncStarted(stage2) {
        var liveSelector = { '$or': this.dbConfigurationObj.configuration.pouchDBSyncEnabledObjectSelectors };
        this.dbprovider.setCurrentObjectSetInLocalStorage();
        this.appFilterArray = this.dbConfigurationObj.configuration.dataFilters;
        for (let i = 0; i < this.appFilterArray.length; i++) {
            const filterDetails = this.appFilterArray[i];
            if (filterDetails.filterType === 'liveSync') {
                this.dbprovider.liveSyncWithSelector(liveSelector);
            } else if (filterDetails.filterType === 'replicateFromServer') {
                this.dbprovider.liveReplicateFromServerWithSelector(liveSelector);
            } else if (filterDetails.filterType === 'replicateToServer') {
                this.dbprovider.liveReplicateToServerWithSelector(liveSelector);
            }
        }

        this.isSyncFailed = false;
        console.log('Success');
        const syncEndTime = new Date().getTime()
        const seconds = (syncEndTime - this.syncStartTime) / 1000;
        if (seconds > 60) {
            const minutes = Math.floor(seconds / 60);
            stage2.synctime = minutes + " m"
        } else {
            stage2.synctime = Math.floor(seconds % 60) + " s"
        }
        this.syncStartTime = syncEndTime;
        stage2.status = "completed"
        if (this.attachmentConfig.configuration.pouchDBSyncEnabledObjectSelectors.length > 0) {
            this.syncattachmentDB()
        } else {
            const stage3 = this.syncStatusArray[2]
            stage3.synctime = "0 s"
            stage3.status = "completed"
            this.syncStartTime = new Date().getTime()
            this.syncExecutionDB();
        }
    }

    // Live sync with couch server
    liveDataSyncWithCouch() {
        const finalSelector = { '$or': this.dbConfigurationObj.configuration.pouchDBSyncEnabledObjectSelectors };
        return this.dbprovider.oneTimeReplicationFromServerWithSelector(finalSelector).then(res => {
            console.log('DBLiveSync completed =>', res);
            if (res['ok']) {
                this.dbprovider.liveSyncWithSelector(finalSelector);
                return Promise.resolve('Success');
            } else {
                return Promise.resolve('Failed');
            }
        }).catch(error => {
            return Promise.resolve(error.message);
        });
    }

    // Live replication to couch server
    liveDataReplicationToServer(dataFilter, params) {
        console.log('DBReplicationToServer started');
        return this.dbprovider.oneTimeReplicationToServerWithFilter(dataFilter, params).then(res => {
            console.log('DBReplicationToServer completed =>', res);
            if (res.status === 'complete') {
                this.dbprovider.liveReplicationToServerWithFilter(dataFilter, params);
                return Promise.resolve('Success');
            } else {
                return Promise.resolve('Failed');
            }
        }).catch(error => {
            return Promise.resolve(error.message);
        });
    }

    // Live replication from couch server
    liveDataReplicationFromServer(dataFilter, params) {
        console.log('DBReplicationFromServer started');
        return this.dbprovider.oneTimeReplicationFromServerWithFilter(dataFilter, params).then(res => {
            console.log('DBReplicationFromServer completed =>', res);
            if (res.status === 'complete') {
                this.dbprovider.liveReplicationFromServerWithFilter(dataFilter, params);
                return Promise.resolve('Success');
            } else {
                return Promise.resolve('Failed');
            }
        }).catch(error => {
            return Promise.resolve(error.message);
        });
    }

    // Navigate to menu page
    navigationMenuPage() {
        if (navigator.onLine) {
            /**********Handle Stage5 Index Creation *******/
            const stage5 = this.syncStatusArray[4]
            if (this.dbConfigurationObj.configuration.pouchDBSyncEnabledObjectSelectors.length > 0) {
                stage5.status = "running"
                this.indexObj.indexCreationProcessingMsg = stage5.stagename + 'process is running...'
                this.indexObj.createIndex(stage5)
            } else {
                stage5.synctime = "0 s"
                stage5.status = "completed"
            }
            /*Field Tracking */
            if (this.isAuditDBAvailable) {
                this.startSyncAuditDB()
            } else {
                const syncCompletedStatus = this.appConfig.configuration.appId + "synCompletedStatus";
                //   this.app.getRootNavs()[0].setRoot(MenuComponent);
                if (this.apputilityObject.isMobile) {
                    this.appPreferences.store("", syncCompletedStatus, true);
                } else {
                    localStorage.setItem(syncCompletedStatus, "true");
                }
                this.events.unsubscribe("startFullSync")
                this.syncCompletedStatus = true;
            }
            document.querySelector('.cs-l-main').classList.remove('cs-l-load');
            document.querySelector('.cs-l-main').classList.add('cs-l-end');
        } else {
            this.getStartedAction()
        }
    }
    getStartedAction() {
        // Start Change Listener for Meta,Data,Attachment
        this.events.unsubscribe("stage5Completed")
        this.apputilityObject.isAppSyncCompleted = true;
        this.metaDbProvider.startChangeListener();
        this.cspfmexecutionPouchDbProvider.startChangeListener();
        if (this.dbConfigurationObj.configuration.pouchDBSyncEnabledObjectSelectors.length > 0) {
            this.dbprovider.startChangeListener();
        }
        if (this.attachmentConfig.configuration.pouchDBSyncEnabledObjectSelectors.length > 0) {
            this.attachdbprovider.startChangeListener()
        }

        if (this.metaValidationResponse) {
            this.handlewarningMsg(this.metaValidationResponse);
        }
        this.couchdbprovider.networkWithConnectivity();
        this.attachementCouchDBProvider.networkWithConnectivity();
        this.router.navigate(['menu'], { skipLocationChange: true });
    }

    // Exit from app
    gotoApplist() {
        if (this.apputilityObject.isMobile) {
            this.broadcaster.fireNativeEvent('ionicNativeBroadcast', { action: 'Exit' })
                .then(() => console.log('Success'))
                .catch(() => console.log('Error'));
        } else {
            window.location.replace('/apps/applist');
        }
    }

    /********SYNC AUDIT/FIELD TRACKING DB ********/
    startSyncAuditDB() {
        const stage6 = this.syncStatusArray[5]
        stage6.status = "running"
        const syncStartTime = new Date().getTime()
        this.auditDBProvider.startSyncOperation().then(response => {
            if (response["records"].includes("FAILED") || response["status"] === "FAILED") {
                console.log("Audit DB sync failed response = ", response);
            } else {
                console.log("Audit DB sync success");
            }
            const syncCompletedStatus = this.appConfig.configuration.appId + "synCompletedStatus";
            //   this.app.getRootNavs()[0].setRoot(MenuComponent);
            if (this.apputilityObject.isMobile) {
                this.appPreferences.store("", syncCompletedStatus, true);
            } else {
                localStorage.setItem(syncCompletedStatus, "true");
            }
            this.events.unsubscribe("startFullSync")
            this.isAuditDBAvailable = false
            this.syncCompletedStatus = true;
            const syncEndTime = new Date().getTime()
            const seconds = (syncEndTime - syncStartTime) / 1000;
            if (seconds > 60) {
                const minutes = Math.floor(seconds / 60);
                stage6.synctime = minutes + " m"
            } else {
                stage6.synctime = Math.floor(seconds % 60) + " s"
            }
            stage6.status = "completed"
        });
    }

    makeSyncStatusObject() {
        this.syncStatusArray = [
            {
                "stagename": "1",
                "status": "running",
                "synctime": ""
            },
            {
                "stagename": "2",
                "status": "waiting",
                "synctime": ""
            },
            {
                "stagename": "3",
                "status": "waiting",
                "synctime": ""
            },
            {
                "stagename": "4",
                "status": "waiting",
                "synctime": ""
            },
            {
                "stagename": "5",
                "status": "waiting",
                "synctime": ""
            }
        ]
        //Validate Audit DB is availble.If available add Stage 6 in sync flow.
        this.auditDBProvider.getAuditIsAvailable().then(res => {
            console.log("getAuditIsAvailable res = ", res);
            if (res) {
                this.isAuditDBAvailable = true;
                const stage6 = {
                    "stagename": "6",
                    "status": "waiting",
                    "synctime": ""
                }
                this.syncStatusArray.push(stage6);
            }
        })
    }

    /****************************************************
     Meta db initialization and assignedApp fetch
  *****************************************************/
    async startMetaDataSync() {
        if (!navigator.onLine) {
            if (this.platform.is('mobile')) {
                this.appPreferences.fetch('', 'isMetaDBSyncSuccess').then((res) => {
                    const isMetaDbSyncSuccess = res;
                    if (isMetaDbSyncSuccess) {
                        // Initialize POuchDB for Meta, Data, Attachment
                        this.metaDbProvider.initializePouchDb()
                        this.dbprovider.initializePouchDb()
                        this.attachdbprovider.initializePouchDb()
                        this.cspfmexecutionPouchDbProvider.initializePouchDb()
                        this.navigationMenuPage();
                        return;
                    } else {
                        this.showInfoAlert('No internet connection. You should have network connectivity for initial sync')
                        return;
                    }
                })
            }
        } else {
            this.makeSyncStatusObject()
            this.metaDbProvider.initializePouchDb();
            if (this.apputilityObject.isMobile) {

                this.appPreferences.fetch('login_response').then((res) => {
                    const assignedAppsDetail = JSON.parse(res);

                    this.oneTimeMetaDataSync(assignedAppsDetail);
                });
            } else {
                if (localStorage['assignedApps']) {
                    const assignedAppsDetail = JSON.parse(localStorage['assignedApps']);
                    this.oneTimeMetaDataSync(assignedAppsDetail);
                }
            }
        }
    }

    /****************************************************
             One time meta data filter base sync
    *****************************************************/

    private oneTimeMetaDataSync(assignedAppsDetail) {
        this.fetchAssignmentObjectForMetaValidation().then(assigenmenfetcResult => {
            if (!assigenmenfetcResult) {
                this.syncFailureMessage = 'meta data initial sync failure';
                this.isSyncFailed = true;
                return
            }

            this.viewDocsInsertFoeMetaValidation(assigenmenfetcResult).then(result => {
                if (!result) {
                    this.syncFailureMessage = 'meta data initial sync failure';
                    this.isSyncFailed = true;
                    return;
                }
                this.syncStartTime = new Date().getTime()
                var appIdListToFetch = assignedAppsDetail.map(obj => obj.appId);
                var finalSelector = this.metaDbProvider.makeSelectorForMetaDataSyncArray(appIdListToFetch, true);
                this.metaDataReplicateFromServerWithSelector(finalSelector).then(response => {
                    if (response && response === 'Success') {
                        console.log("Meta one time sync succes")
                        this.metaDbValidation.metaValidations(this.metaDbProvider, true).then(res => {
                            if (res) {
                                // check all meta validation success
                                if (res.status === 'success') {
                                    console.log("meta validation succes")
                                    const stage1 = this.syncStatusArray[0]
                                    const syncEndTime = new Date().getTime()
                                    const seconds = (syncEndTime - this.syncStartTime) / 1000;
                                    if (seconds > 60) {
                                        const minutes = Math.floor(seconds / 60);
                                        stage1.synctime = minutes + " m"
                                    } else {
                                        stage1.synctime = Math.floor(seconds % 60) + " s"
                                    }
                                    stage1.status = "completed"
                                    this.syncStartTime = syncEndTime;

                                    if (this.platform.is('mobile')) {
                                        this.appPreferences.store('', 'isMetaDBSyncSuccess', true)
                                    }
                                    this.metaValidationResponse = res;

                                    if (this.dbConfigurationObj.configuration.pouchDBSyncEnabledObjectSelectors.length > 0) {
                                        //this.startSyncOperation()
                                        this.getPreviousAndCurrentSyncObjects(appConstant.syncEnabledObjectDocName);
                                    } else if (this.attachmentConfig.configuration.pouchDBSyncEnabledObjectSelectors.length > 0) {
                                        const stage2 = this.syncStatusArray[1]
                                        stage2.synctime = "0 s"
                                        stage2.status = "completed"
                                        this.syncStartTime = new Date().getTime()
                                        this.syncattachmentDB()
                                    } else {
                                        const stage2 = this.syncStatusArray[1]
                                        stage2.synctime = "0 s"
                                        stage2.status = "completed"
                                        const stage3 = this.syncStatusArray[2]
                                        stage3.synctime = "0 s"
                                        stage3.status = "completed"
                                        this.syncStartTime = new Date().getTime()
                                        this.syncExecutionDB();
                                    }
                                } else if (res.status === 'failure') { // Handle meta validation failure


                                    if (res.errorType && res.errorType === this.metaDbConfigurationObj.fetchError) {
                                        this.syncFailureMessage = res.message;
                                        this.isSyncFailed = true;
                                        return
                                    }
                                    this.events.publish('events', res);
                                }
                            } else {
                                this.syncFailureMessage = 'meta data initial sync failure';
                                this.isSyncFailed = true;
                            }
                        });
                    } else {
                        this.syncFailureMessage = 'meta data initial sync failure';
                        this.isSyncFailed = true;
                    }
                })

            })


        })

    }



    metaDataReplicateFromServerWithSelector(finalSelector) {
        return this.metaDbProvider.oneTimeReplicationFromServer(finalSelector).then(res => {
            console.log(res);
            if (res === "Success") {
                return Promise.resolve("Success")
            } else {
                return Promise.resolve("Failed")
            }
        }).catch(error => {
            return Promise.resolve(error.message)
        });
    }

    getSelectorObj(tableName, fieldName, fieldValue) {
        var selector = {}
        selector['data.type'] = tableName

        if (Array.isArray(fieldValue)) {
            selector[fieldName] = {}
            selector[fieldName]['$in'] = fieldValue
        } else {
            selector[fieldName] = parseInt(fieldValue)
        }



        return selector
    }

    /****************************************************
               Handle meta data warning message
    *****************************************************/
    private handlewarningMsg(response) {
        this.metaValidationResponse = ''
        if (!response['warningSet']) {

            return;
        }
        this.events.publish('metaValidationAnnouncement', response);

    }

    /****************************************************
               Toast for meta validation warning
      *****************************************************/
    async displayToast(message) {
        //   setTimeout(() => {
        const toast = await this.toastCtrl.create({
            message: message,
            duration: 2500,
            position: 'top',
            cssClass: 'cs-customToast cs-sucessToast small zoomIn animated'
        });
        setTimeout(() => {
            const section = 'ion-toast[role="dialog"]';
            document.querySelector('.cs-sucessToast').classList.add('zoomOut');
        }, 1500);
        toast.present();
        //   }, 200);
    }

    /****************************************************
             Toast for meta validation warning
    *****************************************************/
    async displayCustomAlert(message) {
        const alert = await this.alertCtrl.create({
            message: message,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Sync',
                    handler: () => {
                        this.callNativePage()
                    }
                }
            ]
        });
        alert.present();
    }

    callNativePage() {
        this.broadcaster.fireNativeEvent('ionicNativeBroadcast', { action: 'forceSync' });

    }

    /****************************************************
             One time meta live sync
    *****************************************************/

    initiateMetaDataDbLiveSync() {
        if (this.metaValidationResponse) {
            if (this.metaValidationResponse['metaLiveSyncSelector']) {
                this.callMetaLiveSync(this.metaValidationResponse['metaLiveSyncSelector'])
            }
            else {
                this.startMetaLiveSync();
            }
        }

    }

    startMetaLiveSync() {
        if (this.apputilityObject.isMobile) {
            this.appPreferences.fetch('login_response').then((res) => {
                const assignedAppsDetail = JSON.parse(res);
                var appIdListToFetch = assignedAppsDetail.map(obj => obj.appId);
                var finalSelector = this.metaDbProvider.makeSelectorForMetaDataSyncArray(appIdListToFetch, true);
                this.callMetaLiveSync(finalSelector)
            });
        } else {
            if (localStorage['assignedApps']) {
                const assignedAppsDetail = JSON.parse(localStorage['assignedApps']);
                var appIdListToFetch = assignedAppsDetail.map(obj => obj.appId);
                var finalSelector = this.metaDbProvider.makeSelectorForMetaDataSyncArray(appIdListToFetch, true);
                this.callMetaLiveSync(finalSelector)
            }
        }
    }
    callMetaLiveSync(finalSelector) {
        this.metaDbProvider.liveReplicationFromServerWithSelector({ "$or": finalSelector });
    }

    reStartSync() {
        if (!navigator.onLine) {
            this.showInfoAlert("No internet connection. You should have network connectivity for initial sync");
            return;
        } else {
            this.startMetaDataSync()
        }
    }

    syncattachmentDB(querySelector?, objectPresentInCurrentObjectSet?) {
        const stage3 = this.syncStatusArray[2]
        stage3.status = "running"
        this.attachdbprovider.initializePouchDb();
        var finalSelector;
        if (querySelector && querySelector.length > 0) {
            finalSelector = querySelector;
        } else {
            finalSelector = this.attachmentConfig.configuration.pouchDBSyncEnabledObjectSelectors;
        }

        this.attachdbprovider.oneTimeReplicationFromServer(finalSelector).then(res => {
            console.log(res);
            if (res === "Success") {
                if (objectPresentInCurrentObjectSet !== undefined && objectPresentInCurrentObjectSet.length > 0) {
                    this.attachdbprovider.oneTimeReplicationFromServerWithSpecificObject(objectPresentInCurrentObjectSet).then(result => {
                        this.attachmentObjectSyncStarted(stage3);
                    }).catch(err => {
                        this.attachmentObjectSyncStarted(stage3);
                    });
                } else {
                    this.attachmentObjectSyncStarted(stage3);
                }
            } else {
                this.syncFailureMessage = "Sync Failed";
                this.isSyncFailed = true;
            }
        }).catch(error => {
            this.syncFailureMessage = error.message;
            this.isSyncFailed = true;
        });
    }
    attachmentObjectSyncStarted(stage3) {
        var liveSelector = { '$or': this.attachmentConfig.configuration.pouchDBSyncEnabledObjectSelectors };
        this.attachdbprovider.setCurrentObjectSetInLocalStorage();
        this.attachdbprovider.liveSyncWithSelector(liveSelector);
        const syncEndTime = new Date().getTime()
        const seconds = (syncEndTime - this.syncStartTime) / 1000;
        if (seconds > 60) {
            const minutes = Math.floor(seconds / 60);
            stage3.synctime = minutes + " m"
        } else {
            stage3.synctime = Math.floor(seconds % 60) + " s"
        }
        this.syncStartTime = syncEndTime;
        stage3.status = "completed"
        this.syncExecutionDB();
    }


    /********SYNC STATUS WORK FLOW EXECUTION DB ********/
    syncExecutionDB() {
        const stage4 = this.syncStatusArray[3];
        stage4.status = "running"
        this.cspfmexecutionPouchDbProvider.initializePouchDb();
        const pfmApproveUserHierarchyJSON = {
            "objectId": this.metaDbConfigurationObj.pfmApproveValueUserObject,
            "objectName": this.metaDbConfigurationObj.pfmApproveValueUserObject,
            "fieldId": 0,
            "objectType": "PRIMARY",
            "relationShipType": null,
            "childObject": []
        };
        const options = {};
        const selector = {}
        selector['data.type'] = this.metaDbConfigurationObj.pfmApproveValueUserObject
        selector['data.user_id'] = Number(this.apputilityObject.userId);
        options['selector'] = selector;
        pfmApproveUserHierarchyJSON['options'] = options;
        this.metaDbProvider.fetchDataWithReference(pfmApproveUserHierarchyJSON).then(result => {
            if (result && result.status === 'SUCCESS') {
                console.log(JSON.stringify(result));
                // Need to hanlde Execution DB Name in Execution DBConfiguration
                if (result.records.length > 0 && this.cspfmExecutionDBConfigurationObject.configuration.databaseName) {
                    let statusWFConfigIdArray = []
                    for (let i = 0; i < result.records.length; i++) {
                        const statusWFConfigId = result.records[i]["status_wf_config_id"]
                        statusWFConfigIdArray.push(statusWFConfigId)
                    }
                    const statusWFSelector = this.getSelectorObj
                        (this.cspfmExecutionDBConfigurationObject.workFlowUserApprovalStatusObject, 'data.statusWFConfigId',
                            statusWFConfigIdArray)
                    console.log("statusWFSelector = ", statusWFSelector);
                    this.statusWorkFlowDBReplicateFromServerWithSelector(statusWFSelector).then(response => {
                        if (response && response === 'Success') {
                            this.cspfmexecutionPouchDbProvider.liveSyncWithSelector(statusWFSelector);
                            const syncEndTime = new Date().getTime()
                            const seconds = (syncEndTime - this.syncStartTime) / 1000;
                            if (seconds > 60) {
                                const minutes = Math.floor(seconds / 60);
                                stage4.synctime = minutes + " m"
                            } else {
                                stage4.synctime = Math.floor(seconds % 60) + " s"
                            }
                            this.syncStartTime = syncEndTime;
                            stage4.status = "completed"
                            this.initiateMetaDataDbLiveSync();
                            this.navigationMenuPage();
                        } else {
                            this.syncFailureMessage = "Sync Failed";
                            this.isSyncFailed = true;
                        }
                    }).catch(error => {
                        this.syncFailureMessage = error.message;
                        this.isSyncFailed = true;
                    })
                } else {
                    stage4.synctime = "0 s"
                    stage4.status = "completed"
                    this.initiateMetaDataDbLiveSync();
                    this.navigationMenuPage();
                }
            } else {
                this.syncFailureMessage = "Sync Failed";
                this.isSyncFailed = true;
            }
        }).catch(error => {
            this.syncFailureMessage = error.message;
            this.isSyncFailed = true;
        });
    }
    statusWorkFlowDBReplicateFromServerWithSelector(finalSelector) {
        return this.cspfmexecutionPouchDbProvider.oneTimeReplicationFromServer(finalSelector).then(res => {
            console.log(res);
            if (res === "Success") {
                return Promise.resolve("Success")
            } else {
                return Promise.resolve("Failed")
            }
        }).catch(error => {
            return Promise.resolve(error.message)
        });
    }

    liveDataSyncWithCouchForattachment() {
        const finalSelcetor = { "$or": this.attachmentConfig.configuration.pouchDBSyncEnabledObjectSelectors }
        return this.attachdbprovider.oneTimeReplicationFromServerWithSelector(finalSelcetor).then(res => {
            console.log('DBLiveSync completed =>', res)
            if (res['ok']) {
                this.attachdbprovider.liveSyncWithSelector(finalSelcetor);
                return Promise.resolve("Success")
            } else {
                return Promise.resolve("Failed")
            }
        }).catch(error => {
            return Promise.resolve(error.message)
        })
    }

    //  Method to custom alert
    async showInfoAlert(info) {
        this.alertCtrl.create({
            message: info, subHeader: "",
            buttons: [
                {
                    text: 'Retry',
                    handler: () => {
                        this.reStartSync();
                        console.log('Retry clicked');
                    }
                },
                {
                    text: 'Exit',
                    handler: () => {
                        console.log('Exit clicked');
                        this.gotoApplist();
                    }
                }
            ],
            backdropDismiss: false
        }).then(alert =>
            alert.present());
    }

    ngOnInit() {
    }

    getPreviousAndCurrentSyncObjects(syncType) {

        const stage2 = this.syncStatusArray[1]
        stage2.status = "running"

        let currentSyncEnabledObjects = []

        if (syncType == appConstant.syncEnabledObjectDocName) {
            this.dbprovider.initializePouchDb();
            this.dbConfigurationObj.configuration.pouchDBSyncEnabledObjectSelectors.forEach(element => {
                if (element['data.type']) {
                    currentSyncEnabledObjects.push(element['data.type']);
                }
            });

            let existingSyncEnabledObjects = []
            this.dbprovider.getDocumentFromLocalStorage(appConstant.syncEnabledObjectDocName).then(doc => {
                if (doc) {
                    existingSyncEnabledObjects = doc['object_set'];
                }

                existingSyncEnabledObjects = existingSyncEnabledObjects.length > 0 ? existingSyncEnabledObjects.sort() : existingSyncEnabledObjects;
                currentSyncEnabledObjects = currentSyncEnabledObjects.length > 0 ? currentSyncEnabledObjects.sort() : currentSyncEnabledObjects;

                this.comparePreviousAndCurrentSyncObjects(syncType, existingSyncEnabledObjects, currentSyncEnabledObjects);
            }).catch(err => {
                console.log(err)
            });
        } else {
            this.attachdbprovider.initializePouchDb();
            this.attachmentConfig.configuration.pouchDBSyncEnabledObjectSelectors.forEach(element => {
                if (element['data.type']) {
                    currentSyncEnabledObjects.push(element['data.type']);
                }
            });

            let existingSyncEnabledObjects = []
            this.attachdbprovider.getDocumentFromLocalStorage(appConstant.syncEnabledObjectDocName).then(doc => {
                if (doc) {
                    existingSyncEnabledObjects = doc['object_set'];
                }

                existingSyncEnabledObjects = existingSyncEnabledObjects.length > 0 ? existingSyncEnabledObjects.sort() : existingSyncEnabledObjects;
                currentSyncEnabledObjects = currentSyncEnabledObjects.length > 0 ? currentSyncEnabledObjects.sort() : currentSyncEnabledObjects;

                this.comparePreviousAndCurrentSyncObjects(syncType, existingSyncEnabledObjects, currentSyncEnabledObjects);
            }).catch(err => {
                console.log(err)
            });
        }

    }

    comparePreviousAndCurrentSyncObjects(syncType, existingSyncEnabledObjects, currentSyncEnabledObject) {
        if (currentSyncEnabledObject.length < existingSyncEnabledObjects.length) {
            this.destroyAndSyncBasedOnSyncType(syncType);
        } else if (currentSyncEnabledObject.length == existingSyncEnabledObjects.length) {
            if (currentSyncEnabledObject.join() === existingSyncEnabledObjects.join()) {
                if (syncType == appConstant.syncEnabledObjectDocName) {
                    this.startSyncOperation();
                } else {
                    this.syncattachmentDB();
                }
            } else {
                this.destroyAndSyncBasedOnSyncType(syncType);
            }
        } else if (currentSyncEnabledObject.length > existingSyncEnabledObjects.length) {

            if (existingSyncEnabledObjects.length == 0) {
                if (syncType == appConstant.syncEnabledObjectDocName) {
                    this.startSyncOperation();
                } else {
                    this.syncattachmentDB();
                }
                return;
            }

            let objectNotPresentInCurrentObjectSet = []
            objectNotPresentInCurrentObjectSet = existingSyncEnabledObjects.filter(value =>
                !currentSyncEnabledObject.includes(value));

            if (objectNotPresentInCurrentObjectSet.length > 0) {
                this.destroyAndSyncBasedOnSyncType(syncType);
                return;
            }

            let objectPresentInCurrentObjectSet = []
            objectPresentInCurrentObjectSet = currentSyncEnabledObject.filter(value =>
                !existingSyncEnabledObjects.includes(value));

            if (objectPresentInCurrentObjectSet.length > 0) {
                if (syncType == appConstant.syncEnabledObjectDocName) {
                    var querySelector = []
                    var offlineObjects = this.dbConfigurationObj.configuration.pouchDBSyncEnabledObjectSelectors;
                    querySelector = offlineObjects.filter(function (item) {
                        if (item['data.type']) {
                            return !objectPresentInCurrentObjectSet.includes(item['data.type']);
                        } else {
                            return true;
                        }
                    })
                    console.log("querySelector : ", JSON.stringify(querySelector));
                    this.startSyncOperation(querySelector, objectPresentInCurrentObjectSet);
                } else {
                    var querySelector = []
                    var offlineAttachmentObjects = this.attachmentConfig.configuration.pouchDBSyncEnabledObjectSelectors;
                    querySelector = offlineAttachmentObjects.filter(function (item) {
                        if (item['data.type']) {
                            return !objectPresentInCurrentObjectSet.includes(item['data.type']);
                        } else {
                            return true;
                        }
                    })
                    console.log("Attachment querySelector : ", JSON.stringify(querySelector));
                    this.syncattachmentDB(querySelector);
                }
            }
        }
    }

    destroyAndSyncBasedOnSyncType(syncType) {
        if (syncType == appConstant.syncEnabledObjectDocName) {
            this.dbprovider.destroyDb().then(result => {
                this.dbprovider.initializePouchDb();
                this.startSyncOperation();
            });
        } else {
            this.attachdbprovider.destroyDb().then(result => {
                this.attachdbprovider.initializePouchDb();
                this.syncattachmentDB();
            });
        }
    }
    async  fetchAssignmentObjectForMetaValidation() {
        const options = {};
        const selector = {}

        selector['data.type'] = this.metaDbConfigurationObj.applicationAssignmentObject;
        selector['data.is_active'] = true;
        selector['data.user_id_p'] = Number(this.apputilityObject.userId);
        options['selector'] = selector;
        this.aplicationAssignmentObjectHierarchyJSON['options'] = options;


        return this.metaDbProvider.fetchDataWithReference(this.aplicationAssignmentObjectHierarchyJSON).then(fetchedResult => {
            if (fetchedResult.status != 'SUCCESS') {

                return Promise.resolve(false)

            }

            return Promise.resolve(fetchedResult.records)

        }).catch(err => {

            return Promise.resolve(false)
        });

    }
    private getAssighedAppDetail() {
        let assignedAppsDetail
        if (this.apputilityObject.isMobile) {
            return this.appPreferences.fetch('login_response').then((response) => {
                return JSON.parse(response);
            })
        }
        else {
            assignedAppsDetail = JSON.parse(localStorage['assignedApps'])
            return assignedAppsDetail;
        }

    }
    async viewDocsInsertFoeMetaValidation(assigenmenfetcResult) {
        var assignedAppIdListInPochDb = [];
        var localAppIdListToBulkInsert = [];
        if (assigenmenfetcResult && assigenmenfetcResult.length > 0)
            assignedAppIdListInPochDb = assigenmenfetcResult.map(obj => obj.application_id_s);
        var assignedAppsDetail = await this.getAssighedAppDetail()
        var assignedAppIdsListInLocal = [];
        assignedAppsDetail.map(obj => assignedAppIdsListInLocal.push(obj.appId));
        if (assignedAppIdListInPochDb.length > 0) {



            for (var i = 0; i < assignedAppIdsListInLocal.length; i++) {
                if (assignedAppIdListInPochDb.indexOf(assignedAppIdsListInLocal[i]) == -1) {
                    localAppIdListToBulkInsert.push(assignedAppIdsListInLocal[i]);
                }
            }

        }
        else {
            localAppIdListToBulkInsert = assignedAppIdsListInLocal;
        }
        if (localAppIdListToBulkInsert.length == 0) {

            return Promise.resolve(true)
        }

        var selectorList = this.metaDbProvider.makeSelectorForMetaDataSyncArray(localAppIdListToBulkInsert, false)

        console.log(selectorList);

        return this.metaDbProvider.fetchMetaDataObjects(selectorList).then(response => {
            if (response) {

                return Promise.resolve(true)
            }
            else {

                return Promise.resolve(false)
            }
        }).catch(err => {
            return Promise.resolve(false)
        });


    }
}
