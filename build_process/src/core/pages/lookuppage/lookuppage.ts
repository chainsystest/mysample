import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams, Platform, ModalController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { appUtility } from 'src/core/utils/appUtility';
import { DatePipe } from '@angular/common';
import { appConstant } from 'src/core/utils/appConstant';
@Component({
    selector: 'app-lookuppage',
    templateUrl: './lookuppage.html',
    styleUrls: ['./lookuppage.scss'],
})
export class lookuppage implements OnInit {

    private objectName: any;
    public lookupColumnDetails = [];
    public lookupTitle: any;
    private serviceObject: any;
    private allItems = [];
    public items = [];
    private searchTerm: String = ''; // Search text
    private lookupInput: any;
    private isPushVal = false;
    private lookupColumnName: String = '';
    loading;
    public dataSource: String;
    constructor(private navCtrl: NavController, public loadingCtrl: LoadingController,
        private navParams: NavParams, public platform: Platform, public route: Router, public activeRoute: ActivatedRoute,
        public modalController: ModalController, public appUtilityConfig: appUtility, public datePipe: DatePipe) {

        this.lookupInput = JSON.parse(JSON.stringify(this.navParams.get('lookupInput')));
        this.serviceObject = this.navParams.get('serviceObject');
        this.objectName = this.navParams.get('objectName');
        this.lookupColumnName = this.navParams.get('lookupColumnName');
        this.dataSource = this.navParams.get('dataSource');

        this.lookupColumnDetails = this.lookupInput.lookupColumnDetails;
        if (this.lookupColumnDetails.length > 3 ) {
            this.lookupColumnDetails.splice(3);
        }
        this.lookupTitle = this.lookupInput.title;
      //   if (this.navCtrl.getActive() === undefined) {
        //       console.log('no nav control');
        //       this.isPushVal = false;

        //   } else {
        //       console.log('get nav control', this.navCtrl.getActive().component.name);
        //       this.isPushVal = true;
        //   }
    }
    ionViewWillEnter() {
        this.fetchAllData({ selector: this.lookupInput.selector });
    }
  async fetchAllData(options) {
        this.loading = await this.loadingCtrl.create({ message: 'Fetching...' });
        this.loading.present();
        if (this.lookupInput['objectHierarchy']) {
            var objectHierarchy = this.lookupInput['objectHierarchy']
            var additionalInfo = this.lookupInput['additionalInfo']
            if (appConstant.pouchDBStaticName === this.dataSource) {
            this.serviceObject.fetchdatawithRelationship(objectHierarchy, additionalInfo).then(res => {
                console.log(res);
                this.loading.dismiss()
                if (res['records'].length > 0) {
                    this.allItems = res['records'];
                    this.setFilteredItems();
                } else {
                   console.log('fetchDataWithReference in lookup list.ts is failed', res['message']);  
                }
               
            }).catch(error => {
                this.loading.dismiss();
            })
           
            }  else if (appConstant.couchDBStaticName === this.dataSource) {
                let query = objectHierarchy['query']
                this.serviceObject.fetchRecordsBySearchFilterPhrases(query, objectHierarchy).then(res => {
                        if (res['status'] = 'SUCCESS') {
                            if (res['records'].length > 0) {
                                this.allItems = res['records'];
                                this.setFilteredItems();
                            } else {
                                console.log('fetchDataWithReference...... no records found');
                            }
                        } else {
                            console.log('fetchDataWithReference in lookup list.ts is failed', res['message']);
                        }
                        this.loading.dismiss()
                    })
                    .catch(error => {
                    this.loading.dismiss()
                    })
            }
        } 
        else {
            this.serviceObject.fetchDocsWithoutRelationshipUsingFindOption(options)
                .then(res => {
                    console.log("fetchDocsWithoutRelationshipUsingFindOption = ", res);
                    if (res['status'] = 'SUCCESS') {
                        if (res['records'].length > 0) {
                            this.allItems = res['records'];
                            this.setFilteredItems();
                        } else {
                            console.log('fetchDocsWithoutRelationshipUsingFindOption...... no records found');
                        }
                    } else {
                        console.log('fetchDocsWithoutRelationshipUsingFindOption in lookup list.ts is failed', res['message']);
                    }
                    this.loading.dismiss();
                })
                .catch(error => {
                    this.loading.dismiss();
                })
        }
    }
    setFilteredItems(event?) {
        if (event) {
            this.searchTerm = event.target.value;
        }
        this.items = this.allItems.filter((item) => {
            return this.isSearchMatched(item, this.lookupColumnDetails, this.searchTerm);
        });
        console.log(this.items);
    }
    itemClick(item) {
        const parent = this.navParams.get('parentPage');
        parent.lookupResponse(this.lookupColumnName, item);
        this.closeButtonClick();
    }
    closeButtonClick() {
        this.modalController.dismiss(true);
    }
    dismiss() {
        this.navCtrl.pop();

    }
    onCancel() {
        this.items = this.allItems;
    }

    ngOnInit() {
    }

    isSearchMatched(item, queryFields, searchText) {
        for (const queryField of queryFields) {
                if (queryField['fieldType'] == 'Text' || queryField['fieldType'] == 'Multilinetextbox' || queryField['fieldType'] == 'Autonumber' || queryField['fieldType'] == 'Primary' || queryField['fieldType'] == 'Textarea' || queryField['fieldType'] == 'Url' || queryField['fieldType'] == 'Email' || queryField['fieldType'] == 'Number' || queryField['fieldType'] == 'Decimal') {
                    if (item[queryField['columnName']] && item[queryField['columnName']].toString().toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
                        return true;
                    }
                } else if (queryField['fieldType'] == 'Multiselect' || queryField['fieldType'] == 'Checkbox') {
                    if (item[queryField['columnName']] !== undefined && item[queryField['columnName']] !== "") {
                        const stateTypeList = item[queryField['columnName']];
                        for (const element of stateTypeList) {
                            if (queryField['mappingValues'][element]
                                && queryField['mappingValues'][element].toString().toLowerCase().
                                    indexOf(searchText.toLowerCase()) > -1) {
                                return true;
                            }
                        }
                    }
                } else if (queryField['fieldType'] == 'Radio' || queryField['fieldType'] == 'Dropdown') {
                    if (item[queryField['columnName']] && item[queryField['columnName']] !== "") {
                        if (queryField['mappingValues'][item[queryField['columnName']]]
                            && queryField['mappingValues'][item[queryField['columnName']]].toString().toLowerCase().
                                indexOf(searchText.toLowerCase()) > -1) {
                            return true;
                        }
                    }
                } else if (queryField['fieldType'] === 'Date') {
                    if (item[queryField['columnName']]) {
                      let dateString = this.datePipe.transform(new Date(item[queryField['columnName']]), this.appUtilityConfig.userDateFormat);
                      if (dateString.toString().toLowerCase().indexOf(searchText.toString().toLowerCase()) > -1) {
                        return true;
                      }
                    }
                } else if (queryField['fieldType'] === 'Timestamp') {
                    if (item[queryField['columnName']]) {
                      let dateTimeString = this.datePipe.transform(new Date(item[queryField['columnName']]), this.appUtilityConfig.userDateTimeFormat, this.appUtilityConfig.userZoneOffsetValue);
                      if (dateTimeString.toString().toLowerCase().indexOf(searchText.toString().toLowerCase()) > -1) {
                        return true;
                      }
                    }
                }
        }
    }
}
