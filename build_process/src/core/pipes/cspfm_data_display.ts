import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe, CurrencyPipe } from "@angular/common";
import { Formatter } from 'angular-slickgrid';

export const CspfmDataFormatter: Formatter = (row: number, cell: number, value: any, columnDef: any, dataContext: any, grid: any) => {
  if (columnDef['params'] && columnDef['params']['pipe'] && columnDef['params']['fieldInfo']) {
    var columnValue = columnDef['params']['pipe'].transform(dataContext, columnDef['params']['fieldInfo']);
    return columnValue;
  } else {
    return "";
  }
};

export interface FieldInfo {
  label: string;
  prop: string;
  fieldName: string;
  fieldType: "PERCENT" | "MASTERDETAIL" | "NUMBER" | "CHECKBOX"
  | "HIDDEN" | "TEXTAREA" | "URL" | "BOOLEAN" | "CURRENCY"
  | "DECIMAL" | "TIMESTAMP" | "FILEUPLOAD" | "EMAIL" | "RICHTEXT"
  | "RADIO" | "LOOKUP" | "DROPDOWN" | "AUTONUMBER" | "PASSWORD"
  | "TEXT" | "IMAGE" | "MULTISELECT" | "DATE" | "GEOLOCATION"
  | "STATUSWORKFLOW" | "FORMULA" | "HEADER";
  mappingDetails: { [key: string]: string; } | "";
  child: FieldInfo | "";
  dateFormat: string;
  currencyDetails: {
    currencyCode?: string;
    display?: "code" | "symbol" | "symbol-narrow" | string | boolean;
    digitsInfo?: string;
    locale?: string;
  } | "";
}

@Pipe({
  name: "cspfm_data_display"
})
export class cspfm_data_display implements PipeTransform {
  constructor(private datePipe: DatePipe, private currencyPipe: CurrencyPipe) { }

  transform(data: any, fieldInfo?: FieldInfo): any {
    if (fieldInfo) {
      return this.getFieldValue(data, fieldInfo);
    } else {
      return data;
    }
  }

  getFieldValue(data, fieldInfo: FieldInfo | "") {
    if (fieldInfo === "") {
      return "";
    }
    if (data[fieldInfo["fieldName"]] === null) {
      return "";
    }
    if (
      fieldInfo["fieldType"] == "NUMBER" ||
      fieldInfo["fieldType"] == "DECIMAL"
    ) {
      if (data[fieldInfo["fieldName"]]) {
        return data[fieldInfo["fieldName"]];
      } else {
        return 0;
      }
    } else if (fieldInfo["fieldType"] === "CURRENCY") {
      if (data[fieldInfo["fieldName"]]) {
        return this.currencyPipe.transform(
          data[fieldInfo["fieldName"]], fieldInfo["currencyDetails"]['currencyCode'], fieldInfo["currencyDetails"]['display'], fieldInfo["currencyDetails"]['digitsInfo'], fieldInfo["currencyDetails"]['locale']);
      } else {
        return this.currencyPipe.transform(0, fieldInfo["currencyDetails"]['currencyCode'], fieldInfo["currencyDetails"]['display'], fieldInfo["currencyDetails"]['digitsInfo'], fieldInfo["currencyDetails"]['locale']);
      }
    } else if (
      fieldInfo["fieldType"] === "DATE" ||
      fieldInfo["fieldType"] === "TIMESTAMP"
    ) {
      if (data[fieldInfo["fieldName"]]) {
        return this.datePipe.transform(
          new Date(data[fieldInfo["fieldName"]]),
          fieldInfo["dateFormat"]
        );
      } else {
        return "";
      }
    } else if (fieldInfo["fieldType"] == "BOOLEAN") {
      if (data[fieldInfo["fieldName"]]) {
        return data[fieldInfo["fieldName"]];
      } else {
        return false;
      }
    } else if (
      fieldInfo["fieldType"] == "TEXT" ||
      fieldInfo["fieldType"] == "TEXTAREA" ||
      fieldInfo["fieldType"] == "EMAIL" ||
      fieldInfo["fieldType"] == "URL" ||
      fieldInfo["fieldType"] == "AUTONUMBER"
    ) {
      return data[fieldInfo["fieldName"]];
    } else if (fieldInfo["fieldType"] == "PASSWORD") {
      return data[fieldInfo["fieldName"]].replace(new RegExp("[^ ]", "g"), "*");
    } else if (
      fieldInfo["fieldType"] == "MULTISELECT" ||
      fieldInfo["fieldType"] == "CHECKBOX"
    ) {
      var displayValue = [];
      if (data[fieldInfo["fieldName"]]) {
        const values = data[fieldInfo["fieldName"]];
        for (const element of values) {
          displayValue.push(fieldInfo["mappingDetails"][element]);
        }
      }
      if (displayValue.length > 0) {
        return displayValue.join(", ");
      } else {
        return "";
      }
    } else if (fieldInfo["fieldType"] == "LOOKUP") {
      if (data[fieldInfo["fieldName"]]) {
        return this.getFieldValue(
          data[fieldInfo["fieldName"]],
          fieldInfo["child"]
        );
      } else {
        return "";
      }
    } else if (fieldInfo["fieldType"] == "MASTERDETAIL"
      || fieldInfo['fieldType'] == "HEADER") {
      if (
        data[fieldInfo["fieldName"]] &&
        data[fieldInfo["fieldName"]].length > 0
      ) {
        return this.getFieldValue(
          data[fieldInfo["fieldName"]][0],
          fieldInfo["child"]
        );
      } else {
        return "";
      }
    } else if (
      fieldInfo["fieldType"] == "RADIO" ||
      fieldInfo["fieldType"] == "DROPDOWN"
    ) {
      if (data[fieldInfo["fieldName"]] && data[fieldInfo["fieldName"]] !== "") {
        return fieldInfo["mappingDetails"][data[fieldInfo["fieldName"]]];
      } else {
        return "";
      }
    }
  }
}
