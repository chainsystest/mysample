import { Component } from '@angular/core';
import { appConfiguration } from '../utils/appConfiguration';
import { appUtility } from '../utils/appUtility';
import { Http } from '@angular/http';
import { MenuController, Platform, AlertController, NavController } from '@ionic/angular';
import { Broadcaster } from '@ionic-native/broadcaster/ngx';
import { File } from '@ionic-native/file/ngx';
import { calendarBridge } from '../nativebridges/calendarBridge';
import { mapBridge } from '../nativebridges/mapBridge';
import { languagepage } from '../pages/languagepage/languagepage';
import { settingpage } from '../pages/settingpage/settingpage';
import { map } from 'rxjs/operators';
import { homepage } from '../pages/homepage/homepage';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import { metaDbConfiguration } from 'src/core/db/metaDbConfiguration';
import { metaDataDbProvider } from 'src/core/db/metaDataDbProvider';




@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
})
export class MenuComponent {
    isRequiredToShowComponent: boolean;
    activeMenuGroups = [0];
    allMenuGroups: Array<{
        menuGroupId: number, menuGroupDisplayName: String, icon: string,
        menuItems: Array<{ title: string, url: string, icon: string }>
    }>;
    defaultGroupObj = {};
    loggedUserDetail = {};

    map: Array<{ title: string, url: any, icon: string }>;
    calender: Array<{ title: string, url: any, icon: string }>;
    defaultMenu: Array<{ title: string, url: any, icon: string }>;
    menuClasses = ["cs-menu-c-one", "cs-menu-c-two", "cs-menu-c-three", "cs-menu-c-four", "cs-menu-c-five", "cs-menu-c-six", "cs-menu-c-seven", "cs-menu-c-eight", "cs-menu-c-nine"]
    public previousExpandedGroup = "";
    public mobileProfilePic = "";
    constructor(public appConfig: appConfiguration,
        public http: Http, public file: File, private menuCtl: MenuController,
        private platform: Platform, private broadcaster: Broadcaster,
        public alertCtrl: AlertController, public nativeBridgeCalendar: calendarBridge, public nativeBridgeMap: mapBridge,
        public router: Router, public activeRoute: ActivatedRoute,
        public location: Location, private appPreferences: AppPreferences, public appUtilityConfig: appUtility, public navController: NavController,
        public metaDbConfig: metaDbConfiguration, public metaDbProvider: metaDataDbProvider) {
        platform.ready().then(() => {
            this.getActiveMenuGroup();
        });

        // Predefined menu

        if (this.appUtilityConfig.isMobile) {
            this.isRequiredToShowComponent = true;
            this.defaultMenu = [];
        } else {
            this.isRequiredToShowComponent = false;

            if (this.appConfig.configuration.defaultMenu.isI18Enable) {
                this.defaultMenu = [
                    { title: 'Settings', url: '/menu/cspfmSettings', icon: 'icon-mat-settings' }
                   
                ];
            } else {
                this.defaultMenu = [
                    { title: 'Settings', url: '/menu/cspfmSettings', icon: 'icon-mat-settings' }
                   
                ];
            }

        }
        // Map menu
        this.map = [
            { title: 'Map', url: '/menu/homepage', icon: 'icon-mat-language' }
        ];
        // Calender menu
        this.calender = [
            { title: 'Calender', url: '/menu/homepage', icon: 'icon-mat-calendar_today' }
        ];

        // Dynamic menu entry start here
        this.allMenuGroups = [{"menuGroupId":0,"menuGroupDisplayName":"HOME","icon":"icon-mat-home","menuItems":[{"title":"Home","url":"/menu/homepage","icon":"icon-mat-home"}]},{"menuGroupId":2351,"menuGroupDisplayName":"MenuGroup.Id_2351","icon":"fa icon-mat-menu cs-ts-21","menuItems":[{"title":"MenuItem.Id_146993","url":"/menu/AccountList_MOBILE_Grid_with_List","icon":"fa icon-mat-menu cs-grey-lighten cs-ts-21"}]},{"menuGroupId":2354,"menuGroupDisplayName":"MenuGroup.Id_2354","icon":"fa icon-mat-menu cs-ts-21","menuItems":[{"title":"MenuItem.Id_151716","url":"/menu/InventoryList_MOBILE_List","icon":"fa icon-mat-menu cs-grey-lighten cs-ts-21"}]},{"menuGroupId":2397,"menuGroupDisplayName":"MenuGroup.Id_2397","icon":"fa icon-mat-menu cs-ts-21","menuItems":[{"title":"MenuItem.Id_152813","url":"/menu/Saleorderlist_MOBILE_Grid_with_List","icon":"fa icon-mat-menu cs-grey-lighten cs-ts-21"},{"title":"MenuItem.Id_143955","url":"/menu/crmsaleorders_d_m_search_list","icon":"fa icon-mat-menu cs-grey-lighten cs-ts-21"}]}]

        this.addMenuGroupExpandableFlag();
        if(this.appUtilityConfig.enableMenu===true){
            this.getLoggedUserDetail();    
        }
    }

    // Get active menu group for user
    getActiveMenuGroup() {


        if (this.appUtilityConfig.isMobile && this.appUtilityConfig.enableMenu) {
            // For device
            this.getActiveMenuGroupFromJson();
        } else {
            const assignedApps = localStorage['assignedApps'];

            if (assignedApps) {
                const assignedAppsJson = JSON.parse(assignedApps);
                assignedAppsJson.forEach(app => {
                    if (app['appId'] === this.appConfig.configuration.appId) {
                        const menuGroups = app['assignedMenuGroups'];
                        if (menuGroups) {
                            menuGroups.forEach(element => {
                                this.activeMenuGroups.push(element);
                            });
                        }
                    }
                });
            }
        }
    }



    addMenuGroupExpandableFlag() {
        // for (let menuGroup of this.allMenuGroups) {
        //     menuGroup['expand'] = false;
        // }

        for (let i = 0; i < this.allMenuGroups.length; i++) {
            this.allMenuGroups[i]['expand'] = false;
        }

        this.defaultGroupObj['expand'] = false;
        this.defaultGroupObj['menuGroupDisplayName'] = 'Settings';
        this.defaultGroupObj['menuItems'] = this.defaultMenu;
    }

    getActiveMenuGroupFromJson() {

        this.platform.ready().then(() => {

            this.checkMenuGroupJsonExist().then(resValue => {
                if (resValue) {

                    // this.http.get(this.file.dataDirectory + 'loginResponse.json')
                    //     .pipe(map(res => res.text())).subscribe(text => {
                    //         const response = JSON.parse(String(text));
                    //         const assignedApps = response['assignedApps'];
                    //         if (assignedApps) {
                    //             assignedApps.forEach(app => {
                    //                 if (app['appId'] === this.appConfig.configuration.appId) {
                    //                     const menuGroups = app['assignedMenuGroups'];
                    //                     if (menuGroups) {
                    //                         menuGroups.forEach(element => {
                    //                             this.activeMenuGroups.push(element);
                    //                         });
                    //                     }
                    //                 }
                    //             });
                    //         }
                    //     }, err => {
                    //         alert(err);
                    //     });

                    return this.appPreferences.fetch('login_response').then(response => {


                        if (response) {
                            JSON.parse(response).forEach(app => {
                                if (app['appId'] === this.appConfig.configuration.appId) {
                                    const menuGroups = app['assignedMenuGroups'];
                                    if (menuGroups) {
                                        menuGroups.forEach(element => {
                                            this.activeMenuGroups.push(element);
                                        });
                                    }
                                }
                            });
                            this.setProfilePic();
                        } else {
                            alert('Invalid response from server. Json not found');
                        }

                    })

                } else {
                    alert('Invalid response from server. Json not found');
                }
            });
        });

    }


    // Read Menu Group Json from data directory
    readMenuGroupJson() {
        return this.file.readAsText(this.file.dataDirectory, 'loginResponse.json').then((result) => {
            return result;
        }).catch((err) => {
            return false;
        });
    }
    // Check Menu Group Json exist in data directory
    checkMenuGroupJsonExist() {
        // return this.file.checkFile(this.file.dataDirectory, 'loginResponse.json').then((result) => {
        //     return true;
        // }).catch(err => {
        //     return false;

        // });

        return this.appPreferences.fetch('login_response').then(res => {

            if (res && res !== '')
                return true
            else
                return false


        }).catch(err => {
            return false
        })
    }

    openPage(page) {
        const pageValue = JSON.stringify(page);
        const pageResultObj = JSON.parse(pageValue);
        const pageTitle = pageResultObj['title'];
        const queryParams = {"isFromMenu": true}
        if (pageTitle === 'Settings' || pageTitle === 'Map' || pageTitle === 'Calender') {
            this.setRootPage(page);
        } else if (pageTitle === 'Exit') {
            this.showExitAlertView(page);
        } else {
            this.navController.navigateBack([page.url], {queryParams: queryParams, skipLocationChange: true });
        }
        if (this.appConfig.configuration.isGridMenuEnabled) {
            var element = document.getElementById("cs-box-menu");
            element.classList.remove("cs-box-menu-active");
        }
    }
    setRootPage(page) {
        this.router.navigate([page.url], { skipLocationChange: true });
        if (this.appUtilityConfig.isMobile) {
            if (page.title === 'Calender') {
                this.nativeBridgeCalendar.startObserver();
            } else if (page.title === 'Map') {
                this.nativeBridgeMap.startObserver();
            }
            this.callNotificationBroadcastPlugin(page);
        }
    }
    callNotificationBroadcastPlugin(page) {
        this.broadcaster.fireNativeEvent('ionicNativeBroadcast', { action: page.title })
            .then(() => console.log('Success'))
            .catch(() => console.log('Error'));
    }
    async showExitAlertView(page) {
        const confirm = await this.alertCtrl.create({
            header: 'Exit',
            message: 'Are you sure want to exit ?',
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: 'OK',
                    handler: () => {

                        if (this.appUtilityConfig.isMobile) {
                            this.callNotificationBroadcastPlugin(page);
                        }
                        window.location.replace('/apps/applist');

                    }
                }
            ]
        });
        confirm.present();
    }

    ionViewDidEnter() {
        var menuList = document.querySelectorAll('.styleLight ion-content.menu-content .menu-content-list');
        var menuListLen = menuList.length;
        for (var i = 0; i < menuListLen; i++) {
            // menuList[i].setAttribute('style', `background:var(--ion-color-primary, #3880ff)`);
        }
        var menuItem = document.querySelectorAll('.styleLight ion-content.menu-content .menu-content-item');
        var menuItemLen = menuItem.length;
        for (var i = 0; i < menuItemLen; i++) {
            // menuItem[i].setAttribute('color', `var(--ion-color-primary, #3880ff)`);
        }
        var menuItem = document.querySelectorAll('.styleLight ion-content.menu-content .menu-content-item');
        var menuItemLen = menuItem.length;
        for (var i = 0; i < menuItemLen; i++) {
            var menuValue = menuItem[i].classList.remove('class', 'active');
        }
        var menuContent = document.querySelectorAll('.styleLight ion-content.menu-content');
        var menuContentLen = menuContent.length;
        for (var i = 0; i < menuContentLen; i++) {
            // menuContent[i].setAttribute('color', `var(--ion-color-primary, #3880ff)`);
        }
    }


    closeMenu(group) {
        if (this.previousExpandedGroup == "") {
            this.previousExpandedGroup = group;
            group['expand'] = !group['expand'];
        } else {
            if (this.previousExpandedGroup == group) {
                group['expand'] = !group['expand'];
            } else {
                this.previousExpandedGroup['expand'] = false;
                group['expand'] = true;
                this.previousExpandedGroup = group;
            }
        }
    }


    public corUsersObjectHierarchyJSON = {
        "objectId": this.metaDbConfig.corUsersObject,
        "objectName": this.metaDbConfig.corUsersObject,
        "fieldId": 0,
        "objectType": "PRIMARY",
        "relationShipType": null,
        "childObject": [

        ]
    };

    getLoggedUserDetail() {
        const options = {};
        const selector = {}
        selector['data.type'] = this.metaDbConfig.corUsersObject;
        selector['data.user_id'] = Number(this.appUtilityConfig.userId);
        options['selector'] = selector;
        this.corUsersObjectHierarchyJSON['options'] = options;

        return this.metaDbProvider.fetchDataWithReference(this.corUsersObjectHierarchyJSON).then(corUserResult => {
            if (corUserResult.status !== 'SUCCESS') {

            }
            if (corUserResult['records'].length == 0) {

            }

            const userInfo = JSON.parse(JSON.stringify(corUserResult['records']))
            this.loggedUserDetail = userInfo[0];

        }).catch(err => {
            console.log('Menu Componenet - Exception Received in core user fetching method')
        });

    }

setProfilePic(){
    this.file.checkFile(this.file.dataDirectory, 'user.png').then((result) => {
    if(result){
    this.mobileProfilePic = this.file.dataDirectory + 'user.png';
    }else{
    this.mobileProfilePic = "assets/img/user_icon.png";
    }
    }).catch(err => {
    this.mobileProfilePic = "assets/img/user_icon.png";
    
    });
    }
}
