import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import { appConfiguration } from './appConfiguration';
import { metaDbConfiguration } from '../db/metaDbConfiguration';
import { appUtility } from "./appUtility";
import { async } from '@angular/core/testing';
@Injectable({
    providedIn: 'root'
})

export class metaDbValidation {

    response = {};
    private warningMessageList = [];
    appNameList = [];
    metaDbProvider;



    public corUsersObjectHierarchyJSON = {
        "objectId": this.metaDbConfig.corUsersObject,
        "objectName": this.metaDbConfig.corUsersObject,
        "fieldId": 0,
        "objectType": "PRIMARY",
        "relationShipType": null,
        "childObject": [

        ]
    };
    public aplicationAssignmentObjectHierarchyJSON = {
        "objectId": this.metaDbConfig.applicationAssignmentObject,
        "objectName": this.metaDbConfig.applicationAssignmentObject,
        "fieldId": 0,
        "objectType": "PRIMARY",
        "relationShipType": null,
        "childObject": [

        ]
    };
    public applicationPublishInfoObjectHierarchyJSON = {
        "objectId": this.metaDbConfig.applicationPublishInfoObject,
        "objectName": this.metaDbConfig.applicationPublishInfoObject,
        "fieldId": 0,
        "objectType": "PRIMARY",
        "relationShipType": null,
        "childObject": [

        ]
    };
    public corMenuGroupObjectHierarchyJSON = {
        "objectId": this.metaDbConfig.corMenuGroup,
        "objectName": this.metaDbConfig.corMenuGroup,
        "fieldId": 0,
        "objectType": "PRIMARY",
        "relationShipType": null,
        "childObject": [

        ]
    };
    public corUserMenuGroupAssignmentObjectHierarchyJSON = {
        "objectId": this.metaDbConfig.corUSerMenuGroupAssignemt,
        "objectName": this.metaDbConfig.corUSerMenuGroupAssignemt,
        "fieldId": 0,
        "objectType": "PRIMARY",
        "relationShipType": null,
        "childObject": [

        ]
    };
    public corRoleMenuGroupAssignmentObjectHierarchyJSON = {
        "objectId": this.metaDbConfig.corRoleMenuGroupAssignemt,
        "objectName": this.metaDbConfig.corRoleMenuGroupAssignemt,
        "fieldId": 0,
        "objectType": "PRIMARY",
        "relationShipType": null,
        "childObject": [

        ]
    };
    private corMobileAppsHierarchyJSON = {
        "objectId": this.metaDbConfig.corMobileApps,
        "objectName": this.metaDbConfig.corMobileApps,
        "fieldId": 0,
        "objectType": "PRIMARY",
        "relationShipType": null,
        "childObject": [
        ]
    };
    private corApplicationHierarchyJSON = {
        "objectId": this.metaDbConfig.corApplications,
        "objectName": this.metaDbConfig.corApplications,
        "fieldId": 0,
        "objectType": "PRIMARY",
        "relationShipType": null,
        "childObject": [
        ]
    };
    private warningType = [];

    constructor(
        public platform: Platform, private appPreferences: AppPreferences, public appConfig: appConfiguration,
        public metaDbConfig: metaDbConfiguration, private apputilityObject: appUtility) {
    }

    private makeResultJson(status, message, extras?) {
        this.response['status'] = status;
        this.response['message'] = message;
        this.response['errorType'] = ''
        if (extras && extras['fetchError'])
            this.response["errorType"] = extras[this.metaDbConfig.fetchError]

    }

    async metaValidations(metaDataDbProvider, isIntialSync, changesType?) {
        this.metaDbProvider = metaDataDbProvider;
        this.warningMessageList = [];
        //  this.appNameList = [];
        //   this.localAppIdListToBulkInsert = [];
        //   this.newAppNameList = [];
        this.response = {};
        this.warningType = [];
        var assignedAppsDetail = await this.getAssighedAppDetail()
        if (isIntialSync) {

            return this.fullValidationProcess(assignedAppsDetail, isIntialSync).then(validationProcessResult => {
                if (this.warningMessageList.length != 0) {
                    this.response["warningSet"] = this.warningMessageList;
                    this.response["warningTypes"] = this.warningType;
                }
                return Promise.resolve(this.response);
            })


        }
        else {
            //undefined
            if (!changesType) {
                this.makeResultJson(this.metaDbConfig.SUCCESS, "");
                return Promise.resolve(true);
            }
            console.log("changesType.....", changesType);

            if (changesType == this.metaDbConfig.corUsersObject) {
                return this.userValidation().then(res => {

                    return Promise.resolve(this.response);
                })
            }
            else if (changesType == this.metaDbConfig.corApplications) {
                return this.appBaseValidation().then(res => {

                    return Promise.resolve(this.response);
                })
            }
            else if (changesType == this.metaDbConfig.applicationAssignmentObject + "true") {
                return this.fetchAndInsertNewAppRecords(assignedAppsDetail, isIntialSync).then(result => {
                    if (!result) {

                        return Promise.resolve(this.response);
                    }
                    return this.appAssignmentValidation().then(res => {
                        if (this.warningMessageList.length != 0) {
                            this.response["warningSet"] = this.warningMessageList;
                            this.response["warningTypes"] = this.warningType;
                        }
                        return Promise.resolve(this.response);
                    })
                })

            }
            else if (changesType == this.metaDbConfig.applicationAssignmentObject + "false") {
                return this.appAssignmentValidation().then(res => {
                    if (this.warningMessageList.length != 0) {
                        this.response["warningSet"] = this.warningMessageList;
                        this.response["warningTypes"] = this.warningType;
                    }
                    return Promise.resolve(this.response);
                })
            }
            else if (changesType == this.metaDbConfig.applicationPublishInfoObject) {
                return this.checkAppNewVersionAvailable(assignedAppsDetail, isIntialSync).then(res => {
                    if (this.warningMessageList.length != 0) {
                        this.response["warningSet"] = this.warningMessageList;
                        this.response["warningTypes"] = this.warningType;
                    }
                    return Promise.resolve(this.response);
                })
            }
            else if (changesType == this.metaDbConfig.corMenuGroup) {
                return this.menuGroupValidation(assignedAppsDetail).then(res => {

                    return Promise.resolve(this.response);
                })
            }
            else if (changesType == this.metaDbConfig.corMobileApps) {
                return this.checkAppNewVersionAvailable(assignedAppsDetail, isIntialSync).then(res => {
                    if (!res) {

                        return Promise.resolve(this.response);
                    }
                    return this.appAssignmentValidation().then(res => {

                        if (!res) {

                            return Promise.resolve(this.response);
                        }
                        return this.menuGroupValidation(assignedAppsDetail).then(res => {
                            if (this.warningMessageList.length != 0) {
                                this.response["warningSet"] = this.warningMessageList;
                                this.response["warningTypes"] = this.warningType;
                            }
                            return Promise.resolve(this.response);
                        })
                    })

                })
            }
            else {
                this.makeResultJson(this.metaDbConfig.SUCCESS, "");
                return true;
            }

        }
    }
    fullValidationProcess(assignedAppsDetail, isIntialSync) {
        return this.fetchAndInsertNewAppRecords(assignedAppsDetail, isIntialSync).then(newAppResponse => {

            if (!newAppResponse) {
                this.makeResultJson(this.metaDbConfig.FAILURE, "Application bulk insert failed", { "fetchError": this.metaDbConfig.fetchError });
                return Promise.resolve(false)
            }
            return this.userValidation().then(userValidationSuccess => {
                if (!userValidationSuccess)
                    return Promise.resolve(false)

                return this.appBaseValidation().then(appValidationSuccess => {
                    if (!appValidationSuccess)
                        return Promise.resolve(false)

                    return this.appAssignmentValidation().then(appAssignementvaldiationSuccess => {
                        if (!appAssignementvaldiationSuccess)
                            return Promise.resolve(false)

                        return this.checkAppNewVersionAvailable(assignedAppsDetail, isIntialSync).then(versionValdiationSuccess => {
                            if (!versionValdiationSuccess)
                                return Promise.resolve(false)

                            return this.menuGroupValidation(assignedAppsDetail)
                        })



                    })
                })
            })

        }).catch(err => {
            this.makeResultJson(this.metaDbConfig.FAILURE, "Application bulk insert failed", { "fetchError": this.metaDbConfig.fetchError });
            return Promise.resolve(false)
        });
    }

    userValidation() {
        const options = {};
        const selector = {}
        selector['data.type'] = this.metaDbConfig.corUsersObject;
        selector['data.user_id'] = Number(this.apputilityObject.userId);
        options['selector'] = selector;
        this.corUsersObjectHierarchyJSON['options'] = options;

        return this.metaDbProvider.fetchDataWithReference(this.corUsersObjectHierarchyJSON).then(corUserResult => {
            if (corUserResult.status !== 'SUCCESS') {
                this.makeResultJson(this.metaDbConfig.FAILURE, "Meta data user fetch failure", { "fetchError": this.metaDbConfig.fetchError });
                return Promise.resolve(this.response);
            }
            if (corUserResult['records'].length == 0) {
                this.makeResultJson(this.metaDbConfig.FAILURE, "Meta data user detail not available", { "fetchError": this.metaDbConfig.fetchError });
                return false
            }

            const userInfo = JSON.parse(JSON.stringify(corUserResult['records']))
            //    const userLoginInfoDetails = userInfo[0].CORLOGINDETAILSs;
            this.setLoggedUserName(corUserResult)
            let userDetail = userInfo[0];
            //   this.corUserObjectData = userDetail;

            if (!(userDetail.is_active == null || userDetail.is_active == 'Y')) {
                this.makeResultJson(this.metaDbConfig.FAILURE, this.metaDbConfig.userInActive);
                return false;

            } else if (!(userDetail.is_locked == null || userDetail.is_locked == 'N')) {
                this.makeResultJson(this.metaDbConfig.FAILURE, this.metaDbConfig.userLocked);
                return false

            }
            else {
                this.makeResultJson(this.metaDbConfig.SUCCESS, "");
                return true;
            }
        }).catch(err => {
            this.makeResultJson(this.metaDbConfig.FAILURE, "Meta data user fetch failure", { "fetchError": this.metaDbConfig.fetchError });
            return Promise.resolve(false)
        });

    }
    appBaseValidation() {
        const currentAppId = this.appConfig.configuration.appId;
        const options = {};
        const selector = {};
        selector['data.type'] = this.metaDbConfig.corApplications
        selector['data.application_id'] = currentAppId
        selector['data.is_active'] = "Y"
        options['selector'] = selector
        this.corApplicationHierarchyJSON['options'] = options

        return this.metaDbProvider.fetchDataWithReference(this.corApplicationHierarchyJSON).then(result => {
            if (result && result.status == 'SUCCESS') {
                console.log(JSON.stringify(result));
                if (result.records.length == 0) {
                    this.makeResultJson(this.metaDbConfig.FAILURE, this.metaDbConfig.applicationInActive);
                    return Promise.resolve(false);
                }

                else {
                    this.makeResultJson(this.metaDbConfig.SUCCESS, "");
                    return Promise.resolve(true);
                }
            }
            else {
                this.makeResultJson(this.metaDbConfig.FAILURE, 'Meta cor application info fetching failure', { "fetchError": this.metaDbConfig.fetchError });
                return Promise.resolve(false);
            }

        }).catch(err => {
            this.makeResultJson(this.metaDbConfig.FAILURE, 'Meta cor application info fetching failure', { "fetchError": this.metaDbConfig.fetchError });
            return Promise.resolve(false);
        });
    }
    async appAssignmentValidation() {
        const options = {};
        const selector = {}
        var assignedAppsDetail = await this.getAssighedAppDetail()
        selector['data.type'] = this.metaDbConfig.applicationAssignmentObject;
        selector['data.is_active'] = true;
        selector['data.user_id_p'] = Number(this.apputilityObject.userId);
        options['selector'] = selector;
        this.aplicationAssignmentObjectHierarchyJSON['options'] = options;
        console.log("appAssignmentValidation.......", JSON.stringify(options));

        return this.metaDbProvider.fetchDataWithReference(this.aplicationAssignmentObjectHierarchyJSON).then(appAssignmentFetchResult => {

            return this.isAssignedAppsAreChanged(assignedAppsDetail, appAssignmentFetchResult)

        }).catch(err => {
            this.makeResultJson(this.metaDbConfig.FAILURE, 'Meta data Application assignment fetching failure', { "fetchError": this.metaDbConfig.fetchError });
            return Promise.resolve(false)
        });

    }

    private getAssighedAppDetail() {
        let assignedAppsDetail
        if (this.apputilityObject.isMobile) {
            return this.appPreferences.fetch('login_response').then((response) => {
                return JSON.parse(response);
            })
        }
        else {
            assignedAppsDetail = JSON.parse(localStorage['assignedApps'])
            return assignedAppsDetail;
        }

    }
    private setLoggedUserName(result) {
        try {
            this.apputilityObject.loggedUserName = result['records'][0]['username']
        } catch (err) {
            console.log(err);
        }
    }

    fetchAndInsertNewAppRecords(assignedAppsDetail, isIntialSync) {


        const assignedAppIdListInLocal = [];
        assignedAppsDetail.map(obj => assignedAppIdListInLocal.push(obj.appId));
        const options = {};
        const selector = {}
        selector['data.type'] = this.metaDbConfig.applicationAssignmentObject;
        selector['data.is_active'] = true;
        selector['data.user_id_p'] = Number(this.apputilityObject.userId);

        options['selector'] = selector;
        this.aplicationAssignmentObjectHierarchyJSON['options'] = options;
        return this.metaDbProvider.fetchDataWithReference(this.aplicationAssignmentObjectHierarchyJSON).then(assignedAppResult => {


            if (assignedAppResult && assignedAppResult.status == ! 'SUCCESS') {
                this.makeResultJson(this.metaDbConfig.FAILURE, "Application bulk insert failed", { "fetchError": this.metaDbConfig.fetchError });
                return Promise.resolve(false)
            }
            const assignedAppIdListInServer = assignedAppResult.records.map(obj => obj.application_id_s);
            var newAssignedAppIdList = [];
            // if (isIntialSync) {

            //     newAssignedAppIdList = this.localAppIdListToBulkInsert;
            // }
            //       else{
            for (var j = 0; j < assignedAppIdListInServer.length; j++) {
                if (assignedAppIdListInLocal.indexOf(assignedAppIdListInServer[j]) == -1) {
                    newAssignedAppIdList.push(assignedAppIdListInServer[j]);
                }
            }
            //      }



            if (newAssignedAppIdList.length == 0) {
                return Promise.resolve(true)
            }

            var selectorList = this.metaDbProvider.makeSelectorForMetaDataSyncArray(newAssignedAppIdList, false);
            return this.metaDbProvider.fetchMetaDataObjects(selectorList).then(response => {
                if (response) {
                    var appIdList = newAssignedAppIdList.concat(assignedAppIdListInLocal)
                    var selectorList = this.metaDbProvider.makeSelectorForMetaDataSyncArray(appIdList, true);
                    if (isIntialSync)
                        this.response["metaLiveSyncSelector"] = selectorList;
                    if (!isIntialSync)
                        this.stopAndStartMetaliveListenerAndModifySelector(selectorList)
                    return Promise.resolve(true)
                }
                else {
                    this.makeResultJson(this.metaDbConfig.FAILURE, "Application bulk insert failed", { "fetchError": this.metaDbConfig.fetchError });
                    return Promise.resolve(false)
                }

            }).catch(err => {
                this.makeResultJson(this.metaDbConfig.FAILURE, "Application bulk insert failed", { "fetchError": this.metaDbConfig.fetchError });
                return Promise.resolve(false)
            });




        }).catch(err => {
            this.makeResultJson(this.metaDbConfig.FAILURE, "Application bulk insert failed", { "fetchError": this.metaDbConfig.fetchError });
            return false;
        });



    }






    /*****************************************
Validate the assigned app ,unassigned app info
***********************************************/

    isAssignedAppsAreChanged(assignedAppsDetail, fetchedResult) {
        //   console.log("fetchedResult.......", JSON.stringify(fetchedResult));
        var assignedAppIdsListInLocal = [];
        assignedAppsDetail.map(obj => assignedAppIdsListInLocal.push(obj.appId));
        if (fetchedResult.status != 'SUCCESS') {


            this.makeResultJson(this.metaDbConfig.FAILURE, 'Application info fetching failure');
            return Promise.resolve(false);

        } else if (fetchedResult.records.length == 0) {

            this.makeResultJson(this.metaDbConfig.FAILURE, this.metaDbConfig.currentlyNoAppsAreAssigned);
            return Promise.resolve(false);
        }

        const assignedAppIdListInServer = fetchedResult.records.map(obj => obj.application_id_s);

        const currentAppId = this.appConfig.configuration.appId;
        if (assignedAppIdListInServer.indexOf(currentAppId) == -1) {
            this.makeResultJson(this.metaDbConfig.FAILURE, this.metaDbConfig.appUnassigned);

            return Promise.resolve(false);
        }



        var newAssignedAppIdList = [];
        for (var j = 0; j < assignedAppIdListInServer.length; j++) {
            if (assignedAppIdsListInLocal.indexOf(assignedAppIdListInServer[j]) == -1) {
                newAssignedAppIdList.push(assignedAppIdListInServer[j]);
            }
        }


        if (newAssignedAppIdList.length > 0) {


            return this.newAppsAreAssigned(newAssignedAppIdList).then(result => {
                this.makeResultJson(this.metaDbConfig.SUCCESS, '');
                return Promise.resolve(true);
            })
        }
        else {
            this.makeResultJson(this.metaDbConfig.SUCCESS, '');
            return Promise.resolve(true);
        }



    }

    stopAndStartMetaliveListenerAndModifySelector(finalSelector) {

        //   var finalSelector = this.metaDbProvider.makeSelectorForMetaDataSyncArray(appIdList)
        this.metaDbProvider.cancelLivereplicationFromServerWithSelector()
        this.metaDbProvider.liveReplicationFromServerWithSelector({ "$or": finalSelector });

    }

    async newAppsAreAssigned(newAssignedAppIdList) {
        var taskList = [];

        for (var j = 0; j < newAssignedAppIdList.length; j++) {
            let value = await this.checkCorMobileAppIsPublishedStatus(newAssignedAppIdList[j]);
            if (value) {
                taskList.push(newAssignedAppIdList[j])
                console.log("newAppsAreAssigned..", taskList)
            }
        }
        return Promise.all(taskList).then(res => {
            if (taskList.length > 0) {
                return this.getApplicationName(taskList, 0).then(response => {
                    if (response) {
                        return true;
                    }
                    else {
                        return false;
                    }
                })
            }
            else {
                return true;
            }

        })
    }
    private getApplicationName(newAssignedAppIdList, indexPosition) {

        const options = {};
        const selector = {};
        selector['data.type'] = this.metaDbConfig.corApplications
        selector['data.application_id'] = newAssignedAppIdList[indexPosition]
        options['selector'] = selector
        this.corApplicationHierarchyJSON['options'] = options

        return this.metaDbProvider.fetchDataWithReference(this.corApplicationHierarchyJSON).then(result => {
            if (result && result.status == 'SUCCESS') {
                if (result.records.length > 0) {
                    // this.newAppNameList.push(result.records[0].display_name)
                    var data = this.makeAnnoucementData(result.records[0].display_name, "", "", newAssignedAppIdList[indexPosition], "newApp")
                    if (this.warningType.indexOf("newApp") == -1)
                        this.warningType.push("newApp");
                    this.warningMessageList.push(data)

                    //   this.newAppNameList[newAssignedAppIdList[indexPosition]] = result.records[0].display_name;
                }
                if (newAssignedAppIdList.length - 1 != indexPosition) {
                    indexPosition = indexPosition + 1;
                    return this.getApplicationName(newAssignedAppIdList, indexPosition);
                } else {

                    //    this.makeNewAppAssignedDisplayMessage()

                    this.makeResultJson(this.metaDbConfig.SUCCESS, '');
                    return Promise.resolve(true);
                }

            }
            else {

                this.makeResultJson(this.metaDbConfig.FAILURE, 'Meta data application info fetching failure', { "fetchError": this.metaDbConfig.fetchError });
                return Promise.resolve(false)

            }

        }).catch(err => {
            this.makeResultJson(this.metaDbConfig.FAILURE, 'Meta cor application info fetching failure', { "fetchError": this.metaDbConfig.fetchError });
            return Promise.resolve(false);
        });
    }


    /*****************************************
    Validate the new app version,force update details
    ***********************************************/

    private checkAppNewVersionAvailable(assignedAppsDetail, isIntialSync) {
        const currentAppId = this.appConfig.configuration.appId;
        return this.checkCorMobileAppIsPublishedStatus(currentAppId).then(async response => {
            if (response) {
                return this.getAvailableVersions(assignedAppsDetail).then(result => {
                    if (result)
                        return Promise.resolve(true);
                    else
                        return Promise.resolve(false)

                })
            }
            else {
                this.makeResultJson(this.metaDbConfig.SUCCESS, '');
                return Promise.resolve(true);
            }
        })
    }

    private getAvailableVersions(assignedAppsDetailList) {
        const currentAppId = this.appConfig.configuration.appId;
        let appObj = assignedAppsDetailList.filter(app => app.appId == currentAppId);
        const options = {};
        const selector = {};
        selector['data.type'] = this.metaDbConfig.applicationPublishInfoObject
        selector['data.application_id'] = currentAppId
        options['selector'] = selector
        this.applicationPublishInfoObjectHierarchyJSON['options'] = options

        return this.metaDbProvider.fetchDataWithReference(this.applicationPublishInfoObjectHierarchyJSON).then(result => {
            var latestVersion;
            if (result && result.status == 'SUCCESS') {

                if (result.records.length == 0) {
                    return Promise.resolve(true);
                }
                if (result.records.length == 1) {
                    // this.appNameList.push(result.records[0].version_number);
                    latestVersion = result.records[0].version_number
                }
                else {
                    latestVersion = this.getLatestVersion(result.records)
                }

                for (let i = 0; i < result.records.length; i++) {
                    if (result.records[i].force_update == 'Y' && result.records[i]['version_number'] > Number(appObj[0]['versionNumber'])) {
                        this.makeResultJson(this.metaDbConfig.FAILURE, this.metaDbConfig.newVersionWithForceUpdate
                            + " V " + result.records[i]['version_number'] + " " + appObj[0]['appDisplayName']);
                        return false
                    }
                }

                if (latestVersion > Number(appObj[0]['versionNumber'])) {
                    var data = this.makeAnnoucementData(appObj[0]['appDisplayName'], appObj[0]['versionNumber'], latestVersion, appObj[0]['appId'], "newVersion")
                    if (this.warningType.indexOf("newVersion") == -1)
                        this.warningType.push("newVersion");
                    this.warningMessageList.push(data)
                }


                this.makeResultJson(this.metaDbConfig.SUCCESS, '');
                return Promise.resolve(true);









            }
            else {
                this.makeResultJson(this.metaDbConfig.FAILURE, 'Application  publish info fetching failure', { "fetchError": this.metaDbConfig.fetchError });
                return false
            }

        }).catch(err => {
            this.makeResultJson(this.metaDbConfig.FAILURE, 'Application  publish info fetching failure', { "fetchError": this.metaDbConfig.fetchError });
            return false
        });

    }

    getLatestVersion(newVersionAppDataList) {
        var maxVersionObject = newVersionAppDataList.reduce((prev, current) => (prev.version_number > current.version_number) ? prev : current)
        //    this.appNameList.push(maxVersionObject.version_number);
        return maxVersionObject.version_number
        // this.NewVersionAppDataList.push(maxVersionObject)
    }
    menuGroupValidation(assignedAppsDetailList) {

        let assignedMenuGroupListInLocal = [];
        var currentAppId = this.appConfig.configuration.appId;

        return this.checkCorMobileAppIsPublishedStatus(currentAppId).then(result => {
            if (result) {
                assignedAppsDetailList.forEach(app => {
                    if (app['appId'] == currentAppId) {
                        assignedMenuGroupListInLocal = app["assignedMenuGroups"];
                    }
                });
                return this.isMenuGroupActive(0, assignedMenuGroupListInLocal).then(result => {
                    if (result) {
                        this.makeResultJson(this.metaDbConfig.SUCCESS, '');
                        return Promise.resolve(true);

                    }
                    else {
                        // this.makeResultJson(this.metaDbConfig.FAILURE, this.metaDbConfig.menuGroupUnAssigned);
                        return Promise.resolve(false);
                    }
                })

            }
            else {
                this.makeResultJson(this.metaDbConfig.SUCCESS, '');
                return Promise.resolve(true);
            }
        }).catch(error => {
            this.makeResultJson(this.metaDbConfig.FAILURE, "Meta data menugroup fetching failure", { "fetchError": this.metaDbConfig.fetchError });
            return false
        })

    }


    isMenuGroupActive(indexPosition, assignedMenuGroupListInLocal) {
        var currentAppId = this.appConfig.configuration.appId;
        let options = {}
        let selector = {}
        selector['data.type'] = this.metaDbConfig.corMenuGroup
        selector['data.application_id'] = currentAppId
        selector['data.menu_group_id'] = assignedMenuGroupListInLocal[indexPosition]
        selector['data.is_active'] = "Y"
        options['selector'] = selector
        this.corMenuGroupObjectHierarchyJSON['options'] = options

        return this.metaDbProvider.fetchDataWithReference(this.corMenuGroupObjectHierarchyJSON).then(result => {
            if (result && result.status == 'SUCCESS') {
                if (result.records.length == 0) {

                    this.makeResultJson(this.metaDbConfig.FAILURE, this.metaDbConfig.menuGroupUnAssigned);
                    return Promise.resolve(false);
                }

                if (assignedMenuGroupListInLocal.length - 1 != indexPosition) {
                    indexPosition = indexPosition + 1;
                    return this.isMenuGroupActive(indexPosition, assignedMenuGroupListInLocal);
                }
                else {
                    this.makeResultJson(this.metaDbConfig.SUCCESS, '');
                    return Promise.resolve(true);
                }

            }
            else {
                this.makeResultJson(this.metaDbConfig.FAILURE, "Meta data menugroup fetching failure", { "fetchError": this.metaDbConfig.fetchError });
                return false
            }
        }).catch(error => {
            this.makeResultJson(this.metaDbConfig.FAILURE, "Meta data menugroup fetching failure", { "fetchError": this.metaDbConfig.fetchError });
            return false
        })

    }




    checkCorMobileAppIsPublishedStatus(appId) {
        const options = {};
        const selector = {};
        selector['data.type'] = this.metaDbConfig.corMobileApps
        selector['data.application_id'] = appId


        options['selector'] = selector
        this.corMobileAppsHierarchyJSON['options'] = options

        return this.metaDbProvider.fetchDataWithReference(this.corMobileAppsHierarchyJSON).then(result => {
            if (result && result.status == 'SUCCESS') {
                if (result.records.length == 0) {
                    //  this.makeResultJson(this.metaDbConfig.FAILURE, 'Application inActive');
                    return false;
                }
                var mobileAppsData = result.records[0]

                if (mobileAppsData.status !== 'PUB') {
                    return false;
                }

                var supportedDevieTypes = result.records[0]['device_type'].split("$$");
                if (!this.apputilityObject.isMobile
                    && supportedDevieTypes.indexOf('BROWSER') !== -1) {
                    return true;
                }
                else if (this.apputilityObject.isMobile
                    && supportedDevieTypes.indexOf(this.apputilityObject.osType.toUpperCase()) !== -1) {
                    return true;
                }
                else {
                    return false;
                }

            }

        }).catch(err => {
            this.makeResultJson(this.metaDbConfig.FAILURE, 'Meta mobileApp info fetching failure', { "fetchError": this.metaDbConfig.fetchError });
            return false;
        });
    }
    checkCurrentAppIsActive() {
        const currentAppId = this.appConfig.configuration.appId;
        const options = {};
        const selector = {};
        selector['data.type'] = this.metaDbConfig.corApplications
        selector['data.application_id'] = currentAppId
        selector['data.is_active'] = "Y"
        options['selector'] = selector
        this.corApplicationHierarchyJSON['options'] = options

        return this.metaDbProvider.fetchDataWithReference(this.corApplicationHierarchyJSON).then(result => {
            if (result && result.status == 'SUCCESS') {
                console.log(JSON.stringify(result));
                if (result.records.length == 0) {
                    this.makeResultJson(this.metaDbConfig.FAILURE, this.metaDbConfig.applicationInActive);
                    return Promise.resolve(false);
                }

                else {
                    return Promise.resolve(true);
                }
            }

        }).catch(err => {
            this.makeResultJson(this.metaDbConfig.FAILURE, 'Meta cor application info fetching failure', { "fetchError": this.metaDbConfig.fetchError });
            return Promise.resolve(false);
        });
    }


    makeAnnoucementData(appName, oldVersion, newVersion, appId, annoucementType) {
        var annoucemnetDataObject = {}
        annoucemnetDataObject['appName'] = appName;
        annoucemnetDataObject['oldVersion'] = oldVersion;
        annoucemnetDataObject['newVersion'] = newVersion;
        annoucemnetDataObject['appId'] = appId;
        annoucemnetDataObject['annoucementType'] = annoucementType;
        return annoucemnetDataObject;
    }





}