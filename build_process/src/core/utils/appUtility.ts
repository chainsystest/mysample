import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import { dbConfiguration } from '../db/dbConfiguration';
import { appConfiguration } from './appConfiguration';
import { ToastController, PopoverController, NavController } from '@ionic/angular';
import * as moment from 'moment';
import 'moment-timezone';
import { popoverpage } from '../pages/popoverpage/popoverpage';
import { DatePipe } from '@angular/common';
import { themeChanger } from "../../theme/themeChanger";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { sessionValidator } from 'src/authentication/sessionValidator';
import { metaDataDbProvider } from '../db/metaDataDbProvider';
import { metaDbConfiguration } from '../db/metaDbConfiguration';

@Injectable({
    providedIn: 'root'
})
export class appUtility {

    private _isMobile: Boolean = true;
    private _enableMenu: Boolean = true;
    private _osType: String = 'android';
    private _userId: number;
    private _roleId: Number;
    private _accessToken: String;
    private _orgId: Number;
    // private _versionNumber: Number;
    private platformJsonPath = './platform.json';
    private _sessionId: String;
    private _appBuilderURL: String;
    private _userTimeZone = '';
    private _userDateFormat = '';
    public hoursFormat = 'hh:mm a'; // hour format
    public userZoneOffsetValue = ''; // user timezone offset value. For eg: +0530, -0400
    public userDateTimeFormat = '';  // user profile datetime format
    public userZoneOffsetValueWithFormat = ''; // user timezone offset value. For eg: +05:30, -04:00
    public userDatePickerFormat = ''; // Date picker format of user profile with uppercase
    public userDateTimePickerFormat = ''; // DateTime picker format of user profile
    public metaDataDbProvider: metaDataDbProvider
    public isAppSyncCompleted: Boolean = false;
    public applicationName = '';
    private _isMobileResolution: Boolean = true;

    // Will be set in Meta Validation class
    public loggedUserName = "";
    private homePageNodeFlag = true;
    homePageNode = {
        "homePageNodeName": "",
        "homePageNodepath": ""
    }
    public eventSubscriptionObject = {}
    constructor(public http: Http, public appPreferences: AppPreferences, private sessionvalidator: sessionValidator,
        public popoverController: PopoverController, private dbConfiguration: dbConfiguration, private appConfig: appConfiguration,
        public toastCtrl: ToastController, public datePipe: DatePipe, public theme: themeChanger, public statusBar: StatusBar, public metaDbConfig: metaDbConfiguration, public navCtrl: NavController) {

    }
    // getter setter for user timezone
    get userTimeZone(): string {
        return this._userTimeZone;
    }
    set userTimeZone(userTimezone: string) {
        this._userTimeZone = userTimezone;
    }

    // getter setter for user dateformat
    get userDateFormat(): string {
        return this._userDateFormat;
    }
    set userDateFormat(userDateformat: string) {
        this._userDateFormat = userDateformat;
    }

    // getter setter for isMobile flag
    get enableMenu(): Boolean {
        return this._enableMenu;
    }
    set enableMenu(enableMenu: Boolean) {
        this._enableMenu = enableMenu;
    }

    // getter setter for isMobile flag
    get isMobile(): Boolean {
        return this._isMobile;
    }
    set isMobile(ismobile: Boolean) {
        this._isMobile = ismobile;
    }

    get osType(): String {
        return this._osType;
    }
    set osType(value: String) {
        this._osType = value;
    }
    // getter setter for userId flag
    get userId(): number {
        return this._userId;
    }
    set userId(userid: number) {
        this._userId = userid;
    }

    // getter setter for roleId flag
    get roleId(): Number {
        return this._roleId;
    }
    set roleId(roleId: Number) {
        this._roleId = roleId;
    }
    get accessToken(): String {
        return this._accessToken;
    }
    set accessToken(accessToken: String) {
        this._accessToken = accessToken;
    }
    // // getter setter for version number
    // get versionNumber(): Number {
    //     return this._versionNumber;
    // }
    // set versionNumber(versionNumber: Number) {
    //     this._versionNumber = versionNumber;
    // }

    // getter setter for userId flag
    get orgId(): Number {
        return this._orgId;
    }
    set orgId(orgid: Number) {
        this._orgId = orgid;
    }

    get sessionId(): String {
        return this._sessionId;
    }
    set sessionId(sessionId: String) {
        this._sessionId = sessionId;
    }
    get appBuilderURL(): String {
        return this._appBuilderURL;
    }
    set appBuilderURL(couchUrl: String) {
        this._appBuilderURL = couchUrl;
    }
    get isMobileResolution(): Boolean {
        return this._isMobileResolution;
    }
    set isMobileResolution(isMobileResolution: Boolean) {
        this._isMobileResolution = isMobileResolution;
    }
    // Date conversion
    displayDateConversionForList(dateFields, dataArray) {
        const docs = dataArray.map((row) => {
            return this.displayDateConversionForSingleObject(dateFields, row);
        });
        return docs;
    }
    dbDateConversionForSingleObject(dateFields, object) {
        dateFields.forEach(element => {
            object[element] = object[element].substring(0, 4) + object[element].substring(5, 7) + object[element].substring(8, 10);
        });
        return object;
    }
    displayDateConversionForSingleObject(dateFields, object) {
        dateFields.forEach(element => {
            object[element] = object[element].substring(0, 4) + '-' + object[element].substring(4, 6) + '-' + object[element].substring(6, 8);
        });
        return object;
    }

    // isMobile checking using platform.json file
            initialSetup() {
            this.isMobile = false;
            return this.fetchUserIdAndOrgIdFromPreference().then(res => {
                return Promise.resolve(res);  })
            }
    /*initialSetup() {
        return this.http.get(this.platformJsonPath)
            .toPromise()
            .then((response) => {
                const resValue = response.json();
                if (resValue['platform'] === 'web') {
                    this.isMobile = false;
                } else {
                    this.isMobile = true;
                    this.osType = resValue['os_type'];
                }
                this.getDeviceResolution();
                return this.fetchUserIdAndOrgIdFromPreference().then(res => {
                    return Promise.resolve(res);
                });
            }).catch((err) => {
                console.log(err);
                return Promise.resolve('Invalid session');
            });
    }

    // isMobile checking using platform.json file
    // initialSetup() {
    //     // return this.http.get(this.platformJsonPath)
    //     //     .toPromise()
    //     //     .then((response) => {
    //     //         const resValue = response.json();
    //     //         if (resValue['platform'] === 'web') {
    //     //             this.isMobile = false;
    //     //         } else {
    //     //             this.isMobile = true;
    //     //             this.osType = resValue['os_type'];
    //     //         }
    //     //         this.getDeviceResolution();
    //     //         return this.fetchUserIdAndOrgIdFromPreference().then(res => {
    //     //             return Promise.resolve(res);
    //     //         });
    //     //     }).catch((err) => {
    //     //         //console.log(err);
    //     //         return Promise.resolve('Invalid session');
    //     //     });

    //     this.isMobile = false;
    //     this.getDeviceResolution();
    //     return this.fetchUserIdAndOrgIdFromPreference().then(res => {
    //         return Promise.resolve(res);
    //     });
    // }
    */ //isMobileResolution checking for list refresh for web using change listener
    getDeviceResolution() {
        const mediaQuery = window.matchMedia("(max-device-width: 480px)");
        if (mediaQuery.matches) {
            this.isMobileResolution = true
        } else {
            this.isMobileResolution = false
        }
    }

    fetchSelectedTheme() {
        if (this.isMobile) {
            this.appPreferences.fetch('selectedTheme').then((res) => {
                console.log("selectedTheme fetch = ", res);
                console.log("selectedTheme fetch JSON = ", JSON.stringify(res));
                if (res) {
                    const theme = JSON.parse(res)
                    const primary = theme['primary'];
                    this.statusBar.backgroundColorByHexString(primary);
                    this.theme.changeTheme(res);
                } else {
                    const defaultTheme = { "primary": "#ffdd67", "secondary": "#4d4d4d", "secondarylow": "#fde79a", "barbg": "#fbf8ec" };
                    this.statusBar.backgroundColorByHexString('#dabc56');
                    this.theme.changeTheme(JSON.stringify(defaultTheme));
                    this.appPreferences.store('selectedTheme', JSON.stringify(defaultTheme)).then(res => {
                        console.log("selectedTheme success =", res);
                    })
                }

            });
        } else {
            const selectedTheme = localStorage.getItem('selectedTheme');
            if (selectedTheme) {
                this.theme.changeTheme(selectedTheme);
            } else {
                const defaultTheme = { "primary": "#ffdd67", "secondary": "#4d4d4d", "secondarylow": "#fde79a", "barbg": "#fbf8ec" };
                this.theme.changeTheme(JSON.stringify(defaultTheme));
                localStorage.setItem('selectedTheme', JSON.stringify(defaultTheme))
            }
        }
    }

    fetchUserIdAndOrgIdFromPreference() {
        if (this.isMobile) {
            const taskList = [];
            taskList.push(this.appPreferences.fetch('userId').then((res) => {
                return res;
            }));

            taskList.push(this.appPreferences.fetch('orgId').then((res) => {
                return res;
            }));

            taskList.push(this.appPreferences.fetch('SessionId').then((res) => {
                return res;
            }));

            taskList.push(this.appPreferences.fetch('appBuilderURL').then((res) => {
                return res;
            }));

            taskList.push(this.appPreferences.fetch('userTimeZone').then((res) => {
                return res;
            }));

            taskList.push(this.appPreferences.fetch('userDateFormat').then((res) => {
                return res;
            }));

            taskList.push(this.appPreferences.fetch('dbname').then((res) => {
                return res;
            }));
            taskList.push(this.appPreferences.fetch('proxy_pass').then((res) => {
                return res;
            }));

            taskList.push(this.appPreferences.fetch('user_name').then((res) => {
                return res;
            }));

            taskList.push(this.appPreferences.fetch('password').then((res) => {
                return res;
            }));

            taskList.push(this.appPreferences.fetch('roleId').then((res) => {
                return res;
            }));
            taskList.push(this.appPreferences.fetch('accessToken').then((res) => {
                return res;
            }));
            // taskList.push(this.appPreferences.fetch('versionNumber').then((res) => {
            //     return res;
            // }));
            return Promise.all(taskList).then(res => {
                this.userId = res[0];
                this.orgId = res[1];
                this.sessionId = res[2];
                this.appBuilderURL = res[3];
                this.userTimeZone = res[4]; // assigning user profile timezone
                this.userDateFormat = res[5]; // assigning user profile date format
                this.dbConfiguration.databaseName = res[6];
                this.dbConfiguration.remoteDbUrl = res[7];
                this.dbConfiguration.username = res[8];
                this.dbConfiguration.password = res[9];
                this.roleId = res[10];
                this.accessToken = res[11];
                // this.versionNumber = res[11];
                this.makeDateTimePickerFormatAndZoneValues();
                return Promise.resolve('Success');
            });
        } else {
            this.userId = JSON.parse(localStorage['userId']);
            this.roleId = JSON.parse(localStorage['roleId']);
            this.orgId = JSON.parse(localStorage['orgId']);
            // this.versionNumber = JSON.parse(localStorage['versionNumber']);
            this.appBuilderURL = localStorage['appBuilderURL'];
            this.sessionId = localStorage['sessionId'];

            this.userTimeZone = localStorage['userTimeZone'];
            this.userDateFormat = localStorage['userDateFormat'];
            this.makeDateTimePickerFormatAndZoneValues();

            if (localStorage['sessionId']) {
                return Promise.resolve(this.getAdditionalInfo(this.orgId, this.userId, this.sessionId));
            } else {
                // Note : If sessionid is there for all the platform(CSPFM,JWT). Need to remove this else part and if condition.
                this.dbConfiguration.databaseName = this.dbConfiguration.configuration.databaseName;
                this.dbConfiguration.remoteDbUrl = this.dbConfiguration.configuration.remoteDbUrl;
                this.dbConfiguration.username = this.dbConfiguration.configuration.user.name;
                this.dbConfiguration.password = this.dbConfiguration.configuration.user.password;
                return Promise.resolve(this.checkAppId(this.isMobile));
            }
        }
    }

    makeDateTimePickerFormatAndZoneValues() {
        this.userDateTimeFormat = this.userDateFormat + ' ' + this.hoursFormat; // assigning user profile date time format
        this.userDatePickerFormat = this.userDateFormat.toUpperCase(); // assigning user profile date format with uppercase for date picker
        this.userDateTimePickerFormat = this.userDatePickerFormat + ' ' +
            this.hoursFormat; // assigning user profile date time format for date time picker

        this.convertUserProfileOffsetToZoneNumber(); // Method calling to get user timezone offset value. For eg: +0530, -0400
        this.convertUserProfileOffsetToZoneNumberWithFormat(); // Method calling to get user timezone offset value. For eg: +05:30, -04:00
    }

    validateSailsSession() {
        return this.sessionvalidator.validateSessionWithOrgidandUserId().then(res => {
            if (res['status'] === 'success') {
                localStorage.setItem('sessionId', res['sessionId']);
                localStorage.setItem('appBuilderURL', res['appBuilderURL']);
                localStorage.setItem('roleId', res['userInfo']['personalInfo']['roleId']);
                localStorage.setItem('orgId', JSON.stringify(res['orgId']));
                localStorage.setItem('userId', JSON.stringify(res['userId']));
                localStorage.setItem('assignedApps', JSON.stringify(res['assignedApps']));
                localStorage.setItem('userTimeZone', res['userInfo']['profileInfo']['timeZoneId']);
                localStorage.setItem('userDateFormat', res['userInfo']['profileInfo']['dateFormat']);
                return Promise.resolve('SUCCESS');
            } else {
                return Promise.resolve(res['message']);
            }
        }).catch(error => {
            console.log('An error occurred', error);
            return Promise.resolve("Failure");
        });
    }

    getAdditionalInfo(orgId, userId, sessionId) {
        const postData = {
            "inputparams": {
                "userId": userId,
                "orgId": orgId,
                "appname": "appM",
                "processInfo": [{
                    "isCouchInfoRequired": "Y",
                    "isPushNotificationInfoRequired": "N"
                }]
            }
        }

        if (this.isMobile) {
            postData.inputparams['sessionToken'] = this.accessToken
            postData.inputparams['sessionType'] = "OAUTH"
        } else {
            postData.inputparams['sessionToken'] = sessionId
            postData.inputparams['sessionType'] = "NODEJS"
        }

        const serviceURl = '/apps/additionalInfoService';
        return this.http
            .post(serviceURl, JSON.stringify(postData))
            .toPromise().then(res => {
                const response = JSON.parse(res['_body']);
                if (response['status'] === 'SUCCESS') {
                    const couchInfo = response['couchInfo'];
                    this.dbConfiguration.databaseName = couchInfo['dbname'];
                    this.dbConfiguration.password = couchInfo['password'];
                    this.dbConfiguration.remoteDbUrl = couchInfo['proxy_pass'] + '/';
                    this.dbConfiguration.username = couchInfo['user_name'];
                    return Promise.resolve(this.checkAppId(this.isMobile));
                } else {
                    if (response === "Session Expired") {
                        this.presentToast("Session Expired");
                        setTimeout(() => {
                            window.location.replace('/apps');
                        }, 1500);
                        return Promise.resolve(response);
                    } else {
                        return Promise.resolve(response);
                    }
                }


            })
            .catch(error => {
                console.log('An error occurred', error);
                return Promise.resolve(error.message || 'Server connection failed');
            });
    }

    convertUserProfileOffsetToZoneNumber() {
        const offsetValue = moment.tz(this._userTimeZone).utcOffset();
        let zoneString = '';
        let positiveNegativeValue = '';

        if (offsetValue >= 0) {
            positiveNegativeValue = '+';
        } else {
            positiveNegativeValue = '-';
        }

        const zoneOffset = Math.abs(offsetValue) / 60;

        const zoneValue = zoneOffset.toString();
        if (zoneValue.length === 1) {
            zoneString = positiveNegativeValue + '0' + zoneValue + '00';
        } else if (zoneValue.length === 2) {
            zoneString = positiveNegativeValue + zoneValue + '00';
        } else {
            const zoneValueSplitArray = zoneValue.split('.');
            const firstIndex = zoneValueSplitArray[0];
            const secondIndex = zoneValueSplitArray[1];

            const percentToHour = (Number(secondIndex) * 60 / 100).toString();

            let hourValue = '';
            if (percentToHour.length === 1) {
                hourValue = percentToHour + '0';
            } else {
                hourValue = percentToHour;
            }

            if (firstIndex.length === 1) {
                zoneString = positiveNegativeValue + '0' + firstIndex + hourValue;
            } else {
                zoneString = positiveNegativeValue + firstIndex + hourValue;
            }
        }
        this.userZoneOffsetValue = zoneString;
    }

    convertUserProfileOffsetToZoneNumberWithFormat() {
        const offsetValue = moment.tz(this._userTimeZone).utcOffset();
        let zoneString = '';
        let positiveNegativeValue = '';

        if (offsetValue >= 0) {
            positiveNegativeValue = '+';
        } else {
            positiveNegativeValue = '-';
        }

        const zoneOffset = Math.abs(offsetValue) / 60;

        const zoneValue = zoneOffset.toString();
        if (zoneValue.length === 1) {
            zoneString = positiveNegativeValue + '0' + zoneValue + ':00';
        } else if (zoneValue.length === 2) {
            zoneString = positiveNegativeValue + zoneValue + ':00';
        } else {
            const zoneValueSplitArray = zoneValue.split('.');
            const firstIndex = zoneValueSplitArray[0];
            const secondIndex = zoneValueSplitArray[1];

            const percentToHour = (Number(secondIndex) * 60 / 100).toString();

            let hourValue = '';
            if (percentToHour.length === 1) {
                hourValue = percentToHour + '0';
            } else {
                hourValue = percentToHour;
            }

            if (firstIndex.length === 1) {
                zoneString = positiveNegativeValue + '0' + firstIndex + ':' + hourValue;
            } else {
                zoneString = positiveNegativeValue + firstIndex + ':' + hourValue;
            }
        }
        this.userZoneOffsetValueWithFormat = zoneString;
    }

    checkAppId(isMobile) {
        if (!isMobile) {
            if (localStorage['assignedApps']) {
                const assignedApps = JSON.parse(localStorage['assignedApps']);

                const assignedAppIds = assignedApps.map(obj => obj.appId);

                if (assignedAppIds.indexOf(this.appConfig.configuration.appId) < 0) {
                    return Promise.resolve('This app not assigned to you!!!');
                } else {
                    return Promise.resolve('Success');
                }

            } else {
                return Promise.resolve('This app not assigned to you!!!');
            }
        } else {
            return Promise.resolve('Success');
        }
    }
    getMultiAndSinglePickerDisplayName(couchValue, jsonValue) {
        const result = [];
        let displayValue: String = '';
        for (const data of couchValue) {
            jsonValue.filter((item) => {
                const value = item[data];
                if (value !== undefined) {
                    result.push(value);
                }
            });
            displayValue = result.toString();
        }
        return displayValue;
    }
    validateWebUrl(weburl) {
        const regexp = new RegExp('^http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$');
        if (!regexp.test(weburl)) {
            this.presentToast('URL is not valid');
        } else {
            window.open(weburl, '_system');
        }
    }
    async presentToast(message) {
        const toast = await this.toastCtrl.create({
            message: message,
            duration: 2000,
            position: 'bottom'
        });
        toast.dismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present();
    }

    async presentPopover(myEvent, items) {
        const popover = await this.popoverController.create({
            component: popoverpage,
            componentProps: { message: 'passed message', popoverController: items },
            event: myEvent,
            translucent: true
        });
        return await popover.present();
    }

    getApplicationName() {
        let appName = '';
        let assignedAppsInfo = JSON.parse(localStorage.getItem('assignedApps'));
        if (assignedAppsInfo.length == 0) {
            return appName;
        }

        assignedAppsInfo.forEach(assignedApp => {
            if (assignedApp['appId'] == this.appConfig.configuration.appId) {
                appName = assignedApp['appName'];
            }
        });

        return appName;
    }

    makeLogObjectWithRequiredValues(logContent, type) {
        if (this._isMobile) {
            return;
        }
        let messageObject = {}
        messageObject['user_id'] = localStorage.getItem('userId');
        messageObject['app_id'] = this.appConfig.configuration.appId;
        messageObject['org_id'] = localStorage.getItem('orgId');
        messageObject['app_name'] = this.getApplicationName();
        messageObject['system_time'] = this.datePipe.transform(new Date(), 'dd-MM-yyyy HH:mm:ss.SSS');
        messageObject['log_content'] = logContent;
        this.writeLogToServer(messageObject, type);
    }

    writeLogToServer(messageObject, type) {
        var url = window.location.href
        var arr = url.split("/");
        var result = arr[0] + "//" + arr[2]
        var path = result.concat('/apps/api/write_log')

        let postParams = { 'type': type, 'message': messageObject };

        this.http.post(path, postParams)
            .subscribe(data => {
                let responseBody = JSON.parse(data['_body']);
                let status = responseBody['status'];
                console.log("Status : " + JSON.stringify(status));
            }, error => {
                console.log("Error : " + JSON.stringify(error));
            });
    }

    setDataRestrictionByRestrictionType(referenceDetail, options, layoutDataRestrictionSet) {

        if (referenceDetail['objectType'].toUpperCase() != 'PRIMARY') {
            return;
        }

        var userIDs = layoutDataRestrictionSet[0]['restrictedDataUserIds'];
        if (userIDs != undefined && userIDs.length != 0) {
            if (userIDs.length > 1) {
                options['selector']['data.createdby'] = {
                    $in: userIDs
                }
            } else {
                options['selector']['data.createdby'] = userIDs[0]
                console.log(options);
            }
        } else {
            console.log("User Id restriction is not set and objectType is : ", referenceDetail['objectType'].toUpperCase());
        }
    }

    setDataRestrictionByUsers(layoutDataRestrictionSet, objectHierarchyJSON) {
        if (layoutDataRestrictionSet.length > 0) {
            layoutDataRestrictionSet.forEach(dataRestriction => {
                if (dataRestriction['restrictionType'] == 'Owner') {//If the layout data restriction level is "Owner"
                    if (objectHierarchyJSON['options']) {
                        if (objectHierarchyJSON['options']['selector']) {
                            objectHierarchyJSON['options']['selector']['data.type'] = "pfm" + objectHierarchyJSON['objectId']
                            objectHierarchyJSON['options']['selector']['data.createdby'] = this.userId
                        }
                    } else {
                        objectHierarchyJSON['options'] = {
                            "selector": {
                                "data.type": "pfm" + objectHierarchyJSON['objectId'],
                                "data.createdby": this.userId
                            }
                        }
                    }
                }
            });
        }
        return objectHierarchyJSON;
    }
    setHomePageNode() {
        if (this.homePageNodeFlag) {
            this.homePageNode['homePageNodeName'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes[0].childNodes[0].textContent
            this.homePageNode['homePageNodepath'] = document.getElementsByTagName('ion-router-outlet')[1].children[0].tagName.toLowerCase()
            this.homePageNodeFlag = false
        }
    }
    getHomePageNode() {
        if (!this.homePageNodeFlag) {                               //this.homePageNodeFlag tells us if the setHomePageNode() is called or not.
            if (this.homePageNode.homePageNodeName === "") {
                this.homePageNode.homePageNodeName = "Home";
                return this.homePageNode
            }
            return this.homePageNode
        }
    }
    navigateToHomepage() {
        this.navCtrl.navigateBack(['menu/'])
    }
    setEventSubscriptionlayoutIds(tableName, layoutId) {
        if (this.eventSubscriptionObject[tableName]) {
            this.eventSubscriptionObject[tableName][layoutId] = "";
        } else {
            this.eventSubscriptionObject[tableName] = {};
            this.eventSubscriptionObject[tableName][layoutId] = "";
        }
    }
    getEventSubscriptionlayoutIds(tableName) {
        if(this.eventSubscriptionObject[tableName]){
            return Object.keys(this.eventSubscriptionObject[tableName])
        }else{
            return
        }
    }
    removeEventSubscriptionlayoutIds(tableName, layoutId) {
        delete this.eventSubscriptionObject[tableName][layoutId]
    }
    checkPageAlreadyInStack(redirectUrl) {
        var stackArray = document.getElementsByTagName('ion-router-outlet')[1].children
        for (let i = 0; i < stackArray.length; i++) {
            if ("/menu/" + stackArray[i].tagName.toLowerCase() == redirectUrl) {
                return true;
            }
        }
        return false;
    }
    innerDependentObjectIdCheck(currentObject, pathArray, type, id) {
        var tempObject = currentObject;
        for (let i = 0; i < pathArray.length; i++) {
            tempObject = this.getObject(tempObject, pathArray[i])
            if (tempObject) {
                if (i == pathArray.length - 1) {
                    return type + "_2_" + tempObject['id'] == id ? true : false
                }
            } else {
                return false
            }
        }
    }
    getObject(object, key) {
        // Chcek for right key
        key = this.getCorrectKey(object, key)
        if (!key) {
            return false
        }
        // Check whether object is JSON or Array and return the inner object
        if (object[key]) {                                //if object is an object
            if (object[key][0]) {
                if (Object.keys(object[key][0]).length > 0) {
                    return object[key][0]
                } else {
                    return false
                }
            } else {
                if (Object.keys(object[key]).length > 0) {
                    return object[key]
                } else {
                    return false
                }
            }
        } else if (object[0]) {                      //if object is an array
            if (object[0][key]) {
                if (object[0][key][0]) {
                    if (Object.keys(object[0][key][0]).length > 0) {
                        return object[0][key][0]
                    } else {
                        return false
                    }
                } else {
                    if (Object.keys(object[0][key]).length > 0) {
                        return object[0][key]
                    } else {
                        return false
                    }
                }
            } else {
                return false
            }
        } else {
            return false
        }
    }
    getCorrectKey(object, key) {
        // Chcek for right key
        if (object[key + "s"]) {
            return key + "s"
        } else if (object[key]) {
            return key
        } else if (object[0]) {
            if (object[0][key + "s"]) {
                return key + "s"
            } if (object[0][key]) {
                return key
            } else {
                return false
            }
        } else {
            return false
        }
    }
}
