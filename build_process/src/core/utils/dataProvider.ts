import { Injectable } from '@angular/core';
import { dbProvider } from '../db/dbProvider';
import { couchdbProvider } from '../db/couchdbProvider';
import { jsondbProvider } from '../db/jsondbProvider';
import { appConstant } from './appConstant';
import { dbConfiguration } from '../db/dbConfiguration';
import { appUtility } from './appUtility';
import { metaDbConfiguration } from '../db/metaDbConfiguration';
import { metaDataDbProvider } from '../db/metaDataDbProvider';

@Injectable({
  providedIn: 'root'
})
// tslint:disable-next-line: class-name
export class dataProvider {

  public keysToSchema = {};

  constructor(private pouchDBProvider: dbProvider, private dbConfigObj: dbConfiguration,
    private couchDBProvider: couchdbProvider, private jsonDataProvider: jsondbProvider, private appUtilityObj: appUtility,
    public metaDbConfig: metaDbConfiguration, public metaDataDbProvider: metaDataDbProvider) {
    this.setSchema();
  }
  tableStructure() {
    return this.dbConfigObj.configuration.tableStructure;
  }

  // Get plural form of table name
  getPluralName(type) {
    const schema = this.keysToSchema[type];
    return schema.plural;
  }
  setSchema() {
    this.dbConfigObj.configuration.schema.forEach(type => {
      this.keysToSchema[type.singular] = type;
    });
  }
  save(type, doc, dataSorce, previousDoc?, fieldTrackAvailable?) {
    if (dataSorce === appConstant.couchDBStaticName) {
      return this.couchDBProvider.save(type, doc);
    } else if (dataSorce === appConstant.pouchDBStaticName) {
      return this.pouchDBProvider.save(type, doc, previousDoc, fieldTrackAvailable);
    } else {
      return this.invalidResponse(appConstant.inValidInput);
    }
  }
  fetchDataFromDataSource(dataFetchingInput) {
    // tslint:disable-next-line: no-shadowed-variable
    if (dataFetchingInput['layoutDataRestrictionSet'] != undefined && dataFetchingInput['layoutDataRestrictionSet'].length > 0) {
      let layoutDataRestrictionSet = dataFetchingInput['layoutDataRestrictionSet'][0]

      if (layoutDataRestrictionSet['restrictionType'].toLowerCase() == 'owner') {
        dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds'] = [this.appUtilityObj.userId]
        return this.fetchDbSelection(dataFetchingInput)

      } else {
        let corUsersObjectHierarchyJSON = {
          "objectId": this.metaDbConfig.corUserHierarchy,
          "objectName": this.metaDbConfig.corUserHierarchy,
          "fieldId": 0,
          "objectType": "PRIMARY",
          "relationShipType": null,
          "childObject": []
        }

        return this.metaDataDbProvider.fetchDataWithReference(corUsersObjectHierarchyJSON).then(res => {

          if (res['status'] == "SUCCESS" && res['records'].length > 0) {
            dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds']
              = this.getHierarchialUserIds(layoutDataRestrictionSet, res['records'][0]);
            return this.fetchDbSelection(dataFetchingInput);

          } else {
            dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds'] = [this.appUtilityObj.userId]
            this.fetchDbSelection(dataFetchingInput);
          }

        }).catch(err => {
          console.log("Data provider error :", err);

          dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds'] = [this.appUtilityObj.userId]
          return this.fetchDbSelection(dataFetchingInput);
        })
      }
    } else {
      return this.fetchDbSelection(dataFetchingInput);
    }
  }

  private fetchDbSelection(dataFetchingInput) {
    const dataProvider = dataFetchingInput['dataSource']

    if (dataProvider) {
      if (dataProvider === appConstant.couchDBStaticName) {
        if(dataFetchingInput["searchListQuery"]){
          let query = dataFetchingInput["searchListQuery"]
          if (dataFetchingInput['layoutDataRestrictionSet'] != undefined
            && dataFetchingInput['layoutDataRestrictionSet'].length > 0
            && dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds']
            && dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds'] > 0) {
            if (dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds'].length == 1) {
              query = query + ' AND ' + "createdby:" + dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds'][0]
            } else {
              query = query + ' AND ' + "createdby:("
                + dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds'].join(' OR ') + ')';
            }
          }
          console.log("Search Query :", query);

          let hierarchyJSON = dataFetchingInput["objectHierarchyJSON"]
          // For online data fetching for table with pagination
          if (dataFetchingInput['pagination']) {
            var limit = 100;
            if (dataFetchingInput['pagination']['limit']) {
              limit = Number(dataFetchingInput['pagination']['limit']);
            }
            var bookmark = '';
            if (dataFetchingInput['pagination']['bookmark']) {
              bookmark = dataFetchingInput['pagination']['bookmark'];
            }

            return this.couchDBProvider.searchRecordsWithPagination(query, hierarchyJSON, { limit: limit, offset: 0, bookmark: bookmark })
          } else {
            // For online data fetching without pagination
            return this.couchDBProvider.fetchRecordsBySearchFilterPhrases(query, hierarchyJSON)
          }
        }
        return this.couchDbDataFetching(dataFetchingInput)
      } else if (dataProvider === appConstant.pouchDBStaticName) {
        return this.pouchDbDataFetching(dataFetchingInput)
      } else if (dataProvider === appConstant.jsonDBStaticName) {
        return this.jsonDbDataFetching(dataFetchingInput)
      }
    } else {
      return this.invalidResponse(appConstant.inValidInput);
    }
  }

  private getHierarchialUserIds(layoutDataRestrictionSet, userHierarchyRecord) {
    var useridArray = [];
    // need to handle Owner , sub, super and created user array

    if (layoutDataRestrictionSet['restrictionType'].toLowerCase() == "subordinate") {
      for (let i = 0; i < layoutDataRestrictionSet['restrictionLevel']; i++) {
        let subordinate_level = "subordinate_level" + (i + 1)
        userHierarchyRecord[subordinate_level].forEach(element => {
          useridArray.push(element)
        });
      }
      useridArray.push(userHierarchyRecord['user_id'])
    } else if (layoutDataRestrictionSet['restrictionType'].toLowerCase() == "manager") {
      for (let i = 0; i < layoutDataRestrictionSet['restrictionLevel']; i++) {
        let manager_level = "manager_level" + (i + 1)
        userHierarchyRecord[manager_level].forEach(element => {
          useridArray.push(element)
        });
      }
      useridArray.push(this.appUtilityObj.userId)
    }
    console.log("final Array : ", useridArray);
    return useridArray;
  }

  couchDbDataFetching(dataFetchingInput) {
    if (navigator.onLine) {
      const objectHierarchyJSON = dataFetchingInput['objectHierarchyJSON'];
      const additionalInfo = dataFetchingInput['additionalInfo'];
      const layoutDataRestrictionSet = dataFetchingInput['layoutDataRestrictionSet']

      if (Object.entries(objectHierarchyJSON).length === 0 && objectHierarchyJSON.constructor === Object) {
        return this.invalidResponse(appConstant.inValidInput);
      } else {
        return this.couchDBProvider.fetchDataWithReference(objectHierarchyJSON, layoutDataRestrictionSet, additionalInfo).then(result => {
          return result
        })
      }
    } else {
      return this.invalidResponse(appConstant.noInternet)
    }
  }

  pouchDbDataFetching(dataFetchingInput) {
    const objectHierarchyJSON = dataFetchingInput['objectHierarchyJSON'];
    const additionalInfo = dataFetchingInput['additionalInfo'];
    const layoutDataRestrictionSet = dataFetchingInput['layoutDataRestrictionSet']

    if (Object.entries(objectHierarchyJSON).length === 0 && objectHierarchyJSON.constructor === Object) {
      return this.invalidResponse(appConstant.inValidInput);
    } else {
      return this.pouchDBProvider.fetchdatawithRelationship(objectHierarchyJSON, layoutDataRestrictionSet, additionalInfo).then(result => {
        return result
      })
    }
  }

  jsonDbDataFetching(dataFetchingInput) {
    if (navigator.onLine) {
      const fetchActionInfo = dataFetchingInput['fetchActionInfo'];
      const layoutId = dataFetchingInput['layoutId'];
      const actionParams = dataFetchingInput['actionParams'];
      if (fetchActionInfo && layoutId) {
        return this.jsonDataProvider.fetchByProcessInfo(fetchActionInfo, layoutId, actionParams).then(result => {
          return result
        })
      } else {
        return this.invalidResponse(appConstant.inValidInput)
      }
    } else {
      return this.invalidResponse(appConstant.noInternet)
    }
  }
  invalidResponse(message) {
    return Promise.resolve({ 'status': appConstant.failed, 'message': message })
  }

  // Convert reldoc to normal doc
  convertRelDocToNormalDoc(modifiedRec) {
    if (modifiedRec['dataProvider'] === appConstant.pouchDBStaticName) {
      return this.pouchDBProvider.convertRelDocToNormalDoc(modifiedRec['doc']);
    } else {
      return this.couchDBProvider.convertRelDocToNormalDoc(modifiedRec['doc']);
    }
  }

  fetchDataByFindOption(dataFetchingInput) {
    // tslint:disable-next-line: no-shadowed-variable
    const dataProvider = dataFetchingInput['dataSource']
    if (dataProvider) {
      if (dataProvider === appConstant.couchDBStaticName) {
        return this.couchDbDataFetchingUsingOptions(dataFetchingInput)
      } else if (dataProvider === appConstant.pouchDBStaticName) {
        return this.pouchDbDataFetchingUsingOptions(dataFetchingInput)
      }
    } else {
      return this.invalidResponse(appConstant.inValidInput);
    }
  }

  couchDbDataFetchingUsingOptions(dataFetchingInput) {
    if (navigator.onLine) {
      const options = dataFetchingInput['options'];
      if (!options) {
        return this.invalidResponse(appConstant.inValidInput);
      } else {
        return this.couchDBProvider.findAPIwithOptions(options).then(result => {
          return result
        })
      }
    } else {
      return this.invalidResponse(appConstant.noInternet)
    }
  }

  pouchDbDataFetchingUsingOptions(dataFetchingInput) {

    const options = dataFetchingInput['options'];
    if (!options) {
      return this.invalidResponse(appConstant.inValidInput);
    } else {
      return this.pouchDBProvider.fetchDataWithReference(options).then(result => {
        return result
      })
    }
  }

  queryDataFromDataSource(dataFetchingInput) {
    // tslint:disable-next-line: no-shadowed-variable
    if (dataFetchingInput['layoutDataRestrictionSet'] != undefined && dataFetchingInput['layoutDataRestrictionSet'].length > 0) {
      let layoutDataRestrictionSet = dataFetchingInput['layoutDataRestrictionSet'][0]

      if (layoutDataRestrictionSet['restrictionType'].toLowerCase() == 'owner') {
        dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds'] = [this.appUtilityObj.userId]
        return this.queryData(dataFetchingInput)

      } else {
        let corUsersObjectHierarchyJSON = {
          "objectId": this.metaDbConfig.corUserHierarchy,
          "objectName": this.metaDbConfig.corUserHierarchy,
          "fieldId": 0,
          "objectType": "PRIMARY",
          "relationShipType": null,
          "childObject": []
        }

        return this.metaDataDbProvider.fetchDataWithReference(corUsersObjectHierarchyJSON).then(res => {

          if (res['status'] == "SUCCESS" && res['records'].length > 0) {
            dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds']
              = this.getHierarchialUserIds(layoutDataRestrictionSet, res['records'][0]);
            return this.queryData(dataFetchingInput);

          } else {
            dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds'] = [this.appUtilityObj.userId]
            this.queryData(dataFetchingInput);
          }

        }).catch(err => {
          console.log("Data provider error :", err);

          dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds'] = [this.appUtilityObj.userId]
          return this.queryData(dataFetchingInput);
        })
      }
    } else {
      return this.queryData(dataFetchingInput);
    }
  }

  private queryData(dataFetchingInput) {
    const dataProvider = dataFetchingInput['dataSource']

    if (dataProvider) {
      if (dataProvider === appConstant.couchDBStaticName) {
        return this.queryCouchData(dataFetchingInput)
      } else if (dataProvider === appConstant.pouchDBStaticName) {
        return this.queryPouchData(dataFetchingInput)
      } else if (dataProvider === appConstant.jsonDBStaticName) {
        return this.jsonDbDataFetching(dataFetchingInput)
      }
    } else {
      return this.invalidResponse(appConstant.inValidInput);
    }
  }

  queryCouchData(dataFetchingInput) {
    if (navigator.onLine) {
      const objectHierarchyJSON = dataFetchingInput['objectHierarchyJSON'];

      if (Object.entries(objectHierarchyJSON).length === 0 && objectHierarchyJSON.constructor === Object) {
        return this.invalidResponse(appConstant.inValidInput);
      } else {
        if (dataFetchingInput['layoutDataRestrictionSet'] != undefined && dataFetchingInput['layoutDataRestrictionSet'].length > 0) {
          return this.couchDBProvider.queryListDataWithBatch(objectHierarchyJSON, dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds']).then(result => {
            return result
          })
        } else {
          return this.couchDBProvider.queryListDataWithBatch(objectHierarchyJSON).then(result => {
            return result
          })
        }
      }
    } else {
      return this.invalidResponse(appConstant.noInternet)
    }
  }

  queryPouchData(dataFetchingInput) {
    const objectHierarchyJSON = dataFetchingInput['objectHierarchyJSON'];

    if (Object.entries(objectHierarchyJSON).length === 0 && objectHierarchyJSON.constructor === Object) {
      return this.invalidResponse(appConstant.inValidInput);
    } else {
      if (dataFetchingInput['layoutDataRestrictionSet'] != undefined && dataFetchingInput['layoutDataRestrictionSet'].length > 0) {
        return this.pouchDBProvider.queryListDataWithBatch(objectHierarchyJSON, dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds']).then(result => {
          return result
        })
      } else {
        return this.pouchDBProvider.queryListDataWithBatch(objectHierarchyJSON).then(result => {
          return result
        })
      }
    }
  }

  querySingleDoc(queryParams) {
    const dataProvider = queryParams['dataSource']

    if (dataProvider) {
      if (dataProvider === appConstant.couchDBStaticName) {
        return this.querySingleCouchData(queryParams)
      } else if (dataProvider === appConstant.pouchDBStaticName) {
        return this.querySinglePouchData(queryParams)
      } else if (dataProvider === appConstant.jsonDBStaticName) {
        return this.jsonDbDataFetching(queryParams)
      }
    } else {
      return this.invalidResponse(appConstant.inValidInput);
    }
  }

  private querySingleCouchData(queryParams) {
    if (navigator.onLine) {
      const objectHierarchyJSON = queryParams['objectHierarchyJSON'];

      if (Object.entries(objectHierarchyJSON).length === 0 && objectHierarchyJSON.constructor === Object) {
        return this.invalidResponse(appConstant.inValidInput);
      } else {
        if (queryParams['additionalInfo'] && queryParams['additionalInfo']['id']) {
          return this.couchDBProvider.querySingleDoc(objectHierarchyJSON, queryParams['additionalInfo']['id']).then(result => {
            return result
          })
        } else {
          return this.invalidResponse(appConstant.inValidInput);
        }
      }
    } else {
      return this.invalidResponse(appConstant.noInternet)
    }
  }

  private querySinglePouchData(queryParams) {
    const objectHierarchyJSON = queryParams['objectHierarchyJSON'];

      if (Object.entries(objectHierarchyJSON).length === 0 && objectHierarchyJSON.constructor === Object) {
        return this.invalidResponse(appConstant.inValidInput);
      } else {
        if (queryParams['additionalInfo'] && queryParams['additionalInfo']['id']) {
          return this.pouchDBProvider.querySingleDoc(objectHierarchyJSON, queryParams['additionalInfo']['id']).then(result => {
            return result
          })
        } else {
          return this.invalidResponse(appConstant.inValidInput);
        }
      }
  }

  /* Formula Data Fetch Methods */
  querySingleFormualDoc(queryParams) {
    const dataProvider = queryParams['dataSource']

    if (dataProvider) {
      if (dataProvider === appConstant.couchDBStaticName) {
        return this.querySingleFormulaCouchData(queryParams)
      } else if (dataProvider === appConstant.pouchDBStaticName) {
        return this.querySingleFormulaPouchData(queryParams)
      } else if (dataProvider === appConstant.jsonDBStaticName) {
        return this.getSingleFormulaJsonData(queryParams)
      }
    } else {
      return this.invalidResponse(appConstant.inValidInput);
    }
  }
  
  private querySingleFormulaCouchData(queryParams) {
    if (navigator.onLine) {
      const objectHierarchyJSON = queryParams['objectHierarchyJSON'];
    const objectReverseHierarchyJSON = queryParams['objectReverseHierarchyJSON']

      if (Object.entries(objectHierarchyJSON).length === 0
      && objectHierarchyJSON.constructor === Object && Object.entries(objectReverseHierarchyJSON).length === 0
      && objectReverseHierarchyJSON.constructor === Object) {
        return this.invalidResponse(appConstant.inValidInput);
      } else {
        if (queryParams['additionalInfo'] && queryParams['fetchParent']) {
          const finalRes = {}
          return this.couchDBProvider.fetchFormulaObjectForParent(objectReverseHierarchyJSON,
             queryParams['additionalInfo']['type'], queryParams['additionalInfo']['id'], finalRes).then(result => {
              return result
            })
        } else if (queryParams['additionalInfo']) {
          const finalRes = {}
          return this.couchDBProvider.fetchFormulaDataObject(objectReverseHierarchyJSON,
            objectHierarchyJSON, queryParams['additionalInfo'], finalRes).then(result => {
              return result
            })
        } else {
          return this.invalidResponse(appConstant.inValidInput);
        }
      }
    } else {
      return this.invalidResponse(appConstant.noInternet)
    }
  }

  private querySingleFormulaPouchData(queryParams) {
    const objectHierarchyJSON = queryParams['objectHierarchyJSON'];
    const objectReverseHierarchyJSON = queryParams['objectReverseHierarchyJSON']

      if (Object.entries(objectHierarchyJSON).length === 0
      && objectHierarchyJSON.constructor === Object && Object.entries(objectReverseHierarchyJSON).length === 0
      && objectReverseHierarchyJSON.constructor === Object) {
        return this.invalidResponse(appConstant.inValidInput);
      } else {
        if (queryParams['additionalInfo'] && queryParams['fetchParent']) {
          const finalRes = {}
          return this.pouchDBProvider.fetchFormulaObjectForParent(objectReverseHierarchyJSON,
             queryParams['additionalInfo']['type'], queryParams['additionalInfo']['id'], finalRes).then(result => {
              return result
            })
        } else if (queryParams['additionalInfo']) {
          const finalRes = {}
          return this.pouchDBProvider.fetchFormulaDataObject(objectReverseHierarchyJSON,
            objectHierarchyJSON, queryParams['additionalInfo'], finalRes).then(result => {
              return result
            })
        } else {
          return this.invalidResponse(appConstant.inValidInput);
        }
      }
  }

  private getSingleFormulaJsonData(queryParams) {
    const objectHierarchyJSON = queryParams['objectHierarchyJSON'];
    const objectReverseHierarchyJSON = queryParams['objectReverseHierarchyJSON']

      if (Object.entries(objectHierarchyJSON).length === 0
      && objectHierarchyJSON.constructor === Object && Object.entries(objectReverseHierarchyJSON).length === 0
      && objectReverseHierarchyJSON.constructor === Object) {
        return this.invalidResponse(appConstant.inValidInput);
      } else {
         if (queryParams['additionalInfo']) {
          const finalRes = {}
          return this.pouchDBProvider.fetchFormulaDataObject(objectReverseHierarchyJSON,
            objectHierarchyJSON, queryParams['additionalInfo'], finalRes).then(result => {
              return result
            })
        } else {
          return this.invalidResponse(appConstant.inValidInput);
        }
      }
  }

  getChildCount(dataFetchingInput) {
    if (dataFetchingInput['layoutDataRestrictionSet'] != undefined && dataFetchingInput['layoutDataRestrictionSet'].length > 0) {
      let layoutDataRestrictionSet = dataFetchingInput['layoutDataRestrictionSet'][0]

      if (layoutDataRestrictionSet['restrictionType'].toLowerCase() == 'owner') {
        dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds'] = [this.appUtilityObj.userId]
        return this.queryRecordCount(dataFetchingInput)

      } else {
        let corUsersObjectHierarchyJSON = {
          "objectId": this.metaDbConfig.corUserHierarchy,
          "objectName": this.metaDbConfig.corUserHierarchy,
          "fieldId": 0,
          "objectType": "PRIMARY",
          "relationShipType": null,
          "childObject": []
        }

        return this.metaDataDbProvider.fetchDataWithReference(corUsersObjectHierarchyJSON).then(res => {

          if (res['status'] == "SUCCESS" && res['records'].length > 0) {
            dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds']
              = this.getHierarchialUserIds(layoutDataRestrictionSet, res['records'][0]);
            return this.queryRecordCount(dataFetchingInput);

          } else {
            dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds'] = [this.appUtilityObj.userId]
            this.queryRecordCount(dataFetchingInput);
          }

        }).catch(err => {
          console.log("Data provider error :", err);

          dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds'] = [this.appUtilityObj.userId]
          return this.queryRecordCount(dataFetchingInput);
        })
      }
    } else {
      return this.queryRecordCount(dataFetchingInput);
    }
  }

  private queryRecordCount(queryParams) {
    const dataProvider = queryParams['dataSource']

    if (dataProvider) {
      if (dataProvider === appConstant.couchDBStaticName) {
        return this.queryChildCountFromCouch(queryParams)
      } else if (dataProvider === appConstant.pouchDBStaticName) {
        return this.queryChildCountFromPouch(queryParams)
      } else if (dataProvider === appConstant.jsonDBStaticName) {
        return this.jsonDbDataFetching(queryParams)
      }
    } else {
      return this.invalidResponse(appConstant.inValidInput);
    }
  }

  queryChildCountFromPouch(queryParams) {
    const objectHierarchyJSON = queryParams['objectHierarchyJSON'];

    if (Object.entries(objectHierarchyJSON).length === 0 && objectHierarchyJSON.constructor === Object) {
      return this.invalidResponse(appConstant.inValidInput);
    } else {
      if (queryParams['additionalInfo'] && queryParams['additionalInfo']['id']) {
        var userIds = undefined;
        if (queryParams['layoutDataRestrictionSet'] != undefined && queryParams['layoutDataRestrictionSet'].length > 0) {
          userIds = queryParams['layoutDataRestrictionSet'][0]['restrictedDataUserIds'];
        }
        return this.pouchDBProvider.getChildCount(objectHierarchyJSON, queryParams['additionalInfo']['id'], userIds).then(result => {
          return result
        })
      } else {
        return this.invalidResponse(appConstant.inValidInput);
      }
    }
  }

  queryChildCountFromCouch(queryParams) {
    if (navigator.onLine) {
      const objectHierarchyJSON = queryParams['objectHierarchyJSON'];

      if (Object.entries(objectHierarchyJSON).length === 0 && objectHierarchyJSON.constructor === Object) {
        return this.invalidResponse(appConstant.inValidInput);
      } else {
        if (queryParams['additionalInfo'] && queryParams['additionalInfo']['id']) {
          var userIds = undefined;
          if (queryParams['layoutDataRestrictionSet'] != undefined && queryParams['layoutDataRestrictionSet'].length > 0) {
            userIds = queryParams['layoutDataRestrictionSet'][0]['restrictedDataUserIds'];
          }
          return this.couchDBProvider.getChildCount(objectHierarchyJSON, queryParams['additionalInfo']['id'], userIds).then(result => {
            return result
          })
        } else {
          return this.invalidResponse(appConstant.inValidInput);
        }
      }
    } else {
      return this.invalidResponse(appConstant.noInternet)
    }
  }

  queryChildDataBatchwise(dataFetchingInput) {

    if (dataFetchingInput['layoutDataRestrictionSet'] != undefined && dataFetchingInput['layoutDataRestrictionSet'].length > 0) {
      let layoutDataRestrictionSet = dataFetchingInput['layoutDataRestrictionSet'][0]

      if (layoutDataRestrictionSet['restrictionType'].toLowerCase() == 'owner') {
        dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds'] = [this.appUtilityObj.userId]
        return this.queryChildData(dataFetchingInput)

      } else {
        let corUsersObjectHierarchyJSON = {
          "objectId": this.metaDbConfig.corUserHierarchy,
          "objectName": this.metaDbConfig.corUserHierarchy,
          "fieldId": 0,
          "objectType": "PRIMARY",
          "relationShipType": null,
          "childObject": []
        }

        return this.metaDataDbProvider.fetchDataWithReference(corUsersObjectHierarchyJSON).then(res => {

          if (res['status'] == "SUCCESS" && res['records'].length > 0) {
            dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds']
              = this.getHierarchialUserIds(layoutDataRestrictionSet, res['records'][0]);
            return this.queryChildData(dataFetchingInput);

          } else {
            dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds'] = [this.appUtilityObj.userId]
            this.queryChildData(dataFetchingInput);
          }

        }).catch(err => {
          console.log("Data provider error :", err);

          dataFetchingInput['layoutDataRestrictionSet'][0]['restrictedDataUserIds'] = [this.appUtilityObj.userId]
          return this.queryChildData(dataFetchingInput);
        })
      }
    } else {
      return this.queryChildData(dataFetchingInput);
    }
  }

  queryChildData(queryParams) {
    const dataProvider = queryParams['dataSource']

    if (dataProvider) {
      if (dataProvider === appConstant.couchDBStaticName) {
        return this.queryChildFromCouch(queryParams)
      } else if (dataProvider === appConstant.pouchDBStaticName) {
        return this.queryChildFromPouch(queryParams)
      } else if (dataProvider === appConstant.jsonDBStaticName) {
        return this.jsonDbDataFetching(queryParams)
      }
    } else {
      return this.invalidResponse(appConstant.inValidInput);
    }
  }

  queryChildFromPouch(queryParams) {
    const objectHierarchyJSON = queryParams['objectHierarchyJSON'];

    if (Object.entries(objectHierarchyJSON).length === 0 && objectHierarchyJSON.constructor === Object) {
      return this.invalidResponse(appConstant.inValidInput);
    } else {
      if (queryParams['additionalInfo'] && queryParams['additionalInfo']['parentId']) {
        var userIds = undefined;
        if (queryParams['layoutDataRestrictionSet'] != undefined && queryParams['layoutDataRestrictionSet'].length > 0) {
          userIds = queryParams['layoutDataRestrictionSet'][0]['restrictedDataUserIds'];
        }
        return this.pouchDBProvider.queryChildListDataWithBatch(objectHierarchyJSON, queryParams['additionalInfo']['parentId'], userIds).then(result => {
          return result
        })
      } else {
        return this.invalidResponse(appConstant.inValidInput);
      }
    }
  }

  queryChildFromCouch(queryParams) {
    if (navigator.onLine) {
      const objectHierarchyJSON = queryParams['objectHierarchyJSON'];

      if (Object.entries(objectHierarchyJSON).length === 0 && objectHierarchyJSON.constructor === Object) {
        return this.invalidResponse(appConstant.inValidInput);
      } else {
        if (queryParams['additionalInfo'] && queryParams['additionalInfo']['parentId']) {
          var userIds = undefined;
          if (queryParams['layoutDataRestrictionSet'] != undefined && queryParams['layoutDataRestrictionSet'].length > 0) {
            userIds = queryParams['layoutDataRestrictionSet'][0]['restrictedDataUserIds'];
          }
          return this.couchDBProvider.queryChildListDataWithBatch(objectHierarchyJSON, queryParams['additionalInfo']['parentId'], userIds).then(result => {
            return result
          })
        } else {
          return this.invalidResponse(appConstant.inValidInput);
        }
      }
    } else {
      return this.invalidResponse(appConstant.noInternet)
    }
  }
  getDbServiceProvider(dataSorce) {
    if (dataSorce === appConstant.couchDBStaticName) {
      return this.couchDBProvider;
    } else if (dataSorce === appConstant.pouchDBStaticName) {
      return this.pouchDBProvider;
    }
  }
  vaidateUniqueField(type,doc,dataSorce,uniqueField,action){
    if (dataSorce === appConstant.couchDBStaticName) {
      return this.couchDBProvider.vaidateUniqueField(type, doc,uniqueField,action);
    }
  }

  fetchDocsWithoutRelationshipByParentTypeAndId(dataSorce,childtype, parent_type, parent_id, referencedetail?) {
    if (dataSorce === appConstant.couchDBStaticName) {
      return this.couchDBProvider.fetchDocsWithoutRelationshipByParentTypeAndId(childtype, parent_type, parent_id, referencedetail); 
    }else if (dataSorce === appConstant.pouchDBStaticName) {
      return  this.pouchDBProvider.fetchDocsWithoutRelationshipByParentTypeAndId(childtype, parent_type, parent_id); 
    }
  }

}
