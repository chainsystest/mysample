import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { IonBottomDrawerModule } from 'ion-bottom-drawer';
import { ReactiveFormsModule } from '@angular/forms';
import { cspfmFormulamodule } from '../components/cspfmFormula/cspfmFormula.module';
import { cspfm_data_display } from '../pipes/cspfm_data_display';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { cspfmActionsmodule } from '../components/cspfmActions/cspfmActions.module';
import { AngularSlickgridModule } from 'angular-slickgrid';

@NgModule({
  exports: [
    TranslateModule,
    IonBottomDrawerModule,
    ReactiveFormsModule,
    cspfmFormulamodule,
    NgxDatatableModule, cspfm_data_display, cspfmActionsmodule, AngularSlickgridModule
  ],
  declarations: [cspfm_data_display],
  imports: [
    CommonModule,

  ]
})
export class SharedModule { }
