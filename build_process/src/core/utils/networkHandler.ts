import { Injectable } from '@angular/core';
import { appConfiguration } from './appConfiguration';
import { appUtility } from './appUtility';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Events } from '@ionic/angular';
import { dbProvider } from '../db/dbProvider';
import { couchdbProvider } from '../db/couchdbProvider';
import { attachmentDbProvider } from '../db/attachmentDbProvider';
import { attachmentCouchDbProvider } from '../db/attachmentCouchDbProvider';
import { Http } from '@angular/http';
import { metaDbValidation } from './metaDbValidation';
import { metaDataDbProvider } from '../db/metaDataDbProvider';
import { Broadcaster } from '@ionic-native/broadcaster/ngx';
import { cspfmExecutionPouchDbProvider } from '../db/cspfmExecutionPouchDbProvider';

@Injectable({
  providedIn: 'root'
})
export class networkHandler {
  public networkConnectSubscription;
  public networkDisconnectSubscription;
  constructor(public appConfig: appConfiguration,
    public appUtilityObj: appUtility,
    public appPreferences: AppPreferences,
    public network: Network,
    public events: Events,
    public dbprovider: dbProvider,
    public couchdbprovider: couchdbProvider,
    public attachmentDbprovider: attachmentDbProvider,
    public attachmentCouchDBProvider: attachmentCouchDbProvider,
    public http: Http,
    public metaDbValidation: metaDbValidation,
    public metaDbProvider: metaDataDbProvider,
    public broadcaster: Broadcaster,
    public cspfmExecutionPouchDBProvider : cspfmExecutionPouchDbProvider) { }

  networkHandlerForSync() {
    if (this.appUtilityObj.isMobile) {
      this.startNetworkConnectListener();
      this.startNetworkDisconnectListener();
    } else {
      window.addEventListener('offline', () => {
        this.stopSyncProcess();
      });
      window.addEventListener('online', () => {
        this.startSyncProcess();
      });
    }
  }
  startNetworkConnectListener() {
    this.networkConnectSubscription = this.network.onConnect().subscribe(() => {
      console.log('now you are online!');
      this.startSyncProcess();
    });
  }
  startNetworkDisconnectListener() {
    this.networkDisconnectSubscription = this.network.onDisconnect().subscribe(() => {
      console.log('now you are offline!');
      this.stopSyncProcess();
    });
  }
  startSyncProcess() {
    const syncCompletedStatus = this.appConfig.configuration.appId + "synCompletedStatus";
    if (this.appUtilityObj.isMobile) { // Mobile
      this.appPreferences.fetch("", syncCompletedStatus).then(res => {
        if (res === true) { // Live Sync
          this.oauthTokenValidation().then(oAuthResponse => {
            if (oAuthResponse === "SUCCESS") {
              this.startLiveSync();
            } else {
              console.log('broadcaster called')
              this.broadcaster.fireNativeEvent('ionicNativeBroadcast', { action: 'Logout' })
                .then(() => console.log('Success'))
                .catch(() => console.log('Error'));
            }
          })
        } else if (res === false) { // Full Sync
          const response = {
            "network": true
          }
          this.events.publish('startFullSync', response);
        }
      });
    } else { // Browser
      const syncCompletedStatusValue = localStorage.getItem(syncCompletedStatus)
      if (syncCompletedStatusValue === "true") { // Live Sync
        this.appUtilityObj.validateSailsSession().then(response => {
          if (response === "SUCCESS") {
            this.startLiveSync();
          } else {
            window.location.replace('/apps/applist');
          }
        })
      } else if (syncCompletedStatusValue === "false") { // Full Sync
        const response = {
          "network": true
        }
        this.events.publish('startFullSync', response);
      }
    }
  }
  stopSyncProcess() {
    const syncCompletedStatus = this.appConfig.configuration.appId + "synCompletedStatus";
    if (this.appUtilityObj.isMobile) { // Mobile
      this.appPreferences.fetch("", syncCompletedStatus).then(res => {
        if (res === true) { // Live Sync
          this.stopLiveSync();
        } else if (res === false) { // Full Sync
          const response = {
            "network": false
          }
          this.events.publish('startFullSync', response);
        }
      });
    } else { // Browser
      const syncCompletedStatusValue = localStorage.getItem(syncCompletedStatus)
      if (syncCompletedStatusValue === "true") { // Live Sync
        this.stopLiveSync();
      } else if (syncCompletedStatusValue === "false") { // Full Sync
        const res = {
          "network": false
        }
        this.events.publish('startFullSync', res);
      }
    }
  }
  stopLiveSync() {
    this.metaDbProvider.cancelLivereplicationFromServerWithSelector();
    this.dbprovider.cancelLivereplicationFromServerWithSelector();
    this.attachmentDbprovider.cancelLivereplicationFromServerWithSelector();
    this.cspfmExecutionPouchDBProvider.cancelLivereplicationFromServerWithSelector();
    this.couchdbprovider.networkWithOutConnectivity();
    this.attachmentCouchDBProvider.networkWithOutConnectivity();
  }
  startLiveSync() {
    //Open Sync Page
    this.events.publish('syncPageSubscribe')
  }
  oauthTokenValidation() {
    const url = this.appUtilityObj.appBuilderURL;
    const postData = {
      'inputparams': {
        'orgId': this.appUtilityObj.orgId,
        'accessToken': this.appUtilityObj.accessToken,
        'appname': 'appM'
      }
    };
    const serviceURl = url + '/appcontainer/validateAccessToken';
    return this.http
      .post(serviceURl, JSON.stringify(postData))
      .toPromise().then(res => {
        const response = JSON.parse(res['_body']);
        if (response['status'] === 'Success') {
          this.appUtilityObj.accessToken = response['accessToken'];
          return Promise.resolve("SUCCESS");
        } else {
          return Promise.resolve("Failure");
        }
      })
      .catch(error => {
        console.log('An error occurred', error);
        return Promise.resolve("Failure");
      });
  }
}
