import { Component, OnInit, Input } from '@angular/core';
import { DatePipe } from '@angular/common';
import { cs_whocolumn_popover } from 'src/core/components/cs_whocolumn_popover/cs_whocolumn_popover';
import { PopoverController } from '@ionic/angular';
import { metaDbConfiguration } from 'src/core/db/metaDbConfiguration';
import { appUtility } from 'src/core/utils/appUtility';
import { metaDataDbProvider } from 'src/core/db/metaDataDbProvider';

@Component({
  selector: 'cs-whocolumn-icon',
  templateUrl: './cs_whocolumn_icon.html',
  styleUrls: ['./cs_whocolumn_icon.scss'],
})
export class cs_whocolumn_icon implements OnInit {
  selectedItem;
  iconName;
  iconClass;
  metaDbProvider;
  tasklist: [];
  public corUsersObjectHierarchyJSON = {
    "objectId": this.metaDbconfig.corUsersObject,
    "objectName": this.metaDbconfig.corUsersObject,
    "fieldId": 0,
    "objectType": "PRIMARY",
    "relationShipType": null,
    "childObject": []
  };


  constructor(public datePipe: DatePipe, public popoverController: PopoverController, public metaDbconfig: metaDbConfiguration,
    public apputilityobject: appUtility, private metaDataDbProvider: metaDataDbProvider) {
  }


  getUserNameAgainstUserId(userid) {
    const options = {};
    const selector = {};
    selector['data.type'] = this.metaDbconfig.corUsersObject;
    selector['data.user_id'] = userid;
    options['selector'] = selector;
    this.corUsersObjectHierarchyJSON['options'] = options;
    return this.metaDataDbProvider.fetchDataWithReference(this.corUsersObjectHierarchyJSON).then(result => {
      return Promise.resolve(result);
    });
  }


  @Input() set setInfo(item) {
    this.selectedItem = item;
  }

  @Input() set setIcon(iconName) {
    this.iconName = iconName;
  }

  @Input() set setIconClass(iconClass) {
    this.iconClass = iconClass;
  }

  iconClick(event) {
    event.stopPropagation();
    var tasklist = [];
    
    if (this.selectedItem) {
      tasklist.push(this.getUserNameAgainstUserId(this.selectedItem['createdby']));
      if (this.selectedItem['createdby'] !== this.selectedItem['lastmodifiedby']) {
        tasklist.push(this.getUserNameAgainstUserId(this.selectedItem['lastmodifiedby']));
      }
      Promise.all(tasklist).then(result => {
        if (result.length === 1
          && result[0]['status'] === 'SUCCESS') {
          this.getUserName(result, event);
        }
        else if (result.length === 2
          && result[0]['status'] === 'SUCCESS'
          && result[1]['status'] === 'SUCCESS') {
          this.getUserName(result, event);
        }
        else {
          this.getUserName([], event);
        }
      }).catch(err => {
        this.getUserName([], event);
      });
    }
  }

  getUserName(userList, event) {
    let values = [];
    var createByUserFirstName = '';
    var modifyeByUserFirstName = '';

    if (userList.length > 0) {
      if (this.selectedItem['createdby'] === this.selectedItem['lastmodifiedby']) {
        var createByUserName = userList[0]['records'];
        createByUserFirstName = createByUserName[0]['first_name'];
        modifyeByUserFirstName = createByUserName[0]['first_name'];
      }
      else {
        var createByUserName = userList[0]['records'];
        var modifyByUserName = userList[1]['records'];
        createByUserFirstName = createByUserName[0]['first_name'];
        modifyeByUserFirstName = modifyByUserName[0]['first_name'];
      }
    }
    else {
      createByUserFirstName = this.selectedItem['createdby'];
      modifyeByUserFirstName = this.selectedItem['lastmodifiedby'];
    }
    let createdon = this.datePipe.transform(this.selectedItem['createdon'], 'medium');
    let lastmodifiedon = this.datePipe.transform(this.selectedItem['lastmodifiedon'], 'medium');

    if (createdon == lastmodifiedon) {
      values = [
        {
          "label1": "Created By",
          "label2": createByUserFirstName,
          "label3": createdon
        }
      ]
    }
    else {
      values = [
        {
          "label1": "Created By",
          "label2": createByUserFirstName,
          "label3": createdon
        },
        {
          "label1": "Lastmodified By",
          "label2": modifyeByUserFirstName,
          "label3": lastmodifiedon
        }
      ]
    }

    this.presentPopover(event, values);
  }

  async presentPopover(event, item) {
    const popover = await this.popoverController.create({
      component: cs_whocolumn_popover,
      event: event,
      componentProps: { popoverData: item },
      translucent: true
    });
    return await popover.present();
  }

  ngOnInit() { }

}
