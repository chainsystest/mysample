import {
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';
import {
  IonicModule
} from '@ionic/angular';
import { cs_whocolumn_icon } from 'src/core/components/cs_whocolumn_icon/cs_whocolumn_icon';
import { cs_whocolumn_popover } from 'src/core/components/cs_whocolumn_popover/cs_whocolumn_popover';
import { cs_whocolumn_popovermodule } from 'src/core/components/cs_whocolumn_popover/cs_whocolumn_popover.module';


@NgModule({
  declarations: [cs_whocolumn_icon],
  imports: [
    CommonModule,
    IonicModule.forRoot(),
    cs_whocolumn_popovermodule
  ],
  entryComponents: [cs_whocolumn_popover],
  exports: [
    cs_whocolumn_icon,
    cs_whocolumn_popover
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class cs_whocolumn_iconmodule {

}