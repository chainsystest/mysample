import {
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';
import {
  IonicModule
} from '@ionic/angular';
import { cspfmFormula } from './cspfmFormula';

@NgModule({
  declarations: [cspfmFormula],
  imports: [
    CommonModule,
    IonicModule.forRoot(),
  ],
  entryComponents: [],
  exports: [
    cspfmFormula
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class cspfmFormulamodule {

}