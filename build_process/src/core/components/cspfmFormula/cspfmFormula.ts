import { Component, OnInit, Input, OnChanges } from '@angular/core';
import * as math from 'mathjs'
import * as lodash from 'lodash';
@Component({
  selector: 'cspfmformula',
  templateUrl: './cspfmFormula.html',
  styleUrls: ['./cspfmFormula.scss'],
})
export class cspfmFormula implements OnInit, OnChanges {
  public formulaResult: any;
  // public showErrorValue: boolean = false
  // public errorMessage: any = "";
  constructor() { }

  formulaFieldLabel = ""
  @Input() set csformulaFieldLabel(formulaFieldLabelName: any) {
    this.formulaFieldLabel = (formulaFieldLabelName !== undefined && formulaFieldLabelName !== null) ? formulaFieldLabelName : "";
  }

  formulaField = ""
  @Input() set csformulaField(formulaFieldName: any) {
    this.formulaField = (formulaFieldName !== undefined && formulaFieldName !== null) ? formulaFieldName : "";
  }

  public defaultValue = ""
  @Input() set csformuladefaultValue(formulaDefaultValue: any) {
    this.defaultValue = (formulaDefaultValue !== undefined && formulaDefaultValue !== null) ? formulaDefaultValue : "";
  }

  public precisionValue = 0
  @Input() set csformulaprecisionValue(formulaPrecisionValue: any) {
    this.precisionValue = (formulaPrecisionValue !== undefined && formulaPrecisionValue !== null) ? formulaPrecisionValue : "";
  }

  public dataSource = ""
  @Input() set csformuladataSource(dataSourceValue: any) {
    this.dataSource = (dataSourceValue !== undefined && dataSourceValue !== null) ? dataSourceValue : "";
  }

  formulaConfig = {}
  @Input() set csformulaConfig(formulaConfigJSON: any) {
    this.formulaConfig = (formulaConfigJSON !== undefined && formulaConfigJSON !== null) ? formulaConfigJSON : "";
  }

  public formulaDataObject: any
  @Input() set csformulaObject(formulaResultObject: any) {
    if (formulaResultObject !== undefined && formulaResultObject !== null && Object.entries(formulaResultObject).length > 0 && formulaResultObject.constructor === Object) {
      this.formulaDataObject = (formulaResultObject !== undefined && formulaResultObject !== null) ? formulaResultObject : "";
      this.formulaCalculation(this.formulaDataObject)
    }
  }

  ngOnInit() {
    console.log("ngOnInit");
  }

  ngOnChanges() {
    console.log("csformula componet - ngOnChanges");
    if (this.formulaDataObject !== undefined && this.formulaDataObject !== null
      && Object.entries(this.formulaDataObject).length > 0 && this.formulaDataObject.constructor === Object) {
      this.formulaCalculation(this.formulaDataObject)
    }
  }

  // ngAfterViewInit() {
  //   console.log("ngAfterViewInit");
  // }
  // ngAfterContentInit() {
  //   console.log("ngAfterContentInit");
  // }
  // ngOnDestroy() {
  //   console.log("ngOnDestroy");
  // }

  async formulaCalculation(dataObject) {
    const formulaArray = []
    const formulaValueJson = {}
    return this.formulaOperandCalculation(this.formulaConfig, dataObject, formulaArray, formulaValueJson).then(res => {
      this.formulaResultCalculation(res)
    })
  }

  async formulaOperandCalculation(formulaConfigObject, dataObject, formulaArray, formulaValueJson) {
    const formulaExpression = formulaConfigObject["formula"]
    console.log("formulaExpression => ", formulaExpression);
    const fieldArray = []
    const formulaObject = {
      "fieldName": formulaConfigObject["fieldName"],
      "formula": formulaConfigObject["formula"],
      "objectId": formulaConfigObject["objectId"],
      "fields": fieldArray
    }
    const operands = formulaConfigObject["operands"]
    console.log("formulaOperandCalculation => ", operands);

    /* Get ObjectID and FieldName from Operands, add this ObjectID_FieldName in Formula Object */
    const fieldNames = lodash.map(operands, 'fieldName');
    const objectIds = lodash.map(operands, 'objectId');
    for (let i = 0; i < fieldNames.length; i++) {
      const fields = formulaObject["fields"]
      const fieldName = fieldNames[i]
      const objectId = objectIds[i]
      let objectWithFieldName = ""
      if (isNaN(objectId)) {// ObjectId is not a number
        objectWithFieldName = objectId + "_" + fieldName
      } else {
        objectWithFieldName = "pfm" + objectId + "_" + fieldName
      }
      const fieldArrayValue = lodash.concat(fields, objectWithFieldName);
      formulaObject["fields"] = fieldArrayValue
    }
    formulaArray.push(formulaObject)

    operands.forEach(formulaOperand => {
      if (formulaOperand["fieldType"] === "formula") {
        const formulaInnerObject = formulaOperand
        this.formulaOperandCalculation(formulaInnerObject, dataObject, formulaArray, formulaValueJson)
      } else {
        const fieldName = formulaOperand["fieldName"]
        const objectType = formulaOperand["objectType"]
        const fieldId = formulaOperand["fieldId"]
        const objectId = formulaOperand["objectId"]
        let objectIdValue = ""
        if (isNaN(objectId)) {// ObjectId is not a number
          objectIdValue = objectId
        } else {
          objectIdValue = "pfm" + objectId
        }
        const objectkey = objectIdValue + "_" + fieldName
        if (objectType === "LOOKUP") {
          const objectId_FieldId = objectIdValue + '_' + fieldId
          const selectedObject = dataObject[objectId_FieldId]
          const selectedValue = selectedObject[fieldName]
          formulaValueJson[objectkey] = selectedValue
        } else {
          const selectedObject = dataObject[objectIdValue]
          const selectedValue = selectedObject[fieldName]
          formulaValueJson[objectkey] = selectedValue
        }
      }
    })
    console.log("formulaValueJson = ", formulaValueJson);
    console.log("formula Array = ", formulaArray);
    const result = {
      "formulaValueJson": formulaValueJson,
      "formulaArray": formulaArray
    }
    return Promise.resolve(result);
  }

  async formulaResultCalculation(response) {
    const formulaValueJson = response["formulaValueJson"]
    const formulaArray = response["formulaArray"]
    for (let i = formulaArray.length - 1; i >= 0; i--) {
      const formulaObject = formulaArray[i]

      // Check Null value is available in Formula Value Json
      const isNullAvailable = lodash.values(formulaValueJson).some(nullValue => nullValue === null || nullValue === undefined || nullValue === "");
      if (isNullAvailable) {
        // Get nul values from Formula Value Json
        const nullValues = lodash.pickBy(formulaValueJson, lodash.isNull);
        const undefinedValue = Object.assign(nullValues, lodash.pickBy(formulaValueJson, lodash.isUndefined));
        const emptyValues = Object.assign(undefinedValue, lodash.pickBy(formulaValueJson, lodash.isUndefined));
        const nullValueKeys = lodash.keysIn(emptyValues);
        const fields = formulaObject["fields"]

        // Compare null values with formula involved fields
        const resultValue = lodash.filter(fields, function (fieldObject) {
          return lodash.some(nullValueKeys, function (nullValueObject) {
            return nullValueObject === fieldObject;
          });
        });

        if (resultValue.length > 0) {
          // this.showErrorValue = true;
          // this.errorMessage = "Please enter ";
          // resultValue.forEach(element => {
          //   const splitValue = lodash.split(element, '_');
          //   const fieldNameValue = splitValue[1]
          //   this.errorMessage = this.errorMessage + fieldNameValue + ','
          //   const fieldName = formulaObject["fieldName"]
          //   const objectId = formulaObject["objectId"]
          //   let objectWithFieldName = ""
          //   if (isNaN(objectId)) {// ObjectId is not a number
          //     objectWithFieldName = objectId + "_" + fieldName
          //   } else {
          //     objectWithFieldName = "pfm" + objectId + "_" + fieldName
          //   }
          //   formulaValueJson[objectWithFieldName] = null
          // });
          // this.errorMessage = this.errorMessage.slice(0, -1);
          this.formulaResult = 0
        } else {
          this.formulaExpressionEvalution(formulaObject, formulaValueJson)
        }
      } else {
        this.formulaExpressionEvalution(formulaObject, formulaValueJson)
      }
    }
  }

  async formulaExpressionEvalution(formulaObject, formulaValueJson) {
    // this.showErrorValue = false;
    // this.errorMessage = ""
    try {
      const expr = formulaObject["formula"]
      const result = math.evaluate(expr, formulaValueJson)
      const fieldName = formulaObject["fieldName"]
      const objectId = formulaObject["objectId"]
      let objectWithFieldName = ""
      if (isNaN(objectId)) {// ObjectId is not a number
        objectWithFieldName = objectId + "_" + fieldName
      } else {
        objectWithFieldName = "pfm" + objectId + "_" + fieldName
      }
      formulaValueJson[objectWithFieldName] = result
      this.formulaResult = result.toFixed(this.precisionValue)
    } catch (err) {
      console.log(err.message);
      this.formulaResult = 0
      // this.showErrorValue = true
    }
  }
}
