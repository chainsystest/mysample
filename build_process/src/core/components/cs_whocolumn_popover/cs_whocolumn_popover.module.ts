import {
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';
import {
  IonicModule
} from '@ionic/angular';
import { cs_whocolumn_popover } from './cs_whocolumn_popover';

@NgModule({
  declarations: [cs_whocolumn_popover],
  imports: [
    CommonModule,
    IonicModule.forRoot(),
  ],
  entryComponents: [cs_whocolumn_popover],
  exports: [
    cs_whocolumn_popover
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class cs_whocolumn_popovermodule {

}