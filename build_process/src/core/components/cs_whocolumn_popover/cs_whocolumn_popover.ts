import { Component, OnInit, Input } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { popoverpage } from 'src/core/pages/popoverpage/popoverpage';

@Component({
  selector: 'cs-whocolumn-popover',
  templateUrl: './cs_whocolumn_popover.html',
  styleUrls: ['./cs_whocolumn_popover.scss'],
})
export class cs_whocolumn_popover implements OnInit {

  values = [];
  constructor(private datepipe: DatePipe, private navParams: NavParams){
    this.values = this.navParams['data']['popoverData'];
  }

  ngOnInit() {}

}
