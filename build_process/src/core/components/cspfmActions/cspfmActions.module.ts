import {
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';
import {
  IonicModule
} from '@ionic/angular';
import { cspfmActions } from './cspfmActions';

@NgModule({
  declarations: [cspfmActions],
  imports: [
    CommonModule,
    IonicModule.forRoot(),
  ],
  entryComponents: [],
  exports: [
    cspfmActions
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class cspfmActionsmodule {

}