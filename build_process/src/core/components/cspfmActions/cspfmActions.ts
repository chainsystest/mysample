import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { SMS } from '@ionic-native/sms/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { cspfmActionDataServices } from 'src/core/utils/cspfmActionDataServices';


@Component({
  selector: 'cspfmactions',
  templateUrl: './cspfmActions.html',
  styleUrls: ['./cspfmActions.scss'],
})

export class cspfmActions {
  public iconName = ''
  constructor(public callNumber: CallNumber, public sms: SMS,
    public emailComposer: EmailComposer, public pfmService: cspfmActionDataServices) { }
  public actionData = {};
  public layoutActionJson;
  public dataObj = "";

  @Input() set actionJson(configJson: string) {
    this.layoutActionJson = (configJson !== undefined && configJson !== null) ? configJson : "";
    this.setActionIcon()
  }

  @Input() set dataObject(selectedDataObejct: string) {
    this.dataObj = (selectedDataObejct !== undefined && selectedDataObejct !== null) ? selectedDataObejct : "";
  }

  setActionIcon() {
    if (this.layoutActionJson['actionName'] == 'Call') {
      this.iconName = 'call'
    }
    else if (this.layoutActionJson['actionName'] == 'Sms') {
      this.iconName = 'chatboxes'
    }
    else if (this.layoutActionJson['actionName'] == 'Mail') {
      this.iconName = 'mail'
    }
  }

  mobileClickActions() {
    this.actionData = this.pfmService.makeActionData(this.layoutActionJson, this.dataObj)
    this.makeActions()
  }

  makeActions() {
    if (this.layoutActionJson['actionName'] == 'Call') {
      this.makeCall()
    }
    else if (this.layoutActionJson['actionName'] == 'Sms') {
      this.makeSms()
    }
    else if (this.layoutActionJson['actionName'] == 'Mail') {
      this.makeMail()
    }

  }

  makeCall() {
    this.callNumber.callNumber(this.actionData['Phone Number'], true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  makeSms() {
    this.sms.send(this.actionData['Phone Number'], this.actionData['Message']).
      then(() => console.log('sms success'))
      .catch(() => console.log('sms error'));
  }

  makeMail() {
    let email = {
      to: this.actionData['To'],
      cc: this.actionData['Cc'],
      bcc: this.actionData['Bcc'],
      attachments: [],
      subject: this.actionData['Subject'],
      body: this.actionData['Body'],
      isHtml: true
    }
    this.emailComposer.open(email).then(() => console.log('mail success'))
      .catch(() => console.log('mail error'));
  }

}
