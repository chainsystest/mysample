import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { cs_status_workflow_popover } from '../cs_status_workflow_popover/cs_status_workflow_popover';
import { appUtility } from 'src/core/utils/appUtility';
import { cspfmExecutionPouchDbProvider } from 'src/core/db/cspfmExecutionPouchDbProvider';
import { cspfmExecutionPouchDbConfiguration } from 'src/core/db/cspfmExecutionPouchDbConfiguration';
import { metaDbConfiguration } from 'src/core/db/metaDbConfiguration';
import { metaDataDbProvider } from 'src/core/db/metaDataDbProvider';

export interface StatusOptions {
  statusLabel: string;
  statusValue: string;
  statusType: string;
}

@Component({
  selector: 'cs-status-workflow',
  templateUrl: './cs_status_workflow.html',
  styleUrls: ['./cs_status_workflow.scss']
})
export class cs_status_workflow implements OnInit {

  isValidData = (input: any): input is StatusOptions => {

    const schema: Record<keyof StatusOptions, string> = {
      statusValue: 'string',
      statusLabel: 'string',
      statusType: 'string'
    };

    const missingProperties = Object.keys(schema)
      .filter(key => input[key] === undefined)
      .map(key => key as keyof StatusOptions)
      .map(key => new Error(`Document is missing ${key} ${schema[key]}`));

    // throw the errors if you choose

    return missingProperties.length === 0;
  }
  items;
  selectedStatus: StatusOptions;
  previousStatus: StatusOptions;
  labelValue: string;
  approveActionMessage = '';
  statusWorkFlowFieldId;
  
  allowApproveAction: boolean = false;
  public statusDisplayString = '';

  @Input() set data(data) {
    if (data !== undefined && data !== null) {
      var keys = Object.keys(data);
      var keyMatched = keys.filter(keyItem => {
        var arrayValue = data[keyItem];
        if (arrayValue.constructor == Array) {
          var matchedRecord = arrayValue.filter(item => {
            return this.isValidData(item);
          })
          return arrayValue.length == matchedRecord.length;
        } else {
          return false;
        }
      })
      if (keys.length == keyMatched.length) {
        this.items = data;
        this.approverType = '';
      } else {
        throw new Error("Required value is missing.")
      }
    } else {
      throw new Error("Required value is missing.")
    }

  }
  @Input() set currentStatus(selectedValue: StatusOptions) {

    if (selectedValue !== undefined && selectedValue !== null && this.isValidData(selectedValue)) {
      this.selectedStatus = selectedValue
      this.previousStatus = selectedValue;
      this.statusDisplayString = selectedValue['statusLabel'];
    } else {
      throw new Error("Required value is missing.")
    }
  }

  @Input() set label(label: string) {
    this.labelValue = (label !== undefined && label !== null) ? label : "Status";
  }
  executionInfoDetail = {}
  @Input() set actionInfo(label: string) {
    var actionInfoValue = (label !== undefined && label !== null) ? label : "";

    if (actionInfoValue == '') {
      this.allowApproveAction = false
      this.approveActionMessage = 'Workflow process initiated'

    }
    else {

      this.approveActionMessage = ''
      this.allowApproveAction = true
    }
  }

  // @Input() set cscomponentactionInfo(label: string) {
  //   this.executionInfoDetail = (label !== undefined && label !== null) ? label : "";
  //   this.setApproveProcessStatus()
  // }

  selectedObject = {}
  @Input() set cscomponentSelectedObject(label: string) {
    this.selectedObject = (label !== undefined && label !== null) ? label : "";
    this.checkLoggedUserIsApprover().then(result => {
      this.setExecutionData()
    })
  }
  @Input() set cscomponentField(label: string) {
    console.log("cscomponentField = ", label);
    
    this.statusWorkFlowFieldId = label;
    console.log("statusWorkFlowFieldId = ", this.statusWorkFlowFieldId);
    
  }

  statusworkflow_fieldId = {}
  @Input() set csstatusworkflow_fieldId(label: string) {
    this.statusworkflow_fieldId = (label !== undefined && label !== null) ? label : "";
  }
  fieldIdValue;
  @Input() set fieldId(label: string) {
    this.fieldIdValue = (label !== undefined && label !== null) ? label : "";
  }

  inputPageName = "";
  @Input() set inputPage(label: string) {
    this.inputPageName = (label !== undefined && label !== null) ? label : "";
    console.log("inputPageName = ", this.inputPageName);

  }
  @Output() onStatusChanged: EventEmitter<any> = new EventEmitter();
  @Output() getApprovalState: EventEmitter<any> = new EventEmitter();

  constructor(public popoverController: PopoverController,
    public appUtilityConfig : appUtility,
    public cspfmexecutionPouchDbProvider : cspfmExecutionPouchDbProvider,
    public executionDbConfigObject : cspfmExecutionPouchDbConfiguration,
    public metaDbConfigurationObj : metaDbConfiguration,
    public metaDbProvider : metaDataDbProvider) { }
  ngOnInit() { }

  selectedStatusValue
  previousStatusValue
  selectedItems
  activeforlessthenthree = false
  approvereject = false
  controlclose = false;
  circleclose = false
  circlecheck = false
  tempObj;
  passOption = true
  WorkFlowUserApprovalStatusDataObject = {}
  approverType
  currentStatusWorkflowActionFiledId
  cscomponentactionInfo = {}

  countCheck(event, item) {
    if (this.executionInfoDetail && this.executionInfoDetail.hasOwnProperty('executionId')) {
      return;
    }
    var input = this.items
    this.selectedStatusValue = this.selectedStatus.statusValue
    this.previousStatusValue = this.previousStatus.statusValue
    this.selectedItems = input[this.previousStatusValue];

    this.circleclose = false
    this.circlecheck = false
    console.log("selected item", item);

    if (this.selectedStatusValue != this.previousStatusValue) {
      this.controlclose = true
    }

    if (this.selectedItems.length <= 2) {
      this.activeforlessthenthree = !this.activeforlessthenthree;
    } else if (this.previousStatus['isApproveInitiateEnabled'] == 'Y' && this.selectedItems.length == 3) {
      this.approvereject = !this.approvereject
    } else {
      this.presentPopover()
    }
  }

  async presentPopover() {
    const popover = await this.popoverController.create({
      component: cs_status_workflow_popover,
      componentProps: {
        parent: this,
        input: this.items,
        selectedStatus: this.selectedStatus,
        previousStatus: this.previousStatus
      },
      translucent: true
    });
    return await popover.present();
  }



  approveAction(previousStatus) {
    this.selectedStatus = this.items[previousStatus['statusValue']].filter(item => { return item['statusType'].toLowerCase() == 'approved' })[0];
    this.onStatusChanged.emit({ 'selectedStatus': this.selectedStatus,
    'workFlowUserApprovalStatusDataObject': this.WorkFlowUserApprovalStatusDataObject });
    this.circleclose = false
    // this.circlecheck = true
    this.circlecheck = false;
    this.approvereject = false
    this.passOption = false
    this.controlclose = true;
    this.setApproveProcessStatus();
  }
  rejectAction(previousStatus) {
    this.selectedStatus = this.items[previousStatus['statusValue']].filter(item => { return item['statusType'].toLowerCase() == 'reject' })[0];
    this.onStatusChanged.emit({ 'selectedStatus': this.selectedStatus,
    'workFlowUserApprovalStatusDataObject': this.WorkFlowUserApprovalStatusDataObject });
    // this.circleclose = true
    this.circleclose = false;
    this.circlecheck = false
    this.approvereject = false
    this.passOption = false
    this.controlclose = true;
    this.setApproveProcessStatus();
  }

  itemClick(item) {
    this.tempObj = item
    if (item['statusValue'] == this.selectedStatusValue) {
      this.controlclose = false
    }
    else {
      this.controlclose = true
    }
    this.circleclose = false
    this.circlecheck = false
    this.popoverItemSelected(item)
    this.activeforlessthenthree = false;
    if (this.selectedStatusValue != this.previousStatusValue) {
      this.controlclose = true
    }
  }

  popoverItemSelected(data) {
    this.selectedStatus = JSON.parse(JSON.stringify(data));
    this.onStatusChanged.emit({ 'selectedStatus': data,
    'workFlowUserApprovalStatusDataObject': this.WorkFlowUserApprovalStatusDataObject });
    if (this.selectedStatus['statusValue'] != this.previousStatusValue) {
      this.controlclose = true
    }
  }

  onPrev() {

    if (this.executionInfoDetail && this.executionInfoDetail.hasOwnProperty('executionId')) {
      return;
    }

    this.circleclose = false
    this.circlecheck = false
    this.activeforlessthenthree = false
    this.approvereject = false

    //console.log("Item Before :",JSON.parse(JSON.stringify(this.items)));

    // Pointing the current status to previous status
    this.selectedStatus = this.previousStatus
    console.log("Item After :", JSON.parse(JSON.stringify(this.items)));

    this.onStatusChanged.emit({ 'selectedStatus': this.selectedStatus,
    'workFlowUserApprovalStatusDataObject': this.WorkFlowUserApprovalStatusDataObject });

    if (this.selectedStatus['statusValue'] == this.previousStatus['statusValue']) {
      this.controlclose = false;
    }
  }
  setApproveProcessStatus() {

    if (this.executionInfoDetail['executionId'] == "") {
      this.allowApproveAction = false;
      this.approvereject = true;
      this.approveActionMessage = 'Workflow process initiated'
    }
    else if (this.executionInfoDetail['executionId'] != ""

      && this.executionInfoDetail['workflowApproveStatusObject']['approvalExecutionStatus'] == '') {
      this.allowApproveAction = true;
      this.approvereject = true;
      this.approveActionMessage = '';
    }
    else if (this.executionInfoDetail['executionId'] != ""

      && this.executionInfoDetail['workflowApproveStatusObject']['approvalExecutionStatus'] == 'INPROGRESS') {
      this.allowApproveAction = false;
      this.approvereject = true;
      this.approveActionMessage = 'Approve inprogress';
    }
    else if (this.executionInfoDetail['executionId'] != ""

      && this.executionInfoDetail['workflowApproveStatusObject']['approvalExecutionStatus'] == 'ERROR') {
      this.allowApproveAction = true;
      this.approvereject = true;
      alert(this.executionInfoDetail['workflowApproveStatusObject']['comment'])
      this.approveActionMessage = '';
    }
    else if (this.executionInfoDetail['executionId'] != ""

      && this.executionInfoDetail['workflowApproveStatusObject']['approvalExecutionStatus'] == 'APPROVED') {
      this.allowApproveAction = false;
      this.approvereject = true;
      this.approveActionMessage = this.executionInfoDetail['workflowApproveStatusObject']['comment'];
    }
    else if (this.executionInfoDetail['executionId'] != ""

      && this.executionInfoDetail['workflowApproveStatusObject']['approvalExecutionStatus'] == 'REJECT') {
      this.allowApproveAction = false;
      this.approvereject = true;
      this.approveActionMessage = this.executionInfoDetail['workflowApproveStatusObject']['comment'];
    }
  }

  checkLoggedUserIsApprover() {

    if (this.selectedObject.hasOwnProperty('systemAttributes')) {
      const obj = this.selectedObject['systemAttributes']
      if (obj['lockedStatus'] === 'INPROGRESS') {

        const pfmApproveUserHierarchyJSON = {
          "objectId": this.metaDbConfigurationObj.pfmApproveValueUserObject,
          "objectName": this.metaDbConfigurationObj.pfmApproveValueUserObject,
          "fieldId": 0,
          "objectType": "PRIMARY",
          "relationShipType": null,
          "childObject": [

          ]
        };

        const options = {};
        const selector = {}
        selector['data.type'] = this.metaDbConfigurationObj.pfmApproveValueUserObject
        selector['data.user_id'] = Number(this.appUtilityConfig.userId);
        selector['data.field_id'] = Number(obj['fieldId'])
        selector['data.status_wf_config_id'] = Number(obj['statusWFConfigId'])
        selector['data.is_active']=true
        options['selector'] = selector;
        pfmApproveUserHierarchyJSON['options'] = options;

        return this.metaDbProvider.fetchDataWithReference(pfmApproveUserHierarchyJSON).then(result => {
          if (result && result.status === 'SUCCESS') {

            if (result.records.length === 0) {
              this.approverType = "non approver";
              this.getApprovalState.emit({ "approverType" : this.approverType });

              return Promise.resolve(false);
            } else {
              this.currentStatusWorkflowActionFiledId = obj['fieldId'];
              this.approverType = "approver";
              this.getApprovalState.emit({ "approverType" : this.approverType });
              return Promise.resolve(true);
            }
          } else {
            console.log("result = ", result);
          }
        }).catch(err => {
        });
      } else {
        return Promise.resolve(true);
      }
    } else {
      this.getApprovalState.emit({ "approverType": "" });
      return Promise.resolve(true);
    }
  }

  private setExecutionData() {

    if (this.selectedObject['systemAttributes']
            && this.selectedObject['systemAttributes']['lockedStatus'] === 'INPROGRESS') {
            this.cscomponentactionInfo['executionId'] = this.selectedObject['systemAttributes']['workFlowExecID']

            if (this.selectedObject['systemAttributes']['workFlowExecID'] !== "") {
              this.fetchWorkFlowUserApprovalStatus()
            } else {
              this.executionInfoDetail = (this.cscomponentactionInfo !== undefined
                && this.cscomponentactionInfo !== null) ? this.cscomponentactionInfo : "";
  
              this.setApproveProcessStatus()
            }
        }
  }

  fetchWorkFlowUserApprovalStatus() {

    const pfmApproveUserStatusHierarchyJSON = {
      "objectId": this.executionDbConfigObject.workFlowUserApprovalStatusObject,
      "objectName": this.executionDbConfigObject.workFlowUserApprovalStatusObject,
      "fieldId": 0,
      "objectType": "PRIMARY",
      "relationShipType": null,
      "childObject": [

      ]
    };
    const options = {};
    const selector = {}
    selector['data.type'] = this.executionDbConfigObject.workFlowUserApprovalStatusObject
    selector['data.workflowExectionId'] = this.selectedObject['systemAttributes']['workFlowExecID']
    selector['data.statusWFConfigId'] = Number(this.selectedObject['systemAttributes']['statusWFConfigId'])

    options['selector'] = selector;

    pfmApproveUserStatusHierarchyJSON['options'] = options;

    return this.cspfmexecutionPouchDbProvider.fetchDataWithReference(pfmApproveUserStatusHierarchyJSON).then(result => {
      if (result && result.status === 'SUCCESS') {

        if (result['records'].length > 0) {
          const approvalStatusList = result['records'][0]['approvalStatus']
          this.WorkFlowUserApprovalStatusDataObject = result['records'][0];
          const loggedUserStatus = approvalStatusList.filter(userDataObject => userDataObject.userId === this.appUtilityConfig.userId);

          if (loggedUserStatus.length > 0) {
            this.cscomponentactionInfo['workflowApproveStatusObject'] = loggedUserStatus[0]
          } else {
            this.cscomponentactionInfo['workflowApproveStatusObject'] = ""
          }
        } else {
          this.cscomponentactionInfo['workflowApproveStatusObject'] = ""
        }
        this.executionInfoDetail = (this.cscomponentactionInfo !== undefined
          && this.cscomponentactionInfo !== null) ? this.cscomponentactionInfo : "";
        this.setApproveProcessStatus()
      } else {
        console.log("result = ", result);
      }
    }).catch(err => {
    });
  }
}