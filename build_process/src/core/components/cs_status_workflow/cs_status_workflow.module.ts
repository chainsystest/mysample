import {
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from '@angular/core';
import {
  CommonModule 
} from '@angular/common';
import {
  IonicModule
} from '@ionic/angular';
import { cs_status_workflow } from './cs_status_workflow';
import { cs_status_workflow_popovermodule } from '../cs_status_workflow_popover/cs_status_workflow_popover.module';
import { cs_status_workflow_popover } from '../cs_status_workflow_popover/cs_status_workflow_popover';

@NgModule({
  declarations: [cs_status_workflow],
  imports: [
    CommonModule,
    IonicModule.forRoot(),
    cs_status_workflow_popovermodule
  ],
  entryComponents: [cs_status_workflow_popover],
  exports: [
    cs_status_workflow
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class cs_status_workflowmodule {

}