import {
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';
import {
  IonicModule
} from '@ionic/angular';
import { cs_status_workflow_popover } from './cs_status_workflow_popover';

@NgModule({
  declarations: [cs_status_workflow_popover],
  imports: [
    CommonModule,
    IonicModule.forRoot(),
  ],
  entryComponents: [],
  exports: [
    cs_status_workflow_popover
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class cs_status_workflow_popovermodule {

}