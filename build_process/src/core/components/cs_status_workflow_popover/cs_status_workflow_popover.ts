import { Component, OnInit, ApplicationRef } from '@angular/core';
import { PopoverController, NavParams } from '@ionic/angular';

@Component({
  selector: 'cs-status-workflow-popover',
  templateUrl: './cs_status_workflow_popover.html',
  styleUrls: ['./cs_status_workflow_popover.scss'],
})
export class cs_status_workflow_popover implements OnInit {
  selectedItems;
  selectedStatusValue = "";
  previousStatusValue = "";
  constructor(private popoverController: PopoverController, private navParams: NavParams, public appRef: ApplicationRef) {
    var input = navParams['data']['input']
    this.selectedStatusValue = navParams['data']['selectedStatus']['statusValue']
    this.previousStatusValue = navParams['data']['previousStatus']['statusValue']
    this.selectedItems = input[this.previousStatusValue];
    var data1=this.previousStatusValue;
    var items=this.selectedItems;
    var i=items.length;
    var returnJson
    while (i--) {
        if(items[i].statusValue.indexOf(data1)!=-1){
          returnJson = items.splice(i,1);
        }
    }
    this.selectedItems.unshift(returnJson[0]);
  }

  ngOnInit() { }
  itemClick(item) {
    this.selectedStatusValue = item['statusValue']
    var parent = this.navParams['data']['parent']
    parent.popoverItemSelected(item)
    this.popoverController.dismiss();
  }
}
