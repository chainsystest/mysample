import {
    Component,
    OnInit,
    ApplicationRef,
    HostListener
} from '@angular/core';
import {
    dataProvider
} from 'src/core/utils/dataProvider';
import {
    appConstant
} from 'src/core/utils/appConstant';
import {
    ModalController,
    Platform,
    LoadingController,
    Events,
    ToastController,
    AlertController
} from '@ionic/angular';
import {
    DrawerState
} from 'ion-bottom-drawer';
import {
    PopoverController
} from '@ionic/angular';
import {
    appUtility
} from 'src/core/utils/appUtility';
import {
    ScreenOrientation
} from '@ionic-native/screen-orientation/ngx';
import {
    lookupFieldMapping
} from 'src/core/pfmmapping/lookupFieldMapping';
import {
    objectTableMapping
} from 'src/core/pfmmapping/objectTableMapping';
import * as moment from 'moment';
import {
    Router,
    ActivatedRoute
} from '@angular/router';
import {
    SocialSharing
} from '@ionic-native/social-sharing/ngx';
import {
    EmailComposer
} from '@ionic-native/email-composer/ngx';
import {
    CallNumber
} from '@ionic-native/call-number/ngx';
import {
    SMS
} from '@ionic-native/sms/ngx';
import {
    attachmentDbProvider
} from 'src/core/db/attachmentDbProvider';
import * as _ from 'underscore';
import * as lodash from 'lodash';
import {
    DatePipe
} from '@angular/common';
import {
    registerLocaleData
} from '@angular/common';
import {
    metaDataDbProvider
} from 'src/core/db/metaDataDbProvider';
import {
    metaDbConfiguration
} from 'src/core/db/metaDbConfiguration';
import {
    cspfmExecutionPouchDbProvider
} from 'src/core/db/cspfmExecutionPouchDbProvider';
import {
    cspfmExecutionPouchDbConfiguration
} from 'src/core/db/cspfmExecutionPouchDbConfiguration';
import {
    FieldInfo
} from "src/core/pipes/cspfm_data_display";

@Component({
    selector: 'contactView_MOBILE_Grid',
    templateUrl: './contactView_MOBILE_Grid.html'
}) export class contactView_MOBILE_Grid implements OnInit {
    constructor(public popoverCtrl: PopoverController, public modalCtrl: ModalController,
        private socialSharing: SocialSharing, private emailComposer: EmailComposer, private callNumber: CallNumber, private sms: SMS,
        platform: Platform, public applicationRef: ApplicationRef, public appUtilityConfig: appUtility, public screenOrientation: ScreenOrientation,
        public events: Events, public router: Router, public activatRoute: ActivatedRoute,
        public objectTableMapping: objectTableMapping, public lookupFieldMapping: lookupFieldMapping,
        public loadingCtrl: LoadingController, public toastCtrl: ToastController, public dataProvider: dataProvider, public metaDbConfigurationObj: metaDbConfiguration, public metaDbProvider: metaDataDbProvider, public cspfmexecutionPouchDbProvider: cspfmExecutionPouchDbProvider,
        public executionDbConfigObject: cspfmExecutionPouchDbConfiguration, private datePipe: DatePipe,
        public alerCtrl: AlertController) {

        this.activatRoute.queryParams.subscribe(params => {
            if (Object.keys(params).length == 0 && params.constructor === Object) {
                console.log("list query params skipped");
                return
            }
            if (params["redirectUrl"]) {
                this.redirectUrl = params["redirectUrl"]
            }
            // this.headerDocItem = JSON.parse(params["parentObj"]);
            // this.parentTitle = params["parentTitle"];
            // this.parentObjLabel = params["parentFieldLabel"];
            // this.parentObjValue = params["parentFieldValue"];
            this.id = params["id"];
            this.prominentDataArray = params["prominientObjectInfo"];
            //change
            this.fetchSelectedObject();
        });
        this.appUtilityConfig.setEventSubscriptionlayoutIds(this.tableName_pfm147274, this.layoutId)

        if (!this.appUtilityConfig.isMobile || this.appUtilityConfig.osType == 'android') {
            this.isBrowser = true;
        };

        const windowHeight = window.innerHeight;
        this.drawerComponentDockedHeight = windowHeight / 2;



        this.screenOrientation.onChange().subscribe(
            () => {
                this.drawerComponentCurrentState = DrawerState.Bottom;
                const windowHeightVal = window.innerHeight;
                this.drawerComponentDockedHeight = windowHeightVal / 2;
            });


        this.events.subscribe(this.layoutId, (modified) => {
            if (modified["dataProvider"] == "PouchDB") {
                this.childObjectModifiedEventTrigger(modified);
            }
        });

    }
    private approverType: string = "";
    private cscomponentactionInfo = {};
    childObjectModifiedEventTrigger(modified) {
        const modifiedData = this.dataProvider.convertRelDocToNormalDoc(modified);
        if (modifiedData['id'] == this.id) {
            this.fetchSelectedObject();
        }
    }
    public obj_pfm147274: any = {};
    private obj_pfm147233: any = {};
    public gridFieldInfo: {
        [key: string]: FieldInfo
    } = {
        "pfm147274_email": {
            "label": "contactView_MOBILE_Grid.Element.contactdetails.email",
            "fieldName": "email",
            "prop": "email",
            "fieldType": "EMAIL",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147274_responsibility": {
            "label": "contactView_MOBILE_Grid.Element.contactdetails.responsibility",
            "fieldName": "responsibility",
            "prop": "responsibility",
            "fieldType": "TEXT",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147274_phonenumber": {
            "label": "contactView_MOBILE_Grid.Element.contactdetails.phonenumber",
            "fieldName": "phonenumber",
            "prop": "phonenumber",
            "fieldType": "NUMBER",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147274_contactname": {
            "label": "contactView_MOBILE_Grid.Element.contactdetails.contactname",
            "fieldName": "contactname",
            "prop": "contactname",
            "fieldType": "TEXT",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147274_name": {
            "label": "contactView_MOBILE_Grid.Element.contactdetails.name",
            "fieldName": "name",
            "prop": "name",
            "fieldType": "AUTONUMBER",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147233_city": {
            "label": "contactView_MOBILE_Grid.Element.addressdetails.city",
            "fieldName": "city",
            "prop": "city",
            "fieldType": "TEXT",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147233_sitenumber": {
            "label": "contactView_MOBILE_Grid.Element.addressdetails.sitenumber",
            "fieldName": "sitenumber",
            "prop": "sitenumber",
            "fieldType": "TEXT",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147233_addressid": {
            "label": "contactView_MOBILE_Grid.Element.addressdetails.addressid",
            "fieldName": "addressid",
            "prop": "addressid",
            "fieldType": "AUTONUMBER",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147233_stateprovince": {
            "label": "contactView_MOBILE_Grid.Element.addressdetails.stateprovince",
            "fieldName": "stateprovince",
            "prop": "stateprovince",
            "fieldType": "DROPDOWN",
            "child": "",
            "dateFormat": "",
            "mappingDetails": {
                "None": "None",
                "California": "California",
                "Florida": "Florida",
                "Texas": "Texas",
                "New Jersey": "New Jersey",
                "North Carolina": "North Carolina",
                "Alberta": "Alberta",
                "British Columbia": "British Columbia",
                "Nova Scotia": "Nova Scotia",
                "Manitoba": "Manitoba",
                "Delhi": "Delhi",
                "Tamil Nadu": "Tamil Nadu",
                "Kerala": "Kerala"
            },
            "currencyDetails": ""
        },
        "pfm147233_addresstype": {
            "label": "contactView_MOBILE_Grid.Element.addressdetails.addresstype",
            "fieldName": "addresstype",
            "prop": "addresstype",
            "fieldType": "CHECKBOX",
            "child": "",
            "dateFormat": "",
            "mappingDetails": {
                "None": "None",
                "Ship To": "Ship To",
                "Bill To": "Bill To"
            },
            "currencyDetails": ""
        },
        "pfm147233_address1": {
            "label": "contactView_MOBILE_Grid.Element.addressdetails.address1",
            "fieldName": "address1",
            "prop": "address1",
            "fieldType": "TEXT",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147233_address2": {
            "label": "contactView_MOBILE_Grid.Element.addressdetails.address2",
            "fieldName": "address2",
            "prop": "address2",
            "fieldType": "TEXT",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147233_country": {
            "label": "contactView_MOBILE_Grid.Element.addressdetails.country",
            "fieldName": "country",
            "prop": "country",
            "fieldType": "DROPDOWN",
            "child": "",
            "dateFormat": "",
            "mappingDetails": {
                "None": "None",
                "USA": "USA",
                "Canada": "Canada",
                "India": "India"
            },
            "currencyDetails": ""
        },
        "pfm147233_postalcode": {
            "label": "contactView_MOBILE_Grid.Element.addressdetails.postalcode",
            "fieldName": "postalcode",
            "prop": "postalcode",
            "fieldType": "TEXT",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        }
    };
    private addresstype_293732 = {
        'None': 'None',
        'Ship To': 'Ship To',
        'Bill To': 'Bill To'
    };
    private stateprovince_293674 = {
        'None': 'None',
        'California': 'California',
        'Florida': 'Florida',
        'Texas': 'Texas',
        'New Jersey': 'New Jersey',
        'North Carolina': 'North Carolina',
        'Alberta': 'Alberta',
        'British Columbia': 'British Columbia',
        'Nova Scotia': 'Nova Scotia',
        'Manitoba': 'Manitoba',
        'Delhi': 'Delhi',
        'Tamil Nadu': 'Tamil Nadu',
        'Kerala': 'Kerala'
    };
    private country_293719 = {
        'None': 'None',
        'USA': 'USA',
        'Canada': 'Canada',
        'India': 'India'
    }; // @ViewChild(Navbar) navbar: Navbar
    isBrowser: boolean = false;
    headerenable = false;
    drawerComponentDockedHeight = 300;
    drawerComponentCurrentState = DrawerState.Top
    drawerComponentPreviousState = DrawerState.Docked

    headerDocItem: any = {};
    public navigationHistoryProperties = {
        'navigatedPagesNameArray': [],
        'navigatedPagesPathArray': [],
        'routerVisLinkTagName': "",
        'secondPreviousPage': "",
        'navigatedPagesLength': 0,
        'previousPage': "",
        'previousPageName': "",
        'secondPreviousPageName': "",
    };
    public layoutId = "58352";
    public isSkeletonLoading = true;

    private tableName_pfm147274 = 'pfm147274';
    public parentObjLabel = '';
    public parentObjValue = '';
    public formulaObject = {};

    public parentTitle: any = 'Address Details';
    public pageTitle = "";
    public errorMessageToDisplay: string = "No Records";
    public id: any = '';
    public prominentDataArray: Array < any > = [];
    private objectHierarchyJSON = {
        "objectName": "contactdetails",
        "objectType": "PRIMARY",
        "relationShipType": null,
        "fieldId": "0",
        "objectId": "147274",
        "childObject": [{
            "objectName": "addressdetails",
            "objectType": "HEADER",
            "relationShipType": "one_to_many",
            "fieldId": "293833",
            "objectId": "147233",
            "childObject": []
        }]
    };
    private loading;
    private redirectUrl = "/";
    private WorkFlowUserApprovalStatusDataObject = {};
    public showNavigationHistoryPopUp: Boolean = false;
    private workFlowMapping = {

    }
    private currentStatusWorkflowActionFiledId;
    public formulaReverseObjectHierarchyJSON = undefined;
    public formulaConfigJSON = {};
    public dbServiceProvider = appConstant.pouchDBStaticName;
    public formulafields = {};

    async fetchSelectedObject() {


        const additionalObjectdata = {};
        additionalObjectdata['id'] = this.id
        const fetchParams = {
            'objectHierarchyJSON': this.objectHierarchyJSON,
            'additionalInfo': additionalObjectdata,
            'dataSource': appConstant.pouchDBStaticName
        }
        this.dataProvider.querySingleDoc(fetchParams).then(result => {
            this.isSkeletonLoading = false;
            this.obj_pfm147274 = []
            if (result['status'] != 'SUCCESS') {
                const errorMessageToDisplay = result["message"];
                if (errorMessageToDisplay === "No internet") {
                    this.presentNoInternetToast();
                }
                return;
            }

            const pluralName = this.dataProvider.getPluralName(this.tableName_pfm147274);
            this.obj_pfm147274 = result['records'][0];
            this.headerDocItem = this.obj_pfm147274['pfm147233s'][0];

            //   if (this.obj_pfm147274[pluralName][0]) {
            //     this.obj_pfm147274 = this.obj_pfm147274[pluralName][0];
            //   }



            this.pageTitle = this.obj_pfm147274["name"];


            this.applicationRef.tick();
        }).catch(error => {
            this.isSkeletonLoading = false;
            console.log(error);
        });
    }

    //   checkLoggedUserIsApprover() {

    //     if (this.obj_pfm147274.hasOwnProperty('systemAttributes')) {
    //         var obj = this.obj_pfm147274['systemAttributes']
    //         if (obj['lockedStatus'] === 'INPROGRESS') {

    //             var pfmApproveUserHierarchyJSON = {
    //                 "objectId": this.metaDbConfigurationObj.pfmApproveValueUserObject,
    //                 "objectName": this.metaDbConfigurationObj.pfmApproveValueUserObject,
    //                 "fieldId": 0,
    //                 "objectType": "PRIMARY",
    //                 "relationShipType": null,
    //                 "childObject": [

    //                 ]
    //             };

    //             const options = {};
    //             const selector = {};
    //             selector['data.type'] = this.metaDbConfigurationObj.pfmApproveValueUserObject
    //             selector['data.user_id'] = Number(this.appUtilityConfig.userId);
    //             selector['data.field_id'] = Number(obj['fieldId'])
    //             selector['data.status_wf_config_id'] = Number(obj['statusWFConfigId'])

    //             options['selector'] = selector;
    //             pfmApproveUserHierarchyJSON['options'] = options;

    //             return this.metaDbProvider.fetchDataWithReference(pfmApproveUserHierarchyJSON).then(result => {
    //                 if (result && result.status == 'SUCCESS') {

    //                     if (result.records.length == 0) {
    //                         this.approverType = "non approver";
    //                         return Promise.resolve(false);
    //                     }

    //                     else {
    //                         this.approverType = "approver";
    //                         this.currentStatusWorkflowActionFiledId = obj['fieldId']
    //                         return Promise.resolve(true);
    //                     }
    //                 }
    //                 else {

    //                 }

    //             }).catch(err => {


    //             });
    //         }
    //         else {
    //             return Promise.resolve(true);
    //         }

    //     }
    //     else {
    //         return Promise.resolve(true);
    //     }
    // }

    //         statusChange(event, selectedStatusField) {
    //             if (selectedStatusField == undefined) {
    //                 selectedStatusField = {}
    //             }

    //             selectedStatusField['statusLabel'] = event['selectedStatus']['statusLabel'];
    //             selectedStatusField['statusValue'] = event['selectedStatus']['statusValue'];
    //             selectedStatusField['statusType'] = event['selectedStatus']['statusType'];
    //             this.approveAction(selectedStatusField, event['workFlowUserApprovalStatusDataObject'])	
    // }	

    //             //Get Approval State from Component	
    //             getApprovalState(event) {	
    //              this.approverType = event['approverType']   
    //         }

    //             fetchLockedUserDetail() {
    //                 var systemAttributes = this.obj_pfm147274['systemAttributes']
    //                 var userId = systemAttributes['lockedBy']
    //                 var date = new Date(systemAttributes['lockedDate']);
    //                 this.showAlert(userId + " has locked for " + this.workFlowMapping[this.obj_pfm147274['systemAttributes']['fieldId']] + " on " + this.datePipe.transform(date, this.appUtilityConfig.userDateFormat));                             
    //             }
    historyButtonAction(event: Event) {
        this.showNavigationHistoryPopUp = !this.showNavigationHistoryPopUp;
        event.stopPropagation();
    }
    @HostListener('click') onClick() {
        this.showNavigationHistoryPopUp = false;
    }
    getNavigationHistory() {
        if (this.appUtilityConfig.getHomePageNode()) {
            let homePageNode = this.appUtilityConfig.getHomePageNode()
            this.navigationHistoryProperties['navigatedPagesNameArray'] = [homePageNode['homePageNodeName']]
            this.navigationHistoryProperties['navigatedPagesPathArray'] = [homePageNode['homePageNodepath']]
        }
        this.navigationHistoryProperties['navigatedPagesLength'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes.length;
        this.navigationHistoryProperties['previousPageName'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 2].childNodes[0].textContent;
        this.navigationHistoryProperties['previousPage'] = ( < HTMLElement > document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 2]).tagName.toLowerCase();
        if (document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3]) {
            this.navigationHistoryProperties['secondPreviousPageName'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3].childNodes[0].textContent;
            this.navigationHistoryProperties['secondPreviousPage'] = ( < HTMLElement > document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3]).tagName.toLowerCase();
        }
        for (let i = 0; i < this.navigationHistoryProperties['navigatedPagesLength'] - 3; i++) {
            this.navigationHistoryProperties['navigatedPagesNameArray'].push(document.getElementsByTagName('ion-router-outlet')[1].childNodes[i].childNodes[0].textContent);
            this.navigationHistoryProperties['navigatedPagesPathArray'].push(document.getElementsByTagName('ion-router-outlet')[1].children[i].tagName.toLowerCase());
        }
    }
    histListNav(e) {
        if (e.currentTarget.getAttribute('data-index') == 'app-homepage') {
            this.appUtilityConfig.navigateToHomepage();
        } else {
            this.router.navigate([`/menu/${e.currentTarget.getAttribute('data-index')}`]);
        }
    }
    previousPageNavigation() {
        this.router.navigate(["/menu/" + this.navigationHistoryProperties['previousPage']]);
    }
    secondPreviousPageNavigation() {
        this.router.navigate(["/menu/" + this.navigationHistoryProperties['secondPreviousPage']]);
    }

    ngOnInit() {
        this.getNavigationHistory()
    }


    // fetchWorkFlowUserApprovalStatus() {

    //     var pfmApproveUserStatusHierarchyJSON = {
    //         "objectId": this.executionDbConfigObject.workFlowUserApprovalStatusObject,
    //         "objectName": this.executionDbConfigObject.workFlowUserApprovalStatusObject,
    //         "fieldId": 0,
    //         "objectType": "PRIMARY",
    //         "relationShipType": null,
    //         "childObject": [

    //         ]
    //     };
    //     const options = {};
    //     const selector = {}
    //     selector['data.type'] = this.executionDbConfigObject.workFlowUserApprovalStatusObject
    //     selector['data.workflowExectionId'] = this.obj_pfm147274['systemAttributes']['workFlowExecID']
    //     selector['data.statusWFConfigId'] = Number(this.obj_pfm147274['systemAttributes']['statusWFConfigId'])

    //     options['selector'] = selector;

    //     pfmApproveUserStatusHierarchyJSON['options'] = options;

    //     return this.cspfmexecutionPouchDbProvider.fetchDataWithReference(pfmApproveUserStatusHierarchyJSON).then(result => {
    //         if (result && result.status == 'SUCCESS') {

    //             if (result['records'].length > 0) {
    //                 var approvalStatusList = result['records'][0]['approvalStatus']
    //                 this.WorkFlowUserApprovalStatusDataObject = result['records'][0];
    //                 var loggedUserStatus = approvalStatusList.filter(userDataObject => userDataObject.userId == this.appUtilityConfig.userId);

    //                 if (loggedUserStatus.length > 0) {
    //                     this.cscomponentactionInfo['workflowApproveStatusObject'] = loggedUserStatus[0]
    //                 }
    //                 else {
    //                     this.cscomponentactionInfo['workflowApproveStatusObject'] = ""
    //                 }


    //             }
    //             else {
    //                 this.cscomponentactionInfo['workflowApproveStatusObject'] = ""

    //             }



    //         }
    //         else {

    //         }

    //     }).catch(err => {


    //     });
    // }

    // private setExecutionData() {

    //     if (this.obj_pfm147274['systemAttributes']
    //         && this.obj_pfm147274['systemAttributes']['lockedStatus'] == 'INPROGRESS') {
    //         this.cscomponentactionInfo['executionId'] = this.obj_pfm147274['systemAttributes']['workFlowExecID']

    //         if (this.obj_pfm147274['systemAttributes']['workFlowExecID'] != "")
    //             this.fetchWorkFlowUserApprovalStatus()
    //     }
    // }

    // approveAction(selectedStatusField, workFlowUserApprovalStatusDataObject) {	
    //     this.WorkFlowUserApprovalStatusDataObject = workFlowUserApprovalStatusDataObject;

    //     this.WorkFlowUserApprovalStatusDataObject['lastmodifiedby'] = this.appUtilityConfig.userId
    //     var userObjectList = this.WorkFlowUserApprovalStatusDataObject['approvalStatus'].filter(userDataObject => userDataObject.userId == this.appUtilityConfig.userId);
    //     var userObject = userObjectList[0]

    //     //   userObject['userId'] = this.appUtilityConfig.userId
    //     userObject['userName'] = this.appUtilityConfig.loggedUserName
    //     userObject['userLevel'] = ""
    //     userObject['statusValue'] = selectedStatusField['statusValue']
    //     userObject['statusType'] = selectedStatusField['statusType']
    //     userObject['approvalExecutionStatus'] = "INPROGRESS"
    //     userObject['execStatusMessage'] = ""
    //     userObject['comment'] = ""
    //     this.cspfmexecutionPouchDbProvider.save(this.executionDbConfigObject.workFlowUserApprovalStatusObject, 
    //         this.WorkFlowUserApprovalStatusDataObject).then(result => {

    //         if (result['status'] != 'SUCCESS') {
    //             alert("failed")

    //             return;
    //         }
    //         this.presentToast("data saved sucessfully")
    //     })
    // }
    // async showAlert(messageContent) {

    //     var customAlert = await this.alerCtrl.create({
    //     backdropDismiss: false,
    //     message: messageContent,
    //     buttons: [

    //     {
    //     text: "OK",
    //     cssClass: "method-color",
    //     handler: () => {

    //     }
    //     }
    //     ]
    //     });
    //     customAlert.present();

    //     }

    ionViewDidLoad() {}

    ionViewWillEnter() {
        document.body.setAttribute('class', 'linelistinnerdetail');
        this.drawerComponentCurrentState = DrawerState.Top;
        for (let i = 0; i < document.getElementsByClassName('cs-divider-dv').length; i++) {
            document.getElementsByClassName('cs-divider-dv')[i].classList.remove('bottom');
            document.getElementsByClassName('cs-divider-dv')[i].classList.remove('center');
            document.getElementsByClassName('cs-divider-dv')[i].classList.add('top');
        }
    }

    ionViewWillLeave() {
        this.drawerComponentCurrentState = DrawerState.Bottom
    }

    ngOnDestroy() {
        console.log('unsubscribe of 2 list')
        this.events.unsubscribe(this.layoutId);
        this.appUtilityConfig.removeEventSubscriptionlayoutIds(this.tableName_pfm147274, this.layoutId)

    }

    ionViewDidEnter() {
        var dvHeader = document.querySelector(".detail-view-sub-header");
        dvHeader.setAttribute("color", "var(--ion-color-primary, #3880ff)");
        var dvHeaderItem = document.querySelector(
            ".detail-view-sub-header ion-item"
        );
        dvHeaderItem.setAttribute("color", "var(--ion-color-primary, #3880ff)");
        var dvHeaderListHd = document.querySelectorAll(
            ".hl-full-detail-content ion-list-header"
        );
        var dvHeaderListHdLen = dvHeaderListHd.length;
        for (var i = 0; i < dvHeaderListHdLen; i++) {
            dvHeaderListHd[i].setAttribute(
                "color",
                "var(--ion-color-primary, #3880ff)"
            );
        }
        var pvHdItembg = document.querySelectorAll(
            ".detail-view-sub-header ion-badge"
        );
        var pvHdItembgLen = pvHdItembg.length;
        for (var i = 0; i < pvHdItembgLen; i++) {
            pvHdItembg[i].setAttribute(
                "background",
                "var(--ion-color-primary-tint, #4c8dff)"
            );
        }
    }

    dockerButtonOnClick() {
        if (this.drawerComponentCurrentState == DrawerState.Bottom) {
            this.drawerComponentCurrentState = DrawerState.Docked;
            this.drawerComponentPreviousState = DrawerState.Bottom;
            // for (let i = 0; i < document.getElementsByClassName('cs-divider-dv').length; i++) {
            //   document.getElementsByClassName('cs-divider-dv')[i].classList.remove('bottom');
            //   document.getElementsByClassName('cs-divider-dv')[i].classList.remove('top');
            //   document.getElementsByClassName('cs-divider-dv')[i].classList.add('center');
            // }              
        } else if (this.drawerComponentCurrentState == DrawerState.Docked) {
            if (this.drawerComponentPreviousState == DrawerState.Bottom) {
                this.drawerComponentCurrentState = DrawerState.Top;
                // for (let i = 0; i < document.getElementsByClassName('cs-divider-dv').length; i++) {
                //   document.getElementsByClassName('cs-divider-dv')[i].classList.remove('bottom');
                //   document.getElementsByClassName('cs-divider-dv')[i].classList.remove('center');
                //   document.getElementsByClassName('cs-divider-dv')[i].classList.add('top');
                // }


            } else {
                this.drawerComponentCurrentState = DrawerState.Bottom;
                // for (let i = 0; i < document.getElementsByClassName('cs-divider-dv').length; i++) {
                //   document.getElementsByClassName('cs-divider-dv')[i].classList.remove('center');
                //   document.getElementsByClassName('cs-divider-dv')[i].classList.remove('top');
                //   document.getElementsByClassName('cs-divider-dv')[i].classList.add('bottom');
                // }

            }
            this.drawerComponentPreviousState = DrawerState.Docked;
        } else if (this.drawerComponentCurrentState == DrawerState.Top) {
            this.drawerComponentCurrentState = DrawerState.Docked;
            this.drawerComponentPreviousState = DrawerState.Top;
            // for (let i = 0; i < document.getElementsByClassName('cs-divider-dv').length; i++) {
            //   document.getElementsByClassName('cs-divider-dv')[i].classList.remove('bottom');
            //   document.getElementsByClassName('cs-divider-dv')[i].classList.remove('top');
            //   document.getElementsByClassName('cs-divider-dv')[i].classList.add('center');
            // }


        }
    }

    async presentToast(message) {
        const toast = await this.toastCtrl.create({
            message: message,
            duration: 2000,
            position: 'bottom'
        });
        toast.dismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present();
    }

    async presentNoInternetToast() {
        const toast = await this.toastCtrl.create({
            message: "No internet connection. Please check your internet connection and try again.",
            showCloseButton: true,
            closeButtonText: "retry",
        });
        toast.onDidDismiss().then(() => {
            toast.dismiss();
            this.refreshData();
        });
        toast.present();
    }

    refreshData() {
        this.fetchSelectedObject();
    }

    backButtonOnclick() {
        this.router.navigate([this.redirectUrl], {
            skipLocationChange: true
        });
    }

    openurl(events, url) {
        events.stopPropagation();
        if (url.indexOf('http') == 0) {
            window.open(url)
        } else {
            window.open('http://'.concat(url))
        }
    }

    editButton_elementId_Onclick() {
        const editNavigationParams = {
            action: 'Edit',
            id: this.obj_pfm147274['id'],
            parentObj: JSON.stringify(this.headerDocItem),
            parentFieldLabel: this.parentObjLabel,
            parentFieldValue: this.parentObjValue,
            parentId: this.headerDocItem['id']
        }
        this.toastCtrl.dismiss();
        if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/ContactEntry_MOBILE_Grid")) {
            editNavigationParams['redirectUrl'] = "/menu/contactView_MOBILE_Grid"
        }
        this.router.navigate(["/menu/ContactEntry_MOBILE_Grid"], {
            queryParams: editNavigationParams,
            skipLocationChange: true
        });
    }
}