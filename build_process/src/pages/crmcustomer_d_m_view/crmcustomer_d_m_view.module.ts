import { NgModule } from '@angular/core';
          import { CommonModule } from '@angular/common';
          import { FormsModule } from '@angular/forms';
          import { Routes, RouterModule } from '@angular/router';
          import { IonicModule } from '@ionic/angular';
           import {crmcustomer_d_m_view} from './crmcustomer_d_m_view';           
           import { SharedModule } from 'src/core/utils/shared.module';
           import { QuillModule } from 'ngx-quill';
           import { cs_whocolumn_iconmodule } from 'src/core/components/cs_whocolumn_icon/cs_whocolumn_icon.module';
           import { cs_status_workflowmodule } from '../../core/components/cs_status_workflow/cs_status_workflow.module';

           const routes: Routes = [
            {
              path: '',
              component: crmcustomer_d_m_view
            }
          ];
           
             @NgModule({ 
              imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes),
                cs_whocolumn_iconmodule,
                QuillModule.forRoot(),
                SharedModule,
                cs_status_workflowmodule
              ],
                 declarations:[crmcustomer_d_m_view]
                })
                export class crmcustomer_d_m_viewmodule{};