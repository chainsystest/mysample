 import {
     Component,
     ViewChild,
     OnInit,
     ApplicationRef,
     ChangeDetectorRef,
     HostListener,
     NgZone
 } from '@angular/core';
 import {
     dbProvider
 } from 'src/core/db/dbProvider';
 import {
     offlineDbIndexCreation
 } from 'src/core/utils/offlineDbIndexCreation';
 import {
     dataProvider
 } from 'src/core/utils/dataProvider';
 import {
     appConstant
 } from 'src/core/utils/appConstant';
 import {
     ToastController,
     LoadingController,
     Events,
     AlertController
 } from '@ionic/angular';
 import * as lodash from 'lodash';
 import {
     appUtility
 } from 'src/core/utils/appUtility';
 import {
     Router,
     ActivatedRoute
 } from '@angular/router';
 import {
     EmailComposer
 } from '@ionic-native/email-composer/ngx';
 import {
     SocialSharing
 } from '@ionic-native/social-sharing/ngx';
 import {
     SMS
 } from '@ionic-native/sms/ngx';
 import {
     CallNumber
 } from '@ionic-native/call-number/ngx';
 import * as _ from 'underscore';
 import {
     DatePipe
 } from '@angular/common';
 import {
     registerLocaleData
 } from '@angular/common';
 import {
     metaDataDbProvider
 } from 'src/core/db/metaDataDbProvider';
 import {
     metaDbConfiguration
 } from 'src/core/db/metaDbConfiguration';
 import {
     cspfmExecutionPouchDbProvider
 } from 'src/core/db/cspfmExecutionPouchDbProvider';
 import {
     cspfmExecutionPouchDbConfiguration
 } from 'src/core/db/cspfmExecutionPouchDbConfiguration';
 import {
     FieldInfo
 } from "src/core/pipes/cspfm_data_display";
 import India from '@angular/common/locales/en-IN';
 registerLocaleData(India);
 @Component({
     selector: 'crmcustomer_d_m_view',
     templateUrl: './crmcustomer_d_m_view.html'
 }) export class crmcustomer_d_m_view implements OnInit {
     constructor(public dataProvider: dataProvider, public appUtilityConfig: appUtility, public socialShare: SocialSharing, public callNumber: CallNumber, public emailComposer: EmailComposer, public sms: SMS, public router: Router, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public activatRoute: ActivatedRoute, public events: Events, public applicationRef: ChangeDetectorRef, public metaDbConfigurationObj: metaDbConfiguration, public metaDbProvider: metaDataDbProvider, public cspfmexecutionPouchDbProvider: cspfmExecutionPouchDbProvider,
         public executionDbConfigObject: cspfmExecutionPouchDbConfiguration, private datePipe: DatePipe,
         public alerCtrl: AlertController, ) {
         this.activatRoute.queryParams.subscribe(params => {
             if (Object.keys(params).length == 0 && params.constructor === Object) {
                 console.log("list query params skipped");
                 return
             }
             if (params["redirectUrl"]) {
                 this.redirectUrl = params["redirectUrl"]
             }
             this.id = params["id"];

             this.fetchSelectedObject();
         });
         this.appUtilityConfig.setEventSubscriptionlayoutIds(this.tableName_pfm141093, this.layoutId);
         this.events.subscribe(this.layoutId, (modified) => {
             if (modified['dataProvider'] == 'PouchDB' && modified['dataProvider'] != 'JsonDB') {
                 this.childObjectModifiedEventTrigger(modified);
             }
         });
     }
     public obj_pfm141093: any = {};
     public gridFieldInfo: {
         [key: string]: FieldInfo
     } = {
         "pfm141093_customerstatus": {
             "label": "crmcustomer_d_m_view.Element.crmcustomer.customerstatus",
             "fieldName": "customerstatus",
             "prop": "customerstatus",
             "fieldType": "DROPDOWN",
             "child": "",
             "dateFormat": "",
             "mappingDetails": {
                 "None": "None",
                 "Active": "Active",
                 "Inactive": "Inactive",
                 "Duplicate": "Duplicate",
                 "Blacklisted": "Blacklisted"
             },
             "currencyDetails": ""
         },
         "pfm141093_customercontact": {
             "label": "crmcustomer_d_m_view.Element.crmcustomer.customercontact",
             "fieldName": "customercontact",
             "prop": "customercontact",
             "fieldType": "TEXT",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm141093_email": {
             "label": "crmcustomer_d_m_view.Element.crmcustomer.email",
             "fieldName": "email",
             "prop": "email",
             "fieldType": "EMAIL",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm141093_name": {
             "label": "crmcustomer_d_m_view.Element.crmcustomer.name",
             "fieldName": "name",
             "prop": "name",
             "fieldType": "AUTONUMBER",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm141093_customername": {
             "label": "crmcustomer_d_m_view.Element.crmcustomer.customername",
             "fieldName": "customername",
             "prop": "customername",
             "fieldType": "TEXT",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm141093_website": {
             "label": "crmcustomer_d_m_view.Element.crmcustomer.website",
             "fieldName": "website",
             "prop": "website",
             "fieldType": "URL",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm141093_customerrisktype": {
             "label": "crmcustomer_d_m_view.Element.crmcustomer.customerrisktype",
             "fieldName": "customerrisktype",
             "prop": "customerrisktype",
             "fieldType": "DROPDOWN",
             "child": "",
             "dateFormat": "",
             "mappingDetails": {
                 "None": "None",
                 "High": "High",
                 "Medium": "Medium",
                 "Low": "Low"
             },
             "currencyDetails": ""
         },
         "pfm141093_phone": {
             "label": "crmcustomer_d_m_view.Element.crmcustomer.phone",
             "fieldName": "phone",
             "prop": "phone",
             "fieldType": "TEXT",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm141093_shiptolocation": {
             "label": "crmcustomer_d_m_view.Element.crmcustomer.shiptolocation",
             "fieldName": "shiptolocation",
             "prop": "shiptolocation",
             "fieldType": "TEXTAREA",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm141093_billtolocation": {
             "label": "crmcustomer_d_m_view.Element.crmcustomer.billtolocation",
             "fieldName": "billtolocation",
             "prop": "billtolocation",
             "fieldType": "TEXTAREA",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm141093_annualrevenue": {
             "label": "crmcustomer_d_m_view.Element.crmcustomer.annualrevenue",
             "fieldName": "annualrevenue",
             "prop": "annualrevenue",
             "fieldType": "CURRENCY",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": {
                 "currencyCode": "₹",
                 "display": true,
                 "digitsInfo": "1.2-2",
                 "locale": "en-IN"
             }
         },
         "pfm141093_customernumber": {
             "label": "crmcustomer_d_m_view.Element.crmcustomer.customernumber",
             "fieldName": "customernumber",
             "prop": "customernumber",
             "fieldType": "TEXT",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm141093_userid": {
             "label": "crmcustomer_d_m_view.Element.crmcustomer.userid",
             "fieldName": "userid",
             "prop": "userid",
             "fieldType": "NUMBER",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         }
     };
     public formulaReverseObjectHierarchyJSON = undefined;
     public formulaConfigJSON = {};
     public formulafields = {};
     private customerstatus_284116 = {
         'None': 'None',
         'Active': 'Active',
         'Inactive': 'Inactive',
         'Duplicate': 'Duplicate',
         'Blacklisted': 'Blacklisted'
     };
     private customerrisktype_284133 = {
         'None': 'None',
         'High': 'High',
         'Medium': 'Medium',
         'Low': 'Low'
     };
     private tableName_pfm141093 = 'pfm141093';
     public errorMessageToDisplay = "No Records";
     public dbServiceProvider = appConstant.pouchDBStaticName;
     public formulaObject = {};
     private currentStatusWorkFlowActionFieldId;
     public WorkFlowUserApprovalStatusDataObject = {};
     private workFlowMapping = {

     }


     public id: any = '';

     private objectHierarchyJSON = {
         "objectName": "crmcustomer",
         "objectType": "PRIMARY",
         "relationShipType": "",
         "fieldId": "0",
         "objectId": "141093",
         "childObject": []
     };
     private prominentDataMapping = {
         "pfm141093": ["name", "customerstatus", "customercontact", "customername", "phone", "customernumber"]
     };;
     private documentInfo = [];
     private attachmentInfo = [];
     private redirectUrl = "/";
     public isSkeletonLoading = true;
     private approverType: string = "";
     private cscomponentactionInfo = {

     };
     public layoutId = "56278";
     private loading;
     childObjectModifiedEventTrigger(modified) {
         const modifiedData = this.dataProvider.convertRelDocToNormalDoc(modified);
         if (modifiedData["id"] === this.id) {
             this.fetchSelectedObject();
         }
     }
     ngOnDestroy() {
         this.events.unsubscribe(this.tableName_pfm141093);
         this.appUtilityConfig.removeEventSubscriptionlayoutIds(this.tableName_pfm141093, this.layoutId)
     }
     backButtonOnclick() {
         this.toastCtrl.dismiss();
         this.router.navigate([this.redirectUrl], {
             skipLocationChange: true
         });
     }

     async fetchSelectedObject() {


         let additionalObjectdata = {};
         additionalObjectdata['id'] = this.id;
         const fetchParams = {
             'objectHierarchyJSON': this.objectHierarchyJSON,
             'additionalInfo': additionalObjectdata,
             'dataSource': appConstant.pouchDBStaticName
         }
         this.dataProvider.querySingleDoc(fetchParams).then(result => {
             this.isSkeletonLoading = false;
             if (result["status"] !== "SUCCESS") {
                 this.errorMessageToDisplay = result["message"];
                 if (this.errorMessageToDisplay === "No internet") {
                     this.presentNoInternetToast();
                 }
                 return;
             }
             this.obj_pfm141093 = result["records"][0];




             if (!this.applicationRef['destroyed']) {
                 this.applicationRef.detectChanges();
             }

         }).catch(error => {
             this.isSkeletonLoading = false;
             console.log(error);
         });
     }


     async presentToast(message) {
         const toast = await this.toastCtrl.create({
             message: message,
             duration: 2000,
             position: 'bottom'
         });
         toast.dismiss(() => {
             console.log('Dismissed toast');
         });
         toast.present();
     }

     async presentNoInternetToast() {
         const toast = await this.toastCtrl.create({
             message: "No internet connection. Please check your internet connection and try again.",
             showCloseButton: true,
             closeButtonText: "retry"
         });
         toast.onDidDismiss().then(() => {
             toast.dismiss();
             this.refreshDataInretry();
         });
         toast.present();
     }

     refreshDataInretry() {
         this.fetchSelectedObject();
     }
     ngOnInit() {}
     editButton_elementId_Onclick() {
         const editNavigationParams = {
             action: 'Edit',
             id: this.obj_pfm141093['id'],
             parentObj: '',
             parentFieldLabel: '',
             parentFieldValue: '',
             parentId: ''
         }
         if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/crmcustomer_d_m_entry")) {
             editNavigationParams['redirectUrl'] = "/menu/crmcustomer_d_m_view"
         }
         this.toastCtrl.dismiss();
         this.router.navigate(["/menu/crmcustomer_d_m_entry"], {
             queryParams: editNavigationParams,
             skipLocationChange: true
         });
     }
 }