import {
    Component,
    OnInit,
    ApplicationRef,
    HostListener
} from '@angular/core';
import {
    dataProvider
} from 'src/core/utils/dataProvider';
import {
    appConstant
} from 'src/core/utils/appConstant';
import {
    ModalController,
    Platform,
    LoadingController,
    Events,
    ToastController,
    AlertController
} from '@ionic/angular';
import {
    DrawerState
} from 'ion-bottom-drawer';
import {
    PopoverController
} from '@ionic/angular';
import {
    appUtility
} from 'src/core/utils/appUtility';
import {
    ScreenOrientation
} from '@ionic-native/screen-orientation/ngx';
import {
    lookupFieldMapping
} from 'src/core/pfmmapping/lookupFieldMapping';
import {
    objectTableMapping
} from 'src/core/pfmmapping/objectTableMapping';
import * as moment from 'moment';
import {
    Router,
    ActivatedRoute
} from '@angular/router';
import {
    SocialSharing
} from '@ionic-native/social-sharing/ngx';
import {
    EmailComposer
} from '@ionic-native/email-composer/ngx';
import {
    CallNumber
} from '@ionic-native/call-number/ngx';
import {
    SMS
} from '@ionic-native/sms/ngx';
import {
    attachmentDbProvider
} from 'src/core/db/attachmentDbProvider';
import * as _ from 'underscore';
import * as lodash from 'lodash';
import {
    DatePipe
} from '@angular/common';
import {
    registerLocaleData
} from '@angular/common';
import {
    metaDataDbProvider
} from 'src/core/db/metaDataDbProvider';
import {
    metaDbConfiguration
} from 'src/core/db/metaDbConfiguration';
import {
    cspfmExecutionPouchDbProvider
} from 'src/core/db/cspfmExecutionPouchDbProvider';
import {
    cspfmExecutionPouchDbConfiguration
} from 'src/core/db/cspfmExecutionPouchDbConfiguration';
import {
    FieldInfo
} from "src/core/pipes/cspfm_data_display";
import India from '@angular/common/locales/en-IN';
registerLocaleData(India);
@Component({
    selector: 'crmsaleorderlines_d_m_hl_detail_view',
    templateUrl: './crmsaleorderlines_d_m_hl_detail_view.html'
}) export class crmsaleorderlines_d_m_hl_detail_view implements OnInit {
    constructor(public popoverCtrl: PopoverController, public modalCtrl: ModalController,
        private socialSharing: SocialSharing, private emailComposer: EmailComposer, private callNumber: CallNumber, private sms: SMS,
        platform: Platform, public applicationRef: ApplicationRef, public appUtilityConfig: appUtility, public screenOrientation: ScreenOrientation,
        public events: Events, public router: Router, public activatRoute: ActivatedRoute,
        public objectTableMapping: objectTableMapping, public lookupFieldMapping: lookupFieldMapping,
        public loadingCtrl: LoadingController, public toastCtrl: ToastController, public dataProvider: dataProvider, public metaDbConfigurationObj: metaDbConfiguration, public metaDbProvider: metaDataDbProvider, public cspfmexecutionPouchDbProvider: cspfmExecutionPouchDbProvider,
        public executionDbConfigObject: cspfmExecutionPouchDbConfiguration, private datePipe: DatePipe,
        public alerCtrl: AlertController) {

        this.activatRoute.queryParams.subscribe(params => {
            if (Object.keys(params).length == 0 && params.constructor === Object) {
                console.log("list query params skipped");
                return
            }
            if (params["redirectUrl"]) {
                this.redirectUrl = params["redirectUrl"]
            }
            // this.headerDocItem = JSON.parse(params["parentObj"]);
            // this.parentTitle = params["parentTitle"];
            // this.parentObjLabel = params["parentFieldLabel"];
            // this.parentObjValue = params["parentFieldValue"];
            this.id = params["id"];
            this.prominentDataArray = params["prominientObjectInfo"];
            //change
            this.fetchSelectedObject();
        });
        this.appUtilityConfig.setEventSubscriptionlayoutIds(this.tableName_pfm144393, this.layoutId)

        if (!this.appUtilityConfig.isMobile || this.appUtilityConfig.osType == 'android') {
            this.isBrowser = true;
        };

        const windowHeight = window.innerHeight;
        this.drawerComponentDockedHeight = windowHeight / 2;



        this.screenOrientation.onChange().subscribe(
            () => {
                this.drawerComponentCurrentState = DrawerState.Bottom;
                const windowHeightVal = window.innerHeight;
                this.drawerComponentDockedHeight = windowHeightVal / 2;
            });


        this.events.subscribe(this.layoutId, (modified) => {
            if (modified["dataProvider"] == "PouchDB") {
                this.childObjectModifiedEventTrigger(modified);
            }
        });

    }
    private approverType: string = "";
    private cscomponentactionInfo = {};
    childObjectModifiedEventTrigger(modified) {
        const modifiedData = this.dataProvider.convertRelDocToNormalDoc(modified);
        if (modifiedData['id'] == this.id) {
            this.fetchSelectedObject();
        }
    }
    public obj_pfm144393: any = {};
    private obj_pfm147213_301933: any = {};
    private obj_pfm142973_289353: any = {};
    private obj_pfm144373: any = {};
    public gridFieldInfo: {
        [key: string]: FieldInfo
    } = {
        "pfm144393_listprice": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorderlines.listprice",
            "fieldName": "listprice",
            "prop": "listprice",
            "fieldType": "CURRENCY",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": {
                "currencyCode": "₹",
                "display": true,
                "digitsInfo": "1.2-2",
                "locale": "en-IN"
            }
        },
        "pfm144393_totalamount": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorderlines.totalamount",
            "fieldName": "totalamount",
            "prop": "totalamount",
            "fieldType": "FORMULA",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm144393_productdescription": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorderlines.productdescription",
            "fieldName": "productdescription",
            "prop": "productdescription",
            "fieldType": "TEXTAREA",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm144393_discountpercentage": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorderlines.discountpercentage",
            "fieldName": "discountpercentage",
            "prop": "discountpercentage",
            "fieldType": "NUMBER",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm144393_olineid": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorderlines.olineid",
            "fieldName": "olineid",
            "prop": "olineid",
            "fieldType": "AUTONUMBER",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm142973_listprice": {
            "child": {
                "label": "",
                "fieldName": "listprice",
                "prop": "listprice",
                "fieldType": "CURRENCY",
                "child": "",
                "dateFormat": "",
                "mappingDetails": "",
                "currencyDetails": {
                    "currencyCode": "₹",
                    "display": true,
                    "digitsInfo": "1.2-2",
                    "locale": "en-IN"
                }
            },
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": "",
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmproduct.listprice",
            "prop": "pfm142973_289353.listprice",
            "fieldName": "pfm142973_289353",
            "fieldType": "LOOKUP"
        },
        "pfm144393_partno": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorderlines.partno",
            "fieldName": "partno",
            "prop": "partno",
            "fieldType": "TEXT",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm144393_taxpercentage": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorderlines.taxpercentage",
            "fieldName": "taxpercentage",
            "prop": "taxpercentage",
            "fieldType": "NUMBER",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm144393_product": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorderlines.product",
            "prop": "pfm142973_289353.product",
            "fieldName": "pfm142973_289353",
            "fieldType": "LOOKUP",
            "child": {
                "child": "",
                "dateFormat": "",
                "mappingDetails": "",
                "currencyDetails": "",
                "label": "name",
                "prop": "name",
                "fieldName": "name",
                "fieldType": "AUTONUMBER"
            },
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm144393_quantity": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorderlines.quantity",
            "fieldName": "quantity",
            "prop": "quantity",
            "fieldType": "NUMBER",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm144373_orderid": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorders.orderid",
            "fieldName": "orderid",
            "prop": "orderid",
            "fieldType": "AUTONUMBER",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm144373_shiptoaddress": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorders.shiptoaddress",
            "fieldName": "shiptoaddress",
            "prop": "shiptoaddress",
            "fieldType": "TEXTAREA",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm144373_customernumber": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorders.customernumber",
            "fieldName": "customernumber",
            "prop": "customernumber",
            "fieldType": "TEXT",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm144373_customername": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorders.customername",
            "prop": "pfm147213_301933.customername",
            "fieldName": "pfm147213_301933",
            "fieldType": "LOOKUP",
            "child": {
                "child": "",
                "dateFormat": "",
                "mappingDetails": "",
                "currencyDetails": "",
                "label": "accountno",
                "prop": "accountno",
                "fieldName": "accountno",
                "fieldType": "AUTONUMBER"
            },
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm144373_orderstatus": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorders.orderstatus",
            "fieldName": "orderstatus",
            "prop": "orderstatus",
            "fieldType": "DROPDOWN",
            "child": "",
            "dateFormat": "",
            "mappingDetails": {
                "Draft": "Draft",
                "In Review": "In Review",
                "Approved": "Approved",
                "Rejected": "Rejected",
                "Confirmed": "Confirmed",
                "Processed": "Processed",
                "Shipped": "Shipped",
                "Delivered": "Delivered",
                "Completed": "Completed",
                "Cancelled": "Cancelled"
            },
            "currencyDetails": ""
        },
        "pfm144373_ordercompleteddate": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorders.ordercompleteddate",
            "fieldName": "ordercompleteddate",
            "prop": "ordercompleteddate",
            "fieldType": "DATE",
            "child": "",
            "dateFormat": this.appUtilityConfig.userDateFormat,
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147213_accountname": {
            "child": {
                "label": "",
                "fieldName": "accountname",
                "prop": "accountname",
                "fieldType": "TEXT",
                "child": "",
                "dateFormat": "",
                "mappingDetails": "",
                "currencyDetails": ""
            },
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": "",
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.customermaster.accountname",
            "prop": "pfm147213_301933.accountname",
            "fieldName": "pfm147213_301933",
            "fieldType": "LOOKUP"
        },
        "pfm144373_billtoaddress": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorders.billtoaddress",
            "fieldName": "billtoaddress",
            "prop": "billtoaddress",
            "fieldType": "TEXTAREA",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm144373_orderconfirmeddate": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorders.orderconfirmeddate",
            "fieldName": "orderconfirmeddate",
            "prop": "orderconfirmeddate",
            "fieldType": "DATE",
            "child": "",
            "dateFormat": this.appUtilityConfig.userDateFormat,
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm144373_customercontact": {
            "label": "crmsaleorderlines_d_m_hl_detail_view.Element.crmsaleorders.customercontact",
            "fieldName": "customercontact",
            "prop": "customercontact",
            "fieldType": "TEXT",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        }
    };
    private orderstatus_289221 = {
        'Draft': 'Draft',
        'In Review': 'In Review',
        'Approved': 'Approved',
        'Rejected': 'Rejected',
        'Confirmed': 'Confirmed',
        'Processed': 'Processed',
        'Shipped': 'Shipped',
        'Delivered': 'Delivered',
        'Completed': 'Completed',
        'Cancelled': 'Cancelled'
    }; // @ViewChild(Navbar) navbar: Navbar
    isBrowser: boolean = false;
    headerenable = false;
    drawerComponentDockedHeight = 300;
    drawerComponentCurrentState = DrawerState.Top
    drawerComponentPreviousState = DrawerState.Docked

    headerDocItem: any = {};
    public navigationHistoryProperties = {
        'navigatedPagesNameArray': [],
        'navigatedPagesPathArray': [],
        'routerVisLinkTagName': "",
        'secondPreviousPage': "",
        'navigatedPagesLength': 0,
        'previousPage': "",
        'previousPageName': "",
        'secondPreviousPageName': "",
    };
    public layoutId = "57267";
    public isSkeletonLoading = true;

    private tableName_pfm144393 = 'pfm144393';
    public parentObjLabel = '';
    public parentObjValue = '';
    public formulaObject = {};

    public parentTitle: any = 'Sales Order';
    public pageTitle = "";
    public errorMessageToDisplay: string = "No Records";
    public id: any = '';
    public prominentDataArray: Array < any > = [];
    private objectHierarchyJSON = {
        "objectName": "crmsaleorderlines",
        "objectType": "PRIMARY",
        "relationShipType": "",
        "fieldId": "0",
        "objectId": "144393",
        "childObject": [{
            "objectName": "crmproduct",
            "objectType": "LOOKUP",
            "relationShipType": "",
            "fieldId": "289353",
            "objectId": "142973",
            "childObject": []
        }, {
            "objectName": "crmsaleorders",
            "objectType": "HEADER",
            "relationShipType": "",
            "fieldId": "0",
            "objectId": "144373",
            "childObject": [{
                "objectName": "customermaster",
                "objectType": "LOOKUP",
                "relationShipType": "",
                "fieldId": "301933",
                "objectId": "147213",
                "childObject": []
            }]
        }]
    };
    private loading;
    private redirectUrl = "/";
    private WorkFlowUserApprovalStatusDataObject = {};
    public showNavigationHistoryPopUp: Boolean = false;
    private workFlowMapping = {
        "289221": "orderstatus",
    }
    private currentStatusWorkflowActionFiledId;
    public formulaReverseObjectHierarchyJSON = [{
        "toLevel": 1,
        "relationShipType": "one_to_many",
        "isFormulaObject": "Y",
        "childObject": [{
            "toLevel": 1,
            "relationShipType": "",
            "isFormulaObject": "N",
            "childObject": [],
            "objectName": "crmproduct",
            "type": "P",
            "referenceObjectId": "144393",
            "objectId": "142973",
            "objectType": "LOOKUP"
        }],
        "objectName": "crmsaleorderlines",
        "type": "P",
        "referenceObjectId": 0,
        "objectId": "144393",
        "objectType": "PRIMARY"
    }];
    public formulaConfigJSON = {
        "pfm144393": {
            "totalamount": {
                "fieldName": "totalamount",
                "objectId": 144393,
                "displayformula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                "formula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                "operands": [{
                    "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                    "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                    "fieldName": "listprice",
                    "level": "1",
                    "refFieldId": 301754,
                    "fieldType": "currency",
                    "objectId": 144393,
                    "fieldId": 289357,
                    "objectType": "PRIMARY"
                }, {
                    "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                    "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                    "fieldName": "quantity",
                    "level": "1",
                    "refFieldId": 301754,
                    "fieldType": "number",
                    "objectId": 144393,
                    "fieldId": 289359,
                    "objectType": "PRIMARY"
                }, {
                    "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                    "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                    "fieldName": "listprice",
                    "level": "1",
                    "refFieldId": 301754,
                    "fieldType": "currency",
                    "objectId": 144393,
                    "fieldId": 289357,
                    "objectType": "PRIMARY"
                }, {
                    "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                    "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                    "fieldName": "quantity",
                    "level": "1",
                    "refFieldId": 301754,
                    "fieldType": "number",
                    "objectId": 144393,
                    "fieldId": 289359,
                    "objectType": "PRIMARY"
                }, {
                    "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                    "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                    "fieldName": "taxpercentage",
                    "level": "1",
                    "refFieldId": 301754,
                    "fieldType": "number",
                    "objectId": 144393,
                    "fieldId": 302833,
                    "objectType": "PRIMARY"
                }, {
                    "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                    "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                    "fieldName": "listprice",
                    "level": "1",
                    "refFieldId": 301754,
                    "fieldType": "currency",
                    "objectId": 144393,
                    "fieldId": 289357,
                    "objectType": "PRIMARY"
                }, {
                    "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                    "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                    "fieldName": "quantity",
                    "level": "1",
                    "refFieldId": 301754,
                    "fieldType": "number",
                    "objectId": 144393,
                    "fieldId": 289359,
                    "objectType": "PRIMARY"
                }, {
                    "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                    "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                    "fieldName": "discountpercentage",
                    "level": "1",
                    "refFieldId": 301754,
                    "fieldType": "number",
                    "objectId": 144393,
                    "fieldId": 302834,
                    "objectType": "PRIMARY"
                }]
            }
        }
    };
    public dbServiceProvider = appConstant.pouchDBStaticName;
    public formulafields = {
        "pfm144393": ["listprice", "quantity", "taxpercentage", "discountpercentage"]
    };

    public orderstatus_289221_defaultStatusValue = "Draft";
    public orderstatus_289221_defaultStatus = {};
    private orderstatus_289221_status = {};
    public orderstatus_289221_swList = {
        "Draft": [{
            "statusLabel": "Draft",
            "statusValue": "Draft",
            "statusType": "Start",
            "statusWFConfigId": 267,
            "isApproveInitiateEnabled": "N",
            "isForwardEnabled": "N"
        }, {
            "statusLabel": "In Review",
            "statusValue": "In Review",
            "statusType": "Initiate",
            "statusWFConfigId": 268,
            "isApproveInitiateEnabled": "Y",
            "isForwardEnabled": "N"
        }],
        "In Review": [{
            "statusLabel": "In Review",
            "statusValue": "In Review",
            "statusType": "Initiate",
            "statusWFConfigId": 268,
            "isApproveInitiateEnabled": "Y",
            "isForwardEnabled": "N"
        }, {
            "statusLabel": "Approved",
            "statusValue": "Approved",
            "statusType": "Approved",
            "statusWFConfigId": 269,
            "isApproveInitiateEnabled": "N",
            "isForwardEnabled": "N"
        }, {
            "statusLabel": "Rejected",
            "statusValue": "Rejected",
            "statusType": "Reject",
            "statusWFConfigId": 270,
            "isApproveInitiateEnabled": "N",
            "isForwardEnabled": "N"
        }],
        "Approved": [{
            "statusLabel": "Approved",
            "statusValue": "Approved",
            "statusType": "Approved",
            "statusWFConfigId": 269,
            "isApproveInitiateEnabled": "N",
            "isForwardEnabled": "N"
        }, {
            "statusLabel": "Confirmed",
            "statusValue": "Confirmed",
            "statusType": "Manual",
            "statusWFConfigId": 271,
            "isApproveInitiateEnabled": "N",
            "isForwardEnabled": "N"
        }],
        "Rejected": [{
            "statusLabel": "Rejected",
            "statusValue": "Rejected",
            "statusType": "Reject",
            "statusWFConfigId": 270,
            "isApproveInitiateEnabled": "N",
            "isForwardEnabled": "N"
        }],
        "Confirmed": [{
            "statusLabel": "Confirmed",
            "statusValue": "Confirmed",
            "statusType": "Manual",
            "statusWFConfigId": 271,
            "isApproveInitiateEnabled": "N",
            "isForwardEnabled": "N"
        }, {
            "statusLabel": "Processed",
            "statusValue": "Processed",
            "statusType": "Manual",
            "statusWFConfigId": 272,
            "isApproveInitiateEnabled": "N",
            "isForwardEnabled": "N"
        }],
        "Processed": [{
            "statusLabel": "Processed",
            "statusValue": "Processed",
            "statusType": "Manual",
            "statusWFConfigId": 272,
            "isApproveInitiateEnabled": "N",
            "isForwardEnabled": "N"
        }, {
            "statusLabel": "Shipped",
            "statusValue": "Shipped",
            "statusType": "Manual",
            "statusWFConfigId": 273,
            "isApproveInitiateEnabled": "N",
            "isForwardEnabled": "N"
        }],
        "Shipped": [{
            "statusLabel": "Shipped",
            "statusValue": "Shipped",
            "statusType": "Manual",
            "statusWFConfigId": 273,
            "isApproveInitiateEnabled": "N",
            "isForwardEnabled": "N"
        }],
        "Delivered": [{
            "statusLabel": "Delivered",
            "statusValue": "Delivered",
            "statusType": "Manual",
            "statusWFConfigId": 274,
            "isApproveInitiateEnabled": "N",
            "isForwardEnabled": "N"
        }, {
            "statusLabel": "Completed",
            "statusValue": "Completed",
            "statusType": "Close",
            "statusWFConfigId": 276,
            "isApproveInitiateEnabled": "N",
            "isForwardEnabled": "N"
        }],
        "Cancelled": [{
            "statusLabel": "Cancelled",
            "statusValue": "Cancelled",
            "statusType": "Manual",
            "statusWFConfigId": 275,
            "isApproveInitiateEnabled": "N",
            "isForwardEnabled": "N"
        }],
        "Completed": [{
            "statusLabel": "Completed",
            "statusValue": "Completed",
            "statusType": "Close",
            "statusWFConfigId": 276,
            "isApproveInitiateEnabled": "N",
            "isForwardEnabled": "N"
        }]
    };

    async fetchSelectedObject() {


        const additionalObjectdata = {};
        additionalObjectdata['id'] = this.id
        const fetchParams = {
            'objectHierarchyJSON': this.objectHierarchyJSON,
            'additionalInfo': additionalObjectdata,
            'dataSource': appConstant.pouchDBStaticName
        }
        this.dataProvider.querySingleDoc(fetchParams).then(result => {
            this.isSkeletonLoading = false;
            this.obj_pfm144393 = []
            if (result['status'] != 'SUCCESS') {
                const errorMessageToDisplay = result["message"];
                if (errorMessageToDisplay === "No internet") {
                    this.presentNoInternetToast();
                }
                return;
            }

            const pluralName = this.dataProvider.getPluralName(this.tableName_pfm144393);
            this.obj_pfm144393 = result['records'][0];
            this.headerDocItem = this.obj_pfm144393['pfm144373s'][0];

            //   if (this.obj_pfm144393[pluralName][0]) {
            //     this.obj_pfm144393 = this.obj_pfm144393[pluralName][0];
            //   }

            if (this.obj_pfm144373["orderstatus"]) {
                this.orderstatus_289221_defaultStatus = this.orderstatus_289221_swList[this.obj_pfm144373["orderstatus"]].filter(item => {
                    return item['statusValue'] == this.obj_pfm144373["orderstatus"]
                })[0]
            } else {
                this.orderstatus_289221_defaultStatus = this.orderstatus_289221_swList[this.orderstatus_289221_defaultStatusValue][0];
            }

            this.pageTitle = this.obj_pfm144393["name"];
            this.obj_pfm142973_289353 = this.obj_pfm144393['pfm142973_289353'] ? this.obj_pfm144393['pfm142973_289353'] : "";
            this.obj_pfm147213_301933 = this.obj_pfm144373['pfm147213_301933'] ? this.obj_pfm144373['pfm147213_301933'] : "";
            this.formulaReverseObjectHierarchyJSON.forEach(hierarchyJSONObject => {
                this.getFormulaDataObject(this.obj_pfm144393, hierarchyJSONObject)
            });
            this.applicationRef.tick();
        }).catch(error => {
            this.isSkeletonLoading = false;
            console.log(error);
        });
    }
    async getFormulaDataObject(resultObject, objectReverseHierarchyJSON) {
        const fetchParams = {
            'objectReverseHierarchyJSON': objectReverseHierarchyJSON,
            'objectHierarchyJSON': this.objectHierarchyJSON,
            'dataSource': appConstant.pouchDBStaticName,
            'additionalInfo': resultObject
        }
        this.dataProvider.querySingleFormualDoc(fetchParams).then(result => {
            console.log("result = ", result);
            if (result['status'] !== 'SUCCESS') {
                this.errorMessageToDisplay = result['message'];
                if (this.errorMessageToDisplay === "No internet") {
                    this.presentNoInternetToast();
                }
                return
            }
            this.formulaObject = result["records"]
        }).catch(error => {
            console.log("error====", error)
        });
    }
    //   checkLoggedUserIsApprover() {

    //     if (this.obj_pfm144393.hasOwnProperty('systemAttributes')) {
    //         var obj = this.obj_pfm144393['systemAttributes']
    //         if (obj['lockedStatus'] === 'INPROGRESS') {

    //             var pfmApproveUserHierarchyJSON = {
    //                 "objectId": this.metaDbConfigurationObj.pfmApproveValueUserObject,
    //                 "objectName": this.metaDbConfigurationObj.pfmApproveValueUserObject,
    //                 "fieldId": 0,
    //                 "objectType": "PRIMARY",
    //                 "relationShipType": null,
    //                 "childObject": [

    //                 ]
    //             };

    //             const options = {};
    //             const selector = {};
    //             selector['data.type'] = this.metaDbConfigurationObj.pfmApproveValueUserObject
    //             selector['data.user_id'] = Number(this.appUtilityConfig.userId);
    //             selector['data.field_id'] = Number(obj['fieldId'])
    //             selector['data.status_wf_config_id'] = Number(obj['statusWFConfigId'])

    //             options['selector'] = selector;
    //             pfmApproveUserHierarchyJSON['options'] = options;

    //             return this.metaDbProvider.fetchDataWithReference(pfmApproveUserHierarchyJSON).then(result => {
    //                 if (result && result.status == 'SUCCESS') {

    //                     if (result.records.length == 0) {
    //                         this.approverType = "non approver";
    //                         return Promise.resolve(false);
    //                     }

    //                     else {
    //                         this.approverType = "approver";
    //                         this.currentStatusWorkflowActionFiledId = obj['fieldId']
    //                         return Promise.resolve(true);
    //                     }
    //                 }
    //                 else {

    //                 }

    //             }).catch(err => {


    //             });
    //         }
    //         else {
    //             return Promise.resolve(true);
    //         }

    //     }
    //     else {
    //         return Promise.resolve(true);
    //     }
    // }

    //         statusChange(event, selectedStatusField) {
    //             if (selectedStatusField == undefined) {
    //                 selectedStatusField = {}
    //             }

    //             selectedStatusField['statusLabel'] = event['selectedStatus']['statusLabel'];
    //             selectedStatusField['statusValue'] = event['selectedStatus']['statusValue'];
    //             selectedStatusField['statusType'] = event['selectedStatus']['statusType'];
    //             this.approveAction(selectedStatusField, event['workFlowUserApprovalStatusDataObject'])	
    // }	

    //             //Get Approval State from Component	
    //             getApprovalState(event) {	
    //              this.approverType = event['approverType']   
    //         }

    //             fetchLockedUserDetail() {
    //                 var systemAttributes = this.obj_pfm144393['systemAttributes']
    //                 var userId = systemAttributes['lockedBy']
    //                 var date = new Date(systemAttributes['lockedDate']);
    //                 this.showAlert(userId + " has locked for " + this.workFlowMapping[this.obj_pfm144393['systemAttributes']['fieldId']] + " on " + this.datePipe.transform(date, this.appUtilityConfig.userDateFormat));                             
    //             }
    historyButtonAction(event: Event) {
        this.showNavigationHistoryPopUp = !this.showNavigationHistoryPopUp;
        event.stopPropagation();
    }
    @HostListener('click') onClick() {
        this.showNavigationHistoryPopUp = false;
    }
    getNavigationHistory() {
        if (this.appUtilityConfig.getHomePageNode()) {
            let homePageNode = this.appUtilityConfig.getHomePageNode()
            this.navigationHistoryProperties['navigatedPagesNameArray'] = [homePageNode['homePageNodeName']]
            this.navigationHistoryProperties['navigatedPagesPathArray'] = [homePageNode['homePageNodepath']]
        }
        this.navigationHistoryProperties['navigatedPagesLength'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes.length;
        this.navigationHistoryProperties['previousPageName'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 2].childNodes[0].textContent;
        this.navigationHistoryProperties['previousPage'] = ( < HTMLElement > document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 2]).tagName.toLowerCase();
        if (document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3]) {
            this.navigationHistoryProperties['secondPreviousPageName'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3].childNodes[0].textContent;
            this.navigationHistoryProperties['secondPreviousPage'] = ( < HTMLElement > document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3]).tagName.toLowerCase();
        }
        for (let i = 0; i < this.navigationHistoryProperties['navigatedPagesLength'] - 3; i++) {
            this.navigationHistoryProperties['navigatedPagesNameArray'].push(document.getElementsByTagName('ion-router-outlet')[1].childNodes[i].childNodes[0].textContent);
            this.navigationHistoryProperties['navigatedPagesPathArray'].push(document.getElementsByTagName('ion-router-outlet')[1].children[i].tagName.toLowerCase());
        }
    }
    histListNav(e) {
        if (e.currentTarget.getAttribute('data-index') == 'app-homepage') {
            this.appUtilityConfig.navigateToHomepage();
        } else {
            this.router.navigate([`/menu/${e.currentTarget.getAttribute('data-index')}`]);
        }
    }
    previousPageNavigation() {
        this.router.navigate(["/menu/" + this.navigationHistoryProperties['previousPage']]);
    }
    secondPreviousPageNavigation() {
        this.router.navigate(["/menu/" + this.navigationHistoryProperties['secondPreviousPage']]);
    }

    ngOnInit() {
        this.getNavigationHistory()
    }


    // fetchWorkFlowUserApprovalStatus() {

    //     var pfmApproveUserStatusHierarchyJSON = {
    //         "objectId": this.executionDbConfigObject.workFlowUserApprovalStatusObject,
    //         "objectName": this.executionDbConfigObject.workFlowUserApprovalStatusObject,
    //         "fieldId": 0,
    //         "objectType": "PRIMARY",
    //         "relationShipType": null,
    //         "childObject": [

    //         ]
    //     };
    //     const options = {};
    //     const selector = {}
    //     selector['data.type'] = this.executionDbConfigObject.workFlowUserApprovalStatusObject
    //     selector['data.workflowExectionId'] = this.obj_pfm144393['systemAttributes']['workFlowExecID']
    //     selector['data.statusWFConfigId'] = Number(this.obj_pfm144393['systemAttributes']['statusWFConfigId'])

    //     options['selector'] = selector;

    //     pfmApproveUserStatusHierarchyJSON['options'] = options;

    //     return this.cspfmexecutionPouchDbProvider.fetchDataWithReference(pfmApproveUserStatusHierarchyJSON).then(result => {
    //         if (result && result.status == 'SUCCESS') {

    //             if (result['records'].length > 0) {
    //                 var approvalStatusList = result['records'][0]['approvalStatus']
    //                 this.WorkFlowUserApprovalStatusDataObject = result['records'][0];
    //                 var loggedUserStatus = approvalStatusList.filter(userDataObject => userDataObject.userId == this.appUtilityConfig.userId);

    //                 if (loggedUserStatus.length > 0) {
    //                     this.cscomponentactionInfo['workflowApproveStatusObject'] = loggedUserStatus[0]
    //                 }
    //                 else {
    //                     this.cscomponentactionInfo['workflowApproveStatusObject'] = ""
    //                 }


    //             }
    //             else {
    //                 this.cscomponentactionInfo['workflowApproveStatusObject'] = ""

    //             }



    //         }
    //         else {

    //         }

    //     }).catch(err => {


    //     });
    // }

    // private setExecutionData() {

    //     if (this.obj_pfm144393['systemAttributes']
    //         && this.obj_pfm144393['systemAttributes']['lockedStatus'] == 'INPROGRESS') {
    //         this.cscomponentactionInfo['executionId'] = this.obj_pfm144393['systemAttributes']['workFlowExecID']

    //         if (this.obj_pfm144393['systemAttributes']['workFlowExecID'] != "")
    //             this.fetchWorkFlowUserApprovalStatus()
    //     }
    // }

    // approveAction(selectedStatusField, workFlowUserApprovalStatusDataObject) {	
    //     this.WorkFlowUserApprovalStatusDataObject = workFlowUserApprovalStatusDataObject;

    //     this.WorkFlowUserApprovalStatusDataObject['lastmodifiedby'] = this.appUtilityConfig.userId
    //     var userObjectList = this.WorkFlowUserApprovalStatusDataObject['approvalStatus'].filter(userDataObject => userDataObject.userId == this.appUtilityConfig.userId);
    //     var userObject = userObjectList[0]

    //     //   userObject['userId'] = this.appUtilityConfig.userId
    //     userObject['userName'] = this.appUtilityConfig.loggedUserName
    //     userObject['userLevel'] = ""
    //     userObject['statusValue'] = selectedStatusField['statusValue']
    //     userObject['statusType'] = selectedStatusField['statusType']
    //     userObject['approvalExecutionStatus'] = "INPROGRESS"
    //     userObject['execStatusMessage'] = ""
    //     userObject['comment'] = ""
    //     this.cspfmexecutionPouchDbProvider.save(this.executionDbConfigObject.workFlowUserApprovalStatusObject, 
    //         this.WorkFlowUserApprovalStatusDataObject).then(result => {

    //         if (result['status'] != 'SUCCESS') {
    //             alert("failed")

    //             return;
    //         }
    //         this.presentToast("data saved sucessfully")
    //     })
    // }
    // async showAlert(messageContent) {

    //     var customAlert = await this.alerCtrl.create({
    //     backdropDismiss: false,
    //     message: messageContent,
    //     buttons: [

    //     {
    //     text: "OK",
    //     cssClass: "method-color",
    //     handler: () => {

    //     }
    //     }
    //     ]
    //     });
    //     customAlert.present();

    //     }

    ionViewDidLoad() {}

    ionViewWillEnter() {
        document.body.setAttribute('class', 'linelistinnerdetail');
        this.drawerComponentCurrentState = DrawerState.Top;
        for (let i = 0; i < document.getElementsByClassName('cs-divider-dv').length; i++) {
            document.getElementsByClassName('cs-divider-dv')[i].classList.remove('bottom');
            document.getElementsByClassName('cs-divider-dv')[i].classList.remove('center');
            document.getElementsByClassName('cs-divider-dv')[i].classList.add('top');
        }
    }

    ionViewWillLeave() {
        this.drawerComponentCurrentState = DrawerState.Bottom
    }

    ngOnDestroy() {
        console.log('unsubscribe of 2 list')
        this.events.unsubscribe(this.layoutId);
        this.appUtilityConfig.removeEventSubscriptionlayoutIds(this.tableName_pfm144393, this.layoutId)

    }

    ionViewDidEnter() {
        var dvHeader = document.querySelector(".detail-view-sub-header");
        dvHeader.setAttribute("color", "var(--ion-color-primary, #3880ff)");
        var dvHeaderItem = document.querySelector(
            ".detail-view-sub-header ion-item"
        );
        dvHeaderItem.setAttribute("color", "var(--ion-color-primary, #3880ff)");
        var dvHeaderListHd = document.querySelectorAll(
            ".hl-full-detail-content ion-list-header"
        );
        var dvHeaderListHdLen = dvHeaderListHd.length;
        for (var i = 0; i < dvHeaderListHdLen; i++) {
            dvHeaderListHd[i].setAttribute(
                "color",
                "var(--ion-color-primary, #3880ff)"
            );
        }
        var pvHdItembg = document.querySelectorAll(
            ".detail-view-sub-header ion-badge"
        );
        var pvHdItembgLen = pvHdItembg.length;
        for (var i = 0; i < pvHdItembgLen; i++) {
            pvHdItembg[i].setAttribute(
                "background",
                "var(--ion-color-primary-tint, #4c8dff)"
            );
        }
    }

    dockerButtonOnClick() {
        if (this.drawerComponentCurrentState == DrawerState.Bottom) {
            this.drawerComponentCurrentState = DrawerState.Docked;
            this.drawerComponentPreviousState = DrawerState.Bottom;
            // for (let i = 0; i < document.getElementsByClassName('cs-divider-dv').length; i++) {
            //   document.getElementsByClassName('cs-divider-dv')[i].classList.remove('bottom');
            //   document.getElementsByClassName('cs-divider-dv')[i].classList.remove('top');
            //   document.getElementsByClassName('cs-divider-dv')[i].classList.add('center');
            // }              
        } else if (this.drawerComponentCurrentState == DrawerState.Docked) {
            if (this.drawerComponentPreviousState == DrawerState.Bottom) {
                this.drawerComponentCurrentState = DrawerState.Top;
                // for (let i = 0; i < document.getElementsByClassName('cs-divider-dv').length; i++) {
                //   document.getElementsByClassName('cs-divider-dv')[i].classList.remove('bottom');
                //   document.getElementsByClassName('cs-divider-dv')[i].classList.remove('center');
                //   document.getElementsByClassName('cs-divider-dv')[i].classList.add('top');
                // }


            } else {
                this.drawerComponentCurrentState = DrawerState.Bottom;
                // for (let i = 0; i < document.getElementsByClassName('cs-divider-dv').length; i++) {
                //   document.getElementsByClassName('cs-divider-dv')[i].classList.remove('center');
                //   document.getElementsByClassName('cs-divider-dv')[i].classList.remove('top');
                //   document.getElementsByClassName('cs-divider-dv')[i].classList.add('bottom');
                // }

            }
            this.drawerComponentPreviousState = DrawerState.Docked;
        } else if (this.drawerComponentCurrentState == DrawerState.Top) {
            this.drawerComponentCurrentState = DrawerState.Docked;
            this.drawerComponentPreviousState = DrawerState.Top;
            // for (let i = 0; i < document.getElementsByClassName('cs-divider-dv').length; i++) {
            //   document.getElementsByClassName('cs-divider-dv')[i].classList.remove('bottom');
            //   document.getElementsByClassName('cs-divider-dv')[i].classList.remove('top');
            //   document.getElementsByClassName('cs-divider-dv')[i].classList.add('center');
            // }


        }
    }

    async presentToast(message) {
        const toast = await this.toastCtrl.create({
            message: message,
            duration: 2000,
            position: 'bottom'
        });
        toast.dismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present();
    }

    async presentNoInternetToast() {
        const toast = await this.toastCtrl.create({
            message: "No internet connection. Please check your internet connection and try again.",
            showCloseButton: true,
            closeButtonText: "retry",
        });
        toast.onDidDismiss().then(() => {
            toast.dismiss();
            this.refreshData();
        });
        toast.present();
    }

    refreshData() {
        this.fetchSelectedObject();
    }

    backButtonOnclick() {
        this.router.navigate([this.redirectUrl], {
            skipLocationChange: true
        });
    }

    openurl(events, url) {
        events.stopPropagation();
        if (url.indexOf('http') == 0) {
            window.open(url)
        } else {
            window.open('http://'.concat(url))
        }
    }

    editButton_elementId_Onclick() {
        if (this.approverType == "approver") {
            this.showAlert(this.workFlowMapping[this.obj_pfm144393['systemAttributes']['fieldId']] + " process initiated can't edit at this time")
            return;
        } else if (this.approverType == "non approver") {
            this.fetchLockedUserDetail()
            return
        }
        const editNavigationParams = {
            action: 'Edit',
            id: this.obj_pfm144393['id'],
            parentObj: JSON.stringify(this.headerDocItem),
            parentFieldLabel: this.parentObjLabel,
            parentFieldValue: this.parentObjValue,
            parentId: this.headerDocItem['id']
        }
        this.toastCtrl.dismiss();
        if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/crmsaleorderlines_d_m_entry")) {
            editNavigationParams['redirectUrl'] = "/menu/crmsaleorderlines_d_m_hl_detail_view"
        }
        this.router.navigate(["/menu/crmsaleorderlines_d_m_entry"], {
            queryParams: editNavigationParams,
            skipLocationChange: true
        });
    }

    fetchLockedUserDetail() {
        var systemAttributes = this.obj_pfm144393['systemAttributes']
        var userId = systemAttributes['lockedBy']
        var date = new Date(systemAttributes['lockedDate']);
        this.showAlert(userId + " has locked for " + this.workFlowMapping[this.obj_pfm144393['systemAttributes']['fieldId']] + " on " + this.datePipe.transform(date, this.appUtilityConfig.userDateFormat));
    }

    statusChange(event, selectedStatusField) {
        if (selectedStatusField == undefined) {
            selectedStatusField = {}
        }
        selectedStatusField['statusLabel'] = event['selectedStatus']['statusLabel'];
        selectedStatusField['statusValue'] = event['selectedStatus']['statusValue'];
        selectedStatusField['statusType'] = event['selectedStatus']['statusType'];
        this.approveAction(selectedStatusField, event['workFlowUserApprovalStatusDataObject'])
    }

    getApprovalState(event) {
        this.approverType = event['approverType']
    }

    approveAction(selectedStatusField, workFlowUserApprovalStatusDataObject) {
        this.WorkFlowUserApprovalStatusDataObject = workFlowUserApprovalStatusDataObject;
        this.WorkFlowUserApprovalStatusDataObject['lastmodifiedby'] = this.appUtilityConfig.userId
        var userObjectList = this.WorkFlowUserApprovalStatusDataObject['approvalStatus'].filter(userDataObject => userDataObject.userId == this.appUtilityConfig.userId);
        var userObject = userObjectList[0]
        userObject['userName'] = this.appUtilityConfig.loggedUserName
        userObject['userLevel'] = ""
        userObject['statusValue'] = selectedStatusField['statusValue']
        userObject['statusType'] = selectedStatusField['statusType']
        userObject['approvalExecutionStatus'] = "INPROGRESS"
        userObject['execStatusMessage'] = ""
        userObject['comment'] = ""
        this.cspfmexecutionPouchDbProvider.save(this.executionDbConfigObject.workFlowUserApprovalStatusObject,
            this.WorkFlowUserApprovalStatusDataObject).then(result => {

            if (result['status'] != 'SUCCESS') {
                alert("failed")

                return;
            }
            this.presentToast("data saved sucessfully")
        })
    }
    async showAlert(messageContent) {

        var customAlert = await this.alerCtrl.create({
            backdropDismiss: false,
            message: messageContent,
            buttons: [

                {
                    text: "OK",
                    cssClass: "method-color",
                    handler: () => {

                    }
                }
            ]
        });
        customAlert.present();

    };
}