 import {
     Component,
     ViewChild,
     OnInit,
     ApplicationRef,
     ChangeDetectorRef,
     HostListener,
     NgZone
 } from '@angular/core';
 import {
     dbProvider
 } from 'src/core/db/dbProvider';
 import {
     offlineDbIndexCreation
 } from 'src/core/utils/offlineDbIndexCreation';
 import {
     dataProvider
 } from 'src/core/utils/dataProvider';
 import {
     appConstant
 } from 'src/core/utils/appConstant';
 import {
     Platform,
     LoadingController,
     Events,
     ToastController,
     IonVirtualScroll,
     AlertController
 } from '@ionic/angular';
 import {
     DrawerState
 } from 'ion-bottom-drawer';
 import {
     appUtility
 } from 'src/core/utils/appUtility';
 import {
     lookupFieldMapping
 } from 'src/core/pfmmapping/lookupFieldMapping';
 import {
     objectTableMapping
 } from 'src/core/pfmmapping/objectTableMapping';
 import {
     Router,
     ActivatedRoute
 } from '@angular/router';
 import {
     ScreenOrientation
 } from '@ionic-native/screen-orientation/ngx';
 import {
     cspfmExecutionPouchDbProvider
 } from 'src/core/db/cspfmExecutionPouchDbProvider';
 import {
     cspfmExecutionPouchDbConfiguration
 } from 'src/core/db/cspfmExecutionPouchDbConfiguration';
 import * as lodash from 'lodash';
 import * as _ from 'underscore';
 import {
     DatePipe
 } from '@angular/common';
 import {
     registerLocaleData
 } from '@angular/common';
 import {
     metaDataDbProvider
 } from 'src/core/db/metaDataDbProvider';
 import {
     metaDbConfiguration
 } from 'src/core/db/metaDbConfiguration';
 import {
     FieldInfo
 } from "src/core/pipes/cspfm_data_display";
 import {
     ColumnMode
 } from "@swimlane/ngx-datatable";

 @Component({
     selector: 'AccountGridView_MOBILE_Grid_with_Listpreview',
     templateUrl: 'AccountGridView_MOBILE_Grid_with_Listpreview.html'
 }) export class AccountGridView_MOBILE_Grid_with_Listpreview implements OnInit {
     @ViewChild(IonVirtualScroll) virtualScroll: IonVirtualScroll;
     public isBrowser: boolean = false;
     public drawerComponentDockedHeight = 300;
     public drawerComponentCurrentState = DrawerState.Top;
     private drawerComponentPreviousState = DrawerState.Docked;
     public pageTitle: string = '';
     public parentTitle: string = '';
     public totalActionInfo = {}
     private selectedObjectName: string = '';
     private selectedObjectParentName: any = "";
     public parentObjLabel = "";
     public fetchActionInfo: any = {}
     public parentObjValue = "";
     public childObjectsInfo;
     public headerDocItem: any = {};
     private prominentDataArray: Array < any > = [];
     private errorMessageToDisplay: string = 'No Records';
     private redirectUrl = "/";
     public showNavigationHistoryPopUp: Boolean = false;

     private currentStatusWorkflowActionFiledId;

     private approverType: string = "";
     private tableName_pfm147213 = "pfm147213";

     public navigationHistoryProperties = {
         'navigatedPagesNameArray': [],
         'navigatedPagesPathArray': [],
         'routerVisLinkTagName': "",
         'secondPreviousPage': "",
         'navigatedPagesLength': 0,
         'previousPage': "",
         'previousPageName': "",
         'secondPreviousPageName': "",
     };
     public layoutId = "58369_preview";
     public objectHierarchyJSON = {};
     public parentHierarchyJSON = {};
     public parentGridFetch;
     readonly headerHeight = 50;
     readonly rowHeight = 50;
     columnMode = ColumnMode;
     public isLoading;
     public gridTableFieldInfoArray: Array < FieldInfo > = [];
     constructor(platform: Platform, public lookupFieldMapping: lookupFieldMapping,
         public appUtilityConfig: appUtility, private screenOrientation: ScreenOrientation,
         public dataProvider: dataProvider, public offlineDbIndexCreation: offlineDbIndexCreation, public objectTableMapping: objectTableMapping,
         public loadingCtrl: LoadingController, public events: Events,
         public applicationRef: ApplicationRef, public router: Router,
         public activatRoute: ActivatedRoute, public toastCtrl: ToastController,
         public cspfmexecutionPouchDbProvider: cspfmExecutionPouchDbProvider,
         public executionDbConfigObject: cspfmExecutionPouchDbConfiguration,
         private datePipe: DatePipe,
         public alerCtrl: AlertController) {

         this.activatRoute.queryParams.subscribe(params => {
             if (Object.keys(params).length == 0 && params.constructor === Object) {
                 console.log("list query params skipped");
                 return
             }
             if (params["redirectUrl"]) {
                 this.redirectUrl = params["redirectUrl"]
             }
             this.headerDocItem = JSON.parse(params["parentObj"]);
             this.selectedObjectParentName = params["parentObjType"];
             this.parentTitle = params["parentLabel"];
             this.parentObjLabel = params["parentFieldLabel"];
             this.parentObjValue = params["parentFieldValue"];
             this.objectHierarchyJSON = JSON.parse(params["childObjectHierarchyJSON"]);
             this.parentHierarchyJSON = JSON.parse(params["parentHierarchyJSON"]);
             this.pageTitle = params["objLabel"];
             this.selectedObjectName = params["objType"];
             this.prominentDataArray = params["prominientObjectInfo"];
             this.appUtilityConfig.setEventSubscriptionlayoutIds(this.selectedObjectName, this.layoutId)


             if (params['gridInfoArray']) {
                 this.gridTableFieldInfoArray = JSON.parse(params['gridInfoArray']);
             }
             this.childObjectsInfo = [];

             this.fetchSelectedObject();

         });


         const windowHeight = window.innerHeight;
         this.drawerComponentDockedHeight = windowHeight / 2;
         if (!this.appUtilityConfig.isMobile || this.appUtilityConfig.osType === "android") {
             this.isBrowser = true;
         }

         this.screenOrientation.onChange().subscribe(
             () => {
                 this.drawerComponentCurrentState = DrawerState.Bottom;
                 const windowHeightVal = window.innerHeight;
                 this.drawerComponentDockedHeight = windowHeightVal / 2;
             }
         );
         this.events.subscribe(this.layoutId, (modified) => {
             if (modified["dataProvider"] == "PouchDB") {
                 this.childObjectModifiedEventTrigger(modified);
             }
         });
         //  this.childObjectsInfo = [];
         //  this.fetchSelectedObject();

     }
     private headerFieldName = "";
     private headerFieldTranslateKey = "";
     public isSkeletonLoading = true;
     public formulaObject = {};
     public layoutDataRestrictionSet = {};
     public obj_pfm147213: any = {};
     private obj_pfm147233: any = {};
     public gridFieldInfo: {
         [key: string]: FieldInfo
     } = {
         "pfm147213_accountno": {
             "label": "AccountGridView_MOBILE_Grid_with_List.Element.customermaster.accountno",
             "fieldName": "accountno",
             "prop": "accountno",
             "fieldType": "AUTONUMBER",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm147213_accountname": {
             "label": "AccountGridView_MOBILE_Grid_with_List.Element.customermaster.accountname",
             "fieldName": "accountname",
             "prop": "accountname",
             "fieldType": "TEXT",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm147213_businessunits": {
             "label": "AccountGridView_MOBILE_Grid_with_List.Element.customermaster.businessunits",
             "fieldName": "businessunits",
             "prop": "businessunits",
             "fieldType": "DROPDOWN",
             "child": "",
             "dateFormat": "",
             "mappingDetails": {
                 "None": "None",
                 "Sole proprietorship": "Sole proprietorship",
                 "Partnership": "Partnership",
                 "Joint Stock Company": "Joint Stock Company"
             },
             "currencyDetails": ""
         },
         "pfm147213_status": {
             "label": "AccountGridView_MOBILE_Grid_with_List.Element.customermaster.status",
             "fieldName": "status",
             "prop": "status",
             "fieldType": "DROPDOWN",
             "child": "",
             "dateFormat": "",
             "mappingDetails": {
                 "New": "New",
                 "InRview": "InReview",
                 "Approved": "Approved",
                 "Rejected": "Rejected",
                 "Active": "Active",
                 "In Active": "In Active"
             },
             "currencyDetails": ""
         },
         "pfm147213_taxregistrationnumber": {
             "label": "AccountGridView_MOBILE_Grid_with_List.Element.customermaster.taxregistrationnumber",
             "fieldName": "taxregistrationnumber",
             "prop": "taxregistrationnumber",
             "fieldType": "TEXT",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         }
     };
     public formulaReverseObjectHierarchyJSON = undefined;
     public formulaConfigJSON = {};
     public formulafields = {};
     public dbServiceProvider = appConstant.pouchDBStaticName;
     private businessunits_293693 = {
         'None': 'None',
         'Sole proprietorship': 'Sole proprietorship',
         'Partnership': 'Partnership',
         'Joint Stock Company': 'Joint Stock Company'
     };
     private status_293810 = {
         'New': 'New',
         'InRview': 'InReview',
         'Approved': 'Approved',
         'Rejected': 'Rejected',
         'Active': 'Active',
         'In Active': 'In Active'
     };
     private stateprovince_293674 = {
         'None': 'None',
         'California': 'California',
         'Florida': 'Florida',
         'Texas': 'Texas',
         'New Jersey': 'New Jersey',
         'North Carolina': 'North Carolina',
         'Alberta': 'Alberta',
         'British Columbia': 'British Columbia',
         'Nova Scotia': 'Nova Scotia',
         'Manitoba': 'Manitoba',
         'Delhi': 'Delhi',
         'Tamil Nadu': 'Tamil Nadu',
         'Kerala': 'Kerala'
     };
     private addresstype_293732 = {
         'None': 'None',
         'Ship To': 'Ship To',
         'Bill To': 'Bill To'
     };
     navigateObjectBaseDetailPage(itemTapNavigationParams, options) {
         if (this.selectedObjectName == 'pfm147233') {
             if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/AddressView_MOBILE_Grid")) {
                 itemTapNavigationParams['redirectUrl'] = "/menu/AccountGridView_MOBILE_Grid_with_Listpreview"
             }
             // this.navCtrl.push(AddressView_MOBILE_Grid, itemTapNavigationParams, options)
             this.router.navigate(["/menu/AddressView_MOBILE_Grid"], {
                 queryParams: itemTapNavigationParams,
                 skipLocationChange: true
             });
         }
     }
     childObjectModifiedEventTrigger(modified) {
         const modifiedData = this.dataProvider.convertRelDocToNormalDoc(modified);
         this.fetchModifiedRec(modifiedData);
     }

     async fetchModifiedRec(modifiedData) {
         const additionalObjectdata = {};
         additionalObjectdata['id'] = modifiedData['id'];
         const fetchParams = {
             'objectHierarchyJSON': this.objectHierarchyJSON,
             'dataSource': appConstant.pouchDBStaticName,
             'additionalInfo': additionalObjectdata
         }

         this.dataProvider.querySingleDoc(fetchParams).then(result => {


             if (result["status"] != "SUCCESS") {
                 this.errorMessageToDisplay = result["message"];
                 return;
             }
             const modifiedRec = result["records"][0];
             const eventsTriggeredindex = this.getChangedObjectIndex(
                 this.childObjectsInfo,
                 modifiedRec,
                 "id"
             );
             if (eventsTriggeredindex > -1) {
                 this.childObjectsInfo.splice(eventsTriggeredindex, 1);
             }


             this.childObjectsInfo.push(modifiedRec);
             if (this.appUtilityConfig.isMobileResolution === false) {
                 this.childObjectsInfo = [...this.childObjectsInfo]
             }
             console.log("this.childDocArray", this.childObjectsInfo);
             this.recentListRefresh();
         }).catch(error => {
             console.log(error);
         });
     }
     recentListRefresh() {
         if (this.virtualScroll) {
             this.virtualScroll.checkRange(0)
         }
     }

     getChangedObjectIndex(array, modifiedData, key) {
         console.log("modifiedData 123", modifiedData);
         return lodash.findIndex(
             array,
             item => {
                 return item[key] === modifiedData[key];
             },
             0
         );
     }
     navigateObjectBaseEntryPage(addNavigationParams, options) {
         if (this.selectedObjectName == 'pfm147233') {
             if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/AddressEntry_MOBILE_Grid")) {
                 addNavigationParams['redirectUrl'] = "/menu/AccountGridView_MOBILE_Grid_with_Listpreview"
             }
             // this.navCtrl.push(AddressEntry_MOBILE_Grid, addNavigationParams, options)
             this.router.navigate(["/menu/AddressEntry_MOBILE_Grid"], {
                 queryParams: addNavigationParams,
                 skipLocationChange: true
             });
         }
     }
     async fetchSelectedObject(event ? ) {
         // selectedObject no need in this method as a passing variable

         const additinoalObjectdata = {};
         additinoalObjectdata["parentType"] = this.selectedObjectParentName;
         additinoalObjectdata["parentId"] = this.headerDocItem["id"];

         const fetchParams = {
             'objectHierarchyJSON': this.objectHierarchyJSON,
             'additionalInfo': additinoalObjectdata,
             'dataSource': appConstant.pouchDBStaticName,
             'layoutDataRestrictionSet': this.layoutDataRestrictionSet
         }

         this.dataProvider.queryChildDataBatchwise(fetchParams).then(result => {
             this.isSkeletonLoading = false
             if (event != undefined) {
                 event.target.complete();
                 this.virtualScroll.checkEnd();
             }
             if (result["status"] !== "SUCCESS") {
                 this.errorMessageToDisplay = result["message"];
                 if (this.errorMessageToDisplay === "No internet") {
                     this.presentNoInternetToast();
                 }
                 return;
             }
             const pluralName = this.dataProvider.getPluralName(this.tableName_pfm147213);
             this.obj_pfm147213 = result['records'][0];

             //   if (this.obj_pfm147213[pluralName][0]) {
             //     this.obj_pfm147213 = this.obj_pfm147213[pluralName][0];
             //   }
             if (result['records'].length > 0) {
                 Array.prototype.push.apply(this.childObjectsInfo, result['records']);
             }
             // this.childObjectsInfo = result["records"];

             this.applicationRef.tick();

         }).catch(error => {
             this.isSkeletonLoading = false;
             console.log(error)
         });
     }

     async presentNoInternetToast() {
         const toast = await this.toastCtrl.create({
             message: "No internet connection. Please check your internet connection and try again.",
             showCloseButton: true,
             closeButtonText: "retry"
         });
         toast.onDidDismiss().then(() => {
             toast.dismiss();
             this.refreshData();
         });
         toast.present();
     }

     refreshData() {
         this.fetchSelectedObject();
     }
     historyButtonAction(event: Event) {
         this.showNavigationHistoryPopUp = !this.showNavigationHistoryPopUp;
         event.stopPropagation();
     }
     @HostListener('click') onClick() {
         this.showNavigationHistoryPopUp = false;
     }
     getNavigationHistory() {
         if (this.appUtilityConfig.getHomePageNode()) {
             let homePageNode = this.appUtilityConfig.getHomePageNode()
             this.navigationHistoryProperties['navigatedPagesNameArray'] = [homePageNode['homePageNodeName']]
             this.navigationHistoryProperties['navigatedPagesPathArray'] = [homePageNode['homePageNodepath']]
         }
         this.navigationHistoryProperties['navigatedPagesLength'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes.length;
         this.navigationHistoryProperties['previousPageName'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 2].childNodes[0].textContent;
         this.navigationHistoryProperties['previousPage'] = ( < HTMLElement > document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 2]).tagName.toLowerCase();
         if (document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3]) {
             this.navigationHistoryProperties['secondPreviousPageName'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3].childNodes[0].textContent;
             this.navigationHistoryProperties['secondPreviousPage'] = ( < HTMLElement > document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3]).tagName.toLowerCase();
         }
         for (let i = 0; i < this.navigationHistoryProperties['navigatedPagesLength'] - 3; i++) {
             this.navigationHistoryProperties['navigatedPagesNameArray'].push(document.getElementsByTagName('ion-router-outlet')[1].childNodes[i].childNodes[0].textContent);
             this.navigationHistoryProperties['navigatedPagesPathArray'].push(document.getElementsByTagName('ion-router-outlet')[1].children[i].tagName.toLowerCase());
         }
     }
     histListNav(e) {
         if (e.currentTarget.getAttribute('data-index') == 'app-homepage') {
             this.appUtilityConfig.navigateToHomepage();
         } else {
             this.router.navigate([`/menu/${e.currentTarget.getAttribute('data-index')}`]);
         }
     }
     previousPageNavigation() {
         this.router.navigate(["/menu/" + this.navigationHistoryProperties['previousPage']]);
     }
     secondPreviousPageNavigation() {
         this.router.navigate(["/menu/" + this.navigationHistoryProperties['secondPreviousPage']]);
     }
     ngOnInit() {
         this.getNavigationHistory()
     }


     ionViewDidLoad() {

     }

     ngOnDestroy() {
         this.events.unsubscribe(this.layoutId);
         this.appUtilityConfig.removeEventSubscriptionlayoutIds(this.selectedObjectName, this.layoutId);
     }

     ionViewWillEnter() {
         document.body.setAttribute('class', 'linelistinner');
         this.drawerComponentCurrentState = DrawerState.Top;
         this.drawerComponentPreviousState = DrawerState.Docked;
         for (let i = 0; i < document.getElementsByClassName('cs-divider-lv').length; i++) {
             document.getElementsByClassName('cs-divider-lv')[i].classList.remove('bottom');
             document.getElementsByClassName('cs-divider-lv')[i].classList.remove('center');
             document.getElementsByClassName('cs-divider-lv')[i].classList.add('top');
         }
     }
     ionViewDidEnter() {
         // this.service.changeToStyleTwo();
         // this.service.changeToStyleOne();
         var pvheader = document.querySelector('.header-listpreview-sub-header');
         pvheader.setAttribute('color', "var(--ion-color-primary, #3880ff)");

         var pvSubhItem = document.querySelector('.header-listpreview-sub-header ion-item');
         pvSubhItem.setAttribute('color', "var(--ion-color-primary, #3880ff)");

         var pvTitleHd = document.querySelectorAll('.header-listtitle-card-item h3');
         var pvTitleHdLen = pvTitleHd.length;
         for (var i = 0; i < pvTitleHdLen; i++) {
             pvTitleHd[i].setAttribute('color', "var(--ion-color-primary, #3880ff)");
         }
         var pvScrollIcon = document.querySelectorAll('ion-item-divider ion-icon');
         var pvScrollIconLen = pvScrollIcon.length;
         for (var i = 0; i < pvScrollIconLen; i++) {
             pvScrollIcon[i].setAttribute('color', "var(--ion-color-primary, #3880ff)");
         }
         var pvHdItembg = document.querySelectorAll('.header-listpreview-sub-header ion-badge');
         var pvHdItembgLen = pvHdItembg.length;
         for (var i = 0; i < pvHdItembgLen; i++) {
             pvHdItembg[i].setAttribute('background', "$cvar");
         }
         if (this.virtualScroll) {
             this.virtualScroll.checkRange(0)
         }
         if (this.appUtilityConfig.isMobileResolution === false) {
             this.childObjectsInfo = [...this.childObjectsInfo]
         }
     }

     ionViewWillLeave() {
         this.drawerComponentCurrentState = DrawerState.Bottom
     }


     addButton_elementId_Onclick() {
         if (this.selectedObjectName === "pfm147213") {
             if (this.childObjectsInfo && this.childObjectsInfo.length >= 1) {
                 alert("You cannot add more than one additional info...");
                 return;
             }
         }
         const options = {
             animate: false
         }
         const addNavigationParams = {
             action: "Add",
             parentId: this.headerDocItem["id"],
             parentFieldLabel: this.parentObjLabel,
             parentFieldValue: this.parentObjValue,
             parentPage: this,
             parentName: this.selectedObjectParentName
         }
         this.navigateObjectBaseEntryPage(addNavigationParams, options)
     }
     undefined
     onItemTap(childItem) {
         const options = {
             animate: false
         }
         const itemTapNavigationParams = {
             //  parentObj: JSON.stringify(this.headerDocItem),
             parentFieldLabel: this.parentObjLabel,
             parentFieldValue: this.parentObjValue,
             parentName: this.selectedObjectParentName,
             id: childItem["id"],
             parentTitle: this.parentTitle,
             prominientObjectInfo: this.prominentDataArray
         }
         this.navigateObjectBaseDetailPage(itemTapNavigationParams, options)
     }
     drawerButtonOnclick() {
         if (this.drawerComponentCurrentState == DrawerState.Bottom) {

             this.drawerComponentCurrentState = DrawerState.Docked;
             this.drawerComponentPreviousState = DrawerState.Bottom;
             for (let i = 0; i < document.getElementsByClassName('cs-divider-lv').length; i++) {
                 document.getElementsByClassName('cs-divider-lv')[i].classList.remove('bottom');
                 document.getElementsByClassName('cs-divider-lv')[i].classList.remove('top');
                 document.getElementsByClassName('cs-divider-lv')[i].classList.add('center');
             }

         } else if (this.drawerComponentCurrentState == DrawerState.Docked) {

             if (this.drawerComponentPreviousState == DrawerState.Bottom) {
                 this.drawerComponentCurrentState = DrawerState.Top;
                 for (let i = 0; i < document.getElementsByClassName('cs-divider-lv').length; i++) {
                     document.getElementsByClassName('cs-divider-lv')[i].classList.remove('bottom');
                     document.getElementsByClassName('cs-divider-lv')[i].classList.remove('center');
                     document.getElementsByClassName('cs-divider-lv')[i].classList.add('top');
                 }


             } else {
                 this.drawerComponentCurrentState = DrawerState.Bottom;
                 for (let i = 0; i < document.getElementsByClassName('cs-divider-lv').length; i++) {
                     document.getElementsByClassName('cs-divider-lv')[i].classList.remove('center');
                     document.getElementsByClassName('cs-divider-lv')[i].classList.remove('top');
                     document.getElementsByClassName('cs-divider-lv')[i].classList.add('bottom');
                 }


             }
             this.drawerComponentPreviousState = DrawerState.Docked;

         } else if (this.drawerComponentCurrentState == DrawerState.Top) {

             this.drawerComponentCurrentState = DrawerState.Docked;
             this.drawerComponentPreviousState = DrawerState.Top;
             for (let i = 0; i < document.getElementsByClassName('cs-divider-lv').length; i++) {
                 document.getElementsByClassName('cs-divider-lv')[i].classList.remove('bottom');
                 document.getElementsByClassName('cs-divider-lv')[i].classList.remove('top');
                 document.getElementsByClassName('cs-divider-lv')[i].classList.add('center');
             }


         }
     }

     backButtonOnclick() {
         this.router.navigate([this.redirectUrl], {
             skipLocationChange: true
         });
     }

     async onScroll(event) {
         const offsetY = event.offsetY;
         // total height of all rows in the viewport
         const offsetHeight = document.getElementById("table").offsetHeight; // this.el.nativeElement.getBoundingClientRect().height - this.headerHeight + 0;
         const viewHeight = offsetHeight - this.headerHeight;

         // check if we scrolled to the end of the viewport
         if (!this.isLoading && offsetY + viewHeight >= this.childObjectsInfo.length * this.rowHeight) {
             this.fetchSelectedObject();
         }
     }

     onTableEventChanged(event) {
         if (event["type"] === "click") {
             this.onItemTap(event["row"]);
         }
     }
 }