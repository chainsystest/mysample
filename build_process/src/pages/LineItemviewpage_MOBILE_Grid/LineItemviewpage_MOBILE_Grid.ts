 import {
     Component,
     ViewChild,
     OnInit,
     ApplicationRef,
     ChangeDetectorRef,
     HostListener,
     NgZone
 } from '@angular/core';
 import {
     dbProvider
 } from 'src/core/db/dbProvider';
 import {
     offlineDbIndexCreation
 } from 'src/core/utils/offlineDbIndexCreation';
 import {
     dataProvider
 } from 'src/core/utils/dataProvider';
 import {
     appConstant
 } from 'src/core/utils/appConstant';
 import {
     ToastController,
     LoadingController,
     Events,
     AlertController
 } from '@ionic/angular';
 import * as lodash from 'lodash';
 import {
     appUtility
 } from 'src/core/utils/appUtility';
 import {
     Router,
     ActivatedRoute
 } from '@angular/router';
 import {
     EmailComposer
 } from '@ionic-native/email-composer/ngx';
 import {
     SocialSharing
 } from '@ionic-native/social-sharing/ngx';
 import {
     SMS
 } from '@ionic-native/sms/ngx';
 import {
     CallNumber
 } from '@ionic-native/call-number/ngx';
 import * as _ from 'underscore';
 import {
     DatePipe
 } from '@angular/common';
 import {
     registerLocaleData
 } from '@angular/common';
 import {
     metaDataDbProvider
 } from 'src/core/db/metaDataDbProvider';
 import {
     metaDbConfiguration
 } from 'src/core/db/metaDbConfiguration';
 import {
     cspfmExecutionPouchDbProvider
 } from 'src/core/db/cspfmExecutionPouchDbProvider';
 import {
     cspfmExecutionPouchDbConfiguration
 } from 'src/core/db/cspfmExecutionPouchDbConfiguration';
 import {
     FieldInfo
 } from "src/core/pipes/cspfm_data_display";

 @Component({
     selector: 'LineItemviewpage_MOBILE_Grid',
     templateUrl: './LineItemviewpage_MOBILE_Grid.html'
 }) export class LineItemviewpage_MOBILE_Grid implements OnInit {
     constructor(public dataProvider: dataProvider, public appUtilityConfig: appUtility, public socialShare: SocialSharing, public callNumber: CallNumber, public emailComposer: EmailComposer, public sms: SMS, public router: Router, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public activatRoute: ActivatedRoute, public events: Events, public applicationRef: ChangeDetectorRef, public metaDbConfigurationObj: metaDbConfiguration, public metaDbProvider: metaDataDbProvider, public cspfmexecutionPouchDbProvider: cspfmExecutionPouchDbProvider,
         public executionDbConfigObject: cspfmExecutionPouchDbConfiguration, private datePipe: DatePipe,
         public alerCtrl: AlertController, ) {
         this.activatRoute.queryParams.subscribe(params => {
             if (Object.keys(params).length == 0 && params.constructor === Object) {
                 console.log("list query params skipped");
                 return
             }
             if (params["redirectUrl"]) {
                 this.redirectUrl = params["redirectUrl"]
             }
             this.id = params["id"];

             this.fetchSelectedObject();
         });
         this.appUtilityConfig.setEventSubscriptionlayoutIds(this.tableName_pfm144393, this.layoutId);
         this.events.subscribe(this.layoutId, (modified) => {
             if (modified['dataProvider'] == 'PouchDB' && modified['dataProvider'] != 'JsonDB') {
                 this.childObjectModifiedEventTrigger(modified);
             }
         });
     }
     public obj_pfm144393: any = {};
     public gridFieldInfo: {
         [key: string]: FieldInfo
     } = {
         "pfm144393_olineid": {
             "label": "LineItemviewpage_MOBILE_Grid.Element.crmsaleorderlines.olineid",
             "fieldName": "olineid",
             "prop": "olineid",
             "fieldType": "AUTONUMBER",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         }
     };
     public formulaReverseObjectHierarchyJSON = undefined;
     public formulaConfigJSON = {};
     public formulafields = {};
     private tableName_pfm144393 = 'pfm144393';
     public errorMessageToDisplay = "No Records";
     public dbServiceProvider = appConstant.pouchDBStaticName;
     public formulaObject = {};
     private currentStatusWorkFlowActionFieldId;
     public WorkFlowUserApprovalStatusDataObject = {};
     private workFlowMapping = {

     }


     public id: any = '';

     private objectHierarchyJSON = {
         "objectName": "crmsaleorderlines",
         "objectType": "PRIMARY",
         "relationShipType": null,
         "fieldId": "0",
         "objectId": "144393",
         "childObject": [{
             "objectName": "crmproduct",
             "objectType": "LOOKUP",
             "relationShipType": "LOOKUP",
             "fieldId": "289353",
             "objectId": "142973",
             "childObject": []
         }]
     };
     private prominentDataMapping = {
         "pfm144393": ["olineid"]
     };;
     private documentInfo = [];
     private attachmentInfo = [];
     private redirectUrl = "/";
     public isSkeletonLoading = true;
     private approverType: string = "";
     private cscomponentactionInfo = {

     };
     public layoutId = "60576";
     private loading;
     childObjectModifiedEventTrigger(modified) {
         const modifiedData = this.dataProvider.convertRelDocToNormalDoc(modified);
         if (modifiedData["id"] === this.id) {
             this.fetchSelectedObject();
         }
     }
     ngOnDestroy() {
         this.events.unsubscribe(this.tableName_pfm144393);
         this.appUtilityConfig.removeEventSubscriptionlayoutIds(this.tableName_pfm144393, this.layoutId)
     }
     backButtonOnclick() {
         this.toastCtrl.dismiss();
         this.router.navigate([this.redirectUrl], {
             skipLocationChange: true
         });
     }

     async fetchSelectedObject() {


         let additionalObjectdata = {};
         additionalObjectdata['id'] = this.id;
         const fetchParams = {
             'objectHierarchyJSON': this.objectHierarchyJSON,
             'additionalInfo': additionalObjectdata,
             'dataSource': appConstant.pouchDBStaticName
         }
         this.dataProvider.querySingleDoc(fetchParams).then(result => {
             this.isSkeletonLoading = false;
             if (result["status"] !== "SUCCESS") {
                 this.errorMessageToDisplay = result["message"];
                 if (this.errorMessageToDisplay === "No internet") {
                     this.presentNoInternetToast();
                 }
                 return;
             }
             this.obj_pfm144393 = result["records"][0];




             if (!this.applicationRef['destroyed']) {
                 this.applicationRef.detectChanges();
             }

         }).catch(error => {
             this.isSkeletonLoading = false;
             console.log(error);
         });
     }


     async presentToast(message) {
         const toast = await this.toastCtrl.create({
             message: message,
             duration: 2000,
             position: 'bottom'
         });
         toast.dismiss(() => {
             console.log('Dismissed toast');
         });
         toast.present();
     }

     async presentNoInternetToast() {
         const toast = await this.toastCtrl.create({
             message: "No internet connection. Please check your internet connection and try again.",
             showCloseButton: true,
             closeButtonText: "retry"
         });
         toast.onDidDismiss().then(() => {
             toast.dismiss();
             this.refreshDataInretry();
         });
         toast.present();
     }

     refreshDataInretry() {
         this.fetchSelectedObject();
     }
     ngOnInit() {}
     editButton_elementId_Onclick() {
         const editNavigationParams = {
             action: 'Edit',
             id: this.obj_pfm144393['id'],
             parentObj: '',
             parentFieldLabel: '',
             parentFieldValue: '',
             parentId: ''
         }
         if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/LineitemCreatepage_MOBILE_Grid")) {
             editNavigationParams['redirectUrl'] = "/menu/LineItemviewpage_MOBILE_Grid"
         }
         this.toastCtrl.dismiss();
         this.router.navigate(["/menu/LineitemCreatepage_MOBILE_Grid"], {
             queryParams: editNavigationParams,
             skipLocationChange: true
         });
     }
 }