 import {
     Component,
     ViewChild,
     OnInit,
     ApplicationRef,
     ChangeDetectorRef,
     HostListener,
     NgZone
 } from '@angular/core';
 import {
     dbProvider
 } from 'src/core/db/dbProvider';
 import {
     offlineDbIndexCreation
 } from 'src/core/utils/offlineDbIndexCreation';
 import {
     dataProvider
 } from 'src/core/utils/dataProvider';
 import {
     appConstant
 } from 'src/core/utils/appConstant';

 import * as lodash from 'lodash';
 import {
     ToastController,
     ModalController,
     AlertController,
     Platform,
     IonContent,
     LoadingController
 } from '@ionic/angular';
 import {
     lookuppage
 } from 'src/core/pages/lookuppage/lookuppage';
 import {
     FormBuilder,
     Validators,
     FormGroup
 } from '@angular/forms';
 import {
     appUtility
 } from 'src/core/utils/appUtility';
 import * as moment from 'moment';
 import 'moment-timezone';
 import {
     Router,
     ActivatedRoute
 } from '@angular/router';
 import {
     lookupFieldMapping
 } from 'src/core/pfmmapping/lookupFieldMapping';
 import {
     objectTableMapping
 } from 'src/core/pfmmapping/objectTableMapping';
 import * as _ from 'underscore';
 import * as Quill from 'quill';
 import {
     cspfmFieldTrackingMapping
 } from 'src/core/pfmmapping/cspfmFieldTrackingMapping';
 import {
     Http,
     Headers
 } from '@angular/http';
 import {
     dbConfiguration
 } from 'src/core/db/dbConfiguration';
 @Component({
     selector: 'crmsaleorderlines_d_m_entry',
     templateUrl: './crmsaleorderlines_d_m_entry.html'
 }) export class crmsaleorderlines_d_m_entry implements OnInit {
     @ViewChild(IonContent) contentArea: IonContent;
     private obj_pfm144393: any = {};
     public ionDateTimeDisplayValue = {};
     private obj_pfm144393_Temp: any = {};
     public formGroup: FormGroup;
     private customAlert;
     public action = 'Add';
     private id: any = {};
     private tableName_pfm144393 = 'pfm144393';
     public isSaveActionTriggered: Boolean = false;
     private isValidFrom: Boolean = true;
     public dbServiceProvider = appConstant.pouchDBStaticName;
     public selectedDataObject: any = {};
     public formulaObject = {};
     public isFieldTrackingEnable: Boolean = false;
     private showAlert: boolean = false;
     infoMessage = '';
     aniClassAddorRemove: boolean = false;
     private workFlowInitiateList = {};
     public savedSuccessMessage = 'data saved sucessfully';
     public updatedSuccessMessage = 'data updated sucessfully';
     public savedErrorMessage = 'Error saving record';
     public fetchErrorMessage = 'Fetching Failed';
     private dependentNumberCount = {};
     public partiallySavedSuccessMessage = 'data partially saved';
     private backButtonSubscribeRef;
     private unRegisterBackButtonAction: Function;
     public parentObjLabel = '';
     public parentObjValue = '';
     private errorMessageToDisplay: string = 'No Records';
     private parentName = '';
     private parentId = '';
     public isParentObjectShow = false;
     public isSkeletonLoading = true;
     isViewRunning = false;
     loading;
     private objectHierarchyJSON = {
         "objectName": "crmsaleorderlines",
         "objectType": "PRIMARY",
         "relationShipType": "",
         "fieldId": "0",
         "objectId": "144393",
         "childObject": [{
             "objectName": "crmproduct",
             "objectType": "LOOKUP",
             "relationShipType": "",
             "fieldId": "289353",
             "objectId": "142973",
             "childObject": []
         }]
     };
     public isFromMenu = false;
     private redirectUrl = '/';
     private dependentFieldTriggerList = {};
     private geoLocationdependentField = {};
     public pickListValues = {};
     public parentObject: any = {};
     private obj_pfm142973_289353: any = {};
     private pfm142973_289353_searchKey;
     public formulaReverseObjectHierarchyJSON = [{
         "toLevel": 1,
         "relationShipType": "one_to_many",
         "isFormulaObject": "Y",
         "childObject": [{
             "toLevel": 1,
             "relationShipType": "",
             "isFormulaObject": "N",
             "childObject": [],
             "objectName": "crmproduct",
             "type": "P",
             "referenceObjectId": "144393",
             "objectId": "142973",
             "objectType": "LOOKUP"
         }],
         "objectName": "crmsaleorderlines",
         "type": "P",
         "referenceObjectId": 0,
         "objectId": "144393",
         "objectType": "PRIMARY"
     }];
     public formulaConfigJSON = {
         "pfm144393": {
             "totalamount": {
                 "fieldName": "totalamount",
                 "objectId": 144393,
                 "displayformula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                 "formula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                 "operands": [{
                     "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                     "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                     "fieldName": "listprice",
                     "level": "1",
                     "refFieldId": 301754,
                     "fieldType": "currency",
                     "objectId": 144393,
                     "fieldId": 289357,
                     "objectType": "PRIMARY"
                 }, {
                     "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                     "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                     "fieldName": "quantity",
                     "level": "1",
                     "refFieldId": 301754,
                     "fieldType": "number",
                     "objectId": 144393,
                     "fieldId": 289359,
                     "objectType": "PRIMARY"
                 }, {
                     "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                     "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                     "fieldName": "listprice",
                     "level": "1",
                     "refFieldId": 301754,
                     "fieldType": "currency",
                     "objectId": 144393,
                     "fieldId": 289357,
                     "objectType": "PRIMARY"
                 }, {
                     "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                     "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                     "fieldName": "quantity",
                     "level": "1",
                     "refFieldId": 301754,
                     "fieldType": "number",
                     "objectId": 144393,
                     "fieldId": 289359,
                     "objectType": "PRIMARY"
                 }, {
                     "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                     "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                     "fieldName": "taxpercentage",
                     "level": "1",
                     "refFieldId": 301754,
                     "fieldType": "number",
                     "objectId": 144393,
                     "fieldId": 302833,
                     "objectType": "PRIMARY"
                 }, {
                     "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                     "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                     "fieldName": "listprice",
                     "level": "1",
                     "refFieldId": 301754,
                     "fieldType": "currency",
                     "objectId": 144393,
                     "fieldId": 289357,
                     "objectType": "PRIMARY"
                 }, {
                     "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                     "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                     "fieldName": "quantity",
                     "level": "1",
                     "refFieldId": 301754,
                     "fieldType": "number",
                     "objectId": 144393,
                     "fieldId": 289359,
                     "objectType": "PRIMARY"
                 }, {
                     "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                     "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                     "fieldName": "discountpercentage",
                     "level": "1",
                     "refFieldId": 301754,
                     "fieldType": "number",
                     "objectId": 144393,
                     "fieldId": 302834,
                     "objectType": "PRIMARY"
                 }]
             }
         }
     };
     public formulafields = {
         "pfm144393": ["listprice", "quantity", "taxpercentage", "discountpercentage"]
     };
     childObjectList = [];
     objResultMap = new Map < string, any > ();
     objDisplayName = {
         'pfm144393': 'crmsaleorderlines',
     };
     recordEntryValidation() {
         const formGroupObjectValue = Object.assign({}, this.formGroup.value);
         this.obj_pfm144393 = Object.assign(this.obj_pfm144393, formGroupObjectValue.pfm144393);
         if (
             this.obj_pfm144393["productdescription"] || this.obj_pfm144393["taxpercentage"] || this.obj_pfm144393["discountpercentage"] || this.obj_pfm144393["listprice"] || this.obj_pfm144393["partno"] || this.obj_pfm144393["quantity"] || this.obj_pfm144393["totalamount"] || this.obj_pfm144393["olineid"] || this.obj_pfm144393["product"] || this.obj_pfm144393["listprice"] || this.obj_pfm144393["listprice"]) {
             return true;
         } else {
             return false;
         }
     }
     constructor(public loadingCtrl: LoadingController, public dbConfiguration: dbConfiguration, public http: Http, public router: Router, public activatRoute: ActivatedRoute, public applicationRef: ApplicationRef, public alerCtrl: AlertController, public modalCtrl: ModalController, public dbService: dbProvider, public dataProvider: dataProvider, public formBuilder: FormBuilder, public appUtility: appUtility, public objectTableMapping: objectTableMapping, public lookupFieldMapping: lookupFieldMapping, private toastCtrl: ToastController, public platform: Platform, public fieldTrackMapping: cspfmFieldTrackingMapping, private ngZone: NgZone) {
         this.activatRoute.queryParams.subscribe(params => {
             if (Object.keys(params).length == 0 && params.constructor === Object) {
                 console.log("list query params skipped");
                 return
             }
             if (params["isFromMenu"]) {
                 this.isFromMenu = params["isFromMenu"];
             }
             if (params["redirectUrl"]) {
                 this.redirectUrl = params["redirectUrl"]
             }
             this.action = params['action'];
             this.parentId = params['parentId'];
             this.id = params['id'];
             this.parentName = params['parentName'];
             if (this.parentId) {
                 this.isParentObjectShow = true;
                 this.parentObjLabel = params['parentFieldLabel'];
                 this.parentObjValue = params['parentFieldValue'];
             }
             this.initializeObjects(dataProvider.tableStructure());
             if (this.action == 'Edit') {
                 this.id = params['id'];
                 this.fetchRecordAgainstSelectedObject();
             } else {
                 this.isSkeletonLoading = false;
                 this.checkboxInitialization();
                 if (this.parentId !== undefined && this.parentName !== "" && this.parentId !== "" && this.parentName !== "") {
                     this.fetchParentObjectForFormulaCalculation(this.parentId, this.parentName)
                 }
             }
         });
         this.createFormGroup();
         this.hardWareBackButtonAction();
     }
     initializeObjects(tableStructure) {
         this.obj_pfm144393 = JSON.parse(JSON.stringify(tableStructure.pfm144393));
         this.obj_pfm142973_289353 = JSON.parse(JSON.stringify(tableStructure.pfm142973));
     }
     fetchRecordAgainstSelectedObject() {
         const additionalObjectdata = {};
         additionalObjectdata['id'] = this.id;
         const fetchParams = {
             'objectHierarchyJSON': this.objectHierarchyJSON,
             'additionalInfo': additionalObjectdata,
             'dataSource': appConstant.pouchDBStaticName
         };
         this.dataProvider.querySingleDoc(fetchParams).then(res => {
             this.isSkeletonLoading = false;
             if (res['status'] !== 'SUCCESS') {
                 this.errorMessageToDisplay = res['message'];
                 if (this.errorMessageToDisplay == 'No internet') {
                     this.presentNoInternetToast();
                 }
                 return;
             }
             if (res['records'].length < 0) {
                 console.log('FetchRecordAgainstSelectedObject No Records');
                 return;
             }
             let dataObj = res['records'][0];
             this.obj_pfm144393_Temp = lodash.extend({}, this.obj_pfm144393, dataObj);
             this.selectedDataObject['pfm144393'] = JSON.stringify(this.obj_pfm144393_Temp);
             this.obj_pfm144393 = lodash.extend({}, this.obj_pfm144393, dataObj);
             this.obj_pfm142973_289353 = this.obj_pfm144393['pfm142973_289353'] ? this.obj_pfm144393['pfm142973_289353'] : "";
             this.formGroup.patchValue({
                 pfm144393: this.obj_pfm144393
             });
             this.applicationRef.tick();
             this.formulaReverseObjectHierarchyJSON.forEach(hierarchyJSONObject => {
                 this.getFormulaDataObject(this.obj_pfm144393, hierarchyJSONObject)
             });
         }).catch(error => {
             this.isSkeletonLoading = false;
             this.showInfoAlert(this.fetchErrorMessage)
         })
     }
     async getFormulaDataObject(resultObject, objectReverseHierarchyJSON) {
         const fetchParams = {
             'objectReverseHierarchyJSON': objectReverseHierarchyJSON,
             'objectHierarchyJSON': this.objectHierarchyJSON,
             'dataSource': appConstant.pouchDBStaticName,
             'additionalInfo': resultObject
         }
         this.dataProvider.querySingleFormualDoc(fetchParams).then(result => {
             console.log("result = ", result);
             if (result['status'] !== 'SUCCESS') {
                 this.errorMessageToDisplay = result['message'];
                 if (this.errorMessageToDisplay === "No internet") {
                     this.presentNoInternetToast();
                 }
                 return
             }
             this.formulaObject = result["records"]
         }).catch(error => {
             console.log("error====", error)
         });
     }
     async fetchParentObjectForFormulaCalculation(parentId, parentName) {
         const additionalObjectdata = {};
         additionalObjectdata['id'] = parentId;
         additionalObjectdata['type'] = parentName;
         for (let i = 0; i < this.formulaReverseObjectHierarchyJSON.length; i++) {
             const objectReverseHierarchyJSON = this.formulaReverseObjectHierarchyJSON[i]
             const fetchParams = {
                 'objectReverseHierarchyJSON': objectReverseHierarchyJSON,
                 'objectHierarchyJSON': this.objectHierarchyJSON,
                 'dataSource': appConstant.pouchDBStaticName,
                 'additionalInfo': additionalObjectdata,
                 'fetchParent': true
             }
             this.dataProvider.querySingleFormualDoc(fetchParams).then(result => {
                 console.log("result = ", result);
                 if (result['status'] !== 'SUCCESS') {
                     this.errorMessageToDisplay = result['message'];
                     if (this.errorMessageToDisplay === "No internet") {
                         this.presentNoInternetToast();
                     }
                     return
                 }
                 this.formulaObject = result["records"]
             }).catch(error => {
                 console.log("error====", error)
             });
         }

     }
     formGroupUpdate() {
         this.formGroup.updateValueAndValidity({
             onlySelf: false,
             emitEvent: true
         });
         this.formGroup.valueChanges.subscribe(form => {
             const formControls = this.formGroup.controls;
             if (formControls.pfm144393['controls'][this.formulafields["pfm144393"][0]].dirty || formControls.pfm144393['controls'][this.formulafields["pfm144393"][1]].dirty || formControls.pfm144393['controls'][this.formulafields["pfm144393"][2]].dirty || formControls.pfm144393['controls'][this.formulafields["pfm144393"][3]].dirty) {
                 this.ngZone.run(() => {
                     console.log("ngzone run");
                     const formulaObject1 = {};
                     formulaObject1["pfm144393"] = form["pfm144393"];
                     this.formulaObject = formulaObject1;
                 });
             }
         });
     };
     updateGeoLocationFlag(objectName, dataObj, existingDataObj) {
         Object.keys(this.geoLocationdependentField[objectName]).forEach(element => {
             const depFlds = this.geoLocationdependentField[objectName][element]['dependentFields'];
             depFlds.forEach(fieldName => {
                 if (this.action == 'Edit') {
                     if (existingDataObj[fieldName] != dataObj[fieldName]) {
                         dataObj[this.geoLocationdependentField[objectName][element]['dependentFieldName']] = 'Y';
                         return;
                     }
                 } else {
                     if (dataObj[fieldName] != '') {
                         dataObj[this.geoLocationdependentField[objectName][element]['dependentFieldName']] = 'Y';
                         return;
                     }
                 }
             })
         });
     }
     async presentNoInternetToast() {
         const toast = await this.toastCtrl.create({
             message: "No internet connection. Please check your internet connection and try again.",
             showCloseButton: true,
             closeButtonText: "retry"
         });
         toast.onDidDismiss().then(() => {
             toast.dismiss();
             this.refreshData();
         });
         toast.present();
     }

     refreshData() {
         this.fetchRecordAgainstSelectedObject();
     }
     async saveButtonOnclick() {
         this.formGroup.patchValue({
             pfm144393: {
                 productdescription: this.obj_pfm144393['productdescription'],
                 taxpercentage: this.obj_pfm144393['taxpercentage'],
                 discountpercentage: this.obj_pfm144393['discountpercentage'],
                 partno: this.obj_pfm144393['partno'],
                 quantity: this.obj_pfm144393['quantity'],
                 totalamount: this.obj_pfm144393['totalamount'],
                 olineid: this.obj_pfm144393['olineid'],
                 listprice: this.obj_pfm144393['listprice']
             }
         });
         if (this.formGroup.valid) {
             this.formGroup.patchValue({});
             this.isValidFrom = true;
             if (this.obj_pfm144393['taxpercentage'] !== null) {
                 this.obj_pfm144393['taxpercentage'] = Number(this.obj_pfm144393['taxpercentage']);
             }
             if (this.obj_pfm144393['discountpercentage'] !== null) {
                 this.obj_pfm144393['discountpercentage'] = Number(this.obj_pfm144393['discountpercentage']);
             }
             if (this.obj_pfm144393['quantity'] !== null) {
                 this.obj_pfm144393['quantity'] = Number(this.obj_pfm144393['quantity']);
             }
             if (this.obj_pfm144393['listprice'] !== null) {
                 this.obj_pfm144393['listprice'] = Number(this.obj_pfm144393['listprice']);
             }
             const fieldTrackObject = this.fieldTrackMapping.mappingDetail[this.tableName_pfm144393]
             if (fieldTrackObject) {
                 this.isFieldTrackingEnable = true;
             } else {
                 this.isFieldTrackingEnable = false;
             }
             let previousParentObject
             if (this.action === "Edit") {
                 previousParentObject = this.selectedDataObject[this.tableName_pfm144393];
                 console.log("previousParentObject = ", previousParentObject);
             } else {
                 previousParentObject = undefined
             }
             if (this.parentId) {
                 this.obj_pfm144393[this.parentName] = this.parentId;
             };
             this.dataProvider.save(this.tableName_pfm144393, this.obj_pfm144393, appConstant.pouchDBStaticName, previousParentObject, this.isFieldTrackingEnable)
                 .then(result => {
                     if (result['status'] != 'SUCCESS') {
                         this.showInfoAlert(result['message']);
                         return;
                     }
                     if (this.childObjectList.length == 0) {
                         this.presentToast(this.savedSuccessMessage);
                         let opts = {
                             animate: false
                         };
                         this.navigatePopUpAction();
                         return
                     };

                 })
                 .catch(error => {
                     this.showInfoAlert(this.savedErrorMessage);
                     console.log(error)
                 });
         } else {
             this.formGroup.patchValue({});
             this.isValidFrom = false;
             this.scrollToValidationFailedField();
         }
         var errorValue = document.querySelector('.entry-page-content');
         errorValue.setAttribute('class', 'entry-page-content entryErrorMessage hydrated');
     };
     scrollToValidationFailedField() {
         let formControls = this.formGroup.controls.pfm144393;
         let formGroupKeys = Object.keys(formControls["controls"]);
         let isValidationSucceed = true;
         formGroupKeys.every(element => {
             if (formControls["controls"][element].status == "INVALID") {
                 let yOffset = document.getElementById("pfm144393_" + element).offsetTop;
                 this.contentArea.scrollToPoint(0, yOffset, 1000);
                 isValidationSucceed = false;
                 return false;
             } else {
                 return true;
             }
         })
     };
     createFormGroup() {
         this.formGroup = this.formBuilder.group({
             pfm144393: this.formBuilder.group({
                 productdescription: [null, Validators.compose([])],
                 taxpercentage: [null, Validators.compose([])],
                 discountpercentage: [null, Validators.compose([])],
                 partno: [null, Validators.compose([Validators.required])],
                 quantity: [null, Validators.compose([Validators.required])],
                 totalamount: [null, Validators.compose([])],
                 pfm142973_289353_searchKey: [''],
                 listprice: [null, Validators.compose([Validators.required])]
             })
         });
         this.formGroupUpdate();
     }
     async showLookup_289353(objectname, label, displayColumns) {
         const lookupInput = {};
         const lookupColumns = [{
             columnName: 'itemno',
             displayName: 'Item No',
             fieldType: 'Text',
             mappingValues: {}
         }, {
             columnName: 'listprice',
             displayName: 'List Price',
             fieldType: 'Currency',
             mappingValues: {}
         }, {
             columnName: 'name',
             displayName: 'Product Id',
             fieldType: 'Autonumber',
             mappingValues: {}
         }];
         const lookupHierarchyJson = {
             "objectName": "crmproduct",
             "objectType": "PRIMARY",
             "relationShipType": "",
             "fieldId": 289353,
             "objectId": "142973",
             "childObject": []
         };
         const selector = {};
         selector['data.type'] = 'pfm142973';;
         if (this.pfm142973_289353_searchKey) {
             var regexp = new RegExp(this.pfm142973_289353_searchKey, 'i');
             selector['data.name'] = {
                 $regex: regexp
             };
         } else if (this.obj_pfm142973_289353['name']) {
             var regexp = new RegExp(this.obj_pfm142973_289353['name'], 'i');
             selector['data.name'] = {
                 $regex: regexp
             };
         }
         lookupHierarchyJson['options'] = {
             'selector': selector
         };
         lookupInput['lookupColumnDetails'] = lookupColumns;
         lookupInput['objectHierarchy'] = lookupHierarchyJson;
         lookupInput['title'] = label;
         const lookupModal = await this.modalCtrl.create({
             component: lookuppage,
             componentProps: {
                 serviceObject: this.dbService,
                 parentPage: this,
                 lookupColumnName: objectname,
                 lookupInput: lookupInput,
                 dataSource: appConstant.pouchDBStaticName
             }
         });
         await lookupModal.present();
     }
     lookupResponse(objectname, selectedValue) {
         this.dependentNumberCount = {};
         if (objectname === 'pfm142973_289353') {
             this.pfm142973_289353_searchKey = '';
             this.obj_pfm142973_289353 = selectedValue;
             this.obj_pfm144393.pfm142973_289353 = this.obj_pfm142973_289353.id;
             this.formGroup.patchValue({
                 obj_pfm144393_product: selectedValue.id
             });
         }
     }
     loadCheckboxEditValues(fieldName, values) {}
     loadDefaultValues() {}
     resetChildDependentInfo(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName) {

     }
     ionViewDidEnter() {}
     checkboxInitialization() {};
     backButtonOnclick() {
         if (this.recordEntryValidation()) {
             this.recordDiscardConfirmAlert();
         } else {
             this.navigatePopUpAction();
         }
     };
     navigatePopUpAction() {
         this.router.navigateByUrl(this.redirectUrl, {
             skipLocationChange: true
         });
     };
     hardWareBackButtonAction() {
         this.backButtonSubscribeRef = this.platform.backButton.subscribeWithPriority(999999, () => {
             if (this.customAlert) {
                 return;
             }
             this.backButtonOnclick();
         });
     }
     lookupClearAction(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName) {
         dataObj[dataObjectFieldName] = null;
         delete looklUpObj["id"];
         delete looklUpObj[lookupObjectFieldName];
         this.formGroup.value.formControlerName = null;
         this.resetChildDependentInfo(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName)
     }
     async recordDiscardConfirmAlert() {
         this.customAlert = await this.alerCtrl.create({
             backdropDismiss: false,
             message: 'Are you sure want to leave this page?',
             buttons: [{
                 text: 'Cancel',
                 cssClass: 'method-color',
                 handler: () => {
                     this.customAlert = null;
                     console.log('Individual clicked');
                 }
             }, {
                 text: 'Yes',
                 cssClass: 'method-color',
                 handler: () => {
                     this.navigatePopUpAction();
                 }
             }]
         });
         this.customAlert.present();
     }
     async presentToast(message) {
         const toast = await this.toastCtrl.create({
             message: message,
             duration: 2000,
             position: 'bottom'
         });
         toast.onDidDismiss().then((res) => {
             this.refreshButtonPressed();
         });
         toast.present();
     }
     refreshButtonPressed() {
         this.fetchRecordAgainstSelectedObject();
     }
     async showInfoAlert(info) {
         this.alerCtrl.create({
             message: info,
             subHeader: '',
             buttons: [{
                 text: 'Ok',
                 handler: () => {
                     console.log('Confirm Okay');
                 }
             }]
         }).then(alert => alert.present());
     }
     closeInfoAlert() {
         this.aniClassAddorRemove = false;
         setTimeout(() => {
             this.showAlert = false;
             this.infoMessage = '';
         }, 500);
     };
     onQuillSelectionChanged() {
         var Link = Quill.import('formats/link');
         Link.sanitize = function(url) {
             let protocol = url.slice(0, url.indexOf(':'));
             if (this.PROTOCOL_WHITELIST.indexOf(protocol) === -1) {
                 url = 'http://' + url;
             }
             let anchor = document.createElement('a');
             anchor.href = url;
             protocol = anchor.href.slice(0, anchor.href.indexOf(':'));
             return (this.PROTOCOL_WHITELIST.indexOf(protocol) > -1) ? url : this.SANITIZED_URL;
         }
         Quill.register(Link, true);
     }
     ionViewWillLeave() {
         this.backButtonSubscribeRef.unsubscribe();
     }
     ngOnInit() {}
 }