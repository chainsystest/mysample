 import {
     Component,
     ViewChild,
     OnInit,
     ApplicationRef,
     ChangeDetectorRef,
     HostListener,
     NgZone
 } from '@angular/core';
 import {
     couchdbProvider
 } from 'src/core/db/couchdbProvider';
 import {
     offlineDbIndexCreation
 } from 'src/core/utils/offlineDbIndexCreation';
 import {
     dataProvider
 } from 'src/core/utils/dataProvider';
 import {
     appConstant
 } from 'src/core/utils/appConstant';

 import {
     LoadingController,
     Events,
     ToastController,
     ModalController,
     IonVirtualScroll
 } from '@ionic/angular';
 import {
     EmailComposer
 } from '@ionic-native/email-composer/ngx';
 import {
     SocialSharing
 } from '@ionic-native/social-sharing/ngx';
 import {
     SMS
 } from '@ionic-native/sms/ngx';
 import {
     CallNumber
 } from '@ionic-native/call-number/ngx';
 import * as lodash from 'lodash';
 import {
     Router,
     ActivatedRoute
 } from '@angular/router';
 import {
     appUtility
 } from 'src/core/utils/appUtility';
 import {
     Platform
 } from '@ionic/angular';
 import {
     registerLocaleData
 } from '@angular/common';
 import {
     ColumnMode
 } from "@swimlane/ngx-datatable";
 import {
     FieldInfo
 } from "src/core/pipes/cspfm_data_display";

 @Component({
     selector: 'InventoryList_MOBILE_List',
     templateUrl: './InventoryList_MOBILE_List.html'
 }) export class InventoryList_MOBILE_List implements OnInit {
     @ViewChild(IonVirtualScroll) virtualScroll: IonVirtualScroll;
     public redirectUrl = "/";
     public isFromMenu = false;
     constructor(public events: Events, public dataProvider: dataProvider, public socialShare: SocialSharing, public loadingCtrl: LoadingController, public modalCtrl: ModalController,
         public callNumber: CallNumber, public emailComposer: EmailComposer, public toastCtrl: ToastController, public sms: SMS, public appUtilityConfig: appUtility, public platform: Platform, public router: Router, public activatRoute: ActivatedRoute, public dbService: couchdbProvider) {
         this.filteredResultList = [];
         this.resultList = [];
         this.activatRoute.queryParams.subscribe(params => {
             if (Object.keys(params).length == 0 && params.constructor === Object) {
                 console.log("list query params skipped");
                 return
             }
             if (params["isFromMenu"]) {
                 this.isFromMenu = params["isFromMenu"];
             }
             if (params["redirectUrl"]) {
                 this.redirectUrl = params["redirectUrl"]
             }
         });


         this.activatRoute.queryParams.subscribe(params => {
             // params['params'] = JSON.parse(Object.values(params).join(''));
             // console.log("Params", params);
             // console.log("Params :", params['params']);
             // console.log("Params parsed :", JSON.parse(Object.values(params['params']).splice(1, Object.values(params['params']).length - 2).join('')));
             if (params['params']) {
                 var queryParamsStr = params['params'];
                 if (queryParamsStr != '') {
                     // var userParameter = JSON.parse(Object.values(queryParamsStr).splice(1, Object.values(queryParamsStr).length - 2).join(''));
                     var userParameter = JSON.parse(queryParamsStr);
                     var dataKeys = Object.keys(userParameter);


                     let userParameterArray = [];

                     dataKeys.forEach(element => {
                         var objData = {};
                         objData['fieldName'] = element;
                         objData['value'] = userParameter[element];

                         userParameterArray.push(objData);
                     })
                     // userParameterArray.push(userParameter)
                     this.fetchAllData(undefined, userParameterArray);
                 } else {
                     this.fetchAllData();
                 }
             } else {
                 this.fetchAllData();
             }
         });


     }


     public dataSource = 'CouchDB';
     public searchQueryForDesignDoc = "";
     public devWidth = this.platform.width();
     resultList: Array < any > = [];
     public filteredResultList;
     // private layoutId  ;
     public filteredEventTriggeredList = [];
     public isSkeletonLoading = true;

     // private layoutId = 60212;
     public fetchActionInfo: any = {
         "processId": 172300,
         "processType": "DATA_OBJECT",
         "actionType": "VIEW",
         "paramValue": "LAYOUT"
     }
     private viewFetchActionInfo: Array < any > = [];

     private tableName_pfm151994 = 'pfm151994';
     public errorMessageToDisplay: string = "No Records";
     public eventsTriggeredList: Array < any > = [];
     public searchTerm: any = "";
     public filterApplied = false;
     public objectHierarchyJSON = {
         "objectName": "inventorymaster",
         "objectType": "PRIMARY",
         "relationShipType": null,
         "fieldId": "0",
         "objectId": "151994",
         "childObject": []
     };

     public layoutDataRestrictionSet = [];
     public layoutId = "60212";
     public newlyAddedRecordCount = 0;


     readonly headerHeight = 50;
     readonly rowHeight = 50;
     columnMode = ColumnMode;
     public isLoading;

     public listTableFieldInfoArray: Array < FieldInfo > = [{
         "label": "InventoryList_MOBILE_List.Element.inventorymaster.name",
         "fieldName": "name",
         "prop": "name",
         "fieldType": "TEXT",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }, {
         "label": "InventoryList_MOBILE_List.Element.inventorymaster.product",
         "fieldName": "product",
         "prop": "product",
         "fieldType": "TEXT",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }, {
         "label": "InventoryList_MOBILE_List.Element.inventorymaster.itemcreateddate",
         "fieldName": "itemcreateddate",
         "prop": "itemcreateddate",
         "fieldType": "TEXT",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }];
     public gridTableFieldInfoArray: Array < FieldInfo > = [{
         "label": "InventoryList_MOBILE_List.Element.inventorymaster.name",
         "fieldName": "name",
         "prop": "name",
         "fieldType": "TEXT",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }, {
         "label": "InventoryList_MOBILE_List.Element.inventorymaster.product",
         "fieldName": "product",
         "prop": "product",
         "fieldType": "TEXT",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }, {
         "label": "InventoryList_MOBILE_List.Element.inventorymaster.itemcreateddate",
         "fieldName": "itemcreateddate",
         "prop": "itemcreateddate",
         "fieldType": "TEXT",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }, {
         "label": "InventoryList_MOBILE_List.Element.inventorymaster.availablestatus",
         "fieldName": "availablestatus",
         "prop": "availablestatus",
         "fieldType": "TEXT",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }, {
         "label": "InventoryList_MOBILE_List.Element.inventorymaster.totalquantity",
         "fieldName": "totalquantity",
         "prop": "totalquantity",
         "fieldType": "NUMBER",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }, {
         "label": "InventoryList_MOBILE_List.Element.inventorymaster.availablequantity",
         "fieldName": "availablequantity",
         "prop": "availablequantity",
         "fieldType": "NUMBER",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }, {
         "label": "InventoryList_MOBILE_List.Element.inventorymaster.inventorydescription",
         "fieldName": "inventorydescription",
         "prop": "inventorydescription",
         "fieldType": "TEXTAREA",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }];
     async fetchAllData(event ? , userParameterArray ? ) {
         this.isLoading = true;

         if (userParameterArray) {
             this.fetchActionInfo['userSearchParams'] = {
                 'user_parameters': userParameterArray
             }
         }


         const fetchParams = {
             'fetchActionInfo': this.fetchActionInfo,
             'layoutId': this.layoutId,
             'dataSource': appConstant.jsonDBStaticName,
         }
         this.dataProvider.queryDataFromDataSource(fetchParams).then(res => {
             console.log("Response :", res);
             this.isLoading = false;
             this.isSkeletonLoading = false;
             if (event != undefined) {
                 event.target.complete();
                 this.virtualScroll.checkEnd();
             }
             if (res['status'] === 'SUCCESS') {
                 if (res['records'].length > 0) {

                     this.filteredResultList = [...res["records"]];
                     this.resultList = [...res["records"]];
                 }
             } else {
                 this.errorMessageToDisplay = res['message'];

                 if (this.errorMessageToDisplay == "No internet") {
                     this.presentNoInternetToast();
                 }
             }
         }).catch(error => {
             this.isSkeletonLoading = false;
         });
     }

     async onScroll(event) {
         const offsetY = event.offsetY;
         // total height of all rows in the viewport
         const offsetHeight = document.getElementById("table").offsetHeight; // this.el.nativeElement.getBoundingClientRect().height - this.headerHeight + 0;
         const viewHeight = offsetHeight - this.headerHeight;

         // check if we scrolled to the end of the viewport
         if (!this.isLoading && offsetY + viewHeight >= this.filteredResultList.length * this.rowHeight) {
             this.fetchAllData();
         }
     }

     onTableEventChanged(event) {
         if (event["type"] === "click") {
             this.onItemTap(event["row"]);
         }
     }


     async presentNoInternetToast() {
         const toast = await this.toastCtrl.create({
             message: "No internet connection. Please check your internet connection and try again.",
             showCloseButton: true,
             closeButtonText: "retry"
         });
         toast.onDidDismiss().then(() => {
             toast.dismiss();
             this.refreshDataInretry();
         });
         toast.present();
     }

     backButtonOnclick() {
         this.router.navigate([this.redirectUrl], {
             skipLocationChange: true
         });
     }

     refreshDataInretry() {
         this.fetchAllData();
     }
     getSearchedItems(searchText) {
         this.searchTerm = searchText;
         var queryFields = [{
             "fieldName": "name",
             "child": [],
             "mappingDetails": "",
             "fieldType": "TEXT"
         }, {
             "fieldName": "product",
             "child": [],
             "mappingDetails": "",
             "fieldType": "TEXT"
         }, {
             "fieldName": "itemcreateddate",
             "child": [],
             "mappingDetails": "",
             "fieldType": "TEXT"
         }, {
             "fieldName": "availablestatus",
             "child": [],
             "mappingDetails": "",
             "fieldType": "TEXT"
         }, {
             "fieldName": "totalquantity",
             "child": [],
             "mappingDetails": "",
             "fieldType": "NUMBER"
         }, {
             "fieldName": "availablequantity",
             "child": [],
             "mappingDetails": "",
             "fieldType": "NUMBER"
         }, {
             "fieldName": "inventorydescription",
             "child": [],
             "mappingDetails": "",
             "fieldType": "TEXTAREA"
         }];
         if (searchText === "") {
             this.filteredResultList = [...this.resultList];
             this.filteredEventTriggeredList = [...this.eventsTriggeredList];
             return;
         }
         this.filteredResultList = this.resultList.filter((item) => {
             return this.isSearchMatched(item, queryFields, searchText);
         });
         if (this.appUtilityConfig.isMobileResolution === false) {
             this.resultList = [...this.resultList]
             this.filteredResultList = [...this.filteredResultList]
         }

         this.filteredEventTriggeredList = this.eventsTriggeredList.filter((item) => {
             return this.isSearchMatched(item, queryFields, searchText);
         });

     }
     isSearchMatched(item, queryFields, searchText) {
         for (const queryField of queryFields) {
             if (queryField['fieldType'] == 'STRING' || queryField['fieldType'] == 'TEXT' || queryField['fieldType'] == 'TEXTAREA' || queryField['fieldType'] == 'EMAIL' || queryField['fieldType'] == 'NUMBER' || queryField['fieldType'] == 'URL' || queryField['fieldType'] == 'DECIMAL' || queryField['fieldType'] == 'AUTONUMBER' || queryField['fieldType'] == 'BOOLEAN') {
                 if (item[queryField['fieldName']] && item[queryField['fieldName']].toString().toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
                     return true;
                 }
             } else if (queryField['fieldType'] == 'MULTISELECT' || queryField['fieldType'] == 'CHECKBOX') {
                 if (item[queryField['fieldName']] !== undefined && item[queryField['fieldName']] !== "") {
                     const stateTypeList = item[queryField['fieldName']];
                     for (const element of stateTypeList) {
                         if (queryField['mappingDetails'][element] &&
                             queryField['mappingDetails'][element].toString().toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
                             return true;
                         }
                     }
                 }
             } else if (queryField['fieldType'] == 'LOOKUP') {
                 if (item[queryField['fieldName']]) {
                     if (this.isSearchMatched(item[queryField['fieldName']], queryField['child'], searchText)) {
                         return true
                     }
                 }
             } else if (queryField['fieldType'] == 'one_to_one') {
                 if (item[queryField['fieldName']] && item[queryField['fieldName']].length > 0) {
                     if (this.isSearchMatched(item[queryField['fieldName']][0], queryField['child'], searchText)) {
                         return true
                     }
                 }
             } else if (queryField['fieldType'] == 'RADIO' || queryField['fieldType'] == 'DROPDOWN') {
                 if (item[queryField['fieldName']] && item[queryField['fieldName']] !== "") {
                     if (queryField['mappingDetails'][item[queryField['fieldName']]] &&
                         queryField['mappingDetails'][item[queryField['fieldName']]].toString().toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
                         return true;
                     }
                 }
             }
         }
     }



     makeValidReverseHierarchyJSONObject(optionsSelectorKeys, hierarchyJSON) {
         if (optionsSelectorKeys.indexOf("pfm" + hierarchyJSON.objectId) > -1) {
             return hierarchyJSON
         } else {
             if (hierarchyJSON.childObject.length > 0) {
                 let tempReverseHierarchyJson = hierarchyJSON.childObject[0]
                 return this.makeValidReverseHierarchyJSONObject(optionsSelectorKeys, tempReverseHierarchyJson)
             } else {
                 return ''
             }
         }
     }

     makeMappingValueForObjects(mappingStr, reverseHierarchyJSON) {
         mappingStr = mappingStr + ',' + reverseHierarchyJSON.objectId
         if (reverseHierarchyJSON.childObject.length > 0) {
             return this.makeMappingValueForObjects(mappingStr, reverseHierarchyJSON.childObject[0])
         } else {
             return mappingStr
         }
     }



     onCancel() {

     }

     ngOnInit() {}
     ionViewDidEnter() {
         if (this.appUtilityConfig.isMobileResolution === false) {
             this.resultList = [...this.resultList]
             this.filteredResultList = [...this.filteredResultList]
         }
     }


     onItemTap(selectedObj) {
         const actionInfo_View = [{
             'fieldName': 'name',
             'value': selectedObj["name"],
         }]
         const queryParamsRouting = {
             id: selectedObj["id"],
             viewFetchActionInfo: JSON.stringify(actionInfo_View)
         };
         if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/InventoryDetails_MOBILE_Grid")) {
             queryParamsRouting['redirectUrl'] = "/menu/InventoryList_MOBILE_List"
         }
         this.router.navigate(["/menu/InventoryDetails_MOBILE_Grid"], {
             queryParams: queryParamsRouting,
             skipLocationChange: true
         });
     }
 }