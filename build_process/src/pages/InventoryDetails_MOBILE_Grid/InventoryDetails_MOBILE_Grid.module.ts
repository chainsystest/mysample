import { NgModule } from '@angular/core';
          import { CommonModule } from '@angular/common';
          import { FormsModule } from '@angular/forms';
          import { Routes, RouterModule } from '@angular/router';
          import { IonicModule } from '@ionic/angular';
           import {InventoryDetails_MOBILE_Grid} from './InventoryDetails_MOBILE_Grid';           
           import { SharedModule } from 'src/core/utils/shared.module';
           import { QuillModule } from 'ngx-quill';
           import { cs_whocolumn_iconmodule } from 'src/core/components/cs_whocolumn_icon/cs_whocolumn_icon.module';
           import { cs_status_workflowmodule } from '../../core/components/cs_status_workflow/cs_status_workflow.module';

           const routes: Routes = [
            {
              path: '',
              component: InventoryDetails_MOBILE_Grid
            }
          ];
           
             @NgModule({ 
              imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes),
                cs_whocolumn_iconmodule,
                QuillModule.forRoot(),
                SharedModule,
                cs_status_workflowmodule
              ],
                 declarations:[InventoryDetails_MOBILE_Grid]
                })
                export class InventoryDetails_MOBILE_Gridmodule{};