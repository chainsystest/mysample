 import {
     Component,
     ViewChild,
     OnInit,
     ApplicationRef,
     ChangeDetectorRef,
     HostListener,
     NgZone
 } from '@angular/core';
 import {
     couchdbProvider
 } from 'src/core/db/couchdbProvider';
 import {
     offlineDbIndexCreation
 } from 'src/core/utils/offlineDbIndexCreation';
 import {
     dataProvider
 } from 'src/core/utils/dataProvider';
 import {
     appConstant
 } from 'src/core/utils/appConstant';
 import {
     ToastController,
     LoadingController,
     Events,
     AlertController
 } from '@ionic/angular';
 import * as lodash from 'lodash';
 import {
     appUtility
 } from 'src/core/utils/appUtility';
 import {
     Router,
     ActivatedRoute
 } from '@angular/router';
 import {
     EmailComposer
 } from '@ionic-native/email-composer/ngx';
 import {
     SocialSharing
 } from '@ionic-native/social-sharing/ngx';
 import {
     SMS
 } from '@ionic-native/sms/ngx';
 import {
     CallNumber
 } from '@ionic-native/call-number/ngx';
 import * as _ from 'underscore';
 import {
     DatePipe
 } from '@angular/common';
 import {
     registerLocaleData
 } from '@angular/common';
 import {
     metaDataDbProvider
 } from 'src/core/db/metaDataDbProvider';
 import {
     metaDbConfiguration
 } from 'src/core/db/metaDbConfiguration';
 import {
     cspfmExecutionPouchDbProvider
 } from 'src/core/db/cspfmExecutionPouchDbProvider';
 import {
     cspfmExecutionPouchDbConfiguration
 } from 'src/core/db/cspfmExecutionPouchDbConfiguration';
 import {
     FieldInfo
 } from "src/core/pipes/cspfm_data_display";

 @Component({
     selector: 'InventoryDetails_MOBILE_Grid',
     templateUrl: './InventoryDetails_MOBILE_Grid.html'
 }) export class InventoryDetails_MOBILE_Grid implements OnInit {
     constructor(public dataProvider: dataProvider, public appUtilityConfig: appUtility, public socialShare: SocialSharing, public callNumber: CallNumber, public emailComposer: EmailComposer, public sms: SMS, public router: Router, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public activatRoute: ActivatedRoute, public events: Events, public applicationRef: ChangeDetectorRef, public metaDbConfigurationObj: metaDbConfiguration, public metaDbProvider: metaDataDbProvider, public cspfmexecutionPouchDbProvider: cspfmExecutionPouchDbProvider,
         public executionDbConfigObject: cspfmExecutionPouchDbConfiguration, private datePipe: DatePipe,
         public alerCtrl: AlertController, ) {
         this.activatRoute.queryParams.subscribe(params => {
             if (Object.keys(params).length == 0 && params.constructor === Object) {
                 console.log("list query params skipped");
                 return
             }
             if (params["redirectUrl"]) {
                 this.redirectUrl = params["redirectUrl"]
             }
             this.id = params["id"];
             this.viewFetchActionInfo = JSON.parse(params["viewFetchActionInfo"]);
             this.fetchSelectedObject();
         });
         this.appUtilityConfig.setEventSubscriptionlayoutIds(this.tableName_pfm151994, this.layoutId);
     }
     public obj_pfm151994: any = {};
     public gridFieldInfo: {
         [key: string]: FieldInfo
     } = {
         "pfm151994_name": {
             "label": "InventoryDetails_MOBILE_Grid.Element.inventorymaster.name",
             "fieldName": "name",
             "prop": "name",
             "fieldType": "TEXT",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm151994_availablestatus": {
             "label": "InventoryDetails_MOBILE_Grid.Element.inventorymaster.availablestatus",
             "fieldName": "availablestatus",
             "prop": "availablestatus",
             "fieldType": "TEXT",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm151994_totalquantity": {
             "label": "InventoryDetails_MOBILE_Grid.Element.inventorymaster.totalquantity",
             "fieldName": "totalquantity",
             "prop": "totalquantity",
             "fieldType": "NUMBER",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm151994_itemcreateddate": {
             "label": "InventoryDetails_MOBILE_Grid.Element.inventorymaster.itemcreateddate",
             "fieldName": "itemcreateddate",
             "prop": "itemcreateddate",
             "fieldType": "TEXT",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm151994_inventorydescription": {
             "label": "InventoryDetails_MOBILE_Grid.Element.inventorymaster.inventorydescription",
             "fieldName": "inventorydescription",
             "prop": "inventorydescription",
             "fieldType": "TEXTAREA",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm151994_product": {
             "label": "InventoryDetails_MOBILE_Grid.Element.inventorymaster.product",
             "fieldName": "product",
             "prop": "product",
             "fieldType": "TEXT",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm151994_availablequantity": {
             "label": "InventoryDetails_MOBILE_Grid.Element.inventorymaster.availablequantity",
             "fieldName": "availablequantity",
             "prop": "availablequantity",
             "fieldType": "NUMBER",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         }
     };
     public formulaReverseObjectHierarchyJSON = undefined;
     public formulaConfigJSON = {};
     public formulafields = {};
     private tableName_pfm151994 = 'pfm151994';
     public errorMessageToDisplay = "No Records";
     public dbServiceProvider = appConstant.jsonDBStaticName;
     public formulaObject = {};
     private currentStatusWorkFlowActionFieldId;
     public WorkFlowUserApprovalStatusDataObject = {};
     private workFlowMapping = {

     }


     public id: any = '';

     // private layoutId = 60213;
     public fetchActionInfo: any = {
         "processId": 172300,
         "processType": "DATA_OBJECT",
         "actionType": "VIEW",
         "paramValue": "LAYOUT"
     }
     private viewFetchActionInfo: Array < any > = [];

     private objectHierarchyJSON = {
         "objectName": "inventorymaster",
         "objectType": "PRIMARY",
         "relationShipType": null,
         "fieldId": "0",
         "objectId": "151994",
         "childObject": []
     };
     private prominentDataMapping = {
         "pfm151994": ["name", "itemcreateddate", "product"]
     };;
     private documentInfo = [];
     private attachmentInfo = [];
     private redirectUrl = "/";
     public isSkeletonLoading = true;
     private approverType: string = "";
     private cscomponentactionInfo = {

     };
     public layoutId = "60213";
     private loading;
     ngOnDestroy() {
         this.events.unsubscribe(this.tableName_pfm151994);
         this.appUtilityConfig.removeEventSubscriptionlayoutIds(this.tableName_pfm151994, this.layoutId)
     }
     backButtonOnclick() {
         this.toastCtrl.dismiss();
         this.router.navigate([this.redirectUrl], {
             skipLocationChange: true
         });
     }

     async fetchSelectedObject() {
         if (this.viewFetchActionInfo && this.viewFetchActionInfo.length > 0) {
             this.fetchActionInfo['userSearchParams'] = {
                 'user_parameters': this.viewFetchActionInfo
             }
         }

         let additionalObjectdata = {};
         additionalObjectdata['id'] = this.id;

         const fetchParams = {
             'fetchActionInfo': this.fetchActionInfo,
             'layoutId': this.layoutId,
             'dataSource': appConstant.jsonDBStaticName,
         }
         this.dataProvider.querySingleDoc(fetchParams).then(result => {
             this.isSkeletonLoading = false;
             if (result["status"] !== "SUCCESS") {
                 this.errorMessageToDisplay = result["message"];
                 if (this.errorMessageToDisplay === "No internet") {
                     this.presentNoInternetToast();
                 }
                 return;
             }
             this.obj_pfm151994 = result["records"][0];




             if (!this.applicationRef['destroyed']) {
                 this.applicationRef.detectChanges();
             }

         }).catch(error => {
             this.isSkeletonLoading = false;
             console.log(error);
         });
     }


     async presentToast(message) {
         const toast = await this.toastCtrl.create({
             message: message,
             duration: 2000,
             position: 'bottom'
         });
         toast.dismiss(() => {
             console.log('Dismissed toast');
         });
         toast.present();
     }

     async presentNoInternetToast() {
         const toast = await this.toastCtrl.create({
             message: "No internet connection. Please check your internet connection and try again.",
             showCloseButton: true,
             closeButtonText: "retry"
         });
         toast.onDidDismiss().then(() => {
             toast.dismiss();
             this.refreshDataInretry();
         });
         toast.present();
     }

     refreshDataInretry() {
         this.fetchSelectedObject();
     }
     ngOnInit() {}
 }