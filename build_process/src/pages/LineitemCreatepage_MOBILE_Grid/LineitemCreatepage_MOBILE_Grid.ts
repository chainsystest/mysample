import {
    Component,
    OnInit,
    ViewChild,
    ApplicationRef,
    HostListener,
    NgZone
} from '@angular/core';
import {
    dataProvider
} from 'src/core/utils/dataProvider';
import {
    appConstant
} from 'src/core/utils/appConstant';
import {
    ModalController,
    Platform,
    LoadingController,
    Events,
    ToastController,
    AlertController,
    IonContent
} from '@ionic/angular';
import {
    DrawerState
} from 'ion-bottom-drawer';
import {
    PopoverController
} from '@ionic/angular';
import {
    appUtility
} from 'src/core/utils/appUtility';
import {
    ScreenOrientation
} from '@ionic-native/screen-orientation/ngx';
import {
    lookupFieldMapping
} from 'src/core/pfmmapping/lookupFieldMapping';
import {
    objectTableMapping
} from 'src/core/pfmmapping/objectTableMapping';
import * as moment from 'moment';
import {
    Router,
    ActivatedRoute
} from '@angular/router';
import {
    SocialSharing
} from '@ionic-native/social-sharing/ngx';
import {
    EmailComposer
} from '@ionic-native/email-composer/ngx';
import {
    CallNumber
} from '@ionic-native/call-number/ngx';
import {
    SMS
} from '@ionic-native/sms/ngx';
import {
    attachmentDbProvider
} from 'src/core/db/attachmentDbProvider';
import * as _ from 'underscore';
import {
    DatePipe
} from '@angular/common';
import {
    registerLocaleData
} from '@angular/common';
import {
    metaDataDbProvider
} from 'src/core/db/metaDataDbProvider';
import {
    metaDbConfiguration
} from 'src/core/db/metaDbConfiguration';
import {
    cspfmExecutionPouchDbProvider
} from 'src/core/db/cspfmExecutionPouchDbProvider';
import {
    cspfmExecutionPouchDbConfiguration
} from 'src/core/db/cspfmExecutionPouchDbConfiguration';
import India from '@angular/common/locales/en-IN';
registerLocaleData(India);
import * as lodash from 'lodash';
import {
    lookuppage
} from 'src/core/pages/lookuppage/lookuppage';
import {
    FormBuilder,
    Validators,
    FormGroup
} from '@angular/forms';
import 'moment-timezone';
import * as Quill from 'quill';
import {
    cspfmFieldTrackingMapping
} from 'src/core/pfmmapping/cspfmFieldTrackingMapping';
import {
    dbProvider
} from 'src/core/db/dbProvider';
import {
    Http
} from '@angular/http';
import {
    dbConfiguration
} from 'src/core/db/dbConfiguration';
import {
    couchdbProvider
} from 'src/core/db/couchdbProvider';
import {
    FieldInfo
} from "src/core/pipes/cspfm_data_display";
@Component({
    selector: 'LineitemCreatepage_MOBILE_Grid',
    templateUrl: './LineitemCreatepage_MOBILE_Grid.html'
}) export class LineitemCreatepage_MOBILE_Grid implements OnInit {
    @ViewChild('parentContent') parentContent: IonContent;
    @ViewChild('childContent') childContent: IonContent;
    isBrowser: boolean = false;
    headerenable = true;
    drawerComponentDockedHeight = 300;
    drawerComponentCurrentState = DrawerState.Top;
    drawerComponentPreviousState = DrawerState.Docked;
    private obj_pfm144393: any = {};
    public ionDateTimeDisplayValue = {};
    private obj_pfm144393_Temp: any = {};
    public formGroup: FormGroup;
    private customAlert;
    public action = 'Add';
    private id: any = {};
    private tableName_pfm144393 = 'pfm144393';
    public isSaveActionTriggered: Boolean = false;
    private isValidFrom: Boolean = true;
    public dbServiceProvider = appConstant.pouchDBStaticName;
    public selectedDataObject: any = {};
    public formulaObject = {};
    public isFieldTrackingEnable: Boolean = false;
    private showAlert: boolean = false;
    infoMessage = '';
    aniClassAddorRemove: boolean = false;
    private workFlowInitiateList = {};
    public savedSuccessMessage = 'data saved sucessfully';
    public updatedSuccessMessage = 'data updated sucessfully';
    public savedErrorMessage = 'Error saving record';
    public fetchErrorMessage = 'Fetching Failed';
    private dependentNumberCount = {};
    public partiallySavedSuccessMessage = 'data partially saved';
    private backButtonSubscribeRef;
    private unRegisterBackButtonAction: Function;
    public parentObjLabel = '';
    public parentObjValue = '';
    private errorMessageToDisplay: string = 'No Records';
    private parentName = '';
    private parentId = '';
    public isParentObjectShow = false;
    public isSkeletonLoading = true;
    isViewRunning = false;
    loading;
    private objectHierarchyJSON = {
        "objectName": "crmsaleorderlines",
        "objectType": "PRIMARY",
        "relationShipType": null,
        "fieldId": "0",
        "objectId": "144393",
        "childObject": [{
            "objectName": "crmproduct",
            "objectType": "LOOKUP",
            "relationShipType": "LOOKUP",
            "fieldId": "289353",
            "objectId": "142973",
            "childObject": []
        }, {
            "objectName": "crmsaleorders",
            "objectType": "HEADER",
            "relationShipType": "one_to_many",
            "fieldId": "301393",
            "objectId": "144373",
            "childObject": []
        }]
    };
    public isFromMenu = false;
    private redirectUrl = '/';
    private dependentFieldTriggerList = {};
    private geoLocationdependentField = {};
    public pickListValues = {};
    public parentObject: any = {};
    private obj_pfm142973_289353: any = {};
    private pfm142973_289353_searchKey;
    public formulaReverseObjectHierarchyJSON = [{
        "toLevel": 1,
        "relationShipType": "one_to_many",
        "isFormulaObject": "Y",
        "childObject": [{
            "toLevel": 1,
            "relationShipType": "",
            "isFormulaObject": "N",
            "childObject": [],
            "objectName": "crmproduct",
            "type": "P",
            "referenceObjectId": "144393",
            "objectId": "142973",
            "objectType": "LOOKUP"
        }],
        "objectName": "crmsaleorderlines",
        "type": "P",
        "referenceObjectId": 0,
        "objectId": "144393",
        "objectType": "PRIMARY"
    }];
    public formulaConfigJSON = {
        "pfm144393": {
            "totalamount": {
                "fieldName": "totalamount",
                "objectId": 144393,
                "displayformula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                "formula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                "operands": [{
                    "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                    "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                    "fieldName": "listprice",
                    "level": "1",
                    "refFieldId": 301754,
                    "fieldType": "currency",
                    "objectId": 144393,
                    "fieldId": 289357,
                    "objectType": "PRIMARY"
                }, {
                    "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                    "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                    "fieldName": "quantity",
                    "level": "1",
                    "refFieldId": 301754,
                    "fieldType": "number",
                    "objectId": 144393,
                    "fieldId": 289359,
                    "objectType": "PRIMARY"
                }, {
                    "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                    "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                    "fieldName": "listprice",
                    "level": "1",
                    "refFieldId": 301754,
                    "fieldType": "currency",
                    "objectId": 144393,
                    "fieldId": 289357,
                    "objectType": "PRIMARY"
                }, {
                    "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                    "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                    "fieldName": "quantity",
                    "level": "1",
                    "refFieldId": 301754,
                    "fieldType": "number",
                    "objectId": 144393,
                    "fieldId": 289359,
                    "objectType": "PRIMARY"
                }, {
                    "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                    "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                    "fieldName": "taxpercentage",
                    "level": "1",
                    "refFieldId": 301754,
                    "fieldType": "number",
                    "objectId": 144393,
                    "fieldId": 302833,
                    "objectType": "PRIMARY"
                }, {
                    "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                    "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                    "fieldName": "listprice",
                    "level": "1",
                    "refFieldId": 301754,
                    "fieldType": "currency",
                    "objectId": 144393,
                    "fieldId": 289357,
                    "objectType": "PRIMARY"
                }, {
                    "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                    "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                    "fieldName": "quantity",
                    "level": "1",
                    "refFieldId": 301754,
                    "fieldType": "number",
                    "objectId": 144393,
                    "fieldId": 289359,
                    "objectType": "PRIMARY"
                }, {
                    "refFormula": "( listprice * quantity ) + ( listprice * quantity * taxpercentage / 100 ) - ( listprice * quantity * discountpercentage / 100 )",
                    "refHiddenFormula": "( pfm144393_listprice * pfm144393_quantity ) + ( pfm144393_listprice * pfm144393_quantity * pfm144393_taxpercentage / 100 ) - ( pfm144393_listprice * pfm144393_quantity * pfm144393_discountpercentage / 100 )",
                    "fieldName": "discountpercentage",
                    "level": "1",
                    "refFieldId": 301754,
                    "fieldType": "number",
                    "objectId": 144393,
                    "fieldId": 302834,
                    "objectType": "PRIMARY"
                }]
            }
        }
    };
    public formulafields = {
        "pfm144393": ["listprice", "quantity", "taxpercentage", "discountpercentage"]
    };
    childObjectList = [];
    objResultMap = new Map < string, any > ();
    objDisplayName = {
        'pfm144393': 'crmsaleorderlines',
    };
    recordEntryValidation() {
        const formGroupObjectValue = Object.assign({}, this.formGroup.value);
        this.obj_pfm144393 = Object.assign(this.obj_pfm144393, formGroupObjectValue.pfm144393);
        if (
            this.obj_pfm144393["quantity"] || this.obj_pfm144393["listprice"] || this.obj_pfm144393["olineid"] || this.obj_pfm144393["productname"] || this.obj_pfm144393["discountpercentage"] || this.obj_pfm144393["partno"] || this.obj_pfm144393["totalamount"] || this.obj_pfm144393["listprice"] || this.obj_pfm144393["itemno"] || this.obj_pfm144393["taxpercentage"] || this.obj_pfm144393["product"] || this.obj_pfm144393["productdescription"] || this.obj_pfm144393["productdescription"]) {
            return true;
        } else {
            return false;
        }
    }
    constructor(public dbConfiguration: dbConfiguration, public http: Http, public popoverCtrl: PopoverController, public modalCtrl: ModalController, public dbService: dbProvider,
        private socialSharing: SocialSharing, private emailComposer: EmailComposer, private callNumber: CallNumber, private sms: SMS,
        public platform: Platform, public applicationRef: ApplicationRef, public formBuilder: FormBuilder,
        public events: Events, public router: Router, public activatRoute: ActivatedRoute,
        public objectTableMapping: objectTableMapping, public lookupFieldMapping: lookupFieldMapping,
        public loadingCtrl: LoadingController, public toastCtrl: ToastController, public dataProvider: dataProvider, public metaDbConfigurationObj: metaDbConfiguration, public metaDbProvider: metaDataDbProvider, public cspfmexecutionPouchDbProvider: cspfmExecutionPouchDbProvider,
        public executionDbConfigObject: cspfmExecutionPouchDbConfiguration, private datePipe: DatePipe, public fieldTrackMapping: cspfmFieldTrackingMapping,
        public alerCtrl: AlertController, public appUtility: appUtility, public screenOrientation: ScreenOrientation, private ngZone: NgZone) {
        this.activatRoute.queryParams.subscribe(params => {
            if (Object.keys(params).length == 0 && params.constructor === Object) {
                console.log("list query params skipped");
                return
            }
            if (params["isFromMenu"]) {
                this.isFromMenu = params["isFromMenu"];
            }
            if (params["redirectUrl"]) {
                this.redirectUrl = params["redirectUrl"]
            }
            this.action = params['action'];
            this.parentId = params['parentId'];
            this.id = params['id'];
            this.fetchParentObj();
            this.parentName = params['parentName'];
            if (this.parentId) {
                this.isParentObjectShow = true;
                this.parentObjLabel = params['parentFieldLabel'];
                this.parentObjValue = params['parentFieldValue'];
            }
            this.initializeObjects(dataProvider.tableStructure());
            if (this.action == 'Edit') {
                this.id = params['id'];
                this.fetchRecordAgainstSelectedObject();
            } else {
                this.isSkeletonLoading = false;
                this.checkboxInitialization();
                if (this.parentId !== undefined && this.parentName !== "" && this.parentId !== "" && this.parentName !== "") {
                    this.fetchParentObjectForFormulaCalculation(this.parentId, this.parentName)
                }
            }
        });
        const windowHeight = window.innerHeight;
        this.drawerComponentDockedHeight = windowHeight / 2;
        this.screenOrientation.onChange().subscribe(() => {
            this.drawerComponentCurrentState = DrawerState.Bottom;
            const windowHeightVal = window.innerHeight;
            this.drawerComponentDockedHeight = windowHeightVal / 2;
        });
        this.createFormGroup();
        this.hardWareBackButtonAction();
    }
    initializeObjects(tableStructure) {
        this.obj_pfm144393 = JSON.parse(JSON.stringify(tableStructure.pfm144393));
        this.obj_pfm142973_289353 = JSON.parse(JSON.stringify(tableStructure.pfm142973));
    }
    fetchParentObj() {
        if (this.action == 'Edit') {
            return;
        }
        for (let childItem of this.objectHierarchyJSON['childObject']) {
            if (childItem['objectType'] == 'HEADER') {
                this.fetchParentWithParentJSON(childItem);
            }
        }
    }
    fetchParentWithParentJSON(parentJSON) {
        const additionalObjectdata = {};
        additionalObjectdata['id'] = this.parentId;
        const fetchParams = {
            'objectHierarchyJSON': parentJSON,
            'additionalInfo': additionalObjectdata,
            'dataSource': appConstant.pouchDBStaticName
        };
        this.dataProvider.querySingleDoc(fetchParams).then(res => {
            this.isSkeletonLoading = false;
            if (res['status'] !== 'SUCCESS') {
                this.errorMessageToDisplay = res['message'];
                if (this.errorMessageToDisplay == 'No internet') {
                    this.presentNoInternetToast();
                }
                return;
            }
            if (res['records'].length < 0) {
                console.log('FetchRecordAgainstSelectedObject No Records');
                return;
            }
            let dataObj = res['records'][0];
            this.parentObject = dataObj;
        }).catch(error => {
            this.isSkeletonLoading = false;
            this.showInfoAlert(this.fetchErrorMessage)
        })
    }
    fetchRecordAgainstSelectedObject() {
        const additionalObjectdata = {};
        additionalObjectdata['id'] = this.id;
        const fetchParams = {
            'objectHierarchyJSON': this.objectHierarchyJSON,
            'additionalInfo': additionalObjectdata,
            'dataSource': appConstant.pouchDBStaticName
        };
        this.dataProvider.querySingleDoc(fetchParams).then(res => {
            this.isSkeletonLoading = false;
            if (res['status'] !== 'SUCCESS') {
                this.errorMessageToDisplay = res['message'];
                if (this.errorMessageToDisplay == 'No internet') {
                    this.presentNoInternetToast();
                }
                return;
            }
            if (res['records'].length < 0) {
                console.log('FetchRecordAgainstSelectedObject No Records');
                return;
            }
            let dataObj = res['records'][0];
            this.obj_pfm144393_Temp = lodash.extend({}, this.obj_pfm144393, dataObj);
            this.selectedDataObject['pfm144393'] = JSON.stringify(this.obj_pfm144393_Temp);
            this.obj_pfm144393 = lodash.extend({}, this.obj_pfm144393, dataObj);
            this.obj_pfm142973_289353 = this.obj_pfm144393['pfm142973_289353'] ? this.obj_pfm144393['pfm142973_289353'] : "";
            this.formGroup.patchValue({
                pfm144393: this.obj_pfm144393
            });
            this.parentObject = dataObj['pfm144373s'][0];
            this.applicationRef.tick();
            this.formulaReverseObjectHierarchyJSON.forEach(hierarchyJSONObject => {
                this.getFormulaDataObject(this.obj_pfm144393, hierarchyJSONObject)
            });
        }).catch(error => {
            this.isSkeletonLoading = false;
            this.showInfoAlert(this.fetchErrorMessage)
        })
    }
    async getFormulaDataObject(resultObject, objectReverseHierarchyJSON) {
        const fetchParams = {
            'objectReverseHierarchyJSON': objectReverseHierarchyJSON,
            'objectHierarchyJSON': this.objectHierarchyJSON,
            'dataSource': appConstant.pouchDBStaticName,
            'additionalInfo': resultObject
        }
        this.dataProvider.querySingleFormualDoc(fetchParams).then(result => {
            console.log("result = ", result);
            if (result['status'] !== 'SUCCESS') {
                this.errorMessageToDisplay = result['message'];
                if (this.errorMessageToDisplay === "No internet") {
                    this.presentNoInternetToast();
                }
                return
            }
            this.formulaObject = result["records"]
        }).catch(error => {
            console.log("error====", error)
        });
    }
    async fetchParentObjectForFormulaCalculation(parentId, parentName) {
        const additionalObjectdata = {};
        additionalObjectdata['id'] = parentId;
        additionalObjectdata['type'] = parentName;
        for (let i = 0; i < this.formulaReverseObjectHierarchyJSON.length; i++) {
            const objectReverseHierarchyJSON = this.formulaReverseObjectHierarchyJSON[i]
            const fetchParams = {
                'objectReverseHierarchyJSON': objectReverseHierarchyJSON,
                'objectHierarchyJSON': this.objectHierarchyJSON,
                'dataSource': appConstant.pouchDBStaticName,
                'additionalInfo': additionalObjectdata,
                'fetchParent': true
            }
            this.dataProvider.querySingleFormualDoc(fetchParams).then(result => {
                console.log("result = ", result);
                if (result['status'] !== 'SUCCESS') {
                    this.errorMessageToDisplay = result['message'];
                    if (this.errorMessageToDisplay === "No internet") {
                        this.presentNoInternetToast();
                    }
                    return
                }
                this.formulaObject = result["records"]
            }).catch(error => {
                console.log("error====", error)
            });
        }

    }
    formGroupUpdate() {
        this.formGroup.updateValueAndValidity({
            onlySelf: false,
            emitEvent: true
        });
        this.formGroup.valueChanges.subscribe(form => {
            const formControls = this.formGroup.controls;
            if (formControls.pfm144393['controls'][this.formulafields["pfm144393"][0]].dirty || formControls.pfm144393['controls'][this.formulafields["pfm144393"][1]].dirty || formControls.pfm144393['controls'][this.formulafields["pfm144393"][2]].dirty || formControls.pfm144393['controls'][this.formulafields["pfm144393"][3]].dirty) {
                this.ngZone.run(() => {
                    console.log("ngzone run");
                    const formulaObject1 = {};
                    formulaObject1["pfm144393"] = form["pfm144393"];
                    this.formulaObject = formulaObject1;
                });
            }
        });
    };
    updateGeoLocationFlag(objectName, dataObj, existingDataObj) {
        Object.keys(this.geoLocationdependentField[objectName]).forEach(element => {
            const depFlds = this.geoLocationdependentField[objectName][element]['dependentFields'];
            depFlds.forEach(fieldName => {
                if (this.action == 'Edit') {
                    if (existingDataObj[fieldName] != dataObj[fieldName]) {
                        dataObj[this.geoLocationdependentField[objectName][element]['dependentFieldName']] = 'Y';
                        return;
                    }
                } else {
                    if (dataObj[fieldName] != '') {
                        dataObj[this.geoLocationdependentField[objectName][element]['dependentFieldName']] = 'Y';
                        return;
                    }
                }
            })
        });
    }
    async presentNoInternetToast() {
        const toast = await this.toastCtrl.create({
            message: "No internet connection. Please check your internet connection and try again.",
            showCloseButton: true,
            closeButtonText: "retry"
        });
        toast.onDidDismiss().then(() => {
            toast.dismiss();
            this.refreshData();
        });
        toast.present();
    }

    refreshData() {
        this.fetchRecordAgainstSelectedObject();
    }
    async saveButtonOnclick() {
        this.formGroup.patchValue({
            pfm144393: {
                quantity: this.obj_pfm144393['quantity'],
                olineid: this.obj_pfm144393['olineid'],
                discountpercentage: this.obj_pfm144393['discountpercentage'],
                partno: this.obj_pfm144393['partno'],
                totalamount: this.obj_pfm144393['totalamount'],
                listprice: this.obj_pfm144393['listprice'],
                taxpercentage: this.obj_pfm144393['taxpercentage'],
                productdescription: this.obj_pfm144393['productdescription']
            }
        });
        if (this.formGroup.valid) {
            this.formGroup.patchValue({});
            this.isValidFrom = true;
            if (this.obj_pfm144393['quantity'] !== null) {
                this.obj_pfm144393['quantity'] = Number(this.obj_pfm144393['quantity']);
            }
            if (this.obj_pfm144393['discountpercentage'] !== null) {
                this.obj_pfm144393['discountpercentage'] = Number(this.obj_pfm144393['discountpercentage']);
            }
            if (this.obj_pfm144393['listprice'] !== null) {
                this.obj_pfm144393['listprice'] = Number(this.obj_pfm144393['listprice']);
            }
            if (this.obj_pfm144393['taxpercentage'] !== null) {
                this.obj_pfm144393['taxpercentage'] = Number(this.obj_pfm144393['taxpercentage']);
            }
            const fieldTrackObject = this.fieldTrackMapping.mappingDetail[this.tableName_pfm144393]
            if (fieldTrackObject) {
                this.isFieldTrackingEnable = true;
            } else {
                this.isFieldTrackingEnable = false;
            }
            let previousParentObject
            if (this.action === "Edit") {
                previousParentObject = this.selectedDataObject[this.tableName_pfm144393];
                console.log("previousParentObject = ", previousParentObject);
            } else {
                previousParentObject = undefined
            }
            if (this.parentId) {
                this.obj_pfm144393[this.parentName] = this.parentId;
            };
            this.dataProvider.save(this.tableName_pfm144393, this.obj_pfm144393, appConstant.pouchDBStaticName, previousParentObject, this.isFieldTrackingEnable)
                .then(result => {
                    if (result['status'] != 'SUCCESS') {
                        this.showInfoAlert(result['message']);
                        return;
                    }
                    if (this.childObjectList.length == 0) {
                        this.presentToast(this.savedSuccessMessage);
                        let opts = {
                            animate: false
                        };
                        this.navigatePopUpAction();
                        return
                    };

                })
                .catch(error => {
                    this.showInfoAlert(this.savedErrorMessage);
                    console.log(error)
                });
        } else {
            this.formGroup.patchValue({});
            this.isValidFrom = false;
            this.scrollToValidationFailedField();
        }
        var errorValue = document.querySelector('.entry-page-content');
        errorValue.setAttribute('class', 'entry-page-content entryErrorMessage hydrated');
    };
    scrollToValidationFailedField() {
        const formControls = this.formGroup.controls.pfm144393;
        const formGroupKeys = Object.keys(formControls["controls"]);
        let isValidationSucceed = true;
        formGroupKeys.every(element => {
            if (formControls["controls"][element].status === "INVALID") {
                const yOffset = document.getElementById("pfm144393_" + element).offsetTop;
                this.childContent.scrollToPoint(0, yOffset, 1000);
                isValidationSucceed = false;
                return false;
            } else {
                return true;
            }
        });
    }
    createFormGroup() {
        this.formGroup = this.formBuilder.group({
            pfm144393: this.formBuilder.group({
                quantity: [null, Validators.compose([Validators.required])],
                discountpercentage: [null, Validators.compose([])],
                partno: [null, Validators.compose([Validators.required])],
                totalamount: [null, Validators.compose([])],
                listprice: [null, Validators.compose([Validators.required])],
                taxpercentage: [null, Validators.compose([])],
                pfm142973_289353_searchKey: [''],
                productdescription: [null, Validators.compose([])]
            })
        });
        this.formGroupUpdate();
    }
    async showLookup_289353(objectname, label, displayColumns) {
        const lookupInput = {};
        const lookupColumns = [{
            columnName: 'name',
            displayName: 'Product Id',
            fieldType: 'Autonumber',
            mappingValues: {}
        }, {
            columnName: 'listprice',
            displayName: 'List Price',
            fieldType: 'Currency',
            mappingValues: {}
        }, {
            columnName: 'productname',
            displayName: 'Product Name',
            fieldType: 'Text',
            mappingValues: {}
        }, {
            columnName: 'itemno',
            displayName: 'Item No',
            fieldType: 'Text',
            mappingValues: {}
        }];
        const lookupHierarchyJson = {
            "objectName": "crmproduct",
            "objectType": "PRIMARY",
            "relationShipType": null,
            "fieldId": 0,
            "objectId": "142973",
            "childObject": []
        };
        const selector = {};
        selector['data.type'] = 'pfm142973';;
        if (this.pfm142973_289353_searchKey) {
            var regexp = new RegExp(this.pfm142973_289353_searchKey, 'i');
            selector['data.name'] = {
                $regex: regexp
            };
        } else if (this.obj_pfm142973_289353['name']) {
            var regexp = new RegExp(this.obj_pfm142973_289353['name'], 'i');
            selector['data.name'] = {
                $regex: regexp
            };
        }
        lookupHierarchyJson['options'] = {
            'selector': selector
        };
        lookupInput['lookupColumnDetails'] = lookupColumns;
        lookupInput['objectHierarchy'] = lookupHierarchyJson;
        lookupInput['title'] = label;
        const lookupModal = await this.modalCtrl.create({
            component: lookuppage,
            componentProps: {
                serviceObject: this.dbService,
                parentPage: this,
                lookupColumnName: objectname,
                lookupInput: lookupInput,
                dataSource: appConstant.pouchDBStaticName
            }
        });
        await lookupModal.present();
    }
    lookupResponse(objectname, selectedValue) {
        this.dependentNumberCount = {};
        if (objectname === 'pfm142973_289353') {
            this.pfm142973_289353_searchKey = '';
            this.obj_pfm142973_289353 = selectedValue;
            this.obj_pfm144393.pfm142973_289353 = this.obj_pfm142973_289353.id;
            this.formGroup.patchValue({
                obj_pfm144393_product: selectedValue.id
            });
        }
    }
    loadCheckboxEditValues(fieldName, values) {}
    loadDefaultValues() {}
    resetChildDependentInfo(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName) {

    }
    public orderstatus_289221 = {
        "Draft": "Draft",
        "In Review": "In Review",
        "Approved": "Approved",
        "Rejected": "Rejected",
        "Confirmed": "Confirmed",
        "Processed": "Processed",
        "Shipped": "Shipped",
        "Delivered": "Delivered",
        "Completed": "Completed",
        "Cancelled": "Cancelled"
    };
    public gridFieldInfo: {
        [key: string]: FieldInfo
    } = {
        "pfm144373_orderid": {
            "label": "LineitemCreatepage_MOBILE_Grid.Element.crmsaleorders.orderid",
            "fieldName": "orderid",
            "prop": "orderid",
            "fieldType": "AUTONUMBER",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm144373_orderstatus": {
            "label": "LineitemCreatepage_MOBILE_Grid.Element.crmsaleorders.orderstatus",
            "fieldName": "orderstatus",
            "prop": "orderstatus",
            "fieldType": "DROPDOWN",
            "child": "",
            "dateFormat": "",
            "mappingDetails": {
                "Draft": "Draft",
                "In Review": "In Review",
                "Approved": "Approved",
                "Rejected": "Rejected",
                "Confirmed": "Confirmed",
                "Processed": "Processed",
                "Shipped": "Shipped",
                "Delivered": "Delivered",
                "Completed": "Completed",
                "Cancelled": "Cancelled"
            },
            "currencyDetails": ""
        },
        "pfm144373_orderconfirmeddate": {
            "label": "LineitemCreatepage_MOBILE_Grid.Element.crmsaleorders.orderconfirmeddate",
            "fieldName": "orderconfirmeddate",
            "prop": "orderconfirmeddate",
            "fieldType": "DATE",
            "child": "",
            "dateFormat": this.appUtility.userDateFormat,
            "mappingDetails": "",
            "currencyDetails": ""
        }
    };
    checkboxInitialization() {};
    backButtonOnclick() {
        if (this.recordEntryValidation()) {
            this.recordDiscardConfirmAlert();
        } else {
            this.navigatePopUpAction();
        }
    };
    navigatePopUpAction() {
        this.router.navigateByUrl(this.redirectUrl, {
            skipLocationChange: true
        });
    };
    hardWareBackButtonAction() {
        this.backButtonSubscribeRef = this.platform.backButton.subscribeWithPriority(999999, () => {
            if (this.customAlert) {
                return;
            }
            this.backButtonOnclick();
        });
    }
    lookupClearAction(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName) {
        dataObj[dataObjectFieldName] = "";
        delete looklUpObj["id"];
        delete looklUpObj[lookupObjectFieldName];
        this.formGroup.value.formControlerName = "";
        this.resetChildDependentInfo(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName)
    }
    async recordDiscardConfirmAlert() {
        this.customAlert = await this.alerCtrl.create({
            backdropDismiss: false,
            message: 'Are you sure want to leave this page?',
            buttons: [{
                text: 'Cancel',
                cssClass: 'method-color',
                handler: () => {
                    this.customAlert = null;
                    console.log('Individual clicked');
                }
            }, {
                text: 'Yes',
                cssClass: 'method-color',
                handler: () => {
                    this.navigatePopUpAction();
                }
            }]
        });
        this.customAlert.present();
    }
    async presentToast(message) {
        const toast = await this.toastCtrl.create({
            message: message,
            duration: 2000,
            position: 'bottom'
        });
        toast.onDidDismiss().then((res) => {
            this.refreshButtonPressed();
        });
        toast.present();
    }
    refreshButtonPressed() {
        this.fetchRecordAgainstSelectedObject();
    }
    async showInfoAlert(info) {
        this.alerCtrl.create({
            message: info,
            subHeader: '',
            buttons: [{
                text: 'Ok',
                handler: () => {
                    console.log('Confirm Okay');
                }
            }]
        }).then(alert => alert.present());
    }
    closeInfoAlert() {
        this.aniClassAddorRemove = false;
        setTimeout(() => {
            this.showAlert = false;
            this.infoMessage = '';
        }, 500);
    };
    onQuillSelectionChanged() {
        var Link = Quill.import('formats/link');
        Link.sanitize = function(url) {
            let protocol = url.slice(0, url.indexOf(':'));
            if (this.PROTOCOL_WHITELIST.indexOf(protocol) === -1) {
                url = 'http://' + url;
            }
            let anchor = document.createElement('a');
            anchor.href = url;
            protocol = anchor.href.slice(0, anchor.href.indexOf(':'));
            return (this.PROTOCOL_WHITELIST.indexOf(protocol) > -1) ? url : this.SANITIZED_URL;
        }
        Quill.register(Link, true);
    }
    ionViewWillLeave() {
        this.backButtonSubscribeRef.unsubscribe();
        this.drawerComponentCurrentState = DrawerState.Bottom
    }
    ngOnInit() {}

    ionViewWillEnter() {
        document.body.setAttribute('class', 'linelistinnerdetail');
        this.drawerComponentCurrentState = DrawerState.Top;
    }

    ionViewDidEnter() {
        if (this.action === 'Edit') {
            const tableStructure = JSON.parse(JSON.stringify(this.dataProvider.tableStructure()["pfm126513"]));
            Object.keys(tableStructure).forEach(key => {
                this.dependentFieldTriggerList[key] = 'triggered';
            });
        }
        const dvHeader = document.querySelector(".detail-view-sub-header");
        dvHeader.setAttribute("color", "var(--ion-color-primary, #3880ff)");
        const dvHeaderItem = document.querySelector(
            ".detail-view-sub-header ion-item"
        );
        dvHeaderItem.setAttribute("color", "var(--ion-color-primary, #3880ff)");
        const dvHeaderListHd = document.querySelectorAll(
            ".hl-full-detail-content ion-list-header"
        );
        const dvHeaderListHdLen = dvHeaderListHd.length;
        for (let i = 0; i < dvHeaderListHdLen; i++) {
            dvHeaderListHd[i].setAttribute(
                "color",
                "var(--ion-color-primary, #3880ff)"
            );
        }
        const pvHdItembg = document.querySelectorAll(
            ".detail-view-sub-header ion-badge"
        );
        const pvHdItembgLen = pvHdItembg.length;
        for (let i = 0; i < pvHdItembgLen; i++) {
            pvHdItembg[i].setAttribute(
                "background",
                "var(--ion-color-primary-tint, #4c8dff)"
            );
        }
    }

    dockerButtonOnClick() {
        if (this.drawerComponentCurrentState === DrawerState.Bottom) {
            this.drawerComponentCurrentState = DrawerState.Docked
            this.drawerComponentPreviousState = DrawerState.Bottom
        } else if (this.drawerComponentCurrentState === DrawerState.Docked) {
            if (this.drawerComponentPreviousState === DrawerState.Bottom) {
                this.drawerComponentCurrentState = DrawerState.Top
            } else {
                this.drawerComponentCurrentState = DrawerState.Bottom
            }
            this.drawerComponentPreviousState = DrawerState.Docked;
        } else if (this.drawerComponentCurrentState === DrawerState.Top) {
            this.drawerComponentCurrentState = DrawerState.Docked
            this.drawerComponentPreviousState = DrawerState.Top
        }
    }
    openurl(events, url) {
        events.stopPropagation();
        if (url.indexOf('http') == 0) {
            window.open(url)
        } else {
            window.open('http://'.concat(url))
        }
    }

}