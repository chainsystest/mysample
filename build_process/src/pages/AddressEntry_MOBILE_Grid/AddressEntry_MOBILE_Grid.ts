import {
    Component,
    OnInit,
    ViewChild,
    ApplicationRef,
    HostListener,
    NgZone
} from '@angular/core';
import {
    dataProvider
} from 'src/core/utils/dataProvider';
import {
    appConstant
} from 'src/core/utils/appConstant';
import {
    ModalController,
    Platform,
    LoadingController,
    Events,
    ToastController,
    AlertController,
    IonContent
} from '@ionic/angular';
import {
    DrawerState
} from 'ion-bottom-drawer';
import {
    PopoverController
} from '@ionic/angular';
import {
    appUtility
} from 'src/core/utils/appUtility';
import {
    ScreenOrientation
} from '@ionic-native/screen-orientation/ngx';
import {
    lookupFieldMapping
} from 'src/core/pfmmapping/lookupFieldMapping';
import {
    objectTableMapping
} from 'src/core/pfmmapping/objectTableMapping';
import * as moment from 'moment';
import {
    Router,
    ActivatedRoute
} from '@angular/router';
import {
    SocialSharing
} from '@ionic-native/social-sharing/ngx';
import {
    EmailComposer
} from '@ionic-native/email-composer/ngx';
import {
    CallNumber
} from '@ionic-native/call-number/ngx';
import {
    SMS
} from '@ionic-native/sms/ngx';
import {
    attachmentDbProvider
} from 'src/core/db/attachmentDbProvider';
import * as _ from 'underscore';
import {
    DatePipe
} from '@angular/common';
import {
    registerLocaleData
} from '@angular/common';
import {
    metaDataDbProvider
} from 'src/core/db/metaDataDbProvider';
import {
    metaDbConfiguration
} from 'src/core/db/metaDbConfiguration';
import {
    cspfmExecutionPouchDbProvider
} from 'src/core/db/cspfmExecutionPouchDbProvider';
import {
    cspfmExecutionPouchDbConfiguration
} from 'src/core/db/cspfmExecutionPouchDbConfiguration';

import * as lodash from 'lodash';
import {
    lookuppage
} from 'src/core/pages/lookuppage/lookuppage';
import {
    FormBuilder,
    Validators,
    FormGroup
} from '@angular/forms';
import 'moment-timezone';
import * as Quill from 'quill';
import {
    cspfmFieldTrackingMapping
} from 'src/core/pfmmapping/cspfmFieldTrackingMapping';
import {
    dbProvider
} from 'src/core/db/dbProvider';
import {
    Http
} from '@angular/http';
import {
    dbConfiguration
} from 'src/core/db/dbConfiguration';
import {
    couchdbProvider
} from 'src/core/db/couchdbProvider';
import {
    FieldInfo
} from "src/core/pipes/cspfm_data_display";
@Component({
    selector: 'AddressEntry_MOBILE_Grid',
    templateUrl: './AddressEntry_MOBILE_Grid.html'
}) export class AddressEntry_MOBILE_Grid implements OnInit {
    @ViewChild('parentContent') parentContent: IonContent;
    @ViewChild('childContent') childContent: IonContent;
    isBrowser: boolean = false;
    headerenable = true;
    drawerComponentDockedHeight = 300;
    drawerComponentCurrentState = DrawerState.Top;
    drawerComponentPreviousState = DrawerState.Docked;
    private obj_pfm147233: any = {};
    public ionDateTimeDisplayValue = {};
    private obj_pfm147233_Temp: any = {};
    public formGroup: FormGroup;
    private customAlert;
    public action = 'Add';
    private id: any = {};
    private tableName_pfm147233 = 'pfm147233';
    public isSaveActionTriggered: Boolean = false;
    private isValidFrom: Boolean = true;
    public dbServiceProvider = appConstant.pouchDBStaticName;
    public selectedDataObject: any = {};
    public formulaObject = {};
    public isFieldTrackingEnable: Boolean = false;
    private showAlert: boolean = false;
    infoMessage = '';
    aniClassAddorRemove: boolean = false;
    private workFlowInitiateList = {};
    public savedSuccessMessage = 'data saved sucessfully';
    public updatedSuccessMessage = 'data updated sucessfully';
    public savedErrorMessage = 'Error saving record';
    public fetchErrorMessage = 'Fetching Failed';
    private dependentNumberCount = {};
    public partiallySavedSuccessMessage = 'data partially saved';
    private backButtonSubscribeRef;
    private unRegisterBackButtonAction: Function;
    public parentObjLabel = '';
    public parentObjValue = '';
    private errorMessageToDisplay: string = 'No Records';
    private parentName = '';
    private parentId = '';
    public isParentObjectShow = false;
    public isSkeletonLoading = true;
    isViewRunning = false;
    loading;
    private objectHierarchyJSON = {
        "objectName": "addressdetails",
        "objectType": "PRIMARY",
        "relationShipType": null,
        "fieldId": "0",
        "objectId": "147233",
        "childObject": [{
            "objectName": "customermaster",
            "objectType": "HEADER",
            "relationShipType": "one_to_many",
            "fieldId": "293790",
            "objectId": "147213",
            "childObject": []
        }]
    };
    public isFromMenu = false;
    private redirectUrl = '/';
    private dependentFieldTriggerList = {};
    private geoLocationdependentField = {};
    public pickListValues = {};
    public parentObject: any = {};
    private country_293719 = {
        "pickListJson": [{
            "targetName": "stateprovince",
            "targetType": "DROPDOWN",
            "condition": [{
                "choice": "USA",
                "choiceValue": "USA",
                "pickList": [{
                    "choice": "California",
                    "choiceValue": "California"
                }, {
                    "choice": "Florida",
                    "choiceValue": "Florida"
                }, {
                    "choice": "Texas",
                    "choiceValue": "Texas"
                }, {
                    "choice": "New Jersey",
                    "choiceValue": "New Jersey"
                }, {
                    "choice": "North Carolina",
                    "choiceValue": "North Carolina"
                }, {
                    "choice": "None",
                    "choiceValue": "None"
                }]
            }, {
                "choice": "Canada",
                "choiceValue": "Canada",
                "pickList": [{
                    "choice": "Alberta",
                    "choiceValue": "Alberta"
                }, {
                    "choice": "British Columbia",
                    "choiceValue": "British Columbia"
                }, {
                    "choice": "Nova Scotia",
                    "choiceValue": "Nova Scotia"
                }, {
                    "choice": "Manitoba",
                    "choiceValue": "Manitoba"
                }, {
                    "choice": "None",
                    "choiceValue": "None"
                }]
            }, {
                "choice": "India",
                "choiceValue": "India",
                "pickList": [{
                    "choice": "Delhi",
                    "choiceValue": "Delhi"
                }, {
                    "choice": "Tamil Nadu",
                    "choiceValue": "Tamil Nadu"
                }, {
                    "choice": "Kerala",
                    "choiceValue": "Kerala"
                }]
            }]
        }],
        "changesApplyFields": ["stateprovince"],
        "parent": ["country", "stateprovince"]
    };
    private addresstype_293732_checkbox = [];
    public formulaConfigJSON = {};
    public formulafields = {};
    childObjectList = [];
    objResultMap = new Map < string, any > ();
    objDisplayName = {
        'pfm147233': 'addressdetails',
    };
    recordEntryValidation() {
        const formGroupObjectValue = Object.assign({}, this.formGroup.value);
        this.obj_pfm147233 = Object.assign(this.obj_pfm147233, formGroupObjectValue.pfm147233);
        if (
            this.obj_pfm147233["addressid"] || this.obj_pfm147233["address2"] || this.obj_pfm147233["address1"] || this.obj_pfm147233["country"] || this.obj_pfm147233["postalcode"] || this.obj_pfm147233["city"] || this.obj_pfm147233["sitenumber"] || this.obj_pfm147233["stateprovince"] || this.obj_pfm147233["addresstype"] || this.obj_pfm147233["addresstype"]) {
            return true;
        } else {
            return false;
        }
    }
    constructor(public dbConfiguration: dbConfiguration, public http: Http, public popoverCtrl: PopoverController, public modalCtrl: ModalController, public dbService: dbProvider,
        private socialSharing: SocialSharing, private emailComposer: EmailComposer, private callNumber: CallNumber, private sms: SMS,
        public platform: Platform, public applicationRef: ApplicationRef, public formBuilder: FormBuilder,
        public events: Events, public router: Router, public activatRoute: ActivatedRoute,
        public objectTableMapping: objectTableMapping, public lookupFieldMapping: lookupFieldMapping,
        public loadingCtrl: LoadingController, public toastCtrl: ToastController, public dataProvider: dataProvider, public metaDbConfigurationObj: metaDbConfiguration, public metaDbProvider: metaDataDbProvider, public cspfmexecutionPouchDbProvider: cspfmExecutionPouchDbProvider,
        public executionDbConfigObject: cspfmExecutionPouchDbConfiguration, private datePipe: DatePipe, public fieldTrackMapping: cspfmFieldTrackingMapping,
        public alerCtrl: AlertController, public appUtility: appUtility, public screenOrientation: ScreenOrientation, private ngZone: NgZone) {
        this.activatRoute.queryParams.subscribe(params => {
            if (Object.keys(params).length == 0 && params.constructor === Object) {
                console.log("list query params skipped");
                return
            }
            if (params["isFromMenu"]) {
                this.isFromMenu = params["isFromMenu"];
            }
            if (params["redirectUrl"]) {
                this.redirectUrl = params["redirectUrl"]
            }
            this.action = params['action'];
            this.parentId = params['parentId'];
            this.id = params['id'];
            this.fetchParentObj();
            this.parentName = params['parentName'];
            if (this.parentId) {
                this.isParentObjectShow = true;
                this.parentObjLabel = params['parentFieldLabel'];
                this.parentObjValue = params['parentFieldValue'];
            }
            this.initializeObjects(dataProvider.tableStructure());
            if (this.action == 'Edit') {
                this.id = params['id'];
                this.fetchRecordAgainstSelectedObject();
            } else {
                this.checkboxinitialisationaddresstype();
                this.checkboxDefaultValueaddresstype();
                this.isSkeletonLoading = false;
                this.checkboxInitialization();
            }
        });
        const windowHeight = window.innerHeight;
        this.drawerComponentDockedHeight = windowHeight / 2;
        this.screenOrientation.onChange().subscribe(() => {
            this.drawerComponentCurrentState = DrawerState.Bottom;
            const windowHeightVal = window.innerHeight;
            this.drawerComponentDockedHeight = windowHeightVal / 2;
        });
        this.createFormGroup();
        this.hardWareBackButtonAction();
    }
    initializeObjects(tableStructure) {
        this.obj_pfm147233 = JSON.parse(JSON.stringify(tableStructure.pfm147233));
    }
    fetchParentObj() {
        if (this.action == 'Edit') {
            return;
        }
        for (let childItem of this.objectHierarchyJSON['childObject']) {
            if (childItem['objectType'] == 'HEADER') {
                this.fetchParentWithParentJSON(childItem);
            }
        }
    }
    fetchParentWithParentJSON(parentJSON) {
        const additionalObjectdata = {};
        additionalObjectdata['id'] = this.parentId;
        const fetchParams = {
            'objectHierarchyJSON': parentJSON,
            'additionalInfo': additionalObjectdata,
            'dataSource': appConstant.pouchDBStaticName
        };
        this.dataProvider.querySingleDoc(fetchParams).then(res => {
            this.isSkeletonLoading = false;
            if (res['status'] !== 'SUCCESS') {
                this.errorMessageToDisplay = res['message'];
                if (this.errorMessageToDisplay == 'No internet') {
                    this.presentNoInternetToast();
                }
                return;
            }
            if (res['records'].length < 0) {
                console.log('FetchRecordAgainstSelectedObject No Records');
                return;
            }
            let dataObj = res['records'][0];
            this.parentObject = dataObj;
        }).catch(error => {
            this.isSkeletonLoading = false;
            this.showInfoAlert(this.fetchErrorMessage)
        })
    }
    fetchRecordAgainstSelectedObject() {
        const additionalObjectdata = {};
        additionalObjectdata['id'] = this.id;
        const fetchParams = {
            'objectHierarchyJSON': this.objectHierarchyJSON,
            'additionalInfo': additionalObjectdata,
            'dataSource': appConstant.pouchDBStaticName
        };
        this.dataProvider.querySingleDoc(fetchParams).then(res => {
            this.isSkeletonLoading = false;
            if (res['status'] !== 'SUCCESS') {
                this.errorMessageToDisplay = res['message'];
                if (this.errorMessageToDisplay == 'No internet') {
                    this.presentNoInternetToast();
                }
                return;
            }
            if (res['records'].length < 0) {
                console.log('FetchRecordAgainstSelectedObject No Records');
                return;
            }
            let dataObj = res['records'][0];
            this.obj_pfm147233_Temp = lodash.extend({}, this.obj_pfm147233, dataObj);
            this.selectedDataObject['pfm147233'] = JSON.stringify(this.obj_pfm147233_Temp);
            this.obj_pfm147233 = lodash.extend({}, this.obj_pfm147233, dataObj);
            if (this.obj_pfm147233['country'] != undefined && this.obj_pfm147233['country'] != '') {
                this.updateChildList(this.country_293719, this.obj_pfm147233['country'], true);
            }
            this.checkboxinitialisationaddresstype();
            this.loadCheckboxEditValues('addresstype', this.pickListValues['addresstype']);
            this.formGroup.patchValue({
                pfm147233: this.obj_pfm147233
            });
            this.parentObject = dataObj['pfm147213s'][0];
            this.applicationRef.tick();
        }).catch(error => {
            this.isSkeletonLoading = false;
            this.showInfoAlert(this.fetchErrorMessage)
        })
    }
    updateGeoLocationFlag(objectName, dataObj, existingDataObj) {
        Object.keys(this.geoLocationdependentField[objectName]).forEach(element => {
            const depFlds = this.geoLocationdependentField[objectName][element]['dependentFields'];
            depFlds.forEach(fieldName => {
                if (this.action == 'Edit') {
                    if (existingDataObj[fieldName] != dataObj[fieldName]) {
                        dataObj[this.geoLocationdependentField[objectName][element]['dependentFieldName']] = 'Y';
                        return;
                    }
                } else {
                    if (dataObj[fieldName] != '') {
                        dataObj[this.geoLocationdependentField[objectName][element]['dependentFieldName']] = 'Y';
                        return;
                    }
                }
            })
        });
    }
    async presentNoInternetToast() {
        const toast = await this.toastCtrl.create({
            message: "No internet connection. Please check your internet connection and try again.",
            showCloseButton: true,
            closeButtonText: "retry"
        });
        toast.onDidDismiss().then(() => {
            toast.dismiss();
            this.refreshData();
        });
        toast.present();
    }

    refreshData() {
        this.fetchRecordAgainstSelectedObject();
    }
    async saveButtonOnclick() {
        this.formGroup.patchValue({
            pfm147233: {
                addressid: this.obj_pfm147233['addressid'],
                address2: this.obj_pfm147233['address2'],
                address1: this.obj_pfm147233['address1'],
                country: this.obj_pfm147233['country'],
                postalcode: this.obj_pfm147233['postalcode'],
                city: this.obj_pfm147233['city'],
                sitenumber: this.obj_pfm147233['sitenumber'],
                stateprovince: this.obj_pfm147233['stateprovince'],
                addresstype: this.obj_pfm147233['addresstype']
            }
        });
        if (this.formGroup.valid) {
            this.formGroup.patchValue({});
            this.isValidFrom = true;
            const fieldTrackObject = this.fieldTrackMapping.mappingDetail[this.tableName_pfm147233]
            if (fieldTrackObject) {
                this.isFieldTrackingEnable = true;
            } else {
                this.isFieldTrackingEnable = false;
            }
            let previousParentObject
            if (this.action === "Edit") {
                previousParentObject = this.selectedDataObject[this.tableName_pfm147233];
                console.log("previousParentObject = ", previousParentObject);
            } else {
                previousParentObject = undefined
            }
            if (this.parentId) {
                this.obj_pfm147233[this.parentName] = this.parentId;
            };
            this.dataProvider.save(this.tableName_pfm147233, this.obj_pfm147233, appConstant.pouchDBStaticName, previousParentObject, this.isFieldTrackingEnable)
                .then(result => {
                    if (result['status'] != 'SUCCESS') {
                        this.showInfoAlert(result['message']);
                        return;
                    }
                    if (this.childObjectList.length == 0) {
                        this.presentToast(this.savedSuccessMessage);
                        let opts = {
                            animate: false
                        };
                        this.navigatePopUpAction();
                        return
                    };

                })
                .catch(error => {
                    this.showInfoAlert(this.savedErrorMessage);
                    console.log(error)
                });
        } else {
            this.formGroup.patchValue({});
            this.isValidFrom = false;
            this.scrollToValidationFailedField();
        }
        var errorValue = document.querySelector('.entry-page-content');
        errorValue.setAttribute('class', 'entry-page-content entryErrorMessage hydrated');
    };
    scrollToValidationFailedField() {
        const formControls = this.formGroup.controls.pfm147233;
        const formGroupKeys = Object.keys(formControls["controls"]);
        let isValidationSucceed = true;
        formGroupKeys.every(element => {
            if (formControls["controls"][element].status === "INVALID") {
                const yOffset = document.getElementById("pfm147233_" + element).offsetTop;
                this.childContent.scrollToPoint(0, yOffset, 1000);
                isValidationSucceed = false;
                return false;
            } else {
                return true;
            }
        });
    }
    createFormGroup() {
        this.formGroup = this.formBuilder.group({
            pfm147233: this.formBuilder.group({
                address2: [null, Validators.compose([])],
                address1: [null, Validators.compose([])],
                country: [null, Validators.compose([])],
                postalcode: [null, Validators.compose([])],
                city: [null, Validators.compose([])],
                sitenumber: [null, Validators.compose([Validators.required])],
                stateprovince: [null, Validators.compose([])],
                addresstype: [null, Validators.compose([])]
            })
        });
    }
    lookupResponse(objectname, selectedValue) {}
    checkboxinitialisationaddresstype() {
        this.pickListValues['addresstype'] = [{
            val: 'None',
            displayName: 'AddressEntry_MOBILE_Grid.Element.addressdetails.addresstype.Option.None',
            isChecked: false
        }, {
            val: 'Ship To',
            displayName: 'AddressEntry_MOBILE_Grid.Element.addressdetails.addresstype.Option.Ship To',
            isChecked: false
        }, {
            val: 'Bill To',
            displayName: 'AddressEntry_MOBILE_Grid.Element.addressdetails.addresstype.Option.Bill To',
            isChecked: false
        }];
    }
    checkboxDefaultValueaddresstype() {
        this.pickListValues['addresstype'] = [{
            val: 'None',
            displayName: 'AddressEntry_MOBILE_Grid.Element.addressdetails.addresstype.Option.None',
            isChecked: true
        }, {
            val: 'Ship To',
            displayName: 'AddressEntry_MOBILE_Grid.Element.addressdetails.addresstype.Option.Ship To',
            isChecked: false
        }, {
            val: 'Bill To',
            displayName: 'AddressEntry_MOBILE_Grid.Element.addressdetails.addresstype.Option.Bill To',
            isChecked: false
        }];
    }
    onChangeCheckbox(checkBoxMappingList, userSelectedState, selectedFieldJSON, userSelectedField, dataObject) {
        let selectedValues: any = [];
        userSelectedState.isChecked = userSelectedState.isChecked ? false : true
        for (let state of checkBoxMappingList) {
            if (state.val == userSelectedState.val) {
                state.isChecked = userSelectedState.isChecked
            }
            if (state.isChecked) {
                selectedValues.push(state.val)
            }
        }
        dataObject[userSelectedField] = selectedValues;
    }
    loadCheckboxEditValues(fieldName, values) {
        if (this.obj_pfm147233[fieldName] != undefined && this.obj_pfm147233[fieldName] != null && values.length > 0) {
            for (let item of values) {
                for (let itemVal of this.obj_pfm147233[fieldName]) {
                    if (item.val == itemVal) {
                        item.isChecked = true
                    }
                }
            }
        }
    }
    loadDefaultValues() {
        this.obj_pfm147233['addresstype'] = [];
        this.checkboxinitialisationaddresstype();
        this.loadCheckboxEditValues('addresstype', this.pickListValues['addresstype']);
    }
    updateChildList(selectedFieldJSON, userSelectedValues, flag, userInteractedField ? ) {
        this.dependentNumberCount = {};
        if (userInteractedField == undefined) {
            userInteractedField = '';
        }
        if (this.action === "Edit" && !flag && !this.dependentFieldTriggerList[userInteractedField]) {
            this.dependentFieldTriggerList[userInteractedField] = "triggered";
            return;
        }
        if (typeof userSelectedValues == 'string') {
            userSelectedValues = [userSelectedValues];
        }
        var myObjStr = JSON.stringify(selectedFieldJSON);
        var tempJson = JSON.parse(myObjStr);
        if (selectedFieldJSON != '') {
            this.clearExisitingDataInPickList(selectedFieldJSON, flag);
        }
        if (userSelectedValues == '' || selectedFieldJSON == '') {
            return;
        }
        var targetName = "";
        userSelectedValues.forEach(userSelectedValue => {
            var pickListJSONList: any = [];
            Array.prototype.push.apply(pickListJSONList, tempJson['pickListJson']);
            pickListJSONList.forEach(elementCondition => {
                var conditionList: any = [];
                Array.prototype.push.apply(conditionList, elementCondition['condition']);
                conditionList.forEach(element => {
                    if (elementCondition['targetType'] == 'CHECKBOX') {
                        if (userSelectedValue == element.choiceValue) {
                            targetName = elementCondition['targetName'];
                            var checkBoxList = element['pickList'];
                            var checkBoxListWithDefaultValue: any = [];
                            checkBoxList.forEach(elementCheckbox => {
                                var object = {};
                                object['val'] = elementCheckbox.choiceValue,
                                    object['displayName'] = elementCheckbox.choice,
                                    object['isChecked'] = false;
                                checkBoxListWithDefaultValue.push(object);
                            });
                            if (this.pickListValues[targetName]) {
                                var targetNameList = this.pickListValues[targetName];
                                Array.prototype.push.apply(targetNameList, checkBoxListWithDefaultValue);
                                this.pickListValues[targetName] = lodash.uniqBy(targetNameList, 'val'); //duplicate records removed
                            } else {
                                this.pickListValues[targetName] = [];
                                this.pickListValues[targetName] = checkBoxListWithDefaultValue;
                            }
                            // For Edit action
                            if (flag) {
                                this.loadCheckboxEditValues(targetName, this.pickListValues[targetName]);
                            }
                        }
                    } else {
                        if (userSelectedValue == element.choiceValue) {
                            targetName = elementCondition['targetName'];
                            if (this.pickListValues[targetName]) {
                                var targetNameList = this.pickListValues[targetName];
                                Array.prototype.push.apply(targetNameList, element['pickList']);
                                this.pickListValues[targetName] = lodash.uniqBy(targetNameList, 'choice'); //duplicate records removed
                            } else {
                                this.pickListValues[targetName] = [];
                                this.pickListValues[targetName] = lodash.uniqBy(element['pickList'], 'choice');
                            }
                        }
                    }
                });
            });
        });
    }
    clearExisitingDataInPickList(userSelectedValue, isNeedToClear) {
        const removeFieldArray = userSelectedValue['changesApplyFields'];

        if (removeFieldArray === undefined || removeFieldArray === '') {
            return;
        }

        if (isNeedToClear) {
            return;
        }

        removeFieldArray.forEach(fieldName => {
            this.pickListValues[fieldName] = [];
            this.obj_pfm147233[fieldName] = '';
        });
    }
    checkIsParentSelected(selectedField, childField, json) {
        this.dependentNumberCount = {};
        if (selectedField == '' || selectedField == undefined)
            return;
        if (this.obj_pfm147233[selectedField].length <= 0) {
            var jsonkey = selectedField;

            var myObjStr = JSON.stringify(json);
            var tempJson = JSON.parse(myObjStr);

            var parentArray = tempJson['parent'];

            if (parentArray == undefined || parentArray == '') {
                return;
            }

            var i = 1;
            parentArray.forEach(fieldName => {
                this.dependentNumberCount[fieldName] = i;
                i++;

                console.log(fieldName, this.dependentNumberCount[fieldName]);


            });
            return;
        } else if (!this.pickListValues[childField] || this.pickListValues[childField].length <= 0) {
            this.showInfoAlert("List not available for " + childField);
            return;
        }
    }
    resetChildDependentInfo(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName) {}
    public status_293810 = {
        "New": "New",
        "InRview": "InReview",
        "Approved": "Approved",
        "Rejected": "Rejected",
        "Active": "Active",
        "In Active": "In Active"
    };
    public businessunits_293693 = {
        "None": "None",
        "Sole proprietorship": "Sole proprietorship",
        "Partnership": "Partnership",
        "Joint Stock Company": "Joint Stock Company"
    };
    public accounttype_293667 = {
        "None": "None",
        "Personal": "Personal",
        "Business": "Business"
    };
    public gridFieldInfo: {
        [key: string]: FieldInfo
    } = {
        "pfm147213_accountno": {
            "label": "AddressEntry_MOBILE_Grid.Element.customermaster.accountno",
            "fieldName": "accountno",
            "prop": "accountno",
            "fieldType": "AUTONUMBER",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147213_accountname": {
            "label": "AddressEntry_MOBILE_Grid.Element.customermaster.accountname",
            "fieldName": "accountname",
            "prop": "accountname",
            "fieldType": "TEXT",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147213_dunsnumber": {
            "label": "AddressEntry_MOBILE_Grid.Element.customermaster.dunsnumber",
            "fieldName": "dunsnumber",
            "prop": "dunsnumber",
            "fieldType": "TEXT",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147213_status": {
            "label": "AddressEntry_MOBILE_Grid.Element.customermaster.status",
            "fieldName": "status",
            "prop": "status",
            "fieldType": "DROPDOWN",
            "child": "",
            "dateFormat": "",
            "mappingDetails": {
                "New": "New",
                "InRview": "InReview",
                "Approved": "Approved",
                "Rejected": "Rejected",
                "Active": "Active",
                "In Active": "In Active"
            },
            "currencyDetails": ""
        },
        "pfm147213_businessunits": {
            "label": "AddressEntry_MOBILE_Grid.Element.customermaster.businessunits",
            "fieldName": "businessunits",
            "prop": "businessunits",
            "fieldType": "DROPDOWN",
            "child": "",
            "dateFormat": "",
            "mappingDetails": {
                "None": "None",
                "Sole proprietorship": "Sole proprietorship",
                "Partnership": "Partnership",
                "Joint Stock Company": "Joint Stock Company"
            },
            "currencyDetails": ""
        },
        "pfm147213_taxregistrationnumber": {
            "label": "AddressEntry_MOBILE_Grid.Element.customermaster.taxregistrationnumber",
            "fieldName": "taxregistrationnumber",
            "prop": "taxregistrationnumber",
            "fieldType": "TEXT",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147213_accounttype": {
            "label": "AddressEntry_MOBILE_Grid.Element.customermaster.accounttype",
            "fieldName": "accounttype",
            "prop": "accounttype",
            "fieldType": "DROPDOWN",
            "child": "",
            "dateFormat": "",
            "mappingDetails": {
                "None": "None",
                "Personal": "Personal",
                "Business": "Business"
            },
            "currencyDetails": ""
        }
    };
    checkboxInitialization() {};
    backButtonOnclick() {
        if (this.recordEntryValidation()) {
            this.recordDiscardConfirmAlert();
        } else {
            this.navigatePopUpAction();
        }
    };
    navigatePopUpAction() {
        this.router.navigateByUrl(this.redirectUrl, {
            skipLocationChange: true
        });
    };
    hardWareBackButtonAction() {
        this.backButtonSubscribeRef = this.platform.backButton.subscribeWithPriority(999999, () => {
            if (this.customAlert) {
                return;
            }
            this.backButtonOnclick();
        });
    }
    lookupClearAction(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName) {
        dataObj[dataObjectFieldName] = "";
        delete looklUpObj["id"];
        delete looklUpObj[lookupObjectFieldName];
        this.formGroup.value.formControlerName = "";
        this.resetChildDependentInfo(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName)
    }
    async recordDiscardConfirmAlert() {
        this.customAlert = await this.alerCtrl.create({
            backdropDismiss: false,
            message: 'Are you sure want to leave this page?',
            buttons: [{
                text: 'Cancel',
                cssClass: 'method-color',
                handler: () => {
                    this.customAlert = null;
                    console.log('Individual clicked');
                }
            }, {
                text: 'Yes',
                cssClass: 'method-color',
                handler: () => {
                    this.navigatePopUpAction();
                }
            }]
        });
        this.customAlert.present();
    }
    async presentToast(message) {
        const toast = await this.toastCtrl.create({
            message: message,
            duration: 2000,
            position: 'bottom'
        });
        toast.onDidDismiss().then((res) => {
            this.refreshButtonPressed();
        });
        toast.present();
    }
    refreshButtonPressed() {
        this.fetchRecordAgainstSelectedObject();
    }
    async showInfoAlert(info) {
        this.alerCtrl.create({
            message: info,
            subHeader: '',
            buttons: [{
                text: 'Ok',
                handler: () => {
                    console.log('Confirm Okay');
                }
            }]
        }).then(alert => alert.present());
    }
    closeInfoAlert() {
        this.aniClassAddorRemove = false;
        setTimeout(() => {
            this.showAlert = false;
            this.infoMessage = '';
        }, 500);
    };
    onQuillSelectionChanged() {
        var Link = Quill.import('formats/link');
        Link.sanitize = function(url) {
            let protocol = url.slice(0, url.indexOf(':'));
            if (this.PROTOCOL_WHITELIST.indexOf(protocol) === -1) {
                url = 'http://' + url;
            }
            let anchor = document.createElement('a');
            anchor.href = url;
            protocol = anchor.href.slice(0, anchor.href.indexOf(':'));
            return (this.PROTOCOL_WHITELIST.indexOf(protocol) > -1) ? url : this.SANITIZED_URL;
        }
        Quill.register(Link, true);
    }
    ionViewWillLeave() {
        this.backButtonSubscribeRef.unsubscribe();
        this.drawerComponentCurrentState = DrawerState.Bottom
    }
    ngOnInit() {}

    ionViewWillEnter() {
        document.body.setAttribute('class', 'linelistinnerdetail');
        this.drawerComponentCurrentState = DrawerState.Top;
    }

    ionViewDidEnter() {
        if (this.action === 'Edit') {
            const tableStructure = JSON.parse(JSON.stringify(this.dataProvider.tableStructure()["pfm126513"]));
            Object.keys(tableStructure).forEach(key => {
                this.dependentFieldTriggerList[key] = 'triggered';
            });
        }
        const dvHeader = document.querySelector(".detail-view-sub-header");
        dvHeader.setAttribute("color", "var(--ion-color-primary, #3880ff)");
        const dvHeaderItem = document.querySelector(
            ".detail-view-sub-header ion-item"
        );
        dvHeaderItem.setAttribute("color", "var(--ion-color-primary, #3880ff)");
        const dvHeaderListHd = document.querySelectorAll(
            ".hl-full-detail-content ion-list-header"
        );
        const dvHeaderListHdLen = dvHeaderListHd.length;
        for (let i = 0; i < dvHeaderListHdLen; i++) {
            dvHeaderListHd[i].setAttribute(
                "color",
                "var(--ion-color-primary, #3880ff)"
            );
        }
        const pvHdItembg = document.querySelectorAll(
            ".detail-view-sub-header ion-badge"
        );
        const pvHdItembgLen = pvHdItembg.length;
        for (let i = 0; i < pvHdItembgLen; i++) {
            pvHdItembg[i].setAttribute(
                "background",
                "var(--ion-color-primary-tint, #4c8dff)"
            );
        }
    }

    dockerButtonOnClick() {
        if (this.drawerComponentCurrentState === DrawerState.Bottom) {
            this.drawerComponentCurrentState = DrawerState.Docked
            this.drawerComponentPreviousState = DrawerState.Bottom
        } else if (this.drawerComponentCurrentState === DrawerState.Docked) {
            if (this.drawerComponentPreviousState === DrawerState.Bottom) {
                this.drawerComponentCurrentState = DrawerState.Top
            } else {
                this.drawerComponentCurrentState = DrawerState.Bottom
            }
            this.drawerComponentPreviousState = DrawerState.Docked;
        } else if (this.drawerComponentCurrentState === DrawerState.Top) {
            this.drawerComponentCurrentState = DrawerState.Docked
            this.drawerComponentPreviousState = DrawerState.Top
        }
    }
    openurl(events, url) {
        events.stopPropagation();
        if (url.indexOf('http') == 0) {
            window.open(url)
        } else {
            window.open('http://'.concat(url))
        }
    }

}