 import {
     Component,
     ViewChild,
     OnInit,
     ApplicationRef,
     ChangeDetectorRef,
     HostListener,
     NgZone
 } from '@angular/core';
 import {
     dbProvider
 } from 'src/core/db/dbProvider';
 import {
     offlineDbIndexCreation
 } from 'src/core/utils/offlineDbIndexCreation';
 import {
     dataProvider
 } from 'src/core/utils/dataProvider';
 import {
     appConstant
 } from 'src/core/utils/appConstant';

 import {
     LoadingController,
     Events,
     ToastController,
     ModalController,
     IonVirtualScroll
 } from '@ionic/angular';
 import {
     EmailComposer
 } from '@ionic-native/email-composer/ngx';
 import {
     SocialSharing
 } from '@ionic-native/social-sharing/ngx';
 import {
     SMS
 } from '@ionic-native/sms/ngx';
 import {
     CallNumber
 } from '@ionic-native/call-number/ngx';
 import * as lodash from 'lodash';
 import {
     Router,
     ActivatedRoute
 } from '@angular/router';
 import {
     appUtility
 } from 'src/core/utils/appUtility';
 import {
     Platform
 } from '@ionic/angular';
 import {
     registerLocaleData
 } from '@angular/common';
 import {
     ColumnMode
 } from "@swimlane/ngx-datatable";
 import {
     FieldInfo
 } from "src/core/pipes/cspfm_data_display";

 @Component({
     selector: 'crmcustomer_d_m_list',
     templateUrl: './crmcustomer_d_m_list.html'
 }) export class crmcustomer_d_m_list implements OnInit {
     @ViewChild(IonVirtualScroll) virtualScroll: IonVirtualScroll;
     public redirectUrl = "/";
     public isFromMenu = false;
     constructor(public events: Events, public dataProvider: dataProvider, public socialShare: SocialSharing, public loadingCtrl: LoadingController, public modalCtrl: ModalController,
         public callNumber: CallNumber, public emailComposer: EmailComposer, public toastCtrl: ToastController, public sms: SMS, public appUtilityConfig: appUtility, public platform: Platform, public router: Router, public activatRoute: ActivatedRoute, public dbService: dbProvider, public offlineDbIndexCreation: offlineDbIndexCreation) {
         this.filteredResultList = [];
         this.resultList = [];
         this.activatRoute.queryParams.subscribe(params => {
             if (Object.keys(params).length == 0 && params.constructor === Object) {
                 console.log("list query params skipped");
                 return
             }
             if (params["isFromMenu"]) {
                 this.isFromMenu = params["isFromMenu"];
             }
             if (params["redirectUrl"]) {
                 this.redirectUrl = params["redirectUrl"]
             }
         });
         this.fetchAllData();

         this.appUtilityConfig.setEventSubscriptionlayoutIds(this.tableName_pfm141093, this.layoutId);
         this.events.subscribe(this.layoutId, (modified) => {
             if (modified["dataProvider"] == "PouchDB") {
                 this.dataModifiedEventTrigger(modified);
             }
         });



     }

     ngOnDestroy() {
         /*event unsubscriber*/
         console.log('unsubscribe in list')
         if (this.appUtilityConfig.isMobile) {
             this.events.unsubscribe(this.layoutId);
             this.appUtilityConfig.removeEventSubscriptionlayoutIds(this.tableName_pfm141093, this.layoutId);
         }
     }

     dataModifiedEventTrigger(modified) {
         const modifiedData = this.dataProvider.convertRelDocToNormalDoc(modified);
         this.fetchModifiedRec(modifiedData);
     }
     restrictNewRecordsLimit(eventsTriggeredList, limit) {
         if (this.eventsTriggeredList.length > limit) {
             const removedRec = this.eventsTriggeredList.splice(limit, 1)
             Array.prototype.push.apply(this.filteredResultList, removedRec);
             this.filteredResultList = lodash.orderBy(this.filteredResultList, ['name'], ['asc']);
             this.resultList = this.filteredResultList;
         }
     }
     async displayToast(message) {
         const toast = await this.toastCtrl.create({
             message: message,
             duration: 1000,
             position: 'bottom'
         });
         await toast.present();
     }
     checkChildObjectExists(object, childRelationshipJson) {

         if (!this.checkInnerChildObjectExists(object, childRelationshipJson)) {
             return false
         }

         if (childRelationshipJson['innerChild']) {
             if (!this.checkInnerChildObjectExists(object[childRelationshipJson['childId']][0], childRelationshipJson['innerChild'])) {
                 return false
             }

             return this.checkChildObjectExists(object[childRelationshipJson['childId']][0], childRelationshipJson['innerChild'])

         } else {
             if (childRelationshipJson['lookupDetails']) {
                 if (object[childRelationshipJson['childId']][0][childRelationshipJson['lookupDetails']['lookupId']] && object[childRelationshipJson['childId']][0][childRelationshipJson['lookupDetails']['lookupId']] != "") {
                     return true
                 }
                 return false
             }
             return true
         }
     }

     checkInnerChildObjectExists(object, childRelationshipJson) {
         let childKey = childRelationshipJson['childId']
         if (object[childKey] && object[childKey].length > 0) {
             return true
         } else {
             return false
         }
     }

     getChildFieldValue(object, childRelationshipJson) {
         if (!childRelationshipJson['innerChild']) {
             return this.getInnerChildFieldValue(object, childRelationshipJson)
         }

         return this.getChildFieldValue(object[childRelationshipJson['childId']][0], childRelationshipJson['innerChild'])
     }

     getInnerChildFieldValue(object, childRelationshipJson) {
         if (childRelationshipJson['lookupDetails']) {
             let lookupId = childRelationshipJson['lookupDetails']['lookupId']
             return object[childRelationshipJson['childId']][0][lookupId][childRelationshipJson['lookupDetails']['fieldName']]
         }
         return object[childRelationshipJson['childId']][0][childRelationshipJson['fieldName']]
     }

     async fetchModifiedRec(modifiedData) {

         const additionalObjectdata = {};
         // var updatedHierarchyJSON = JSON.parse(JSON.stringify(this.objectHierarchyJSON));
         // if(updatedHierarchyJSON['options']){
         //     updatedHierarchyJSON['options']['selector']['_id'] = "pfm" + updatedHierarchyJSON['objectId'] + "_2_" + modifiedData["id"];
         // }else{
         //     additionalObjectdata['id'] =  modifiedData['id']
         // }

         additionalObjectdata['id'] = modifiedData['id']
         const objHierarchyJSON = JSON.parse(JSON.stringify(this.objectHierarchyJSON))
         if (objHierarchyJSON['options']) {
             delete objHierarchyJSON['options']
         }

         const fetchParams = {
             'objectHierarchyJSON': objHierarchyJSON,
             // 'objectHierarchyJSON' : updatedHierarchyJSON,
             'dataSource': this.dataSource,
             'additionalInfo': additionalObjectdata
         }


         this.dataProvider.querySingleDoc(fetchParams)
             .then(result => {

                 if (result["status"] !== "SUCCESS") {
                     this.errorMessageToDisplay = result["message"];
                     return;
                 }

                 const index = this.getChangedObjectIndex(
                     this.resultList,
                     modifiedData,
                     "id"
                 );
                 if (index > -1) {
                     //already exist
                     this.resultList.splice(index, 1);
                     this.resultList = [...this.resultList];
                     const filteredindex = this.getChangedObjectIndex(
                         this.filteredResultList,
                         modifiedData,
                         "id"
                     );
                     if (filteredindex > -1) {
                         this.filteredResultList.splice(filteredindex, 1);
                         this.filteredResultList = [...this.filteredResultList];
                     }
                 }
                 const modifiedRec = result["records"][0];
                 const eventsTriggeredindex = this.getChangedObjectIndex(this.eventsTriggeredList, modifiedData, "id");
                 if (eventsTriggeredindex > -1) {
                     this.eventsTriggeredList.splice(eventsTriggeredindex, 1);
                 }

                 if (modifiedRec) {
                     this.eventsTriggeredList.splice(0, 0, modifiedRec);
                     if (this.appUtilityConfig.isMobileResolution === false) {
                         this.filteredResultList.splice(index, 0, modifiedRec);
                         this.resultList.splice(index, 0, modifiedRec);
                     }
                 }
                 if (this.appUtilityConfig.isMobile) {
                     this.restrictNewRecordsLimit(this.eventsTriggeredList, 2)
                 } else {
                     this.restrictNewRecordsLimit(this.eventsTriggeredList, 3)
                 }
                 this.recentListRefresh();
                 this.getSearchedItems(this.searchTerm);

             })
             .catch(error => {
                 console.log(error);
             });
     }
     recentListRefresh() {
         if (this.virtualScroll) {
             this.virtualScroll.checkRange(0)
         }
     }

     getChangedObjectIndex(array, modifiedData, key) {
         return lodash.findIndex(array, item => {
             return item[key] === modifiedData[key];
         }, 0)
     }


     public dataSource = 'PouchDB';
     public searchQueryForDesignDoc = "";
     public devWidth = this.platform.width();
     resultList: Array < any > = [];
     public filteredResultList;
     // private layoutId  ;
     public filteredEventTriggeredList = [];
     public isSkeletonLoading = true;

     private tableName_pfm141093 = 'pfm141093';
     public errorMessageToDisplay: string = "No Records";
     public eventsTriggeredList: Array < any > = [];
     public searchTerm: any = "";
     public filterApplied = false;
     public objectHierarchyJSON = {
         "objectName": "crmcustomer",
         "objectType": "PRIMARY",
         "relationShipType": "",
         "fieldId": "0",
         "objectId": "141093",
         "childObject": []
     };

     public layoutDataRestrictionSet = [];
     public layoutId = "56280";
     public newlyAddedRecordCount = 0;


     readonly headerHeight = 50;
     readonly rowHeight = 50;
     columnMode = ColumnMode;
     public isLoading;

     public listTableFieldInfoArray: Array < FieldInfo > = [{
         "label": "crmcustomer_d_m_list.Element.crmcustomer.name",
         "fieldName": "name",
         "prop": "name",
         "fieldType": "AUTONUMBER",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }, {
         "label": "crmcustomer_d_m_list.Element.crmcustomer.customername",
         "fieldName": "customername",
         "prop": "customername",
         "fieldType": "TEXT",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }, {
         "label": "crmcustomer_d_m_list.Element.crmcustomer.customercontact",
         "fieldName": "customercontact",
         "prop": "customercontact",
         "fieldType": "TEXT",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }];
     public gridTableFieldInfoArray: Array < FieldInfo > = [{
         "label": "crmcustomer_d_m_list.Element.crmcustomer.name",
         "fieldName": "name",
         "prop": "name",
         "fieldType": "AUTONUMBER",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }, {
         "label": "crmcustomer_d_m_list.Element.crmcustomer.customername",
         "fieldName": "customername",
         "prop": "customername",
         "fieldType": "TEXT",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }, {
         "label": "crmcustomer_d_m_list.Element.crmcustomer.customercontact",
         "fieldName": "customercontact",
         "prop": "customercontact",
         "fieldType": "TEXT",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }];
     async fetchAllData(event ? , userParameterArray ? ) {
         this.isLoading = true;




         //this.objectHierarchyJSON = this.appUtility.setDataRestrictionByUsers(this.layoutDataRestrictionSet, this.objectHierarchyJSON);
         const fetchParams = {
             'objectHierarchyJSON': this.objectHierarchyJSON,
             'layoutDataRestrictionSet': this.layoutDataRestrictionSet,
             'dataSource': this.dataSource
         }

         this.dataProvider.queryDataFromDataSource(fetchParams).then(res => {
             console.log("Response :", res);
             this.isLoading = false;
             this.isSkeletonLoading = false;
             if (event != undefined) {
                 event.target.complete();
                 this.virtualScroll.checkEnd();
             }
             if (res['status'] === 'SUCCESS') {
                 if (res['records'].length > 0) {

                     const filteredResultList = [...this.filteredResultList, ...res["records"]];
                     this.filteredResultList = filteredResultList;
                     const resultList = [...this.resultList, ...res["records"]];
                     this.resultList = resultList;
                 }
             } else {
                 this.errorMessageToDisplay = res['message'];

                 if (this.errorMessageToDisplay == "No internet") {
                     this.presentNoInternetToast();
                 }
             }
         }).catch(error => {
             this.isSkeletonLoading = false;
         });
     }

     async onScroll(event) {
         const offsetY = event.offsetY;
         // total height of all rows in the viewport
         const offsetHeight = document.getElementById("table").offsetHeight; // this.el.nativeElement.getBoundingClientRect().height - this.headerHeight + 0;
         const viewHeight = offsetHeight - this.headerHeight;

         // check if we scrolled to the end of the viewport
         if (!this.isLoading && offsetY + viewHeight >= this.filteredResultList.length * this.rowHeight) {
             this.fetchAllData();
         }
     }

     onTableEventChanged(event) {
         if (event["type"] === "click") {
             this.onItemTap(event["row"]);
         }
     }


     async presentNoInternetToast() {
         const toast = await this.toastCtrl.create({
             message: "No internet connection. Please check your internet connection and try again.",
             showCloseButton: true,
             closeButtonText: "retry"
         });
         toast.onDidDismiss().then(() => {
             toast.dismiss();
             this.refreshDataInretry();
         });
         toast.present();
     }

     backButtonOnclick() {
         this.router.navigate([this.redirectUrl], {
             skipLocationChange: true
         });
     }

     refreshDataInretry() {
         this.fetchAllData();
     }
     getSearchedItems(searchText) {
         this.searchTerm = searchText;
         var queryFields = [{
             "fieldName": "name",
             "child": [],
             "mappingDetails": "",
             "fieldType": "AUTONUMBER"
         }, {
             "fieldName": "customername",
             "child": [],
             "mappingDetails": "",
             "fieldType": "TEXT"
         }, {
             "fieldName": "customercontact",
             "child": [],
             "mappingDetails": "",
             "fieldType": "TEXT"
         }];
         if (searchText === "") {
             this.filteredResultList = [...this.resultList];
             this.filteredEventTriggeredList = [...this.eventsTriggeredList];
             return;
         }
         this.filteredResultList = this.resultList.filter((item) => {
             return this.isSearchMatched(item, queryFields, searchText);
         });
         if (this.appUtilityConfig.isMobileResolution === false) {
             this.resultList = [...this.resultList]
             this.filteredResultList = [...this.filteredResultList]
         }

         this.filteredEventTriggeredList = this.eventsTriggeredList.filter((item) => {
             return this.isSearchMatched(item, queryFields, searchText);
         });

     }
     isSearchMatched(item, queryFields, searchText) {
         for (const queryField of queryFields) {
             if (queryField['fieldType'] == 'STRING' || queryField['fieldType'] == 'TEXT' || queryField['fieldType'] == 'TEXTAREA' || queryField['fieldType'] == 'EMAIL' || queryField['fieldType'] == 'NUMBER' || queryField['fieldType'] == 'URL' || queryField['fieldType'] == 'DECIMAL' || queryField['fieldType'] == 'AUTONUMBER' || queryField['fieldType'] == 'BOOLEAN') {
                 if (item[queryField['fieldName']] && item[queryField['fieldName']].toString().toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
                     return true;
                 }
             } else if (queryField['fieldType'] == 'MULTISELECT' || queryField['fieldType'] == 'CHECKBOX') {
                 if (item[queryField['fieldName']] !== undefined && item[queryField['fieldName']] !== "") {
                     const stateTypeList = item[queryField['fieldName']];
                     for (const element of stateTypeList) {
                         if (queryField['mappingDetails'][element] &&
                             queryField['mappingDetails'][element].toString().toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
                             return true;
                         }
                     }
                 }
             } else if (queryField['fieldType'] == 'LOOKUP') {
                 if (item[queryField['fieldName']]) {
                     if (this.isSearchMatched(item[queryField['fieldName']], queryField['child'], searchText)) {
                         return true
                     }
                 }
             } else if (queryField['fieldType'] == 'one_to_one') {
                 if (item[queryField['fieldName']] && item[queryField['fieldName']].length > 0) {
                     if (this.isSearchMatched(item[queryField['fieldName']][0], queryField['child'], searchText)) {
                         return true
                     }
                 }
             } else if (queryField['fieldType'] == 'RADIO' || queryField['fieldType'] == 'DROPDOWN') {
                 if (item[queryField['fieldName']] && item[queryField['fieldName']] !== "") {
                     if (queryField['mappingDetails'][item[queryField['fieldName']]] &&
                         queryField['mappingDetails'][item[queryField['fieldName']]].toString().toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
                         return true;
                     }
                 }
             }
         }
     }



     makeValidReverseHierarchyJSONObject(optionsSelectorKeys, hierarchyJSON) {
         if (optionsSelectorKeys.indexOf("pfm" + hierarchyJSON.objectId) > -1) {
             return hierarchyJSON
         } else {
             if (hierarchyJSON.childObject.length > 0) {
                 let tempReverseHierarchyJson = hierarchyJSON.childObject[0]
                 return this.makeValidReverseHierarchyJSONObject(optionsSelectorKeys, tempReverseHierarchyJson)
             } else {
                 return ''
             }
         }
     }

     makeMappingValueForObjects(mappingStr, reverseHierarchyJSON) {
         mappingStr = mappingStr + ',' + reverseHierarchyJSON.objectId
         if (reverseHierarchyJSON.childObject.length > 0) {
             return this.makeMappingValueForObjects(mappingStr, reverseHierarchyJSON.childObject[0])
         } else {
             return mappingStr
         }
     }



     onCancel() {

     }

     ngOnInit() {}
     ionViewDidEnter() {
         if (this.appUtilityConfig.isMobileResolution === false) {
             this.resultList = [...this.resultList]
             this.filteredResultList = [...this.filteredResultList]
         }
     }


     addButton_1533356_Onclick() {
         const queryParamsRouting = {
             action: 'Add'
         };
         if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/crmcustomer_d_m_entry")) {
             queryParamsRouting['redirectUrl'] = "/menu/crmcustomer_d_m_list"
         }
         this.router.navigate(['/menu/crmcustomer_d_m_entry'], {
             queryParams: queryParamsRouting,
             skipLocationChange: true
         });
     }
     onItemTap(selectedObj) {
         const actionInfo_View = []
         const queryParamsRouting = {
             id: selectedObj["id"],
             viewFetchActionInfo: JSON.stringify(actionInfo_View)
         };
         if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/crmcustomer_d_m_view")) {
             queryParamsRouting['redirectUrl'] = "/menu/crmcustomer_d_m_list"
         }
         this.router.navigate(["/menu/crmcustomer_d_m_view"], {
             queryParams: queryParamsRouting,
             skipLocationChange: true
         });
     }
 }