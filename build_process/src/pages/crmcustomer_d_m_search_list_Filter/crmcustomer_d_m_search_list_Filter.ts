 import {
     Component,
     ViewChild,
     OnInit,
     ApplicationRef,
     ChangeDetectorRef,
     HostListener,
     NgZone
 } from '@angular/core';
 import {
     dbProvider
 } from 'src/core/db/dbProvider';
 import {
     offlineDbIndexCreation
 } from 'src/core/utils/offlineDbIndexCreation';
 import {
     dataProvider
 } from 'src/core/utils/dataProvider';
 import {
     appConstant
 } from 'src/core/utils/appConstant';
 import {
     NavController,
     NavParams,
     ModalController,
     PopoverController,
     AlertController
 } from '@ionic/angular';
 import {
     appUtility
 } from 'src/core/utils/appUtility';
 import * as moment from 'moment';
 import 'moment-timezone';
 import {
     lookuppage
 } from 'src/core/pages/lookuppage/lookuppage';
 import {
     lookupFieldMapping
 } from 'src/core/pfmmapping/lookupFieldMapping';
 import {
     registerLocaleData
 } from '@angular/common';
 import India from '@angular/common/locales/en-IN';
 registerLocaleData(India);
 @Component({
     selector: 'crmcustomer_d_m_search_list_Filter',
     templateUrl: './crmcustomer_d_m_search_list_Filter.html'
 }) export class crmcustomer_d_m_search_list_Filter implements OnInit {
     public filterCustomFieldWithValues = {};
     private filterCustomFieldWithoutValues = {};
     private filterCustomFieldWithValuesTemp = {};
     public filterFieldWithValues = {};
     private filterFieldWithoutValues = {};
     private filterFieldWithValuesTemp = {};



     public customerrisktype = {
         "input": [],
         "selected": [],
         "visible": false,
         "value": []
     };
     public customerstatus = {
         "input": [],
         "selected": [],
         "visible": false,
         "value": []
     };

     private dbprovider;
     private dataSource;
     private tableName_pfm141093 = 'pfm141093';

     constructor(public navCtrl: NavController, public navParams: NavParams,
         public modalCtrl: ModalController, public popoverCtrl: PopoverController,
         public alertCtrl: AlertController, public appUtility: appUtility, public lookupFieldMapping: lookupFieldMapping) {
         this.filterCustomFieldWithValues = this.navParams.get("filterCustomFieldWithValues");
         this.filterCustomFieldWithoutValues = this.navParams.get("filterCustomFieldWithoutValues");
         this.filterCustomFieldWithValuesTemp = JSON.parse(JSON.stringify(this.filterCustomFieldWithValues))
         this.filterFieldWithValues = this.navParams.get("filterFieldWithValues");
         this.filterFieldWithoutValues = this.navParams.get("filterFieldWithoutValues");
         this.filterFieldWithValuesTemp = JSON.parse(JSON.stringify(this.filterFieldWithValues));
         this.customerrisktype = this.filterCustomFieldWithValues['pfm141093']['customerrisktype'];
         this.customerstatus = this.filterCustomFieldWithValues['pfm141093']['customerstatus'];
         this.dbprovider = this.navParams.get('dbProvider');
         this.dataSource = this.navParams.get('dataSource');
     }
     ngOnInit() {}
     initializedatetime(event, objectName, filedName, index ? ) {
         if (typeof(index) == 'number' && index == 0) {
             var currentDateTimeValue = this.filterFieldWithValues[objectName][filedName][this.filterFieldWithValues[objectName][filedName]['betweenfields'][index]]
             if ((currentDateTimeValue == undefined || currentDateTimeValue == '') && event['type'] == "click") {
                 this.filterFieldWithValues[objectName][filedName][this.filterFieldWithValues[objectName][filedName]['betweenfields'][index]] = moment(new Date()).tz(this.appUtility.userTimeZone).format();
             } else if (event['type'] == "ionChange") {
                 this.filterFieldWithValues[objectName][filedName][this.filterFieldWithValues[objectName][filedName]['betweenfields'][index]] = event['detail']['value'];
             } else if (event['type'] == "ionCancel") {
                 this.filterFieldWithValues[objectName][filedName][this.filterFieldWithValues[objectName][filedName]['betweenfields'][index]] = '';
             }
         } else if (typeof(index) == 'number' && index == 1) {
             var currentDateTimeValue = this.filterFieldWithValues[objectName][filedName][this.filterFieldWithValues[objectName][filedName]['betweenfields'][index]]
             if ((currentDateTimeValue == undefined || currentDateTimeValue == '') && event['type'] == "click") {
                 this.filterFieldWithValues[objectName][filedName][this.filterFieldWithValues[objectName][filedName]['betweenfields'][index]] = moment(new Date()).tz(this.appUtility.userTimeZone).format();
             } else if (event['type'] == "ionChange") {
                 this.filterFieldWithValues[objectName][filedName][this.filterFieldWithValues[objectName][filedName]['betweenfields'][index]] = event['detail']['value'];
             } else if (event['type'] == "ionCancel") {
                 this.filterFieldWithValues[objectName][filedName][this.filterFieldWithValues[objectName][filedName]['betweenfields'][index]] = '';
             }
         } else {
             var currentDateTimeValue = this.filterFieldWithValues[objectName][filedName]['fieldValue']
             if ((currentDateTimeValue == undefined || currentDateTimeValue == '') && event['type'] == "click") {
                 this.filterFieldWithValues[objectName][filedName]['fieldValue'] = moment(new Date()).tz(this.appUtility.userTimeZone).format();
             } else if (event['type'] == "ionChange") {
                 this.filterFieldWithValues[objectName][filedName]['fieldValue'] = event['detail']['value'];
             } else if (event['type'] == "ionCancel") {
                 this.filterFieldWithValues[objectName][filedName]['fieldValue'] = '';
             }
         }
     }

     /* Make String Field Type Option */
     makeStringFieldOption(fieldObject) {
         const fieldValue = fieldObject["fieldValue"].trim()
         if (fieldValue) {
             if (this.dataSource === appConstant.pouchDBStaticName) {
                 // For pouchdb
                 const regexp = new RegExp(fieldValue, 'i');
                 return {
                     $regex: regexp
                 };
             } else {
                 // For couchdb
                 return fieldValue.replace(/[^\w\s]/gi, '?').toLowerCase().split(' ').join('?');
             }
         }
     }

     /* Make Number Field Type Option */
     makeNumberFieldOption(fieldObject, objectName, objectKey) {
         // For number fields
         const betweenflag = fieldObject["betweenflag"]
         if (betweenflag) {
             const max = this.filterFieldWithValues[objectName][objectKey]['betweenfields'][0]
             const min = this.filterFieldWithValues[objectName][objectKey]['betweenfields'][1]
             if ((typeof(max) === "number") || (typeof(min) === "number")) {
                 if ((typeof(max) === "number") && (typeof(min) === "number")) {
                     if (Number(min) > Number(max)) {
                         this.showAlert(objectKey + 'from should be lesser value');
                         return
                     } else {
                         if (this.dataSource == appConstant.pouchDBStaticName) {
                             return {
                                 "$gte": Number(min),
                                 "$lte": Number(max)
                             }
                         } else {
                             return "[" + Number(min) + " TO " + Number(max) + "]"
                         }
                     }
                 } else if (max == null || max == "" || min == null || min == "") {
                     this.showAlert(objectKey + ' is empty');
                     return false
                 }
             }
         } else {
             const fieldValue = fieldObject["fieldValue"]
             if (typeof(fieldValue) === "number") {
                 if (this.dataSource == appConstant.pouchDBStaticName) {
                     return {
                         "$eq": Number(fieldValue)
                     }
                 } else {
                     return Number(fieldValue)
                 }
             }
         }
     }

     /* Make Multi Select Field Type Option */
     makeMulitiSelectOption(fieldObject) {
         const fieldValue = fieldObject["fieldValue"]
         console.log("makeMulitiSelectOption fieldValue = ", fieldValue);
         return fieldValue
     }

     /* Make Date/Time Field Type Option */
     makeDateTimeOption(fieldObject, objectName, objectKey) {
         // For date fields
         const betweenflag = fieldObject["betweenflag"]
         const fieldType = fieldObject["fieldType"]
         if (betweenflag) {
             let fromFieldKey = this.filterFieldWithValues[objectName][objectKey]['betweenfields'][0]
             let toFieldKey = this.filterFieldWithValues[objectName][objectKey]['betweenfields'][1]
             const fromDate = this.filterFieldWithValues[objectName][objectKey][fromFieldKey]
             const toDate = this.filterFieldWithValues[objectName][objectKey][toFieldKey]
             if (new Date(toDate).getTime() > 0 && new Date(fromDate).getTime() > 0) {
                 const maxDate = new Date(toDate);
                 const minDate = new Date(fromDate);
                 if (fieldType === "DATE") {
                     maxDate.setHours(23, 59, 59);
                     minDate.setHours(0, 0, 0);
                 }
                 const maxDateMillis = maxDate.getTime();
                 const minDateMillis = minDate.getTime();
                 if (maxDateMillis < minDateMillis) {
                     this.showAlert("From date must be lesser value")
                     return;
                 }
                 if (this.dataSource == appConstant.pouchDBStaticName) {
                     return {
                         "$gte": minDateMillis,
                         "$lte": maxDateMillis
                     }
                 } else {
                     return "[" + minDateMillis + " TO " + maxDateMillis + "]"
                 }
             }
         } else {
             const fieldValue = fieldObject["fieldValue"]
             const fieldType = fieldObject["fieldType"]
             if (new Date(fieldValue).getTime() > 0) {
                 const maxDate = new Date(fieldValue)
                 const minDate = new Date(fieldValue)
                 if (fieldType === "DATE") {
                     maxDate.setHours(23, 59, 59)
                     minDate.setHours(0, 0, 0)
                 } else if (fieldType === "TIMESTAMP") {
                     maxDate.setSeconds(59);
                     minDate.setSeconds(0);
                 }
                 const maxDateMillis = maxDate.getTime();
                 const minDateMillis = minDate.getTime();
                 if (this.dataSource == appConstant.pouchDBStaticName) {
                     return {
                         "$gte": minDateMillis,
                         "$lte": maxDateMillis
                     }
                 } else {
                     return "[" + minDateMillis + " TO " + maxDateMillis + "]"
                 }
             }
         }
     }

     /* Make Boolean Field Type Option */
     makeBooleanOption(fieldObject) {
         const fieldValue = fieldObject["fieldValue"]
         if (!fieldValue[0]) {
             if (fieldValue[1]) {
                 return "false"
             }
             return ""
         }
         if (!fieldValue[1]) {
             if (fieldValue[0]) {
                 return "true"
             }
             return ""
         }
         if (fieldValue[0] && fieldValue[1]) {
             return ""
         }
         console.log("fieldValue = ", fieldValue);
     }

     /* Make Lookup Field Type Option */
     makeLookupOption(objectName, objectKey) {
         const field = this.lookupFieldMapping.mappingDetail[objectName][objectKey];
         return ''
     }

     applyAction() {
         const filterInputParamKeys = Object.keys(this.filterFieldWithValues)
         const optionArray = {}
         for (const objectName of filterInputParamKeys) {
             const filterObjectName = this.filterFieldWithValues[objectName];
             const objectKeys = Object.keys(filterObjectName)
             const option = {}
             const selector = {}
             var query = ""
             for (const objectKey of objectKeys) {
                 const fieldObject = filterObjectName[objectKey]
                 const fieldtype = fieldObject['fieldType'];
                 if (fieldtype === "PASSWORD" || fieldtype === 'TEXT' ||
                     fieldtype === 'URL' || fieldtype === 'AUTONUMBER' ||
                     fieldtype === 'TEXTAREA' ||
                     fieldtype === 'EMAIL') {
                     console.log("makeStringFieldOption");
                     const fieldValue = this.makeStringFieldOption(fieldObject)
                     if (fieldValue) {
                         if (this.dataSource == appConstant.pouchDBStaticName) {
                             selector['data.' + objectKey] = fieldValue
                         } else {
                             query = this.makeQuery(query, objectKey, fieldValue + "*")
                         }
                     }
                 } else if (fieldtype === "NUMBER" || fieldtype === 'DECIMAL' || fieldtype === 'CURRENCY') {
                     console.log("makeNumberFieldOption");
                     const fieldValue = this.makeNumberFieldOption(fieldObject, objectName, objectKey)
                     if (fieldValue) {
                         if (this.dataSource == appConstant.pouchDBStaticName) {
                             selector['data.' + objectKey] = fieldValue
                         } else {
                             query = this.makeQuery(query, objectKey, fieldValue)
                         }

                     } else if (fieldValue === false) {
                         return
                     }
                 } else if (fieldtype === "MULTISELECT" || fieldtype === "CHECKBOX" || fieldtype === 'RADIO' || fieldtype === 'DROPDOWN') {
                     console.log("makeMulitiSelectOption");
                     const fieldValue = this.makeMulitiSelectOption(fieldObject)
                     if (fieldValue.constructor === Array && fieldValue.length > 0) {
                         if (this.dataSource == appConstant.pouchDBStaticName) {
                             selector['data.' + objectKey] = {}
                             selector['data.' + objectKey]["$in"] = fieldValue
                         } else {
                             var selectedValue = "";
                             if (fieldValue.length > 1) {
                                 fieldValue.forEach(element => {
                                     if (selectedValue)
                                         selectedValue = selectedValue + " OR " + element
                                     else
                                         selectedValue = selectedValue + element
                                 });
                                 query = this.makeQuery(query, objectKey, "(" + selectedValue + ")")
                             } else {
                                 selectedValue = fieldValue[0]
                                 query = this.makeQuery(query, objectKey, selectedValue)
                             }

                         }
                     }

                 } else if (fieldtype === "TIMESTAMP" || fieldtype === 'DATE') {
                     console.log("makeDateTimeOption");
                     const fieldValue = this.makeDateTimeOption(fieldObject, objectName, objectKey)
                     if (fieldValue) {
                         if (this.dataSource == appConstant.pouchDBStaticName) {
                             selector['data.' + objectKey] = fieldValue
                         } else {
                             query = this.makeQuery(query, objectKey, fieldValue)
                         }
                     }
                 } else if (fieldtype === "BOOLEAN") {
                     console.log("makeBooleanOption");
                     const fieldValue = this.makeBooleanOption(fieldObject)
                     console.log("fieldValue boolean= ", fieldValue);
                     if (this.dataSource == appConstant.pouchDBStaticName) {
                         if (fieldValue === "true") {
                             selector['data.' + objectKey] = true
                         } else if (fieldValue === "false") {
                             selector['data.' + objectKey] = false
                         }
                     } else {
                         if (fieldValue === "true") {
                             query = this.makeQuery(query, objectKey, true)
                         } else if (fieldValue === "false") {
                             query = this.makeQuery(query, objectKey, false)
                         }
                     }
                 } else if (fieldtype === "LOOKUP") {
                     const fieldValue = this.makeLookupOption(objectName, objectKey)
                     console.log("fieldValue lookup => ", fieldValue);
                     if (fieldValue) {
                         const field = this.lookupFieldMapping.mappingDetail[objectName][objectKey];
                         if (this.dataSource == appConstant.pouchDBStaticName) {
                             selector['data.' + field] = fieldValue
                         } else {
                             query = this.makeQuery(query, field, fieldValue)
                         }
                     }
                 }
             }
             if (this.dataSource == appConstant.pouchDBStaticName) {
                 if (Object.keys(selector).length > 0) {
                     selector['data.type'] = objectName
                 }
                 if (Object.keys(selector).length > 0) {
                     option['selector'] = selector
                 }
                 if (Object.keys(option).length > 0) {
                     optionArray[objectName] = option
                 }
             } else {
                 if (query) {
                     query = "type:" + objectName + " AND " + query
                     optionArray[objectName] = query
                 }
             }
         }

         const optionArrayKeys = Object.keys(optionArray);
         const parent = this.navParams.get("parentPage");
         if (optionArrayKeys.length > 0) {
             if (optionArrayKeys.length === 1) {
                 // Primary Object Search Field only in filter page
                 if (optionArrayKeys.indexOf(this.tableName_pfm141093) > -1) {
                     let parentObjectFilter = {}
                     parentObjectFilter = optionArray[this.tableName_pfm141093];
                     parent.filterResponse(parentObjectFilter);
                 } else {
                     if (this.dataSource === appConstant.couchDBStaticName) {
                         parent.makeQueryForPrimayDataFetching(optionArray)
                     } else {
                         parent.makeFilterPrimaryId(optionArray)
                     }
                 }
             } else {
                 if (this.dataSource === appConstant.couchDBStaticName) {
                     parent.makeQueryForPrimayDataFetching(optionArray)
                 } else {
                     parent.makeFilterPrimaryId(optionArray)
                 }
             }
         } else {
             parent.filterResponse();
             parent.filterParamReset(this.filterFieldWithValues, this.filterCustomFieldWithValues);
         }
         //this.closeAction();
         this.modalCtrl.dismiss(true);
     }

     clearAction() {
         this.filterFieldWithValues = JSON.parse(JSON.stringify(this.filterFieldWithoutValues));
         this.filterCustomFieldWithValues = JSON.parse(JSON.stringify(this.filterCustomFieldWithoutValues));
         this.customerstatus['input'].forEach(element => {
             element['isChecked'] = false;
         });
         this.customerstatus['value'] = [];
         this.customerstatus['selected'] = [];
         this.customerstatus['visible'] = false;
         this.customerrisktype['input'].forEach(element => {
             element['isChecked'] = false;
         });
         this.customerrisktype['value'] = [];
         this.customerrisktype['selected'] = [];
         this.customerrisktype['visible'] = false;
     }


     closeAction() {
         const parent = this.navParams.get("parentPage");
         console.log("parent.filterApplied => ", parent.filterApplied);
         this.filterFieldWithValues = JSON.parse(JSON.stringify(this.filterFieldWithValuesTemp))
         this.filterCustomFieldWithValues = JSON.parse(JSON.stringify(this.filterCustomFieldWithValuesTemp))
         parent.filterParamReset(this.filterFieldWithValues, this.filterCustomFieldWithValues)
         this.modalCtrl.dismiss(true);
     }
     toggleSelectedItem(resultList, item, fieldData, selectedFilterValue, objectName, fieldName) {
         item.isChecked = !item.isChecked;

         if (item.isChecked) {
             resultList.push(item)
             fieldData['value'].push(item['value'])
         } else {
             var index = resultList.findIndex(element => element['value'] === item.value);
             if (index > -1) {
                 resultList.splice(index, 1)
             }
             fieldData['value'].splice(fieldData['value'].indexOf(item['value']), 1)
         }
         selectedFilterValue["fieldValue"] = fieldData["value"]
         this.filterCustomFieldWithValues[objectName][fieldName] = fieldData;
     }
     removeSelectedItem(event, resultList, item, fieldData, objectName, fieldName) {
         event.stopPropagation();
         item.isChecked = false;
         resultList.splice(resultList.indexOf(item), 1)
         var index = fieldData['input'].findIndex(element => element['value'] === item.value);
         if (index > -1) {
             fieldData['input'][index]['isChecked'] = false
         }
         fieldData['value'].splice(fieldData['value'].indexOf(item['value']), 1)
         this.filterCustomFieldWithValues[objectName][fieldName] = fieldData;
     }

     openMultiSelect(event, fieldData) {
         event.stopPropagation();
         fieldData['visible'] = true;
     }

     closeMultiSelect(fieldData) {
         fieldData['visible'] = false;
     }

     async showAlert(message) {
         let method = await this.alertCtrl.create({
             message: message,
             buttons: [{
                 text: 'Ok',
                 cssClass: 'method-color',
                 handler: () => {
                     console.log('Individual clicked');
                 }
             }]
         });
         await method.present()
     }
     makeQuery(query, objectKey, fieldValue) {
         if (query.length == 0) {
             query = query + objectKey + ":" + fieldValue
         } else {
             query = query + " AND " + objectKey + ":" + fieldValue
         }
         return query
     }

     getSearchedItems(searchText) {}
     onCancel() {}
 }