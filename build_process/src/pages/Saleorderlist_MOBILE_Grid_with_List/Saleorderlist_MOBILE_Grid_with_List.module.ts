import { NgModule } from '@angular/core';
          import { CommonModule } from '@angular/common';
          import { FormsModule } from '@angular/forms';
          import { Routes, RouterModule } from '@angular/router';
          import { IonicModule } from '@ionic/angular';
           import {Saleorderlist_MOBILE_Grid_with_List} from './Saleorderlist_MOBILE_Grid_with_List';           
           import { SharedModule } from 'src/core/utils/shared.module';
           import { QuillModule } from 'ngx-quill';
           import { cs_whocolumn_iconmodule } from 'src/core/components/cs_whocolumn_icon/cs_whocolumn_icon.module';
           import { cs_status_workflowmodule } from '../../core/components/cs_status_workflow/cs_status_workflow.module';

           const routes: Routes = [
            {
              path: '',
              component: Saleorderlist_MOBILE_Grid_with_List
            }
          ];
           
             @NgModule({ 
              imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes),
                cs_whocolumn_iconmodule,
                QuillModule.forRoot(),
                SharedModule,
                cs_status_workflowmodule
              ],
                 declarations:[Saleorderlist_MOBILE_Grid_with_List]
                })
                export class Saleorderlist_MOBILE_Grid_with_Listmodule{};