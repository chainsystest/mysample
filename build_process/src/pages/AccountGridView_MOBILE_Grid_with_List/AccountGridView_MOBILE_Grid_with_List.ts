 import {
     Component,
     ViewChild,
     OnInit,
     ApplicationRef,
     ChangeDetectorRef,
     HostListener,
     NgZone
 } from '@angular/core';
 import {
     dbProvider
 } from 'src/core/db/dbProvider';
 import {
     offlineDbIndexCreation
 } from 'src/core/utils/offlineDbIndexCreation';
 import {
     dataProvider
 } from 'src/core/utils/dataProvider';
 import {
     appConstant
 } from 'src/core/utils/appConstant';
 import {
     Platform,
     LoadingController,
     Events,
     ToastController,
     IonVirtualScroll,
     AlertController
 } from '@ionic/angular';
 import {
     DrawerState
 } from 'ion-bottom-drawer';
 import {
     appUtility
 } from 'src/core/utils/appUtility';
 import {
     lookupFieldMapping
 } from 'src/core/pfmmapping/lookupFieldMapping';
 import {
     objectTableMapping
 } from 'src/core/pfmmapping/objectTableMapping';
 import {
     Router,
     ActivatedRoute
 } from '@angular/router';
 import {
     ScreenOrientation
 } from '@ionic-native/screen-orientation/ngx';
 import {
     cspfmExecutionPouchDbProvider
 } from 'src/core/db/cspfmExecutionPouchDbProvider';
 import {
     cspfmExecutionPouchDbConfiguration
 } from 'src/core/db/cspfmExecutionPouchDbConfiguration';
 import * as lodash from 'lodash';
 import * as _ from 'underscore';
 import {
     DatePipe
 } from '@angular/common';
 import {
     registerLocaleData
 } from '@angular/common';
 import {
     metaDataDbProvider
 } from 'src/core/db/metaDataDbProvider';
 import {
     metaDbConfiguration
 } from 'src/core/db/metaDbConfiguration';
 import {
     FieldInfo
 } from "src/core/pipes/cspfm_data_display";
 import {
     ColumnMode
 } from "@swimlane/ngx-datatable";

 import {
     AccountGridView_MOBILE_Grid_with_Listpreview
 } from '../AccountGridView_MOBILE_Grid_with_Listpreview/AccountGridView_MOBILE_Grid_with_Listpreview';
 @Component({
     selector: 'AccountGridView_MOBILE_Grid_with_List',
     templateUrl: './AccountGridView_MOBILE_Grid_with_List.html'
 }) export class AccountGridView_MOBILE_Grid_with_List implements OnInit {
     private prominetDataMapping = {
         "pfm147233": [null, "sitenumber", "addresstype", "stateprovince"],
         "pfm147213": [null, "accountno", "accountname", "businessunits", "status", "taxregistrationnumber"]
     };
     public isBrowser = false;
     private isScroll = true;
     public drawerComponentDockedHeight = 300;
     public drawerComponentCurrentState = DrawerState.Top;
     private drawerComponentPreviousState = DrawerState.Docked;
     private errorMessageToDisplay = "No Records";
     // private obj_pfm147213:any = {};
     // private layoutId ;
     public childObjectsInfo = [];
     public eventsTriggeredList: Array < any > = [];
     private tableName_pfm147213 = "pfm147213";
     private id = "";
     private redirectUrl = "/";
     public showNavigationHistoryPopUp: Boolean = false;
     public navigationHistoryProperties = {
         'navigatedPagesNameArray': [],
         'navigatedPagesPathArray': [],
         'routerVisLinkTagName': "",
         'secondPreviousPage': "",
         'navigatedPagesLength': 0,
         'previousPage': "",
         'previousPageName': "",
         'secondPreviousPageName': "",
     };
     public layoutId = "58369";
     public isSkeletonLoading = true;
     private approverType: string = "";
     private currentStatusWorkFlowActionFieldId;


     constructor(public router: Router, public activatRoute: ActivatedRoute,
         platform: Platform, public dataProvider: dataProvider,
         public appUtilityConfig: appUtility,
         public loadingCtrl: LoadingController,
         public toastCtrl: ToastController,
         public applicationRef: ApplicationRef,
         private screenOrientation: ScreenOrientation,
         public events: Events,
         public offlineDbIndexCreation: offlineDbIndexCreation,
         public metaDbConfigurationObj: metaDbConfiguration,
         public metaDbProvider: metaDataDbProvider, public cspfmexecutionPouchDbProvider: cspfmExecutionPouchDbProvider,
         public executionDbConfigObject: cspfmExecutionPouchDbConfiguration, private datePipe: DatePipe,
         public alerCtrl: AlertController) {
         this.activatRoute.queryParams.subscribe(params => {
             if (Object.keys(params).length == 0 && params.constructor === Object) {
                 console.log("list query params skipped");
                 return
             }
             if (params["redirectUrl"]) {
                 this.redirectUrl = params["redirectUrl"]
             }
             this.id = params["id"];
             this.fetchSelectedObject(this.id);
         });

         const windowHeight = window.innerHeight;
         this.drawerComponentDockedHeight = windowHeight / 2;
         if (!this.appUtilityConfig.isMobile || this.appUtilityConfig.osType === "android") {
             this.isBrowser = true
         }
         //Handle docker height in screen orientation
         this.screenOrientation.onChange().subscribe(() => {
             this.drawerComponentCurrentState = DrawerState.Bottom;
             const windowHeightVal = window.innerHeight;
             this.drawerComponentDockedHeight = windowHeightVal / 2;
         });

         //create event sudscribtion with the name published in list page
         this.appUtilityConfig.setEventSubscriptionlayoutIds(this.tableName_pfm147213, this.layoutId);
         this.events.subscribe(this.layoutId, (modified) => {
             if (modified["dataProvider"] == "PouchDB") {
                 this.stackModifiedEventTrigger(modified);
             }
         });
         for (let i = 0; i < this.childObjects.length; i++) {
             this.appUtilityConfig.setEventSubscriptionlayoutIds(this.childObjects[i], this.layoutId + '_' + this.childObjects[i])
             this.events.subscribe(this.layoutId + '_' + this.childObjects[i], (modified) => {
                 if (modified["dataProvider"] == "PouchDB") {
                     this.childObjectModifiedEventTrigger(modified, i);
                 }
             });
         }
     }
     public obj_pfm147213: any = {};
     private obj_pfm147233: any = {};
     public gridFieldInfo: {
         [key: string]: FieldInfo
     } = {
         "pfm147213_accountno": {
             "label": "AccountGridView_MOBILE_Grid_with_List.Element.customermaster.accountno",
             "fieldName": "accountno",
             "prop": "accountno",
             "fieldType": "AUTONUMBER",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm147213_accountname": {
             "label": "AccountGridView_MOBILE_Grid_with_List.Element.customermaster.accountname",
             "fieldName": "accountname",
             "prop": "accountname",
             "fieldType": "TEXT",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm147213_businessunits": {
             "label": "AccountGridView_MOBILE_Grid_with_List.Element.customermaster.businessunits",
             "fieldName": "businessunits",
             "prop": "businessunits",
             "fieldType": "DROPDOWN",
             "child": "",
             "dateFormat": "",
             "mappingDetails": {
                 "None": "None",
                 "Sole proprietorship": "Sole proprietorship",
                 "Partnership": "Partnership",
                 "Joint Stock Company": "Joint Stock Company"
             },
             "currencyDetails": ""
         },
         "pfm147213_status": {
             "label": "AccountGridView_MOBILE_Grid_with_List.Element.customermaster.status",
             "fieldName": "status",
             "prop": "status",
             "fieldType": "DROPDOWN",
             "child": "",
             "dateFormat": "",
             "mappingDetails": {
                 "New": "New",
                 "InRview": "InReview",
                 "Approved": "Approved",
                 "Rejected": "Rejected",
                 "Active": "Active",
                 "In Active": "In Active"
             },
             "currencyDetails": ""
         },
         "pfm147213_taxregistrationnumber": {
             "label": "AccountGridView_MOBILE_Grid_with_List.Element.customermaster.taxregistrationnumber",
             "fieldName": "taxregistrationnumber",
             "prop": "taxregistrationnumber",
             "fieldType": "TEXT",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         }
     };
     private childObjects = ['pfm147233'];
     public formulaObject = {};
     public sectionLayoutId = 'undefined';
     private viewFetchActionInfo: Array < any > = [];
     private objectNameMapping = {
         "pfm147233s": "Address List",
         "pfm0s": "Account View"
     };
     private objectRelationshipMapping = {
         "pfm147233": "one_to_many"
     };
     private objectHierarchyJSON = {
         "objectName": "customermaster",
         "objectType": "PRIMARY",
         "relationShipType": null,
         "fieldId": "0",
         "objectId": "147213",
         "childObject": [{
             "objectName": "addressdetails",
             "objectType": "MASTERDETAIL",
             "relationShipType": "one_to_many",
             "fieldId": "293790",
             "objectId": "147233",
             "childObject": []
         }]
     };
     public layoutDataRestrictionSet = [];
     private sectionObjectsHierarchy = {
         "pfm147233": {
             "objectName": "addressdetails",
             "objectType": "MASTERDETAIL",
             "relationShipType": "one_to_many",
             "fieldId": "293790",
             "objectId": "147233",
             "childObject": []
         }
     };
     public gridTableFieldInfoArray: {
         [key: string]: Array < FieldInfo >
     } = {
         "pfm147233": [{
             "label": "AccountGridView_MOBILE_Grid_with_List.Element.addressdetails.sitenumber",
             "fieldName": "sitenumber",
             "prop": "sitenumber",
             "fieldType": "TEXT",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         }, {
             "label": "AccountGridView_MOBILE_Grid_with_List.Element.addressdetails.addresstype",
             "fieldName": "addresstype",
             "prop": "addresstype",
             "fieldType": "CHECKBOX",
             "child": "",
             "dateFormat": "",
             "mappingDetails": {
                 "None": "None",
                 "Ship To": "Ship To",
                 "Bill To": "Bill To"
             },
             "currencyDetails": ""
         }, {
             "label": "AccountGridView_MOBILE_Grid_with_List.Element.addressdetails.stateprovince",
             "fieldName": "stateprovince",
             "prop": "stateprovince",
             "fieldType": "DROPDOWN",
             "child": "",
             "dateFormat": "",
             "mappingDetails": {
                 "None": "None",
                 "California": "California",
                 "Florida": "Florida",
                 "Texas": "Texas",
                 "New Jersey": "New Jersey",
                 "North Carolina": "North Carolina",
                 "Alberta": "Alberta",
                 "British Columbia": "British Columbia",
                 "Nova Scotia": "Nova Scotia",
                 "Manitoba": "Manitoba",
                 "Delhi": "Delhi",
                 "Tamil Nadu": "Tamil Nadu",
                 "Kerala": "Kerala"
             },
             "currencyDetails": ""
         }]
     };
     public sectionalFetchMapping = {};
     public fetchActionInfo: any = {};
     public layoutDBName = 'Not Web Service';
     public formulaReverseObjectHierarchyJSON = undefined;
     public formulaConfigJSON = {};
     public formulafields = {};
     public dbServiceProvider = appConstant.pouchDBStaticName;
     private businessunits_293693 = {
         'None': 'None',
         'Sole proprietorship': 'Sole proprietorship',
         'Partnership': 'Partnership',
         'Joint Stock Company': 'Joint Stock Company'
     };
     private status_293810 = {
         'New': 'New',
         'InRview': 'InReview',
         'Approved': 'Approved',
         'Rejected': 'Rejected',
         'Active': 'Active',
         'In Active': 'In Active'
     };
     async fetchSelectedObject(id) {
         const additionalObjectdata = {};
         additionalObjectdata["id"] = this.id;
         additionalObjectdata["isFirstLevelFetchNeed"] = true;
         const fetchParams = {
             'objectHierarchyJSON': this.objectHierarchyJSON,
             'additionalInfo': additionalObjectdata,
             'dataSource': appConstant.pouchDBStaticName,
         }
         var taskList = [];
         this.childObjectsInfo = [];
         taskList.push(this.dataProvider.querySingleDoc(fetchParams).then(result => {
             console.log("Response :", JSON.parse(JSON.stringify(result)));


             if (result["status"] !== "SUCCESS") {
                 this.errorMessageToDisplay = result["message"];
                 if (this.errorMessageToDisplay === "No internet") {
                     this.presentNoInternetToast();
                 }
                 return;
             }

             this.obj_pfm147213 = result["records"][0];


             if (this.obj_pfm147213 != undefined && this.obj_pfm147213[this.dataProvider.getPluralName('pfm147233')] && this.obj_pfm147213[this.dataProvider.getPluralName('pfm147233')].length > 0) {
                 this.obj_pfm147233 = this.obj_pfm147213[this.dataProvider.getPluralName('pfm147233')][0];
             }

             this.applicationRef.tick();

             return result;
         }).catch(error => {
             this.isSkeletonLoading = false
             console.log(error);
         }));

         this.childObjects.forEach(childObject => {
             var sectionChildObjectHierarchy = this.sectionObjectsHierarchy[childObject]
             const sectionChildFetchParams = {
                 'objectHierarchyJSON': sectionChildObjectHierarchy,
                 'additionalInfo': additionalObjectdata,
                 'dataSource': appConstant.pouchDBStaticName,
                 'layoutDataRestrictionSet': this.layoutDataRestrictionSet
             }
             taskList.push(this.dataProvider.getChildCount(sectionChildFetchParams).then(childResponse => {
                 if (childResponse["status"] !== "SUCCESS") {
                     this.errorMessageToDisplay = childResponse["message"];
                     if (this.errorMessageToDisplay === "No internet") {
                         this.presentNoInternetToast();
                     }
                     return;
                 }
                 const pluralName = this.dataProvider.getPluralName(childObject);
                 const obj = {
                     displayName: this.objectNameMapping[pluralName],
                     objectName: childObject,
                     childDocsArray: childResponse['records'],
                     relationshipType: this.objectRelationshipMapping[childObject]
                 };
                 // this.addChildHierarchyJson(childObject, obj);
                 this.childObjectsInfo.push(obj);
                 // console.log("Object :", this.objectNameMapping[pluralName], obj);

                 return childResponse;
             }))
         })
         Promise.all(taskList).then(res => {
             console.log("All res :", res);
             this.isSkeletonLoading = false
         })
     }

     async presentNoInternetToast() {
         const toast = await this.toastCtrl.create({
             message: "No internet connection. Please check your internet connection and try again.",
             showCloseButton: true,
             closeButtonText: "retry"
         });
         toast.onDidDismiss().then(() => {
             toast.dismiss();
             this.refreshData();
         });
         toast.present();
     }

     refreshData() {
         this.fetchSelectedObject(this.id);
     }


     historyButtonAction(event: Event) {
         this.showNavigationHistoryPopUp = !this.showNavigationHistoryPopUp;
         event.stopPropagation();
     }
     @HostListener('click') onClick() {
         this.showNavigationHistoryPopUp = false;
     }
     getNavigationHistory() {
         if (this.appUtilityConfig.getHomePageNode()) {
             let homePageNode = this.appUtilityConfig.getHomePageNode();
             this.navigationHistoryProperties['navigatedPagesNameArray'] = [homePageNode['homePageNodeName']];
             this.navigationHistoryProperties['navigatedPagesPathArray'] = [homePageNode['homePageNodepath']];
         }
         this.navigationHistoryProperties['navigatedPagesLength'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes.length;
         this.navigationHistoryProperties['previousPageName'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 2].childNodes[0].textContent;
         this.navigationHistoryProperties['previousPage'] = ( < HTMLElement > document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 2]).tagName.toLowerCase();
         if (document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3]) {
             this.navigationHistoryProperties['secondPreviousPageName'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3].childNodes[0].textContent;
             this.navigationHistoryProperties['secondPreviousPage'] = ( < HTMLElement > document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3]).tagName.toLowerCase();
         }
         for (let i = 0; i < this.navigationHistoryProperties['navigatedPagesLength'] - 3; i++) {
             this.navigationHistoryProperties['navigatedPagesNameArray'].push(document.getElementsByTagName('ion-router-outlet')[1].childNodes[i].childNodes[0].textContent);
             this.navigationHistoryProperties['navigatedPagesPathArray'].push(document.getElementsByTagName('ion-router-outlet')[1].children[i].tagName.toLowerCase());
         }
     }
     histListNav(e) {
         if (e.currentTarget.getAttribute('data-index') == 'app-homepage') {
             this.appUtilityConfig.navigateToHomepage();
         } else {
             this.router.navigate([`/menu/${e.currentTarget.getAttribute('data-index')}`]);
         }
     }
     previousPageNavigation() {
         this.router.navigate(["/menu/" + this.navigationHistoryProperties['previousPage']]);
     }
     secondPreviousPageNavigation() {
         this.router.navigate(["/menu/" + this.navigationHistoryProperties['secondPreviousPage']]);
     }
     ngOnInit() {
         this.getNavigationHistory()
     }

     async fetchModifiedRec(modifiedData) {

         let additionalObjectdata = {};

         additionalObjectdata['id'] = modifiedData

         const fetchParams = {
             'objectHierarchyJSON': this.objectHierarchyJSON,
             'dataSource': appConstant.pouchDBStaticName,
             'additionalInfo': additionalObjectdata
         }

         this.dataProvider.fetchDataFromDataSource(fetchParams).then(result => {

             if (result['status'] != 'SUCCESS') {
                 this.errorMessageToDisplay = result['message'];
                 return
             }
             this.obj_pfm147213 = result['records'][0];
             // Handling Lookup

         }).catch(error => {
             console.log(error)
         });
     }

     addChildHierarchyJson(childObjectName, object) {
         for (let i = 0; i < this.objectHierarchyJSON.childObject.length; i++) {
             const childHierarchyJson = this.objectHierarchyJSON.childObject[i];
             if ("pfm" + childHierarchyJson.objectId == childObjectName) {
                 object['childHierarchyJson'] = childHierarchyJson;
             }
         }
     }

     navigateObjectBaseEntryPage(selectedObjectName, addNavigationParams, options) {}


     onItemTap(selectedRecord) {
         let prominientObject = this.prominetDataMapping[selectedRecord.objectName];
         let optionObj = {
             animate: false
         }
         let childDocsArray = selectedRecord['childDocsArray'];
         if (selectedRecord['relationshipType'] == 'one_to_one' &&
             childDocsArray != undefined &&
             childDocsArray.length > 0) {
             let itemtapnavigationObj = {
                 //     selectedObjectParent: this.obj_pfm147213,
                 //     selectedObject: childDocsArray[0],
                 //     prominientObjectInfo: prominientObject,                                             
                 //     objectNameParent:this.tableName_pfm147213,
                 // parentTitle: this.obj_pfm147213['displayName'],
                 // headerDocId: this.obj_pfm147213['id'],
                 // parentFieldLabel:"accountno",
                 // parentFieldValue:"accountno"
                 parentObj: JSON.stringify(this.obj_pfm147213),
                 parentFieldLabel: "Account No",
                 parentTitle: this.objectNameMapping['pfm0s'],
                 parentFieldValue: this.obj_pfm147213['accountno'],
                 id: childDocsArray[0]['id'].split("_")[2],
                 prominientObjectInfo: prominientObject
             };
             this.navigateObjectBaseDetailPage(selectedRecord['objectName'], itemtapnavigationObj, optionObj)
         } else if (selectedRecord['relationshipType'] == 'one_to_one' && childDocsArray.length == 0) {
             let itemtapnavigationObj = {
                 action: 'Add',
                 parentId: this.obj_pfm147213['id'],
                 parentFieldLabel: "Account No",
                 parentFieldValue: this.obj_pfm147213['accountno'],
                 parentName: this.tableName_pfm147213,
                 parentPage: this,
                 parentTitle: this.objectNameMapping['pfm0s'],
                 parentObj: JSON.stringify(this.obj_pfm147213)
             }
             this.navigateObjectBaseEntryPage(selectedRecord['objectName'], itemtapnavigationObj, optionObj)
         } else {
             selectedRecord['parentTitle'] = this.objectNameMapping['pfm0s'];
             let itemtapnavigationObj = {
                 // selectedObjectParent: this.obj_pfm147213,
                 // selectedObject: selectedRecord,
                 // prominientObjectInfo: prominientObject,                                             
                 // objectNameParent:this.tableName_pfm147213,
                 // headerFieldName:"accountno",
                 // headerFieldTranslateKey:"Account No",
                 // childObjectHierarchyJSON: selectedRecord.childHierarchyJson
                 parentObj: JSON.stringify(this.obj_pfm147213),
                 parentObjType: this.tableName_pfm147213,
                 parentName: this.tableName_pfm147213,
                 parentLabel: this.objectNameMapping['pfm0s'],
                 parentFieldLabel: "Account No",
                 parentFieldValue: this.obj_pfm147213['accountno'],
                 childObjectHierarchyJSON: JSON.stringify(this.sectionObjectsHierarchy[selectedRecord['objectName']]),
                 parentHierarchyJSON: JSON.stringify(this.objectHierarchyJSON),
                 objLabel: selectedRecord['displayName'],
                 objType: selectedRecord['objectName'],
                 prominientObjectInfo: prominientObject,
                 gridInfoArray: JSON.stringify(this.gridTableFieldInfoArray[selectedRecord["objectName"]])
             };
             this.toastCtrl.dismiss();
             if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/AccountGridView_MOBILE_Grid_with_Listpreview")) {
                 itemtapnavigationObj['redirectUrl'] = "/menu/AccountGridView_MOBILE_Grid_with_List"
             }
             this.router.navigate(["/menu/AccountGridView_MOBILE_Grid_with_Listpreview"], {
                 queryParams: itemtapnavigationObj,
                 skipLocationChange: true
             })
         }
     }
     navigateObjectBaseDetailPage(selectedObjectName, itemTapNavigationParams, options) {}


     ionViewDidLoad() {
         //    this.navBar.backButtonClick = (e:UIEvent)=>{
         //       let opts = {animate:false};
         //       this.navCtrl.pop(opts);
         //     };                        
     };

     ionViewWillEnter() {
         document.body.setAttribute('class', 'linedetail');
         this.drawerComponentCurrentState = DrawerState.Docked;
         this.drawerComponentPreviousState = DrawerState.Bottom;
         for (let i = 0; i < document.getElementsByClassName('cs-divider').length; i++) {
             document.getElementsByClassName('cs-divider')[i].classList.remove('bottom');
             document.getElementsByClassName('cs-divider')[i].classList.remove('top');
             document.getElementsByClassName('cs-divider')[i].classList.add('center');
         }

     };

     ionViewWillLeave() {
         this.drawerComponentCurrentState = DrawerState.Bottom;
     };

     ngOnDestroy() {
         this.events.unsubscribe(this.layoutId);
         for (let i = 0; i < this.childObjects.length; i++) {
             this.events.unsubscribe(this.layoutId + '_' + this.childObjects[i]);
             this.appUtilityConfig.removeEventSubscriptionlayoutIds(this.childObjects[i], this.layoutId + '_' + this.childObjects[i])
         }
         this.appUtilityConfig.removeEventSubscriptionlayoutIds(this.tableName_pfm147213, this.layoutId);
     }

     //event method for parent object
     //object name should be come infront of the Modified 
     stackModifiedEventTrigger(modified) {
         const modifiedData = this.dataProvider.convertRelDocToNormalDoc(modified);
         if (modifiedData.id == this.obj_pfm147213['id']) {
             //data need to modify
             this.fetchModifiedRec(modifiedData.id);
         }
     }

     //event method for parent object 
     childObjectModifiedEventTrigger(modified, index) {
         const modifiedData = this.dataProvider.convertRelDocToNormalDoc(modified);
         if (modifiedData.pfm147213 == this.obj_pfm147213['id']) {
             modifiedData['id'] = modifiedData['type'] + '_2_' + modifiedData['id']

             console.log('modified data', modifiedData, this.childObjects[index])
             let dictVal = this.childObjectsInfo[index];
             let arrayValue = dictVal.childDocsArray;
             let eventsTriggeredindex = this.getChangedObjectIndex(arrayValue, modifiedData, 'id');
             if (eventsTriggeredindex > -1) {
                 arrayValue.splice(eventsTriggeredindex, 1);
             }
             arrayValue.push(modifiedData);
             dictVal.childDocsArray = arrayValue;
             this.childObjectsInfo[index] = dictVal;
         }
     }

     getChangedObjectIndex(array, modifiedData, key) {
         let str = modifiedData['type'] + '_2_' //getting the pfm_2_
         return lodash.findIndex(array, item => {
             if (!item['id'].includes(str)) { //checking if the item['id'] has pfm_2_.
                 item['id'] = str + item['id'] //if not we are adding pfm_2_ to the item['id']
             }
             return item[key] === modifiedData[key];
         }, 0);
     }

     dockerButtonOnclick() {
         if (this.drawerComponentCurrentState == DrawerState.Bottom) {
             this.drawerComponentCurrentState = DrawerState.Docked
             this.drawerComponentPreviousState = DrawerState.Bottom
             // for (let i = 0; i < document.getElementsByClassName('cs-divider').length; i++) {
             //     document.getElementsByClassName('cs-divider')[i].classList.remove('bottom');
             //     document.getElementsByClassName('cs-divider')[i].classList.remove('top');
             //     document.getElementsByClassName('cs-divider')[i].classList.add('center');
             //   }

         } else if (this.drawerComponentCurrentState == DrawerState.Docked) {
             if (this.drawerComponentPreviousState == DrawerState.Bottom) {
                 this.drawerComponentCurrentState = DrawerState.Top
                 // for (let i = 0; i < document.getElementsByClassName('cs-divider').length; i++) {
                 //     document.getElementsByClassName('cs-divider')[i].classList.remove('bottom');
                 //     document.getElementsByClassName('cs-divider')[i].classList.remove('center');
                 //     document.getElementsByClassName('cs-divider')[i].classList.add('top');
                 //   }


             } else {
                 this.drawerComponentCurrentState = DrawerState.Bottom
                 // for (let i = 0; i < document.getElementsByClassName('cs-divider').length; i++) {
                 //     document.getElementsByClassName('cs-divider')[i].classList.remove('center');
                 //     document.getElementsByClassName('cs-divider')[i].classList.remove('top');
                 //     document.getElementsByClassName('cs-divider')[i].classList.add('bottom');
                 //   }


             }
             this.drawerComponentPreviousState = DrawerState.Docked;
         } else if (this.drawerComponentCurrentState == DrawerState.Top) {
             this.drawerComponentCurrentState = DrawerState.Docked
             this.drawerComponentPreviousState = DrawerState.Top
             // for (let i = 0; i < document.getElementsByClassName('cs-divider').length; i++) {
             //     document.getElementsByClassName('cs-divider')[i].classList.remove('bottom');
             //     document.getElementsByClassName('cs-divider')[i].classList.remove('top');
             //     document.getElementsByClassName('cs-divider')[i].classList.add('center');
             //   }


         }
     }
     backButtonOnclick() {
         this.router.navigate([this.redirectUrl], {
             skipLocationChange: true
         });
     };
     editButton_elementId_Onclick() {
         let navigationParameters = {
             action: 'Edit',
             id: this.obj_pfm147213['id'],
             parentPage: this
         }
         if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/AccountEntry_MOBILE_Grid")) {
             navigationParameters['redirectUrl'] = "/menu/AccountGridView_MOBILE_Grid_with_List"
         }
         this.router.navigate(["/menu/AccountEntry_MOBILE_Grid"], {
             queryParams: navigationParameters,
             skipLocationChange: true
         })
     }
 }