 import {
     Component,
     ViewChild,
     OnInit,
     ApplicationRef,
     ChangeDetectorRef,
     HostListener,
     NgZone
 } from '@angular/core';
 import {
     dbProvider
 } from 'src/core/db/dbProvider';
 import {
     offlineDbIndexCreation
 } from 'src/core/utils/offlineDbIndexCreation';
 import {
     dataProvider
 } from 'src/core/utils/dataProvider';
 import {
     appConstant
 } from 'src/core/utils/appConstant';
 import {
     Platform,
     LoadingController,
     Events,
     ToastController,
     IonVirtualScroll,
     AlertController
 } from '@ionic/angular';
 import {
     DrawerState
 } from 'ion-bottom-drawer';
 import {
     appUtility
 } from 'src/core/utils/appUtility';
 import {
     lookupFieldMapping
 } from 'src/core/pfmmapping/lookupFieldMapping';
 import {
     objectTableMapping
 } from 'src/core/pfmmapping/objectTableMapping';
 import {
     Router,
     ActivatedRoute
 } from '@angular/router';
 import {
     ScreenOrientation
 } from '@ionic-native/screen-orientation/ngx';
 import {
     cspfmExecutionPouchDbProvider
 } from 'src/core/db/cspfmExecutionPouchDbProvider';
 import {
     cspfmExecutionPouchDbConfiguration
 } from 'src/core/db/cspfmExecutionPouchDbConfiguration';
 import * as lodash from 'lodash';
 import * as _ from 'underscore';
 import {
     DatePipe
 } from '@angular/common';
 import {
     registerLocaleData
 } from '@angular/common';
 import {
     metaDataDbProvider
 } from 'src/core/db/metaDataDbProvider';
 import {
     metaDbConfiguration
 } from 'src/core/db/metaDbConfiguration';
 import {
     FieldInfo
 } from "src/core/pipes/cspfm_data_display";
 import {
     ColumnMode
 } from "@swimlane/ngx-datatable";

 import {
     Saleorderview_MOBILE_Grid_with_Listpreview
 } from '../Saleorderview_MOBILE_Grid_with_Listpreview/Saleorderview_MOBILE_Grid_with_Listpreview';
 @Component({
     selector: 'Saleorderview_MOBILE_Grid_with_List',
     templateUrl: './Saleorderview_MOBILE_Grid_with_List.html'
 }) export class Saleorderview_MOBILE_Grid_with_List implements OnInit {
     private prominetDataMapping = {
         "pfm144373": [null, "orderid", "customername", null, null, null, "orderstatus", "orderconfirmeddate", "ordercompleteddate", "shiptoaddress", "billtoaddress"],
         "pfm147213": [null, null, null, "customercontact", "accountname", "customernumber"],
         "pfm144393": [null, "olineid", "product", "partno", "listprice", "productdescription"]
     };
     public isBrowser = false;
     private isScroll = true;
     public drawerComponentDockedHeight = 300;
     public drawerComponentCurrentState = DrawerState.Top;
     private drawerComponentPreviousState = DrawerState.Docked;
     private errorMessageToDisplay = "No Records";
     // private obj_pfm144373:any = {};
     // private layoutId ;
     public childObjectsInfo = [];
     public eventsTriggeredList: Array < any > = [];
     private tableName_pfm144373 = "pfm144373";
     private id = "";
     private redirectUrl = "/";
     public showNavigationHistoryPopUp: Boolean = false;
     public navigationHistoryProperties = {
         'navigatedPagesNameArray': [],
         'navigatedPagesPathArray': [],
         'routerVisLinkTagName': "",
         'secondPreviousPage': "",
         'navigatedPagesLength': 0,
         'previousPage': "",
         'previousPageName': "",
         'secondPreviousPageName': "",
     };
     public layoutId = "60579";
     public isSkeletonLoading = true;
     private approverType: string = "";
     private currentStatusWorkFlowActionFieldId;

     public WorkFlowUserApprovalStatusDataObject = {};
     private workFlowMapping = {
         "289221": "orderstatus",
     }
     private cscomponentactionInfo = {};

     public orderstatus_289221_defaultStatusValue = "Draft";
     public orderstatus_289221_defaultStatus = {};
     private orderstatus_289221_status = {};
     public orderstatus_289221_swList = {
         "Draft": [{
             "statusLabel": "Draft",
             "statusValue": "Draft",
             "statusType": "Start",
             "statusWFConfigId": 267,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "In Review",
             "statusValue": "In Review",
             "statusType": "Initiate",
             "statusWFConfigId": 268,
             "isApproveInitiateEnabled": "Y",
             "isForwardEnabled": "N"
         }],
         "In Review": [{
             "statusLabel": "In Review",
             "statusValue": "In Review",
             "statusType": "Initiate",
             "statusWFConfigId": 268,
             "isApproveInitiateEnabled": "Y",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Approved",
             "statusValue": "Approved",
             "statusType": "Approved",
             "statusWFConfigId": 269,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Rejected",
             "statusValue": "Rejected",
             "statusType": "Reject",
             "statusWFConfigId": 270,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Approved": [{
             "statusLabel": "Approved",
             "statusValue": "Approved",
             "statusType": "Approved",
             "statusWFConfigId": 269,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Confirmed",
             "statusValue": "Confirmed",
             "statusType": "Manual",
             "statusWFConfigId": 271,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Rejected": [{
             "statusLabel": "Rejected",
             "statusValue": "Rejected",
             "statusType": "Reject",
             "statusWFConfigId": 270,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Confirmed": [{
             "statusLabel": "Confirmed",
             "statusValue": "Confirmed",
             "statusType": "Manual",
             "statusWFConfigId": 271,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Processed",
             "statusValue": "Processed",
             "statusType": "Manual",
             "statusWFConfigId": 272,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Processed": [{
             "statusLabel": "Processed",
             "statusValue": "Processed",
             "statusType": "Manual",
             "statusWFConfigId": 272,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Shipped",
             "statusValue": "Shipped",
             "statusType": "Manual",
             "statusWFConfigId": 273,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Shipped": [{
             "statusLabel": "Shipped",
             "statusValue": "Shipped",
             "statusType": "Manual",
             "statusWFConfigId": 273,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Delivered": [{
             "statusLabel": "Delivered",
             "statusValue": "Delivered",
             "statusType": "Manual",
             "statusWFConfigId": 274,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Completed",
             "statusValue": "Completed",
             "statusType": "Close",
             "statusWFConfigId": 276,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Cancelled": [{
             "statusLabel": "Cancelled",
             "statusValue": "Cancelled",
             "statusType": "Manual",
             "statusWFConfigId": 275,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Completed": [{
             "statusLabel": "Completed",
             "statusValue": "Completed",
             "statusType": "Close",
             "statusWFConfigId": 276,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }]
     };

     constructor(public router: Router, public activatRoute: ActivatedRoute,
         platform: Platform, public dataProvider: dataProvider,
         public appUtilityConfig: appUtility,
         public loadingCtrl: LoadingController,
         public toastCtrl: ToastController,
         public applicationRef: ApplicationRef,
         private screenOrientation: ScreenOrientation,
         public events: Events,
         public offlineDbIndexCreation: offlineDbIndexCreation,
         public metaDbConfigurationObj: metaDbConfiguration,
         public metaDbProvider: metaDataDbProvider, public cspfmexecutionPouchDbProvider: cspfmExecutionPouchDbProvider,
         public executionDbConfigObject: cspfmExecutionPouchDbConfiguration, private datePipe: DatePipe,
         public alerCtrl: AlertController) {
         this.activatRoute.queryParams.subscribe(params => {
             if (Object.keys(params).length == 0 && params.constructor === Object) {
                 console.log("list query params skipped");
                 return
             }
             if (params["redirectUrl"]) {
                 this.redirectUrl = params["redirectUrl"]
             }
             this.id = params["id"];
             this.fetchSelectedObject(this.id);
         });

         const windowHeight = window.innerHeight;
         this.drawerComponentDockedHeight = windowHeight / 2;
         if (!this.appUtilityConfig.isMobile || this.appUtilityConfig.osType === "android") {
             this.isBrowser = true
         }
         //Handle docker height in screen orientation
         this.screenOrientation.onChange().subscribe(() => {
             this.drawerComponentCurrentState = DrawerState.Bottom;
             const windowHeightVal = window.innerHeight;
             this.drawerComponentDockedHeight = windowHeightVal / 2;
         });

         //create event sudscribtion with the name published in list page
         this.appUtilityConfig.setEventSubscriptionlayoutIds(this.tableName_pfm144373, this.layoutId);
         this.events.subscribe(this.layoutId, (modified) => {
             if (modified["dataProvider"] == "PouchDB") {
                 this.stackModifiedEventTrigger(modified);
             }
         });
         for (let i = 0; i < this.childObjects.length; i++) {
             this.appUtilityConfig.setEventSubscriptionlayoutIds(this.childObjects[i], this.layoutId + '_' + this.childObjects[i])
             this.events.subscribe(this.layoutId + '_' + this.childObjects[i], (modified) => {
                 if (modified["dataProvider"] == "PouchDB") {
                     this.childObjectModifiedEventTrigger(modified, i);
                 }
             });
         }
     }
     public obj_pfm144373: any = {};
     private obj_pfm142973_289353: any = {};
     private obj_pfm147213_301933: any = {};
     private obj_pfm144393: any = {};
     public gridFieldInfo: {
         [key: string]: FieldInfo
     } = {
         "pfm147213_customercontact": {
             "child": {
                 "label": "",
                 "fieldName": "customercontact",
                 "prop": "customercontact",
                 "fieldType": "TEXT",
                 "child": "",
                 "dateFormat": "",
                 "mappingDetails": "",
                 "currencyDetails": ""
             },
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": "",
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.customermaster.customercontact",
             "prop": "pfm147213_301933.customercontact",
             "fieldName": "pfm147213_301933",
             "fieldType": "LOOKUP"
         },
         "pfm147213_customernumber": {
             "child": {
                 "label": "",
                 "fieldName": "customernumber",
                 "prop": "customernumber",
                 "fieldType": "TEXT",
                 "child": "",
                 "dateFormat": "",
                 "mappingDetails": "",
                 "currencyDetails": ""
             },
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": "",
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.customermaster.customernumber",
             "prop": "pfm147213_301933.customernumber",
             "fieldName": "pfm147213_301933",
             "fieldType": "LOOKUP"
         },
         "pfm144373_orderstatus": {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorders.orderstatus",
             "fieldName": "orderstatus",
             "prop": "orderstatus",
             "fieldType": "DROPDOWN",
             "child": "",
             "dateFormat": "",
             "mappingDetails": {
                 "Draft": "Draft",
                 "In Review": "In Review",
                 "Approved": "Approved",
                 "Rejected": "Rejected",
                 "Confirmed": "Confirmed",
                 "Processed": "Processed",
                 "Shipped": "Shipped",
                 "Delivered": "Delivered",
                 "Completed": "Completed",
                 "Cancelled": "Cancelled"
             },
             "currencyDetails": ""
         },
         "pfm147213_accountname": {
             "child": {
                 "label": "",
                 "fieldName": "accountname",
                 "prop": "accountname",
                 "fieldType": "TEXT",
                 "child": "",
                 "dateFormat": "",
                 "mappingDetails": "",
                 "currencyDetails": ""
             },
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": "",
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.customermaster.accountname",
             "prop": "pfm147213_301933.accountname",
             "fieldName": "pfm147213_301933",
             "fieldType": "LOOKUP"
         },
         "pfm144373_billtoaddress": {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorders.billtoaddress",
             "fieldName": "billtoaddress",
             "prop": "billtoaddress",
             "fieldType": "TEXTAREA",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm144373_customername": {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorders.customername",
             "prop": "pfm147213_301933.customername",
             "fieldName": "pfm147213_301933",
             "fieldType": "LOOKUP",
             "child": {
                 "child": "",
                 "dateFormat": "",
                 "mappingDetails": "",
                 "currencyDetails": "",
                 "label": "accountno",
                 "prop": "accountno",
                 "fieldName": "accountno",
                 "fieldType": "AUTONUMBER"
             },
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm144373_orderid": {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorders.orderid",
             "fieldName": "orderid",
             "prop": "orderid",
             "fieldType": "AUTONUMBER",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm144373_ordercompleteddate": {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorders.ordercompleteddate",
             "fieldName": "ordercompleteddate",
             "prop": "ordercompleteddate",
             "fieldType": "DATE",
             "child": "",
             "dateFormat": this.appUtilityConfig.userDateFormat,
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm144373_orderconfirmeddate": {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorders.orderconfirmeddate",
             "fieldName": "orderconfirmeddate",
             "prop": "orderconfirmeddate",
             "fieldType": "DATE",
             "child": "",
             "dateFormat": this.appUtilityConfig.userDateFormat,
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm144373_shiptoaddress": {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorders.shiptoaddress",
             "fieldName": "shiptoaddress",
             "prop": "shiptoaddress",
             "fieldType": "TEXTAREA",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         }
     };
     private childObjects = ['pfm144393'];
     public formulaObject = {};
     public sectionLayoutId = 'undefined';
     private viewFetchActionInfo: Array < any > = [];
     private objectNameMapping = {
         "pfm0s": "Address Details",
         "pfm147274s": "Contact Details",
         "pfm144393s": "Sale Order lines"
     };
     private objectRelationshipMapping = {
         "pfm144393": "one_to_many"
     };
     private objectHierarchyJSON = {
         "objectName": "crmsaleorders",
         "objectType": "PRIMARY",
         "relationShipType": null,
         "fieldId": "0",
         "objectId": "144373",
         "childObject": [{
             "objectName": "customermaster",
             "objectType": "LOOKUP",
             "relationShipType": "LOOKUP",
             "fieldId": "301933",
             "objectId": "147213",
             "childObject": []
         }, {
             "objectName": "crmsaleorderlines",
             "objectType": "MASTERDETAIL",
             "relationShipType": "one_to_many",
             "fieldId": "301393",
             "objectId": "144393",
             "childObject": [{
                 "objectName": "crmproduct",
                 "objectType": "LOOKUP",
                 "relationShipType": "LOOKUP",
                 "fieldId": "289353",
                 "objectId": "142973",
                 "childObject": []
             }]
         }]
     };
     public layoutDataRestrictionSet = [];
     private sectionObjectsHierarchy = {
         "pfm144393": {
             "objectName": "crmsaleorderlines",
             "objectType": "MASTERDETAIL",
             "relationShipType": "one_to_many",
             "fieldId": "301393",
             "objectId": "144393",
             "childObject": [{
                 "objectName": "crmproduct",
                 "objectType": "LOOKUP",
                 "relationShipType": "LOOKUP",
                 "fieldId": "289353",
                 "objectId": "142973",
                 "childObject": []
             }]
         }
     };
     public gridTableFieldInfoArray: {
         [key: string]: Array < FieldInfo >
     } = {
         "pfm144393": [{
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorderlines.olineid",
             "fieldName": "olineid",
             "prop": "olineid",
             "fieldType": "AUTONUMBER",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         }, {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorderlines.product",
             "prop": "pfm142973_289353.product",
             "fieldName": "pfm142973_289353",
             "fieldType": "LOOKUP",
             "child": {
                 "child": "",
                 "dateFormat": "",
                 "mappingDetails": "",
                 "currencyDetails": "",
                 "label": "name",
                 "prop": "name",
                 "fieldName": "name",
                 "fieldType": "AUTONUMBER"
             },
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         }, {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorderlines.partno",
             "fieldName": "partno",
             "prop": "partno",
             "fieldType": "TEXT",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         }, {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorderlines.listprice",
             "fieldName": "listprice",
             "prop": "listprice",
             "fieldType": "CURRENCY",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": {
                 "currencyCode": "₹",
                 "display": true,
                 "digitsInfo": "1.2-2",
                 "locale": "en-IN"
             }
         }, {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorderlines.productdescription",
             "fieldName": "productdescription",
             "prop": "productdescription",
             "fieldType": "TEXTAREA",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         }]
     };
     public sectionalFetchMapping = {};
     public fetchActionInfo: any = {};
     public layoutDBName = 'Not Web Service';
     public formulaReverseObjectHierarchyJSON = undefined;
     public formulaConfigJSON = {};
     public formulafields = {};
     public dbServiceProvider = appConstant.pouchDBStaticName;
     private orderstatus_289221 = {
         'Draft': 'Draft',
         'In Review': 'In Review',
         'Approved': 'Approved',
         'Rejected': 'Rejected',
         'Confirmed': 'Confirmed',
         'Processed': 'Processed',
         'Shipped': 'Shipped',
         'Delivered': 'Delivered',
         'Completed': 'Completed',
         'Cancelled': 'Cancelled'
     };
     async fetchSelectedObject(id) {
         const additionalObjectdata = {};
         additionalObjectdata["id"] = this.id;
         additionalObjectdata["isFirstLevelFetchNeed"] = true;
         const fetchParams = {
             'objectHierarchyJSON': this.objectHierarchyJSON,
             'additionalInfo': additionalObjectdata,
             'dataSource': appConstant.pouchDBStaticName,
         }
         var taskList = [];
         this.childObjectsInfo = [];
         taskList.push(this.dataProvider.querySingleDoc(fetchParams).then(result => {
             console.log("Response :", JSON.parse(JSON.stringify(result)));


             if (result["status"] !== "SUCCESS") {
                 this.errorMessageToDisplay = result["message"];
                 if (this.errorMessageToDisplay === "No internet") {
                     this.presentNoInternetToast();
                 }
                 return;
             }

             this.obj_pfm144373 = result["records"][0];
             if (this.obj_pfm144373["orderstatus"]) {
                 this.orderstatus_289221_defaultStatus = this.orderstatus_289221_swList[this.obj_pfm144373["orderstatus"]].filter(item => {
                     return item['statusValue'] == this.obj_pfm144373["orderstatus"]
                 })[0]
             } else {
                 this.orderstatus_289221_defaultStatus = this.orderstatus_289221_swList[this.orderstatus_289221_defaultStatusValue][0];
             }

             if (this.obj_pfm144373 != undefined && this.obj_pfm144373[this.dataProvider.getPluralName('pfm144393')] && this.obj_pfm144373[this.dataProvider.getPluralName('pfm144393')].length > 0) {
                 this.obj_pfm144393 = this.obj_pfm144373[this.dataProvider.getPluralName('pfm144393')][0];
             }
             this.obj_pfm147213_301933 = this.obj_pfm144373['pfm147213_301933'] ? this.obj_pfm144373['pfm147213_301933'] : "";

             this.applicationRef.tick();

             return result;
         }).catch(error => {
             this.isSkeletonLoading = false
             console.log(error);
         }));

         this.childObjects.forEach(childObject => {
             var sectionChildObjectHierarchy = this.sectionObjectsHierarchy[childObject]
             const sectionChildFetchParams = {
                 'objectHierarchyJSON': sectionChildObjectHierarchy,
                 'additionalInfo': additionalObjectdata,
                 'dataSource': appConstant.pouchDBStaticName,
                 'layoutDataRestrictionSet': this.layoutDataRestrictionSet
             }
             taskList.push(this.dataProvider.getChildCount(sectionChildFetchParams).then(childResponse => {
                 if (childResponse["status"] !== "SUCCESS") {
                     this.errorMessageToDisplay = childResponse["message"];
                     if (this.errorMessageToDisplay === "No internet") {
                         this.presentNoInternetToast();
                     }
                     return;
                 }
                 const pluralName = this.dataProvider.getPluralName(childObject);
                 const obj = {
                     displayName: this.objectNameMapping[pluralName],
                     objectName: childObject,
                     childDocsArray: childResponse['records'],
                     relationshipType: this.objectRelationshipMapping[childObject]
                 };
                 // this.addChildHierarchyJson(childObject, obj);
                 this.childObjectsInfo.push(obj);
                 // console.log("Object :", this.objectNameMapping[pluralName], obj);

                 return childResponse;
             }))
         })
         Promise.all(taskList).then(res => {
             console.log("All res :", res);
             this.isSkeletonLoading = false
         })
     }

     async presentNoInternetToast() {
         const toast = await this.toastCtrl.create({
             message: "No internet connection. Please check your internet connection and try again.",
             showCloseButton: true,
             closeButtonText: "retry"
         });
         toast.onDidDismiss().then(() => {
             toast.dismiss();
             this.refreshData();
         });
         toast.present();
     }

     refreshData() {
         this.fetchSelectedObject(this.id);
     }


     historyButtonAction(event: Event) {
         this.showNavigationHistoryPopUp = !this.showNavigationHistoryPopUp;
         event.stopPropagation();
     }
     @HostListener('click') onClick() {
         this.showNavigationHistoryPopUp = false;
     }
     getNavigationHistory() {
         if (this.appUtilityConfig.getHomePageNode()) {
             let homePageNode = this.appUtilityConfig.getHomePageNode();
             this.navigationHistoryProperties['navigatedPagesNameArray'] = [homePageNode['homePageNodeName']];
             this.navigationHistoryProperties['navigatedPagesPathArray'] = [homePageNode['homePageNodepath']];
         }
         this.navigationHistoryProperties['navigatedPagesLength'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes.length;
         this.navigationHistoryProperties['previousPageName'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 2].childNodes[0].textContent;
         this.navigationHistoryProperties['previousPage'] = ( < HTMLElement > document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 2]).tagName.toLowerCase();
         if (document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3]) {
             this.navigationHistoryProperties['secondPreviousPageName'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3].childNodes[0].textContent;
             this.navigationHistoryProperties['secondPreviousPage'] = ( < HTMLElement > document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3]).tagName.toLowerCase();
         }
         for (let i = 0; i < this.navigationHistoryProperties['navigatedPagesLength'] - 3; i++) {
             this.navigationHistoryProperties['navigatedPagesNameArray'].push(document.getElementsByTagName('ion-router-outlet')[1].childNodes[i].childNodes[0].textContent);
             this.navigationHistoryProperties['navigatedPagesPathArray'].push(document.getElementsByTagName('ion-router-outlet')[1].children[i].tagName.toLowerCase());
         }
     }
     histListNav(e) {
         if (e.currentTarget.getAttribute('data-index') == 'app-homepage') {
             this.appUtilityConfig.navigateToHomepage();
         } else {
             this.router.navigate([`/menu/${e.currentTarget.getAttribute('data-index')}`]);
         }
     }
     previousPageNavigation() {
         this.router.navigate(["/menu/" + this.navigationHistoryProperties['previousPage']]);
     }
     secondPreviousPageNavigation() {
         this.router.navigate(["/menu/" + this.navigationHistoryProperties['secondPreviousPage']]);
     }
     ngOnInit() {
         this.getNavigationHistory()
     }

     async fetchModifiedRec(modifiedData) {

         let additionalObjectdata = {};

         additionalObjectdata['id'] = modifiedData

         const fetchParams = {
             'objectHierarchyJSON': this.objectHierarchyJSON,
             'dataSource': appConstant.pouchDBStaticName,
             'additionalInfo': additionalObjectdata
         }

         this.dataProvider.fetchDataFromDataSource(fetchParams).then(result => {

             if (result['status'] != 'SUCCESS') {
                 this.errorMessageToDisplay = result['message'];
                 return
             }
             this.obj_pfm144373 = result['records'][0];
             // Handling Lookup
             this.obj_pfm147213_301933 = this.obj_pfm144373['pfm147213_301933'] ? this.obj_pfm144373['pfm147213_301933'] : "";
             if (this.obj_pfm144373["orderstatus"]) {
                 this.orderstatus_289221_defaultStatus = this.orderstatus_289221_swList[this.obj_pfm144373["orderstatus"]].filter(item => {
                     return item['statusValue'] == this.obj_pfm144373["orderstatus"]
                 })[0]
             } else {
                 this.orderstatus_289221_defaultStatus = this.orderstatus_289221_swList[this.orderstatus_289221_defaultStatusValue][0];
             }
         }).catch(error => {
             console.log(error)
         });
     }

     addChildHierarchyJson(childObjectName, object) {
         for (let i = 0; i < this.objectHierarchyJSON.childObject.length; i++) {
             const childHierarchyJson = this.objectHierarchyJSON.childObject[i];
             if ("pfm" + childHierarchyJson.objectId == childObjectName) {
                 object['childHierarchyJson'] = childHierarchyJson;
             }
         }
     }

     navigateObjectBaseEntryPage(selectedObjectName, addNavigationParams, options) {}


     onItemTap(selectedRecord) {
         let prominientObject = this.prominetDataMapping[selectedRecord.objectName];
         let optionObj = {
             animate: false
         }
         let childDocsArray = selectedRecord['childDocsArray'];
         if (selectedRecord['relationshipType'] == 'one_to_one' &&
             childDocsArray != undefined &&
             childDocsArray.length > 0) {
             let itemtapnavigationObj = {
                 //     selectedObjectParent: this.obj_pfm144373,
                 //     selectedObject: childDocsArray[0],
                 //     prominientObjectInfo: prominientObject,                                             
                 //     objectNameParent:this.tableName_pfm144373,
                 // parentTitle: this.obj_pfm144373['displayName'],
                 // headerDocId: this.obj_pfm144373['id'],
                 // parentFieldLabel:"orderid",
                 // parentFieldValue:"orderid"
                 parentObj: JSON.stringify(this.obj_pfm144373),
                 parentFieldLabel: "Order ID",
                 parentTitle: this.objectNameMapping['pfm0s'],
                 parentFieldValue: this.obj_pfm144373['orderid'],
                 id: childDocsArray[0]['id'].split("_")[2],
                 prominientObjectInfo: prominientObject
             };
             this.navigateObjectBaseDetailPage(selectedRecord['objectName'], itemtapnavigationObj, optionObj)
         } else if (selectedRecord['relationshipType'] == 'one_to_one' && childDocsArray.length == 0) {
             let itemtapnavigationObj = {
                 action: 'Add',
                 parentId: this.obj_pfm144373['id'],
                 parentFieldLabel: "Order ID",
                 parentFieldValue: this.obj_pfm144373['orderid'],
                 parentName: this.tableName_pfm144373,
                 parentPage: this,
                 parentTitle: this.objectNameMapping['pfm0s'],
                 parentObj: JSON.stringify(this.obj_pfm144373)
             }
             this.navigateObjectBaseEntryPage(selectedRecord['objectName'], itemtapnavigationObj, optionObj)
         } else {
             selectedRecord['parentTitle'] = this.objectNameMapping['pfm0s'];
             let itemtapnavigationObj = {
                 // selectedObjectParent: this.obj_pfm144373,
                 // selectedObject: selectedRecord,
                 // prominientObjectInfo: prominientObject,                                             
                 // objectNameParent:this.tableName_pfm144373,
                 // headerFieldName:"orderid",
                 // headerFieldTranslateKey:"Order ID",
                 // childObjectHierarchyJSON: selectedRecord.childHierarchyJson
                 parentObj: JSON.stringify(this.obj_pfm144373),
                 parentObjType: this.tableName_pfm144373,
                 parentName: this.tableName_pfm144373,
                 parentLabel: this.objectNameMapping['pfm0s'],
                 parentFieldLabel: "Order ID",
                 parentFieldValue: this.obj_pfm144373['orderid'],
                 childObjectHierarchyJSON: JSON.stringify(this.sectionObjectsHierarchy[selectedRecord['objectName']]),
                 parentHierarchyJSON: JSON.stringify(this.objectHierarchyJSON),
                 objLabel: selectedRecord['displayName'],
                 objType: selectedRecord['objectName'],
                 prominientObjectInfo: prominientObject,
                 gridInfoArray: JSON.stringify(this.gridTableFieldInfoArray[selectedRecord["objectName"]])
             };
             this.toastCtrl.dismiss();
             if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/Saleorderview_MOBILE_Grid_with_Listpreview")) {
                 itemtapnavigationObj['redirectUrl'] = "/menu/Saleorderview_MOBILE_Grid_with_List"
             }
             this.router.navigate(["/menu/Saleorderview_MOBILE_Grid_with_Listpreview"], {
                 queryParams: itemtapnavigationObj,
                 skipLocationChange: true
             })
         }
     }
     navigateObjectBaseDetailPage(selectedObjectName, itemTapNavigationParams, options) {}


     ionViewDidLoad() {
         //    this.navBar.backButtonClick = (e:UIEvent)=>{
         //       let opts = {animate:false};
         //       this.navCtrl.pop(opts);
         //     };                        
     };

     ionViewWillEnter() {
         document.body.setAttribute('class', 'linedetail');
         this.drawerComponentCurrentState = DrawerState.Docked;
         this.drawerComponentPreviousState = DrawerState.Bottom;
         for (let i = 0; i < document.getElementsByClassName('cs-divider').length; i++) {
             document.getElementsByClassName('cs-divider')[i].classList.remove('bottom');
             document.getElementsByClassName('cs-divider')[i].classList.remove('top');
             document.getElementsByClassName('cs-divider')[i].classList.add('center');
         }

     };

     ionViewWillLeave() {
         this.drawerComponentCurrentState = DrawerState.Bottom;
     };

     ngOnDestroy() {
         this.events.unsubscribe(this.layoutId);
         for (let i = 0; i < this.childObjects.length; i++) {
             this.events.unsubscribe(this.layoutId + '_' + this.childObjects[i]);
             this.appUtilityConfig.removeEventSubscriptionlayoutIds(this.childObjects[i], this.layoutId + '_' + this.childObjects[i])
         }
         this.appUtilityConfig.removeEventSubscriptionlayoutIds(this.tableName_pfm144373, this.layoutId);
     }

     //event method for parent object
     //object name should be come infront of the Modified 
     stackModifiedEventTrigger(modified) {
         const modifiedData = this.dataProvider.convertRelDocToNormalDoc(modified);
         if (modifiedData.id == this.obj_pfm144373['id']) {
             //data need to modify
             this.fetchModifiedRec(modifiedData.id);
         }
     }

     //event method for parent object 
     childObjectModifiedEventTrigger(modified, index) {
         const modifiedData = this.dataProvider.convertRelDocToNormalDoc(modified);
         if (modifiedData.pfm144373 == this.obj_pfm144373['id']) {
             modifiedData['id'] = modifiedData['type'] + '_2_' + modifiedData['id']

             console.log('modified data', modifiedData, this.childObjects[index])
             let dictVal = this.childObjectsInfo[index];
             let arrayValue = dictVal.childDocsArray;
             let eventsTriggeredindex = this.getChangedObjectIndex(arrayValue, modifiedData, 'id');
             if (eventsTriggeredindex > -1) {
                 arrayValue.splice(eventsTriggeredindex, 1);
             }
             arrayValue.push(modifiedData);
             dictVal.childDocsArray = arrayValue;
             this.childObjectsInfo[index] = dictVal;
         }
     }

     getChangedObjectIndex(array, modifiedData, key) {
         let str = modifiedData['type'] + '_2_' //getting the pfm_2_
         return lodash.findIndex(array, item => {
             if (!item['id'].includes(str)) { //checking if the item['id'] has pfm_2_.
                 item['id'] = str + item['id'] //if not we are adding pfm_2_ to the item['id']
             }
             return item[key] === modifiedData[key];
         }, 0);
     }

     dockerButtonOnclick() {
         if (this.drawerComponentCurrentState == DrawerState.Bottom) {
             this.drawerComponentCurrentState = DrawerState.Docked
             this.drawerComponentPreviousState = DrawerState.Bottom
             // for (let i = 0; i < document.getElementsByClassName('cs-divider').length; i++) {
             //     document.getElementsByClassName('cs-divider')[i].classList.remove('bottom');
             //     document.getElementsByClassName('cs-divider')[i].classList.remove('top');
             //     document.getElementsByClassName('cs-divider')[i].classList.add('center');
             //   }

         } else if (this.drawerComponentCurrentState == DrawerState.Docked) {
             if (this.drawerComponentPreviousState == DrawerState.Bottom) {
                 this.drawerComponentCurrentState = DrawerState.Top
                 // for (let i = 0; i < document.getElementsByClassName('cs-divider').length; i++) {
                 //     document.getElementsByClassName('cs-divider')[i].classList.remove('bottom');
                 //     document.getElementsByClassName('cs-divider')[i].classList.remove('center');
                 //     document.getElementsByClassName('cs-divider')[i].classList.add('top');
                 //   }


             } else {
                 this.drawerComponentCurrentState = DrawerState.Bottom
                 // for (let i = 0; i < document.getElementsByClassName('cs-divider').length; i++) {
                 //     document.getElementsByClassName('cs-divider')[i].classList.remove('center');
                 //     document.getElementsByClassName('cs-divider')[i].classList.remove('top');
                 //     document.getElementsByClassName('cs-divider')[i].classList.add('bottom');
                 //   }


             }
             this.drawerComponentPreviousState = DrawerState.Docked;
         } else if (this.drawerComponentCurrentState == DrawerState.Top) {
             this.drawerComponentCurrentState = DrawerState.Docked
             this.drawerComponentPreviousState = DrawerState.Top
             // for (let i = 0; i < document.getElementsByClassName('cs-divider').length; i++) {
             //     document.getElementsByClassName('cs-divider')[i].classList.remove('bottom');
             //     document.getElementsByClassName('cs-divider')[i].classList.remove('top');
             //     document.getElementsByClassName('cs-divider')[i].classList.add('center');
             //   }


         }
     }
     backButtonOnclick() {
         this.router.navigate([this.redirectUrl], {
             skipLocationChange: true
         });
     };
     editButton_elementId_Onclick() {
         if (this.approverType == "approver") {
             this.showAlert(this.workFlowMapping[this.obj_pfm144373['systemAttributes']['fieldId']] + " process initiated can't edit at this time")
             return;
         } else if (this.approverType == "non approver") {
             this.fetchLockedUserDetail()
             return
         }
         let navigationParameters = {
             action: 'Edit',
             id: this.obj_pfm144373['id'],
             parentPage: this
         }
         if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/NewSalesorder_MOBILE_Grid")) {
             navigationParameters['redirectUrl'] = "/menu/Saleorderview_MOBILE_Grid_with_List"
         }
         this.router.navigate(["/menu/NewSalesorder_MOBILE_Grid"], {
             queryParams: navigationParameters,
             skipLocationChange: true
         })
     }

     fetchLockedUserDetail() {
         var systemAttributes = this.obj_pfm144373['systemAttributes']
         var userId = systemAttributes['lockedBy']
         var date = new Date(systemAttributes['lockedDate']);
         this.showAlert(userId + " has locked for " + this.workFlowMapping[this.obj_pfm144373['systemAttributes']['fieldId']] + " on " + this.datePipe.transform(date, this.appUtilityConfig.userDateFormat));
     }

     statusChange(event, selectedStatusField) {
         if (selectedStatusField == undefined) {
             selectedStatusField = {}
         }
         selectedStatusField['statusLabel'] = event['selectedStatus']['statusLabel'];
         selectedStatusField['statusValue'] = event['selectedStatus']['statusValue'];
         selectedStatusField['statusType'] = event['selectedStatus']['statusType'];
         this.approveAction(selectedStatusField, event['workFlowUserApprovalStatusDataObject'])
     }

     getApprovalState(event) {
         this.approverType = event['approverType']
     }

     approveAction(selectedStatusField, workFlowUserApprovalStatusDataObject) {
         this.WorkFlowUserApprovalStatusDataObject = workFlowUserApprovalStatusDataObject;
         this.WorkFlowUserApprovalStatusDataObject['lastmodifiedby'] = this.appUtilityConfig.userId
         var userObjectList = this.WorkFlowUserApprovalStatusDataObject['approvalStatus'].filter(userDataObject => userDataObject.userId == this.appUtilityConfig.userId);
         var userObject = userObjectList[0]
         userObject['userName'] = this.appUtilityConfig.loggedUserName
         userObject['userLevel'] = ""
         userObject['statusValue'] = selectedStatusField['statusValue']
         userObject['statusType'] = selectedStatusField['statusType']
         userObject['approvalExecutionStatus'] = "INPROGRESS"
         userObject['execStatusMessage'] = ""
         userObject['comment'] = ""
         this.cspfmexecutionPouchDbProvider.save(this.executionDbConfigObject.workFlowUserApprovalStatusObject,
             this.WorkFlowUserApprovalStatusDataObject).then(result => {

             if (result['status'] != 'SUCCESS') {
                 alert("failed")

                 return;
             }
             this.presentToast("data saved sucessfully")
         })
     }
     async showAlert(messageContent) {

         var customAlert = await this.alerCtrl.create({
             backdropDismiss: false,
             message: messageContent,
             buttons: [

                 {
                     text: "OK",
                     cssClass: "method-color",
                     handler: () => {

                     }
                 }
             ]
         });
         customAlert.present();

     };
     async presentToast(message) { //ch
         const toast = await this.toastCtrl.create({
             message: message,
             duration: 2000,
             position: 'bottom'
         });
         toast.onDidDismiss().then((res) => {
             toast.dismiss()
         });
         toast.present();
     };
 }