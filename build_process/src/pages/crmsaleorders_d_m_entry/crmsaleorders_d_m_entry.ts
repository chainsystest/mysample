 import {
     Component,
     ViewChild,
     OnInit,
     ApplicationRef,
     ChangeDetectorRef,
     HostListener,
     NgZone
 } from '@angular/core';
 import {
     dbProvider
 } from 'src/core/db/dbProvider';
 import {
     offlineDbIndexCreation
 } from 'src/core/utils/offlineDbIndexCreation';
 import {
     dataProvider
 } from 'src/core/utils/dataProvider';
 import {
     appConstant
 } from 'src/core/utils/appConstant';

 import * as lodash from 'lodash';
 import {
     ToastController,
     ModalController,
     AlertController,
     Platform,
     IonContent,
     LoadingController
 } from '@ionic/angular';
 import {
     lookuppage
 } from 'src/core/pages/lookuppage/lookuppage';
 import {
     FormBuilder,
     Validators,
     FormGroup
 } from '@angular/forms';
 import {
     appUtility
 } from 'src/core/utils/appUtility';
 import * as moment from 'moment';
 import 'moment-timezone';
 import {
     Router,
     ActivatedRoute
 } from '@angular/router';
 import {
     lookupFieldMapping
 } from 'src/core/pfmmapping/lookupFieldMapping';
 import {
     objectTableMapping
 } from 'src/core/pfmmapping/objectTableMapping';
 import * as _ from 'underscore';
 import * as Quill from 'quill';
 import {
     cspfmFieldTrackingMapping
 } from 'src/core/pfmmapping/cspfmFieldTrackingMapping';
 import {
     Http,
     Headers
 } from '@angular/http';
 import {
     dbConfiguration
 } from 'src/core/db/dbConfiguration';
 @Component({
     selector: 'crmsaleorders_d_m_entry',
     templateUrl: './crmsaleorders_d_m_entry.html'
 }) export class crmsaleorders_d_m_entry implements OnInit {
     @ViewChild(IonContent) contentArea: IonContent;
     private obj_pfm144373: any = {};
     public ionDateTimeDisplayValue = {};
     private obj_pfm144373_Temp: any = {};
     public formGroup: FormGroup;
     private customAlert;
     public action = 'Add';
     private id: any = {};
     private tableName_pfm144373 = 'pfm144373';
     public isSaveActionTriggered: Boolean = false;
     private isValidFrom: Boolean = true;
     public dbServiceProvider = appConstant.pouchDBStaticName;
     public selectedDataObject: any = {};
     public formulaObject = {};
     public isFieldTrackingEnable: Boolean = false;
     private showAlert: boolean = false;
     infoMessage = '';
     aniClassAddorRemove: boolean = false;
     private workFlowInitiateList = {};
     public savedSuccessMessage = 'data saved sucessfully';
     public updatedSuccessMessage = 'data updated sucessfully';
     public savedErrorMessage = 'Error saving record';
     public fetchErrorMessage = 'Fetching Failed';
     private dependentNumberCount = {};
     public partiallySavedSuccessMessage = 'data partially saved';
     private backButtonSubscribeRef;
     private unRegisterBackButtonAction: Function;
     public parentObjLabel = '';
     public parentObjValue = '';
     private errorMessageToDisplay: string = 'No Records';
     private parentName = '';
     private parentId = '';
     public isParentObjectShow = false;
     public isSkeletonLoading = true;
     isViewRunning = false;
     loading;
     private objectHierarchyJSON = {
         "objectName": "crmsaleorders",
         "objectType": "PRIMARY",
         "relationShipType": "",
         "fieldId": "0",
         "objectId": "144373",
         "childObject": [{
             "objectName": "customermaster",
             "objectType": "LOOKUP",
             "relationShipType": "",
             "fieldId": "301933",
             "objectId": "147213",
             "childObject": []
         }]
     };
     public isFromMenu = false;
     private redirectUrl = '/';
     private dependentFieldTriggerList = {};
     private geoLocationdependentField = {};
     public pickListValues = {};
     public parentObject: any = {};
     private obj_pfm147213_301933: any = {};
     private pfm147213_301933_searchKey;
     public orderstatus_289221_defaultStatusValue = "Draft";
     public orderstatus_289221_defaultStatus = {};
     private orderstatus_289221_status = {};
     public orderstatus_289221_swList = {
         "Draft": [{
             "statusLabel": "Draft",
             "statusValue": "Draft",
             "statusType": "Start",
             "statusWFConfigId": 267,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "In Review",
             "statusValue": "In Review",
             "statusType": "Initiate",
             "statusWFConfigId": 268,
             "isApproveInitiateEnabled": "Y",
             "isForwardEnabled": "N"
         }],
         "In Review": [{
             "statusLabel": "In Review",
             "statusValue": "In Review",
             "statusType": "Initiate",
             "statusWFConfigId": 268,
             "isApproveInitiateEnabled": "Y",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Approved",
             "statusValue": "Approved",
             "statusType": "Approved",
             "statusWFConfigId": 269,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Rejected",
             "statusValue": "Rejected",
             "statusType": "Reject",
             "statusWFConfigId": 270,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Approved": [{
             "statusLabel": "Approved",
             "statusValue": "Approved",
             "statusType": "Approved",
             "statusWFConfigId": 269,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Confirmed",
             "statusValue": "Confirmed",
             "statusType": "Manual",
             "statusWFConfigId": 271,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Rejected": [{
             "statusLabel": "Rejected",
             "statusValue": "Rejected",
             "statusType": "Reject",
             "statusWFConfigId": 270,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Confirmed": [{
             "statusLabel": "Confirmed",
             "statusValue": "Confirmed",
             "statusType": "Manual",
             "statusWFConfigId": 271,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Processed",
             "statusValue": "Processed",
             "statusType": "Manual",
             "statusWFConfigId": 272,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Processed": [{
             "statusLabel": "Processed",
             "statusValue": "Processed",
             "statusType": "Manual",
             "statusWFConfigId": 272,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Shipped",
             "statusValue": "Shipped",
             "statusType": "Manual",
             "statusWFConfigId": 273,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Shipped": [{
             "statusLabel": "Shipped",
             "statusValue": "Shipped",
             "statusType": "Manual",
             "statusWFConfigId": 273,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Delivered": [{
             "statusLabel": "Delivered",
             "statusValue": "Delivered",
             "statusType": "Manual",
             "statusWFConfigId": 274,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Completed",
             "statusValue": "Completed",
             "statusType": "Close",
             "statusWFConfigId": 276,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Cancelled": [{
             "statusLabel": "Cancelled",
             "statusValue": "Cancelled",
             "statusType": "Manual",
             "statusWFConfigId": 275,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Completed": [{
             "statusLabel": "Completed",
             "statusValue": "Completed",
             "statusType": "Close",
             "statusWFConfigId": 276,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }]
     };
     public formulaConfigJSON = {};
     public formulafields = {};
     childObjectList = [];
     objResultMap = new Map < string, any > ();
     objDisplayName = {
         'pfm144373': 'crmsaleorders',
     };
     recordEntryValidation() {
         const formGroupObjectValue = Object.assign({}, this.formGroup.value);
         this.obj_pfm144373 = Object.assign(this.obj_pfm144373, formGroupObjectValue.pfm144373);
         if (
             this.obj_pfm144373["customercontact"] || this.obj_pfm144373["billtoaddress"] || this.obj_pfm144373["shiptoaddress"] || this.obj_pfm144373["ordercompleteddate"] || this.obj_pfm144373["customernumber"] || this.obj_pfm144373["orderstatus"] || this.obj_pfm144373["accountname"] || this.obj_pfm144373["orderid"] || this.obj_pfm144373["customername"] || this.obj_pfm144373["orderconfirmeddate"] || this.obj_pfm144373["orderconfirmeddate"]) {
             return true;
         } else {
             return false;
         }
     }
     constructor(public loadingCtrl: LoadingController, public dbConfiguration: dbConfiguration, public http: Http, public router: Router, public activatRoute: ActivatedRoute, public applicationRef: ApplicationRef, public alerCtrl: AlertController, public modalCtrl: ModalController, public dbService: dbProvider, public dataProvider: dataProvider, public formBuilder: FormBuilder, public appUtility: appUtility, public objectTableMapping: objectTableMapping, public lookupFieldMapping: lookupFieldMapping, private toastCtrl: ToastController, public platform: Platform, public fieldTrackMapping: cspfmFieldTrackingMapping, private ngZone: NgZone) {
         this.activatRoute.queryParams.subscribe(params => {
             if (Object.keys(params).length == 0 && params.constructor === Object) {
                 console.log("list query params skipped");
                 return
             }
             if (params["isFromMenu"]) {
                 this.isFromMenu = params["isFromMenu"];
             }
             if (params["redirectUrl"]) {
                 this.redirectUrl = params["redirectUrl"]
             }
             this.action = params['action'];
             this.parentId = params['parentId'];
             this.id = params['id'];
             this.parentName = params['parentName'];
             if (this.parentId) {
                 this.isParentObjectShow = true;
                 this.parentObjLabel = params['parentFieldLabel'];
                 this.parentObjValue = params['parentFieldValue'];
             }
             this.initializeObjects(dataProvider.tableStructure());
             this.orderstatus_289221_defaultStatus = this.orderstatus_289221_swList[this.orderstatus_289221_defaultStatusValue].filter(item => {
                 return item['statusValue'] == this.orderstatus_289221_defaultStatusValue
             })[0];
             if (this.action == 'Edit') {
                 this.id = params['id'];
                 this.fetchRecordAgainstSelectedObject();
             } else {
                 this.isSkeletonLoading = false;
                 this.checkboxInitialization();
             }
         });
         this.createFormGroup();
         this.hardWareBackButtonAction();
     }
     initializeObjects(tableStructure) {
         this.obj_pfm144373 = JSON.parse(JSON.stringify(tableStructure.pfm144373));
         this.obj_pfm147213_301933 = JSON.parse(JSON.stringify(tableStructure.pfm147213));
     }
     fetchRecordAgainstSelectedObject() {
         const additionalObjectdata = {};
         additionalObjectdata['id'] = this.id;
         const fetchParams = {
             'objectHierarchyJSON': this.objectHierarchyJSON,
             'additionalInfo': additionalObjectdata,
             'dataSource': appConstant.pouchDBStaticName
         };
         this.dataProvider.querySingleDoc(fetchParams).then(res => {
             this.isSkeletonLoading = false;
             if (res['status'] !== 'SUCCESS') {
                 this.errorMessageToDisplay = res['message'];
                 if (this.errorMessageToDisplay == 'No internet') {
                     this.presentNoInternetToast();
                 }
                 return;
             }
             if (res['records'].length < 0) {
                 console.log('FetchRecordAgainstSelectedObject No Records');
                 return;
             }
             let dataObj = res['records'][0];
             this.obj_pfm144373_Temp = lodash.extend({}, this.obj_pfm144373, dataObj);
             this.selectedDataObject['pfm144373'] = JSON.stringify(this.obj_pfm144373_Temp);
             this.obj_pfm144373 = lodash.extend({}, this.obj_pfm144373, dataObj);
             this.obj_pfm147213_301933 = this.obj_pfm144373['pfm147213_301933'] ? this.obj_pfm144373['pfm147213_301933'] : "";
             if (this.obj_pfm144373['ordercompleteddate'] !== null) {
                 this.obj_pfm144373.ordercompleteddate = moment(new Date(this.obj_pfm144373.ordercompleteddate)).tz(this.appUtility.userTimeZone).format();
                 this.ionDateTimeDisplayValue['obj_pfm144373_ordercompleteddate'] = moment(new Date(this.obj_pfm144373.ordercompleteddate)).tz(this.appUtility.userTimeZone).format('llll');
             }
             if (this.obj_pfm144373['orderconfirmeddate'] !== null) {
                 this.obj_pfm144373.orderconfirmeddate = moment(new Date(this.obj_pfm144373.orderconfirmeddate)).tz(this.appUtility.userTimeZone).format();
                 this.ionDateTimeDisplayValue['obj_pfm144373_orderconfirmeddate'] = moment(new Date(this.obj_pfm144373.orderconfirmeddate)).tz(this.appUtility.userTimeZone).format('llll');
             }
             if (this.obj_pfm144373["orderstatus"]) {
                 this.orderstatus_289221_defaultStatus = this.orderstatus_289221_swList[this.obj_pfm144373["orderstatus"]].filter(item => {
                     return item['statusValue'] == this.obj_pfm144373["orderstatus"]
                 })[0]
             }
             this.formGroup.patchValue({
                 pfm144373: this.obj_pfm144373
             });
             this.applicationRef.tick();
         }).catch(error => {
             this.isSkeletonLoading = false;
             this.showInfoAlert(this.fetchErrorMessage)
         })
     }
     updateGeoLocationFlag(objectName, dataObj, existingDataObj) {
         Object.keys(this.geoLocationdependentField[objectName]).forEach(element => {
             const depFlds = this.geoLocationdependentField[objectName][element]['dependentFields'];
             depFlds.forEach(fieldName => {
                 if (this.action == 'Edit') {
                     if (existingDataObj[fieldName] != dataObj[fieldName]) {
                         dataObj[this.geoLocationdependentField[objectName][element]['dependentFieldName']] = 'Y';
                         return;
                     }
                 } else {
                     if (dataObj[fieldName] != '') {
                         dataObj[this.geoLocationdependentField[objectName][element]['dependentFieldName']] = 'Y';
                         return;
                     }
                 }
             })
         });
     }
     async presentNoInternetToast() {
         const toast = await this.toastCtrl.create({
             message: "No internet connection. Please check your internet connection and try again.",
             showCloseButton: true,
             closeButtonText: "retry"
         });
         toast.onDidDismiss().then(() => {
             toast.dismiss();
             this.refreshData();
         });
         toast.present();
     }

     refreshData() {
         this.fetchRecordAgainstSelectedObject();
     }
     async saveButtonOnclick() {
         if (this.workFlowInitiateList && Object.keys(this.workFlowInitiateList).length == 1) {
             this.addSystemAttributes(Object.keys(this.workFlowInitiateList)[0]);
         };
         if (Object.keys(this.workFlowInitiateList).length > 1) {
             var initateActionList = this.getInitiateProcessDisplayName();
             alert(initateActionList + ' can not process at same time');
             return;
         };
         this.formGroup.patchValue({
             pfm144373: {
                 customercontact: this.obj_pfm144373['customercontact'],
                 billtoaddress: this.obj_pfm144373['billtoaddress'],
                 shiptoaddress: this.obj_pfm144373['shiptoaddress'],
                 ordercompleteddate: this.obj_pfm144373['ordercompleteddate'],
                 customernumber: this.obj_pfm144373['customernumber'],
                 orderstatus: this.obj_pfm144373['orderstatus'],
                 orderid: this.obj_pfm144373['orderid'],
                 orderconfirmeddate: this.obj_pfm144373['orderconfirmeddate']
             }
         });
         if (this.formGroup.valid) {
             this.formGroup.patchValue({});
             this.isValidFrom = true;
             if (this.obj_pfm144373['ordercompleteddate'] !== null) {
                 this.obj_pfm144373['ordercompleteddate'] = new Date(this.obj_pfm144373['ordercompleteddate']).getTime();
             }
             if (this.obj_pfm144373['orderconfirmeddate'] !== null) {
                 this.obj_pfm144373['orderconfirmeddate'] = new Date(this.obj_pfm144373['orderconfirmeddate']).getTime();
             }
             if (this.orderstatus_289221_status && this.orderstatus_289221_status['statusValue']) {
                 this.obj_pfm144373["orderstatus"] = this.orderstatus_289221_status['statusValue'];
             } else {
                 this.obj_pfm144373["orderstatus"] = this.orderstatus_289221_defaultStatus['statusValue'];
             }
             const fieldTrackObject = this.fieldTrackMapping.mappingDetail[this.tableName_pfm144373]
             if (fieldTrackObject) {
                 this.isFieldTrackingEnable = true;
             } else {
                 this.isFieldTrackingEnable = false;
             }
             let previousParentObject
             if (this.action === "Edit") {
                 previousParentObject = this.selectedDataObject[this.tableName_pfm144373];
                 console.log("previousParentObject = ", previousParentObject);
             } else {
                 previousParentObject = undefined
             }
             if (this.parentId) {
                 this.obj_pfm144373[this.parentName] = this.parentId;
             };
             this.dataProvider.save(this.tableName_pfm144373, this.obj_pfm144373, appConstant.pouchDBStaticName, previousParentObject, this.isFieldTrackingEnable)
                 .then(result => {
                     if (result['status'] != 'SUCCESS') {
                         this.showInfoAlert(result['message']);
                         return;
                     }
                     if (this.childObjectList.length == 0) {
                         this.presentToast(this.savedSuccessMessage);
                         let opts = {
                             animate: false
                         };
                         this.navigatePopUpAction();
                         return
                     };

                 })
                 .catch(error => {
                     this.showInfoAlert(this.savedErrorMessage);
                     console.log(error)
                 });
         } else {
             this.formGroup.patchValue({});
             this.isValidFrom = false;
             this.scrollToValidationFailedField();
         }
         var errorValue = document.querySelector('.entry-page-content');
         errorValue.setAttribute('class', 'entry-page-content entryErrorMessage hydrated');
     };
     scrollToValidationFailedField() {
         let formControls = this.formGroup.controls.pfm144373;
         let formGroupKeys = Object.keys(formControls["controls"]);
         let isValidationSucceed = true;
         formGroupKeys.every(element => {
             if (formControls["controls"][element].status == "INVALID") {
                 let yOffset = document.getElementById("pfm144373_" + element).offsetTop;
                 this.contentArea.scrollToPoint(0, yOffset, 1000);
                 isValidationSucceed = false;
                 return false;
             } else {
                 return true;
             }
         })
     };
     createFormGroup() {
         this.formGroup = this.formBuilder.group({
             pfm144373: this.formBuilder.group({
                 customercontact: [null, Validators.compose([])],
                 billtoaddress: [null, Validators.compose([])],
                 shiptoaddress: [null, Validators.compose([])],
                 ordercompleteddate: [null, Validators.compose([])],
                 customernumber: [null, Validators.compose([])],
                 orderstatus: [null, Validators.compose([])],
                 pfm147213_301933_searchKey: [''],
                 orderconfirmeddate: [null, Validators.compose([Validators.required])]
             })
         });
     }
     async showLookup_301933(objectname, label, displayColumns) {
         const lookupInput = {};
         const lookupColumns = [{
             columnName: 'accountno',
             displayName: 'Account No',
             fieldType: 'Autonumber',
             mappingValues: {}
         }, {
             columnName: 'accountname',
             displayName: 'Customer Name',
             fieldType: 'Text',
             mappingValues: {}
         }, {
             columnName: 'customercontact',
             displayName: 'Customer Contact',
             fieldType: 'Text',
             mappingValues: {}
         }];
         const lookupHierarchyJson = {
             "objectName": "customermaster",
             "objectType": "PRIMARY",
             "relationShipType": "",
             "fieldId": 301933,
             "objectId": "147213",
             "childObject": []
         };
         const selector = {};
         selector['data.type'] = 'pfm147213';;
         if (this.pfm147213_301933_searchKey) {
             var regexp = new RegExp(this.pfm147213_301933_searchKey, 'i');
             selector['data.accountno'] = {
                 $regex: regexp
             };
         } else if (this.obj_pfm147213_301933['accountno']) {
             var regexp = new RegExp(this.obj_pfm147213_301933['accountno'], 'i');
             selector['data.accountno'] = {
                 $regex: regexp
             };
         }
         lookupHierarchyJson['options'] = {
             'selector': selector
         };
         lookupInput['lookupColumnDetails'] = lookupColumns;
         lookupInput['objectHierarchy'] = lookupHierarchyJson;
         lookupInput['title'] = label;
         const lookupModal = await this.modalCtrl.create({
             component: lookuppage,
             componentProps: {
                 serviceObject: this.dbService,
                 parentPage: this,
                 lookupColumnName: objectname,
                 lookupInput: lookupInput,
                 dataSource: appConstant.pouchDBStaticName
             }
         });
         await lookupModal.present();
     }
     lookupResponse(objectname, selectedValue) {
         this.dependentNumberCount = {};
         if (objectname === 'pfm147213_301933') {
             this.pfm147213_301933_searchKey = '';
             this.obj_pfm147213_301933 = selectedValue;
             this.obj_pfm144373.pfm147213_301933 = this.obj_pfm147213_301933.id;
             this.formGroup.patchValue({
                 obj_pfm144373_customername: selectedValue.id
             });
         }
     }
     loadCheckboxEditValues(fieldName, values) {}
     loadDefaultValues() {}
     resetChildDependentInfo(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName) {

     }
     ionViewDidEnter() {}
     statusChange(event, selectedStatusField, displayname, fieldId) {
         if (selectedStatusField == undefined) {
             selectedStatusField = {}
         }
         if (event['selectedStatus']['isApproveInitiateEnabled'] == 'Y') {
             if (this.workFlowInitiateList && !this.workFlowInitiateList.hasOwnProperty(fieldId))
                 this.workFlowInitiateList[fieldId] = {
                     "displayname": displayname,
                     "configId": event['selectedStatus']['statusWFConfigId']
                 };
         } else {
             if (this.workFlowInitiateList && this.workFlowInitiateList.hasOwnProperty(fieldId)) {
                 delete this.workFlowInitiateList[fieldId];
             }
         }
         selectedStatusField['statusLabel'] = event['selectedStatus']['statusLabel'];
         selectedStatusField['statusValue'] = event['selectedStatus']['statusValue'];
         selectedStatusField['statusType'] = event['selectedStatus']['statusType'];
         console.log("Status Change :", selectedStatusField);
     };
     getInitiateProcessDisplayName() {
         var fieldNameList = Object.keys(this.workFlowInitiateList);
         var lable = ''
         for (let i = 0; i < fieldNameList.length; i++) {
             if (lable == '') {
                 lable = this.workFlowInitiateList[fieldNameList[i]].displayname

             } else {
                 lable = lable + ", " + this.workFlowInitiateList[fieldNameList[i]].displayname
             }
         }
         return lable;
     };
     addSystemAttributes(fieldId) {

         var configId = this.workFlowInitiateList[fieldId]['configId']

         var systemAttributeObject = {};
         const date = new Date();
         systemAttributeObject['lockedBy'] = this.appUtility.userId;
         systemAttributeObject['lockedDate'] = date.getTime();
         systemAttributeObject['fieldId'] = fieldId;
         systemAttributeObject['lockedStatus'] = 'INPROGRESS';
         systemAttributeObject['statusWFConfigId'] = configId;
         systemAttributeObject['workFlowExecID'] = '';
         this.obj_pfm144373['systemAttributes'] = systemAttributeObject;
     }
     checkboxInitialization() {};
     backButtonOnclick() {
         if (this.recordEntryValidation()) {
             this.recordDiscardConfirmAlert();
         } else {
             this.navigatePopUpAction();
         }
     };
     navigatePopUpAction() {
         this.router.navigateByUrl(this.redirectUrl, {
             skipLocationChange: true
         });
     };
     hardWareBackButtonAction() {
         this.backButtonSubscribeRef = this.platform.backButton.subscribeWithPriority(999999, () => {
             if (this.customAlert) {
                 return;
             }
             this.backButtonOnclick();
         });
     }
     lookupClearAction(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName) {
         dataObj[dataObjectFieldName] = null;
         delete looklUpObj["id"];
         delete looklUpObj[lookupObjectFieldName];
         this.formGroup.value.formControlerName = null;
         this.resetChildDependentInfo(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName)
     }
     async recordDiscardConfirmAlert() {
         this.customAlert = await this.alerCtrl.create({
             backdropDismiss: false,
             message: 'Are you sure want to leave this page?',
             buttons: [{
                 text: 'Cancel',
                 cssClass: 'method-color',
                 handler: () => {
                     this.customAlert = null;
                     console.log('Individual clicked');
                 }
             }, {
                 text: 'Yes',
                 cssClass: 'method-color',
                 handler: () => {
                     this.navigatePopUpAction();
                 }
             }]
         });
         this.customAlert.present();
     }
     async presentToast(message) {
         const toast = await this.toastCtrl.create({
             message: message,
             duration: 2000,
             position: 'bottom'
         });
         toast.onDidDismiss().then((res) => {
             this.refreshButtonPressed();
         });
         toast.present();
     }
     refreshButtonPressed() {
         this.fetchRecordAgainstSelectedObject();
     }
     async showInfoAlert(info) {
         this.alerCtrl.create({
             message: info,
             subHeader: '',
             buttons: [{
                 text: 'Ok',
                 handler: () => {
                     console.log('Confirm Okay');
                 }
             }]
         }).then(alert => alert.present());
     }
     closeInfoAlert() {
         this.aniClassAddorRemove = false;
         setTimeout(() => {
             this.showAlert = false;
             this.infoMessage = '';
         }, 500);
     };
     onQuillSelectionChanged() {
         var Link = Quill.import('formats/link');
         Link.sanitize = function(url) {
             let protocol = url.slice(0, url.indexOf(':'));
             if (this.PROTOCOL_WHITELIST.indexOf(protocol) === -1) {
                 url = 'http://' + url;
             }
             let anchor = document.createElement('a');
             anchor.href = url;
             protocol = anchor.href.slice(0, anchor.href.indexOf(':'));
             return (this.PROTOCOL_WHITELIST.indexOf(protocol) > -1) ? url : this.SANITIZED_URL;
         }
         Quill.register(Link, true);
     }
     ionViewWillLeave() {
         this.backButtonSubscribeRef.unsubscribe();
     }
     ngOnInit() {}
 }