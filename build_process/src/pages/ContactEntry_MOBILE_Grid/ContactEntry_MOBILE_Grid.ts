import {
    Component,
    OnInit,
    ViewChild,
    ApplicationRef,
    HostListener,
    NgZone
} from '@angular/core';
import {
    dataProvider
} from 'src/core/utils/dataProvider';
import {
    appConstant
} from 'src/core/utils/appConstant';
import {
    ModalController,
    Platform,
    LoadingController,
    Events,
    ToastController,
    AlertController,
    IonContent
} from '@ionic/angular';
import {
    DrawerState
} from 'ion-bottom-drawer';
import {
    PopoverController
} from '@ionic/angular';
import {
    appUtility
} from 'src/core/utils/appUtility';
import {
    ScreenOrientation
} from '@ionic-native/screen-orientation/ngx';
import {
    lookupFieldMapping
} from 'src/core/pfmmapping/lookupFieldMapping';
import {
    objectTableMapping
} from 'src/core/pfmmapping/objectTableMapping';
import * as moment from 'moment';
import {
    Router,
    ActivatedRoute
} from '@angular/router';
import {
    SocialSharing
} from '@ionic-native/social-sharing/ngx';
import {
    EmailComposer
} from '@ionic-native/email-composer/ngx';
import {
    CallNumber
} from '@ionic-native/call-number/ngx';
import {
    SMS
} from '@ionic-native/sms/ngx';
import {
    attachmentDbProvider
} from 'src/core/db/attachmentDbProvider';
import * as _ from 'underscore';
import {
    DatePipe
} from '@angular/common';
import {
    registerLocaleData
} from '@angular/common';
import {
    metaDataDbProvider
} from 'src/core/db/metaDataDbProvider';
import {
    metaDbConfiguration
} from 'src/core/db/metaDbConfiguration';
import {
    cspfmExecutionPouchDbProvider
} from 'src/core/db/cspfmExecutionPouchDbProvider';
import {
    cspfmExecutionPouchDbConfiguration
} from 'src/core/db/cspfmExecutionPouchDbConfiguration';

import * as lodash from 'lodash';
import {
    lookuppage
} from 'src/core/pages/lookuppage/lookuppage';
import {
    FormBuilder,
    Validators,
    FormGroup
} from '@angular/forms';
import 'moment-timezone';
import * as Quill from 'quill';
import {
    cspfmFieldTrackingMapping
} from 'src/core/pfmmapping/cspfmFieldTrackingMapping';
import {
    dbProvider
} from 'src/core/db/dbProvider';
import {
    Http
} from '@angular/http';
import {
    dbConfiguration
} from 'src/core/db/dbConfiguration';
import {
    couchdbProvider
} from 'src/core/db/couchdbProvider';
import {
    FieldInfo
} from "src/core/pipes/cspfm_data_display";
@Component({
    selector: 'ContactEntry_MOBILE_Grid',
    templateUrl: './ContactEntry_MOBILE_Grid.html'
}) export class ContactEntry_MOBILE_Grid implements OnInit {
    @ViewChild('parentContent') parentContent: IonContent;
    @ViewChild('childContent') childContent: IonContent;
    isBrowser: boolean = false;
    headerenable = true;
    drawerComponentDockedHeight = 300;
    drawerComponentCurrentState = DrawerState.Top;
    drawerComponentPreviousState = DrawerState.Docked;
    private obj_pfm147274: any = {};
    public ionDateTimeDisplayValue = {};
    private obj_pfm147274_Temp: any = {};
    public formGroup: FormGroup;
    private customAlert;
    public action = 'Add';
    private id: any = {};
    private tableName_pfm147274 = 'pfm147274';
    public isSaveActionTriggered: Boolean = false;
    private isValidFrom: Boolean = true;
    public dbServiceProvider = appConstant.pouchDBStaticName;
    public selectedDataObject: any = {};
    public formulaObject = {};
    public isFieldTrackingEnable: Boolean = false;
    private showAlert: boolean = false;
    infoMessage = '';
    aniClassAddorRemove: boolean = false;
    private workFlowInitiateList = {};
    public savedSuccessMessage = 'data saved sucessfully';
    public updatedSuccessMessage = 'data updated sucessfully';
    public savedErrorMessage = 'Error saving record';
    public fetchErrorMessage = 'Fetching Failed';
    private dependentNumberCount = {};
    public partiallySavedSuccessMessage = 'data partially saved';
    private backButtonSubscribeRef;
    private unRegisterBackButtonAction: Function;
    public parentObjLabel = '';
    public parentObjValue = '';
    private errorMessageToDisplay: string = 'No Records';
    private parentName = '';
    private parentId = '';
    public isParentObjectShow = false;
    public isSkeletonLoading = true;
    isViewRunning = false;
    loading;
    private objectHierarchyJSON = {
        "objectName": "contactdetails",
        "objectType": "PRIMARY",
        "relationShipType": null,
        "fieldId": "0",
        "objectId": "147274",
        "childObject": [{
            "objectName": "addressdetails",
            "objectType": "HEADER",
            "relationShipType": "one_to_many",
            "fieldId": "293833",
            "objectId": "147233",
            "childObject": []
        }]
    };
    public isFromMenu = false;
    private redirectUrl = '/';
    private dependentFieldTriggerList = {};
    private geoLocationdependentField = {};
    public pickListValues = {};
    public parentObject: any = {};
    private obj_pfm147233_293833: any = {};
    private pfm147233_293833_searchKey;
    public formulaConfigJSON = {};
    public formulafields = {};
    childObjectList = [];
    objResultMap = new Map < string, any > ();
    objDisplayName = {
        'pfm147274': 'contactdetails',
    };
    recordEntryValidation() {
        const formGroupObjectValue = Object.assign({}, this.formGroup.value);
        this.obj_pfm147274 = Object.assign(this.obj_pfm147274, formGroupObjectValue.pfm147274);
        if (
            this.obj_pfm147274["email"] || this.obj_pfm147274["contactname"] || this.obj_pfm147274["responsibility"] || this.obj_pfm147274["address"] || this.obj_pfm147274["phonenumber"] || this.obj_pfm147274["name"] || this.obj_pfm147274["name"]) {
            return true;
        } else {
            return false;
        }
    }
    constructor(public dbConfiguration: dbConfiguration, public http: Http, public popoverCtrl: PopoverController, public modalCtrl: ModalController, public dbService: dbProvider,
        private socialSharing: SocialSharing, private emailComposer: EmailComposer, private callNumber: CallNumber, private sms: SMS,
        public platform: Platform, public applicationRef: ApplicationRef, public formBuilder: FormBuilder,
        public events: Events, public router: Router, public activatRoute: ActivatedRoute,
        public objectTableMapping: objectTableMapping, public lookupFieldMapping: lookupFieldMapping,
        public loadingCtrl: LoadingController, public toastCtrl: ToastController, public dataProvider: dataProvider, public metaDbConfigurationObj: metaDbConfiguration, public metaDbProvider: metaDataDbProvider, public cspfmexecutionPouchDbProvider: cspfmExecutionPouchDbProvider,
        public executionDbConfigObject: cspfmExecutionPouchDbConfiguration, private datePipe: DatePipe, public fieldTrackMapping: cspfmFieldTrackingMapping,
        public alerCtrl: AlertController, public appUtility: appUtility, public screenOrientation: ScreenOrientation, private ngZone: NgZone) {
        this.activatRoute.queryParams.subscribe(params => {
            if (Object.keys(params).length == 0 && params.constructor === Object) {
                console.log("list query params skipped");
                return
            }
            if (params["isFromMenu"]) {
                this.isFromMenu = params["isFromMenu"];
            }
            if (params["redirectUrl"]) {
                this.redirectUrl = params["redirectUrl"]
            }
            this.action = params['action'];
            this.parentId = params['parentId'];
            this.id = params['id'];
            this.fetchParentObj();
            this.parentName = params['parentName'];
            if (this.parentId) {
                this.isParentObjectShow = true;
                this.parentObjLabel = params['parentFieldLabel'];
                this.parentObjValue = params['parentFieldValue'];
            }
            this.initializeObjects(dataProvider.tableStructure());
            if (this.action == 'Edit') {
                this.id = params['id'];
                this.fetchRecordAgainstSelectedObject();
            } else {
                this.isSkeletonLoading = false;
                this.checkboxInitialization();
            }
        });
        const windowHeight = window.innerHeight;
        this.drawerComponentDockedHeight = windowHeight / 2;
        this.screenOrientation.onChange().subscribe(() => {
            this.drawerComponentCurrentState = DrawerState.Bottom;
            const windowHeightVal = window.innerHeight;
            this.drawerComponentDockedHeight = windowHeightVal / 2;
        });
        this.createFormGroup();
        this.hardWareBackButtonAction();
    }
    initializeObjects(tableStructure) {
        this.obj_pfm147274 = JSON.parse(JSON.stringify(tableStructure.pfm147274));
    }
    fetchParentObj() {
        if (this.action == 'Edit') {
            return;
        }
        for (let childItem of this.objectHierarchyJSON['childObject']) {
            if (childItem['objectType'] == 'HEADER') {
                this.fetchParentWithParentJSON(childItem);
            }
        }
    }
    fetchParentWithParentJSON(parentJSON) {
        const additionalObjectdata = {};
        additionalObjectdata['id'] = this.parentId;
        const fetchParams = {
            'objectHierarchyJSON': parentJSON,
            'additionalInfo': additionalObjectdata,
            'dataSource': appConstant.pouchDBStaticName
        };
        this.dataProvider.querySingleDoc(fetchParams).then(res => {
            this.isSkeletonLoading = false;
            if (res['status'] !== 'SUCCESS') {
                this.errorMessageToDisplay = res['message'];
                if (this.errorMessageToDisplay == 'No internet') {
                    this.presentNoInternetToast();
                }
                return;
            }
            if (res['records'].length < 0) {
                console.log('FetchRecordAgainstSelectedObject No Records');
                return;
            }
            let dataObj = res['records'][0];
            this.parentObject = dataObj;
        }).catch(error => {
            this.isSkeletonLoading = false;
            this.showInfoAlert(this.fetchErrorMessage)
        })
    }
    fetchRecordAgainstSelectedObject() {
        const additionalObjectdata = {};
        additionalObjectdata['id'] = this.id;
        const fetchParams = {
            'objectHierarchyJSON': this.objectHierarchyJSON,
            'additionalInfo': additionalObjectdata,
            'dataSource': appConstant.pouchDBStaticName
        };
        this.dataProvider.querySingleDoc(fetchParams).then(res => {
            this.isSkeletonLoading = false;
            if (res['status'] !== 'SUCCESS') {
                this.errorMessageToDisplay = res['message'];
                if (this.errorMessageToDisplay == 'No internet') {
                    this.presentNoInternetToast();
                }
                return;
            }
            if (res['records'].length < 0) {
                console.log('FetchRecordAgainstSelectedObject No Records');
                return;
            }
            let dataObj = res['records'][0];
            this.obj_pfm147274_Temp = lodash.extend({}, this.obj_pfm147274, dataObj);
            this.selectedDataObject['pfm147274'] = JSON.stringify(this.obj_pfm147274_Temp);
            this.obj_pfm147274 = lodash.extend({}, this.obj_pfm147274, dataObj);
            this.obj_pfm147233_293833 = dataObj['pfm147233s'][0] ? dataObj['pfm147233s'][0] : "";
            this.formGroup.patchValue({
                pfm147274: this.obj_pfm147274
            });
            this.parentObject = dataObj['pfm147233s'][0];
            this.applicationRef.tick();
        }).catch(error => {
            this.isSkeletonLoading = false;
            this.showInfoAlert(this.fetchErrorMessage)
        })
    }
    updateGeoLocationFlag(objectName, dataObj, existingDataObj) {
        Object.keys(this.geoLocationdependentField[objectName]).forEach(element => {
            const depFlds = this.geoLocationdependentField[objectName][element]['dependentFields'];
            depFlds.forEach(fieldName => {
                if (this.action == 'Edit') {
                    if (existingDataObj[fieldName] != dataObj[fieldName]) {
                        dataObj[this.geoLocationdependentField[objectName][element]['dependentFieldName']] = 'Y';
                        return;
                    }
                } else {
                    if (dataObj[fieldName] != '') {
                        dataObj[this.geoLocationdependentField[objectName][element]['dependentFieldName']] = 'Y';
                        return;
                    }
                }
            })
        });
    }
    async presentNoInternetToast() {
        const toast = await this.toastCtrl.create({
            message: "No internet connection. Please check your internet connection and try again.",
            showCloseButton: true,
            closeButtonText: "retry"
        });
        toast.onDidDismiss().then(() => {
            toast.dismiss();
            this.refreshData();
        });
        toast.present();
    }

    refreshData() {
        this.fetchRecordAgainstSelectedObject();
    }
    async saveButtonOnclick() {
        this.formGroup.patchValue({
            pfm147274: {
                email: this.obj_pfm147274['email'],
                contactname: this.obj_pfm147274['contactname'],
                responsibility: this.obj_pfm147274['responsibility'],
                phonenumber: this.obj_pfm147274['phonenumber'],
                name: this.obj_pfm147274['name']
            }
        });
        if (this.formGroup.valid) {
            this.formGroup.patchValue({});
            this.isValidFrom = true;
            if (this.obj_pfm147274['phonenumber'] !== null) {
                this.obj_pfm147274['phonenumber'] = Number(this.obj_pfm147274['phonenumber']);
            }
            const fieldTrackObject = this.fieldTrackMapping.mappingDetail[this.tableName_pfm147274]
            if (fieldTrackObject) {
                this.isFieldTrackingEnable = true;
            } else {
                this.isFieldTrackingEnable = false;
            }
            let previousParentObject
            if (this.action === "Edit") {
                previousParentObject = this.selectedDataObject[this.tableName_pfm147274];
                console.log("previousParentObject = ", previousParentObject);
            } else {
                previousParentObject = undefined
            }
            if (this.parentId) {
                this.obj_pfm147274[this.parentName] = this.parentId;
            };
            this.dataProvider.save(this.tableName_pfm147274, this.obj_pfm147274, appConstant.pouchDBStaticName, previousParentObject, this.isFieldTrackingEnable)
                .then(result => {
                    if (result['status'] != 'SUCCESS') {
                        this.showInfoAlert(result['message']);
                        return;
                    }
                    if (this.childObjectList.length == 0) {
                        this.presentToast(this.savedSuccessMessage);
                        let opts = {
                            animate: false
                        };
                        this.navigatePopUpAction();
                        return
                    };

                })
                .catch(error => {
                    this.showInfoAlert(this.savedErrorMessage);
                    console.log(error)
                });
        } else {
            this.formGroup.patchValue({});
            this.isValidFrom = false;
            this.scrollToValidationFailedField();
        }
        var errorValue = document.querySelector('.entry-page-content');
        errorValue.setAttribute('class', 'entry-page-content entryErrorMessage hydrated');
    };
    scrollToValidationFailedField() {
        const formControls = this.formGroup.controls.pfm147274;
        const formGroupKeys = Object.keys(formControls["controls"]);
        let isValidationSucceed = true;
        formGroupKeys.every(element => {
            if (formControls["controls"][element].status === "INVALID") {
                const yOffset = document.getElementById("pfm147274_" + element).offsetTop;
                this.childContent.scrollToPoint(0, yOffset, 1000);
                isValidationSucceed = false;
                return false;
            } else {
                return true;
            }
        });
    }
    createFormGroup() {
        this.formGroup = this.formBuilder.group({
            pfm147274: this.formBuilder.group({
                email: [null, Validators.compose([, Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')])],
                contactname: [null, Validators.compose([Validators.required])],
                responsibility: [null, Validators.compose([])],
                pfm147233_293833_searchKey: [''],
                phonenumber: [null, Validators.compose([])]
            })
        });
    }
    async showLookup_293833(objectname, label, displayColumns) {
        const lookupInput = {};
        const lookupColumns = [{
            columnName: 'sitenumber',
            displayName: 'Site Number',
            fieldType: 'Text',
            mappingValues: {}
        }, {
            columnName: 'addressid',
            displayName: 'Address ID',
            fieldType: 'Autonumber',
            mappingValues: {}
        }];
        const lookupHierarchyJson = {
            "objectName": "addressdetails",
            "objectType": "PRIMARY",
            "relationShipType": null,
            "fieldId": 0,
            "objectId": "147233",
            "childObject": []
        };
        const selector = {};
        selector['data.type'] = 'pfm147233';;
        if (this.pfm147233_293833_searchKey) {
            var regexp = new RegExp(this.pfm147233_293833_searchKey, 'i');
            selector['data.addressid'] = {
                $regex: regexp
            };
        } else if (this.obj_pfm147233_293833['addressid']) {
            var regexp = new RegExp(this.obj_pfm147233_293833['addressid'], 'i');
            selector['data.addressid'] = {
                $regex: regexp
            };
        }
        lookupHierarchyJson['options'] = {
            'selector': selector
        };
        lookupInput['lookupColumnDetails'] = lookupColumns;
        lookupInput['objectHierarchy'] = lookupHierarchyJson;
        lookupInput['title'] = label;
        const lookupModal = await this.modalCtrl.create({
            component: lookuppage,
            componentProps: {
                serviceObject: this.dbService,
                parentPage: this,
                lookupColumnName: objectname,
                lookupInput: lookupInput,
                dataSource: appConstant.pouchDBStaticName
            }
        });
        await lookupModal.present();
    }
    lookupResponse(objectname, selectedValue) {
        this.dependentNumberCount = {};
        if (objectname === 'pfm147233_293833') {
            this.obj_pfm147233_293833 = selectedValue;
            this.obj_pfm147274['pfm147233'] = this.obj_pfm147233_293833['id'];
            this.formGroup.patchValue({
                obj_pfm147274_address: selectedValue.id
            });

        }
    }
    loadCheckboxEditValues(fieldName, values) {}
    loadDefaultValues() {}
    resetChildDependentInfo(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName) {

    }
    public country_293719 = {
        "None": "None",
        "USA": "USA",
        "Canada": "Canada",
        "India": "India"
    };
    public addresstype_293732 = {
        "None": "None",
        "Ship To": "Ship To",
        "Bill To": "Bill To"
    };
    public gridFieldInfo: {
        [key: string]: FieldInfo
    } = {
        "pfm147233_addressid": {
            "label": "ContactEntry_MOBILE_Grid.Element.addressdetails.addressid",
            "fieldName": "addressid",
            "prop": "addressid",
            "fieldType": "AUTONUMBER",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147233_sitenumber": {
            "label": "ContactEntry_MOBILE_Grid.Element.addressdetails.sitenumber",
            "fieldName": "sitenumber",
            "prop": "sitenumber",
            "fieldType": "TEXT",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        },
        "pfm147233_country": {
            "label": "ContactEntry_MOBILE_Grid.Element.addressdetails.country",
            "fieldName": "country",
            "prop": "country",
            "fieldType": "DROPDOWN",
            "child": "",
            "dateFormat": "",
            "mappingDetails": {
                "None": "None",
                "USA": "USA",
                "Canada": "Canada",
                "India": "India"
            },
            "currencyDetails": ""
        },
        "pfm147233_addresstype": {
            "label": "ContactEntry_MOBILE_Grid.Element.addressdetails.addresstype",
            "fieldName": "addresstype",
            "prop": "addresstype",
            "fieldType": "CHECKBOX",
            "child": "",
            "dateFormat": "",
            "mappingDetails": {
                "None": "None",
                "Ship To": "Ship To",
                "Bill To": "Bill To"
            },
            "currencyDetails": ""
        },
        "pfm147233_postalcode": {
            "label": "ContactEntry_MOBILE_Grid.Element.addressdetails.postalcode",
            "fieldName": "postalcode",
            "prop": "postalcode",
            "fieldType": "TEXT",
            "child": "",
            "dateFormat": "",
            "mappingDetails": "",
            "currencyDetails": ""
        }
    };
    checkboxInitialization() {};
    backButtonOnclick() {
        if (this.recordEntryValidation()) {
            this.recordDiscardConfirmAlert();
        } else {
            this.navigatePopUpAction();
        }
    };
    navigatePopUpAction() {
        this.router.navigateByUrl(this.redirectUrl, {
            skipLocationChange: true
        });
    };
    hardWareBackButtonAction() {
        this.backButtonSubscribeRef = this.platform.backButton.subscribeWithPriority(999999, () => {
            if (this.customAlert) {
                return;
            }
            this.backButtonOnclick();
        });
    }
    lookupClearAction(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName) {
        dataObj[dataObjectFieldName] = "";
        delete looklUpObj["id"];
        delete looklUpObj[lookupObjectFieldName];
        this.formGroup.value.formControlerName = "";
        this.resetChildDependentInfo(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName)
    }
    async recordDiscardConfirmAlert() {
        this.customAlert = await this.alerCtrl.create({
            backdropDismiss: false,
            message: 'Are you sure want to leave this page?',
            buttons: [{
                text: 'Cancel',
                cssClass: 'method-color',
                handler: () => {
                    this.customAlert = null;
                    console.log('Individual clicked');
                }
            }, {
                text: 'Yes',
                cssClass: 'method-color',
                handler: () => {
                    this.navigatePopUpAction();
                }
            }]
        });
        this.customAlert.present();
    }
    async presentToast(message) {
        const toast = await this.toastCtrl.create({
            message: message,
            duration: 2000,
            position: 'bottom'
        });
        toast.onDidDismiss().then((res) => {
            this.refreshButtonPressed();
        });
        toast.present();
    }
    refreshButtonPressed() {
        this.fetchRecordAgainstSelectedObject();
    }
    async showInfoAlert(info) {
        this.alerCtrl.create({
            message: info,
            subHeader: '',
            buttons: [{
                text: 'Ok',
                handler: () => {
                    console.log('Confirm Okay');
                }
            }]
        }).then(alert => alert.present());
    }
    closeInfoAlert() {
        this.aniClassAddorRemove = false;
        setTimeout(() => {
            this.showAlert = false;
            this.infoMessage = '';
        }, 500);
    };
    onQuillSelectionChanged() {
        var Link = Quill.import('formats/link');
        Link.sanitize = function(url) {
            let protocol = url.slice(0, url.indexOf(':'));
            if (this.PROTOCOL_WHITELIST.indexOf(protocol) === -1) {
                url = 'http://' + url;
            }
            let anchor = document.createElement('a');
            anchor.href = url;
            protocol = anchor.href.slice(0, anchor.href.indexOf(':'));
            return (this.PROTOCOL_WHITELIST.indexOf(protocol) > -1) ? url : this.SANITIZED_URL;
        }
        Quill.register(Link, true);
    }
    ionViewWillLeave() {
        this.backButtonSubscribeRef.unsubscribe();
        this.drawerComponentCurrentState = DrawerState.Bottom
    }
    ngOnInit() {}

    ionViewWillEnter() {
        document.body.setAttribute('class', 'linelistinnerdetail');
        this.drawerComponentCurrentState = DrawerState.Top;
    }

    ionViewDidEnter() {
        if (this.action === 'Edit') {
            const tableStructure = JSON.parse(JSON.stringify(this.dataProvider.tableStructure()["pfm126513"]));
            Object.keys(tableStructure).forEach(key => {
                this.dependentFieldTriggerList[key] = 'triggered';
            });
        }
        const dvHeader = document.querySelector(".detail-view-sub-header");
        dvHeader.setAttribute("color", "var(--ion-color-primary, #3880ff)");
        const dvHeaderItem = document.querySelector(
            ".detail-view-sub-header ion-item"
        );
        dvHeaderItem.setAttribute("color", "var(--ion-color-primary, #3880ff)");
        const dvHeaderListHd = document.querySelectorAll(
            ".hl-full-detail-content ion-list-header"
        );
        const dvHeaderListHdLen = dvHeaderListHd.length;
        for (let i = 0; i < dvHeaderListHdLen; i++) {
            dvHeaderListHd[i].setAttribute(
                "color",
                "var(--ion-color-primary, #3880ff)"
            );
        }
        const pvHdItembg = document.querySelectorAll(
            ".detail-view-sub-header ion-badge"
        );
        const pvHdItembgLen = pvHdItembg.length;
        for (let i = 0; i < pvHdItembgLen; i++) {
            pvHdItembg[i].setAttribute(
                "background",
                "var(--ion-color-primary-tint, #4c8dff)"
            );
        }
    }

    dockerButtonOnClick() {
        if (this.drawerComponentCurrentState === DrawerState.Bottom) {
            this.drawerComponentCurrentState = DrawerState.Docked
            this.drawerComponentPreviousState = DrawerState.Bottom
        } else if (this.drawerComponentCurrentState === DrawerState.Docked) {
            if (this.drawerComponentPreviousState === DrawerState.Bottom) {
                this.drawerComponentCurrentState = DrawerState.Top
            } else {
                this.drawerComponentCurrentState = DrawerState.Bottom
            }
            this.drawerComponentPreviousState = DrawerState.Docked;
        } else if (this.drawerComponentCurrentState === DrawerState.Top) {
            this.drawerComponentCurrentState = DrawerState.Docked
            this.drawerComponentPreviousState = DrawerState.Top
        }
    }
    openurl(events, url) {
        events.stopPropagation();
        if (url.indexOf('http') == 0) {
            window.open(url)
        } else {
            window.open('http://'.concat(url))
        }
    }

}