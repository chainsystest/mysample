 import {
     Component,
     ViewChild,
     OnInit,
     ApplicationRef,
     ChangeDetectorRef,
     HostListener,
     NgZone
 } from '@angular/core';
 import {
     dbProvider
 } from 'src/core/db/dbProvider';
 import {
     offlineDbIndexCreation
 } from 'src/core/utils/offlineDbIndexCreation';
 import {
     dataProvider
 } from 'src/core/utils/dataProvider';
 import {
     appConstant
 } from 'src/core/utils/appConstant';
 import {
     Platform,
     LoadingController,
     Events,
     ToastController,
     IonVirtualScroll,
     AlertController
 } from '@ionic/angular';
 import {
     DrawerState
 } from 'ion-bottom-drawer';
 import {
     appUtility
 } from 'src/core/utils/appUtility';
 import {
     lookupFieldMapping
 } from 'src/core/pfmmapping/lookupFieldMapping';
 import {
     objectTableMapping
 } from 'src/core/pfmmapping/objectTableMapping';
 import {
     Router,
     ActivatedRoute
 } from '@angular/router';
 import {
     ScreenOrientation
 } from '@ionic-native/screen-orientation/ngx';
 import {
     cspfmExecutionPouchDbProvider
 } from 'src/core/db/cspfmExecutionPouchDbProvider';
 import {
     cspfmExecutionPouchDbConfiguration
 } from 'src/core/db/cspfmExecutionPouchDbConfiguration';
 import * as lodash from 'lodash';
 import * as _ from 'underscore';
 import {
     DatePipe
 } from '@angular/common';
 import {
     registerLocaleData
 } from '@angular/common';
 import {
     metaDataDbProvider
 } from 'src/core/db/metaDataDbProvider';
 import {
     metaDbConfiguration
 } from 'src/core/db/metaDbConfiguration';
 import {
     FieldInfo
 } from "src/core/pipes/cspfm_data_display";
 import {
     ColumnMode
 } from "@swimlane/ngx-datatable";

 @Component({
     selector: 'Saleorderview_MOBILE_Grid_with_Listpreview',
     templateUrl: 'Saleorderview_MOBILE_Grid_with_Listpreview.html'
 }) export class Saleorderview_MOBILE_Grid_with_Listpreview implements OnInit {
     @ViewChild(IonVirtualScroll) virtualScroll: IonVirtualScroll;
     public isBrowser: boolean = false;
     public drawerComponentDockedHeight = 300;
     public drawerComponentCurrentState = DrawerState.Top;
     private drawerComponentPreviousState = DrawerState.Docked;
     public pageTitle: string = '';
     public parentTitle: string = '';
     public totalActionInfo = {}
     private selectedObjectName: string = '';
     private selectedObjectParentName: any = "";
     public parentObjLabel = "";
     public fetchActionInfo: any = {}
     public parentObjValue = "";
     public childObjectsInfo;
     public headerDocItem: any = {};
     private prominentDataArray: Array < any > = [];
     private errorMessageToDisplay: string = 'No Records';
     private redirectUrl = "/";
     public showNavigationHistoryPopUp: Boolean = false;

     public WorkFlowUserApprovalStatusDataObject = {};
     private workFlowMapping = {
         "289221": "orderstatus",
     }

     private cscomponentactionInfo = {};
     private currentStatusWorkflowActionFiledId;

     public orderstatus_289221_defaultStatusValue = "Draft";
     public orderstatus_289221_defaultStatus = {};
     private orderstatus_289221_status = {};
     public orderstatus_289221_swList = {
         "Draft": [{
             "statusLabel": "Draft",
             "statusValue": "Draft",
             "statusType": "Start",
             "statusWFConfigId": 267,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "In Review",
             "statusValue": "In Review",
             "statusType": "Initiate",
             "statusWFConfigId": 268,
             "isApproveInitiateEnabled": "Y",
             "isForwardEnabled": "N"
         }],
         "In Review": [{
             "statusLabel": "In Review",
             "statusValue": "In Review",
             "statusType": "Initiate",
             "statusWFConfigId": 268,
             "isApproveInitiateEnabled": "Y",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Approved",
             "statusValue": "Approved",
             "statusType": "Approved",
             "statusWFConfigId": 269,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Rejected",
             "statusValue": "Rejected",
             "statusType": "Reject",
             "statusWFConfigId": 270,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Approved": [{
             "statusLabel": "Approved",
             "statusValue": "Approved",
             "statusType": "Approved",
             "statusWFConfigId": 269,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Confirmed",
             "statusValue": "Confirmed",
             "statusType": "Manual",
             "statusWFConfigId": 271,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Rejected": [{
             "statusLabel": "Rejected",
             "statusValue": "Rejected",
             "statusType": "Reject",
             "statusWFConfigId": 270,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Confirmed": [{
             "statusLabel": "Confirmed",
             "statusValue": "Confirmed",
             "statusType": "Manual",
             "statusWFConfigId": 271,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Processed",
             "statusValue": "Processed",
             "statusType": "Manual",
             "statusWFConfigId": 272,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Processed": [{
             "statusLabel": "Processed",
             "statusValue": "Processed",
             "statusType": "Manual",
             "statusWFConfigId": 272,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Shipped",
             "statusValue": "Shipped",
             "statusType": "Manual",
             "statusWFConfigId": 273,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Shipped": [{
             "statusLabel": "Shipped",
             "statusValue": "Shipped",
             "statusType": "Manual",
             "statusWFConfigId": 273,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Delivered": [{
             "statusLabel": "Delivered",
             "statusValue": "Delivered",
             "statusType": "Manual",
             "statusWFConfigId": 274,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }, {
             "statusLabel": "Completed",
             "statusValue": "Completed",
             "statusType": "Close",
             "statusWFConfigId": 276,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Cancelled": [{
             "statusLabel": "Cancelled",
             "statusValue": "Cancelled",
             "statusType": "Manual",
             "statusWFConfigId": 275,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }],
         "Completed": [{
             "statusLabel": "Completed",
             "statusValue": "Completed",
             "statusType": "Close",
             "statusWFConfigId": 276,
             "isApproveInitiateEnabled": "N",
             "isForwardEnabled": "N"
         }]
     };

     private approverType: string = "";
     private tableName_pfm144373 = "pfm144373";

     public navigationHistoryProperties = {
         'navigatedPagesNameArray': [],
         'navigatedPagesPathArray': [],
         'routerVisLinkTagName': "",
         'secondPreviousPage': "",
         'navigatedPagesLength': 0,
         'previousPage': "",
         'previousPageName': "",
         'secondPreviousPageName': "",
     };
     public layoutId = "60579_preview";
     public objectHierarchyJSON = {};
     public parentHierarchyJSON = {};
     public parentGridFetch;
     readonly headerHeight = 50;
     readonly rowHeight = 50;
     columnMode = ColumnMode;
     public isLoading;
     public gridTableFieldInfoArray: Array < FieldInfo > = [];
     constructor(platform: Platform, public lookupFieldMapping: lookupFieldMapping,
         public appUtilityConfig: appUtility, private screenOrientation: ScreenOrientation,
         public dataProvider: dataProvider, public offlineDbIndexCreation: offlineDbIndexCreation, public objectTableMapping: objectTableMapping,
         public loadingCtrl: LoadingController, public events: Events,
         public applicationRef: ApplicationRef, public router: Router,
         public activatRoute: ActivatedRoute, public toastCtrl: ToastController,
         public cspfmexecutionPouchDbProvider: cspfmExecutionPouchDbProvider,
         public executionDbConfigObject: cspfmExecutionPouchDbConfiguration,
         private datePipe: DatePipe,
         public alerCtrl: AlertController) {

         this.activatRoute.queryParams.subscribe(params => {
             if (Object.keys(params).length == 0 && params.constructor === Object) {
                 console.log("list query params skipped");
                 return
             }
             if (params["redirectUrl"]) {
                 this.redirectUrl = params["redirectUrl"]
             }
             this.headerDocItem = JSON.parse(params["parentObj"]);
             this.selectedObjectParentName = params["parentObjType"];
             this.parentTitle = params["parentLabel"];
             this.parentObjLabel = params["parentFieldLabel"];
             this.parentObjValue = params["parentFieldValue"];
             this.objectHierarchyJSON = JSON.parse(params["childObjectHierarchyJSON"]);
             this.parentHierarchyJSON = JSON.parse(params["parentHierarchyJSON"]);
             this.pageTitle = params["objLabel"];
             this.selectedObjectName = params["objType"];
             this.prominentDataArray = params["prominientObjectInfo"];
             this.appUtilityConfig.setEventSubscriptionlayoutIds(this.selectedObjectName, this.layoutId)


             if (params['gridInfoArray']) {
                 this.gridTableFieldInfoArray = JSON.parse(params['gridInfoArray']);
             }
             this.childObjectsInfo = [];
             if (this.headerDocItem["orderstatus"]) {
                 this.orderstatus_289221_defaultStatus = this.orderstatus_289221_swList[this.headerDocItem["orderstatus"]].filter(item => {
                     return item['statusValue'] == this.headerDocItem["orderstatus"]
                 })[0]
             } else {
                 this.orderstatus_289221_defaultStatus = this.orderstatus_289221_swList[this.orderstatus_289221_defaultStatusValue][0];
             }
             this.fetchSelectedObject();

         });


         const windowHeight = window.innerHeight;
         this.drawerComponentDockedHeight = windowHeight / 2;
         if (!this.appUtilityConfig.isMobile || this.appUtilityConfig.osType === "android") {
             this.isBrowser = true;
         }

         this.screenOrientation.onChange().subscribe(
             () => {
                 this.drawerComponentCurrentState = DrawerState.Bottom;
                 const windowHeightVal = window.innerHeight;
                 this.drawerComponentDockedHeight = windowHeightVal / 2;
             }
         );
         this.events.subscribe(this.layoutId, (modified) => {
             if (modified["dataProvider"] == "PouchDB") {
                 this.childObjectModifiedEventTrigger(modified);
             }
         });
         //  this.childObjectsInfo = [];
         //  this.fetchSelectedObject();

     }
     private headerFieldName = "";
     private headerFieldTranslateKey = "";
     public isSkeletonLoading = true;
     public formulaObject = {};
     public layoutDataRestrictionSet = {};
     public obj_pfm144373: any = {};
     private obj_pfm142973_289353: any = {};
     private obj_pfm147213_301933: any = {};
     private obj_pfm144393: any = {};
     public gridFieldInfo: {
         [key: string]: FieldInfo
     } = {
         "pfm147213_customercontact": {
             "child": {
                 "label": "",
                 "fieldName": "customercontact",
                 "prop": "customercontact",
                 "fieldType": "TEXT",
                 "child": "",
                 "dateFormat": "",
                 "mappingDetails": "",
                 "currencyDetails": ""
             },
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": "",
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.customermaster.customercontact",
             "prop": "pfm147213_301933.customercontact",
             "fieldName": "pfm147213_301933",
             "fieldType": "LOOKUP"
         },
         "pfm147213_customernumber": {
             "child": {
                 "label": "",
                 "fieldName": "customernumber",
                 "prop": "customernumber",
                 "fieldType": "TEXT",
                 "child": "",
                 "dateFormat": "",
                 "mappingDetails": "",
                 "currencyDetails": ""
             },
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": "",
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.customermaster.customernumber",
             "prop": "pfm147213_301933.customernumber",
             "fieldName": "pfm147213_301933",
             "fieldType": "LOOKUP"
         },
         "pfm144373_orderstatus": {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorders.orderstatus",
             "fieldName": "orderstatus",
             "prop": "orderstatus",
             "fieldType": "DROPDOWN",
             "child": "",
             "dateFormat": "",
             "mappingDetails": {
                 "Draft": "Draft",
                 "In Review": "In Review",
                 "Approved": "Approved",
                 "Rejected": "Rejected",
                 "Confirmed": "Confirmed",
                 "Processed": "Processed",
                 "Shipped": "Shipped",
                 "Delivered": "Delivered",
                 "Completed": "Completed",
                 "Cancelled": "Cancelled"
             },
             "currencyDetails": ""
         },
         "pfm147213_accountname": {
             "child": {
                 "label": "",
                 "fieldName": "accountname",
                 "prop": "accountname",
                 "fieldType": "TEXT",
                 "child": "",
                 "dateFormat": "",
                 "mappingDetails": "",
                 "currencyDetails": ""
             },
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": "",
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.customermaster.accountname",
             "prop": "pfm147213_301933.accountname",
             "fieldName": "pfm147213_301933",
             "fieldType": "LOOKUP"
         },
         "pfm144373_billtoaddress": {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorders.billtoaddress",
             "fieldName": "billtoaddress",
             "prop": "billtoaddress",
             "fieldType": "TEXTAREA",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm144373_customername": {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorders.customername",
             "prop": "pfm147213_301933.customername",
             "fieldName": "pfm147213_301933",
             "fieldType": "LOOKUP",
             "child": {
                 "child": "",
                 "dateFormat": "",
                 "mappingDetails": "",
                 "currencyDetails": "",
                 "label": "accountno",
                 "prop": "accountno",
                 "fieldName": "accountno",
                 "fieldType": "AUTONUMBER"
             },
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm144373_orderid": {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorders.orderid",
             "fieldName": "orderid",
             "prop": "orderid",
             "fieldType": "AUTONUMBER",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm144373_ordercompleteddate": {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorders.ordercompleteddate",
             "fieldName": "ordercompleteddate",
             "prop": "ordercompleteddate",
             "fieldType": "DATE",
             "child": "",
             "dateFormat": this.appUtilityConfig.userDateFormat,
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm144373_orderconfirmeddate": {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorders.orderconfirmeddate",
             "fieldName": "orderconfirmeddate",
             "prop": "orderconfirmeddate",
             "fieldType": "DATE",
             "child": "",
             "dateFormat": this.appUtilityConfig.userDateFormat,
             "mappingDetails": "",
             "currencyDetails": ""
         },
         "pfm144373_shiptoaddress": {
             "label": "Saleorderview_MOBILE_Grid_with_List.Element.crmsaleorders.shiptoaddress",
             "fieldName": "shiptoaddress",
             "prop": "shiptoaddress",
             "fieldType": "TEXTAREA",
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": ""
         }
     };
     public formulaReverseObjectHierarchyJSON = undefined;
     public formulaConfigJSON = {};
     public formulafields = {};
     public dbServiceProvider = appConstant.pouchDBStaticName;
     private orderstatus_289221 = {
         'Draft': 'Draft',
         'In Review': 'In Review',
         'Approved': 'Approved',
         'Rejected': 'Rejected',
         'Confirmed': 'Confirmed',
         'Processed': 'Processed',
         'Shipped': 'Shipped',
         'Delivered': 'Delivered',
         'Completed': 'Completed',
         'Cancelled': 'Cancelled'
     };
     navigateObjectBaseDetailPage(itemTapNavigationParams, options) {
         if (this.selectedObjectName == 'pfm144393') {
             if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/Lineitemviewpage1_MOBILE_Grid")) {
                 itemTapNavigationParams['redirectUrl'] = "/menu/Saleorderview_MOBILE_Grid_with_Listpreview"
             }
             // this.navCtrl.push(Lineitemviewpage1_MOBILE_Grid, itemTapNavigationParams, options)
             this.router.navigate(["/menu/Lineitemviewpage1_MOBILE_Grid"], {
                 queryParams: itemTapNavigationParams,
                 skipLocationChange: true
             });
         }
     }
     childObjectModifiedEventTrigger(modified) {
         const modifiedData = this.dataProvider.convertRelDocToNormalDoc(modified);
         this.fetchModifiedRec(modifiedData);
     }

     async fetchModifiedRec(modifiedData) {
         const additionalObjectdata = {};
         additionalObjectdata['id'] = modifiedData['id'];
         const fetchParams = {
             'objectHierarchyJSON': this.objectHierarchyJSON,
             'dataSource': appConstant.pouchDBStaticName,
             'additionalInfo': additionalObjectdata
         }

         this.dataProvider.querySingleDoc(fetchParams).then(result => {


             if (result["status"] != "SUCCESS") {
                 this.errorMessageToDisplay = result["message"];
                 return;
             }
             const modifiedRec = result["records"][0];
             const eventsTriggeredindex = this.getChangedObjectIndex(
                 this.childObjectsInfo,
                 modifiedRec,
                 "id"
             );
             if (eventsTriggeredindex > -1) {
                 this.childObjectsInfo.splice(eventsTriggeredindex, 1);
             }


             this.childObjectsInfo.push(modifiedRec);
             if (this.appUtilityConfig.isMobileResolution === false) {
                 this.childObjectsInfo = [...this.childObjectsInfo]
             }
             console.log("this.childDocArray", this.childObjectsInfo);
             this.recentListRefresh();
         }).catch(error => {
             console.log(error);
         });
     }
     recentListRefresh() {
         if (this.virtualScroll) {
             this.virtualScroll.checkRange(0)
         }
     }

     getChangedObjectIndex(array, modifiedData, key) {
         console.log("modifiedData 123", modifiedData);
         return lodash.findIndex(
             array,
             item => {
                 return item[key] === modifiedData[key];
             },
             0
         );
     }
     navigateObjectBaseEntryPage(addNavigationParams, options) {
         if (this.selectedObjectName == 'pfm144393') {
             if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/LineitemCreatepage_MOBILE_Grid")) {
                 addNavigationParams['redirectUrl'] = "/menu/Saleorderview_MOBILE_Grid_with_Listpreview"
             }
             // this.navCtrl.push(LineitemCreatepage_MOBILE_Grid, addNavigationParams, options)
             this.router.navigate(["/menu/LineitemCreatepage_MOBILE_Grid"], {
                 queryParams: addNavigationParams,
                 skipLocationChange: true
             });
         }
     }
     async fetchSelectedObject(event ? ) {
         // selectedObject no need in this method as a passing variable

         const additinoalObjectdata = {};
         additinoalObjectdata["parentType"] = this.selectedObjectParentName;
         additinoalObjectdata["parentId"] = this.headerDocItem["id"];

         const fetchParams = {
             'objectHierarchyJSON': this.objectHierarchyJSON,
             'additionalInfo': additinoalObjectdata,
             'dataSource': appConstant.pouchDBStaticName,
             'layoutDataRestrictionSet': this.layoutDataRestrictionSet
         }

         this.dataProvider.queryChildDataBatchwise(fetchParams).then(result => {
             this.isSkeletonLoading = false
             if (event != undefined) {
                 event.target.complete();
                 this.virtualScroll.checkEnd();
             }
             if (result["status"] !== "SUCCESS") {
                 this.errorMessageToDisplay = result["message"];
                 if (this.errorMessageToDisplay === "No internet") {
                     this.presentNoInternetToast();
                 }
                 return;
             }
             const pluralName = this.dataProvider.getPluralName(this.tableName_pfm144373);
             this.obj_pfm144373 = result['records'][0];

             //   if (this.obj_pfm144373[pluralName][0]) {
             //     this.obj_pfm144373 = this.obj_pfm144373[pluralName][0];
             //   }
             if (result['records'].length > 0) {
                 Array.prototype.push.apply(this.childObjectsInfo, result['records']);
             }
             // this.childObjectsInfo = result["records"];

             this.applicationRef.tick();

         }).catch(error => {
             this.isSkeletonLoading = false;
             console.log(error)
         });
     }

     async presentNoInternetToast() {
         const toast = await this.toastCtrl.create({
             message: "No internet connection. Please check your internet connection and try again.",
             showCloseButton: true,
             closeButtonText: "retry"
         });
         toast.onDidDismiss().then(() => {
             toast.dismiss();
             this.refreshData();
         });
         toast.present();
     }

     refreshData() {
         this.fetchSelectedObject();
     }
     historyButtonAction(event: Event) {
         this.showNavigationHistoryPopUp = !this.showNavigationHistoryPopUp;
         event.stopPropagation();
     }
     @HostListener('click') onClick() {
         this.showNavigationHistoryPopUp = false;
     }
     getNavigationHistory() {
         if (this.appUtilityConfig.getHomePageNode()) {
             let homePageNode = this.appUtilityConfig.getHomePageNode()
             this.navigationHistoryProperties['navigatedPagesNameArray'] = [homePageNode['homePageNodeName']]
             this.navigationHistoryProperties['navigatedPagesPathArray'] = [homePageNode['homePageNodepath']]
         }
         this.navigationHistoryProperties['navigatedPagesLength'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes.length;
         this.navigationHistoryProperties['previousPageName'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 2].childNodes[0].textContent;
         this.navigationHistoryProperties['previousPage'] = ( < HTMLElement > document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 2]).tagName.toLowerCase();
         if (document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3]) {
             this.navigationHistoryProperties['secondPreviousPageName'] = document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3].childNodes[0].textContent;
             this.navigationHistoryProperties['secondPreviousPage'] = ( < HTMLElement > document.getElementsByTagName('ion-router-outlet')[1].childNodes[this.navigationHistoryProperties['navigatedPagesLength'] - 3]).tagName.toLowerCase();
         }
         for (let i = 0; i < this.navigationHistoryProperties['navigatedPagesLength'] - 3; i++) {
             this.navigationHistoryProperties['navigatedPagesNameArray'].push(document.getElementsByTagName('ion-router-outlet')[1].childNodes[i].childNodes[0].textContent);
             this.navigationHistoryProperties['navigatedPagesPathArray'].push(document.getElementsByTagName('ion-router-outlet')[1].children[i].tagName.toLowerCase());
         }
     }
     histListNav(e) {
         if (e.currentTarget.getAttribute('data-index') == 'app-homepage') {
             this.appUtilityConfig.navigateToHomepage();
         } else {
             this.router.navigate([`/menu/${e.currentTarget.getAttribute('data-index')}`]);
         }
     }
     previousPageNavigation() {
         this.router.navigate(["/menu/" + this.navigationHistoryProperties['previousPage']]);
     }
     secondPreviousPageNavigation() {
         this.router.navigate(["/menu/" + this.navigationHistoryProperties['secondPreviousPage']]);
     }
     ngOnInit() {
         this.getNavigationHistory()
     }

     statusChange(event, selectedStatusField) {
         if (selectedStatusField == undefined) {
             selectedStatusField = {}
         }

         selectedStatusField['statusLabel'] = event['selectedStatus']['statusLabel'];
         selectedStatusField['statusValue'] = event['selectedStatus']['statusValue'];
         selectedStatusField['statusType'] = event['selectedStatus']['statusType'];
         this.approveAction(selectedStatusField, event['workFlowUserApprovalStatusDataObject'])
     }

     //Get Approval State from Component	
     getApprovalState(event) {
         this.approverType = event['approverType']
     }

     fetchLockedUserDetail() {
         var systemAttributes = this.obj_pfm144373['systemAttributes']
         var userId = systemAttributes['lockedBy']
         var date = new Date(systemAttributes['lockedDate']);
         this.showAlert(userId + " has locked for " + this.workFlowMapping[this.obj_pfm144373['systemAttributes']['fieldId']] + " on " + this.datePipe.transform(date, this.appUtilityConfig.userDateFormat));
     }
     approveAction(selectedStatusField, workFlowUserApprovalStatusDataObject) {
         this.WorkFlowUserApprovalStatusDataObject = workFlowUserApprovalStatusDataObject;
         this.WorkFlowUserApprovalStatusDataObject['lastmodifiedby'] = this.appUtilityConfig.userId
         var userObjectList = this.WorkFlowUserApprovalStatusDataObject['approvalStatus'].filter(userDataObject => userDataObject.userId == this.appUtilityConfig.userId);
         var userObject = userObjectList[0]

         //   userObject['userId'] = this.appUtilityConfig.userId
         userObject['userName'] = this.appUtilityConfig.loggedUserName
         userObject['userLevel'] = ""
         userObject['statusValue'] = selectedStatusField['statusValue']
         userObject['statusType'] = selectedStatusField['statusType']
         userObject['approvalExecutionStatus'] = "INPROGRESS"
         userObject['execStatusMessage'] = ""
         userObject['comment'] = ""
         this.cspfmexecutionPouchDbProvider.save(this.executionDbConfigObject.workFlowUserApprovalStatusObject,
             this.WorkFlowUserApprovalStatusDataObject).then(result => {

             if (result['status'] != 'SUCCESS') {
                 alert("failed")

                 return;
             }
             this.presentToast("data saved sucessfully")
         })
     }
     async showAlert(messageContent) {

         var customAlert = await this.alerCtrl.create({
             backdropDismiss: false,
             message: messageContent,
             buttons: [{
                 text: "OK",
                 cssClass: "method-color",
                 handler: () => {}
             }]
         });
         customAlert.present();
     }
     async presentToast(message) { //ch
         const toast = await this.toastCtrl.create({
             message: message,
             duration: 2000,
             position: 'bottom'
         });
         toast.onDidDismiss().then((res) => {
             toast.dismiss()
         });
         toast.present();
     };


     ionViewDidLoad() {

     }

     ngOnDestroy() {
         this.events.unsubscribe(this.layoutId);
         this.appUtilityConfig.removeEventSubscriptionlayoutIds(this.selectedObjectName, this.layoutId);
     }

     ionViewWillEnter() {
         document.body.setAttribute('class', 'linelistinner');
         this.drawerComponentCurrentState = DrawerState.Top;
         this.drawerComponentPreviousState = DrawerState.Docked;
         for (let i = 0; i < document.getElementsByClassName('cs-divider-lv').length; i++) {
             document.getElementsByClassName('cs-divider-lv')[i].classList.remove('bottom');
             document.getElementsByClassName('cs-divider-lv')[i].classList.remove('center');
             document.getElementsByClassName('cs-divider-lv')[i].classList.add('top');
         }
     }
     ionViewDidEnter() {
         // this.service.changeToStyleTwo();
         // this.service.changeToStyleOne();
         var pvheader = document.querySelector('.header-listpreview-sub-header');
         pvheader.setAttribute('color', "var(--ion-color-primary, #3880ff)");

         var pvSubhItem = document.querySelector('.header-listpreview-sub-header ion-item');
         pvSubhItem.setAttribute('color', "var(--ion-color-primary, #3880ff)");

         var pvTitleHd = document.querySelectorAll('.header-listtitle-card-item h3');
         var pvTitleHdLen = pvTitleHd.length;
         for (var i = 0; i < pvTitleHdLen; i++) {
             pvTitleHd[i].setAttribute('color', "var(--ion-color-primary, #3880ff)");
         }
         var pvScrollIcon = document.querySelectorAll('ion-item-divider ion-icon');
         var pvScrollIconLen = pvScrollIcon.length;
         for (var i = 0; i < pvScrollIconLen; i++) {
             pvScrollIcon[i].setAttribute('color', "var(--ion-color-primary, #3880ff)");
         }
         var pvHdItembg = document.querySelectorAll('.header-listpreview-sub-header ion-badge');
         var pvHdItembgLen = pvHdItembg.length;
         for (var i = 0; i < pvHdItembgLen; i++) {
             pvHdItembg[i].setAttribute('background', "$cvar");
         }
         if (this.virtualScroll) {
             this.virtualScroll.checkRange(0)
         }
         if (this.appUtilityConfig.isMobileResolution === false) {
             this.childObjectsInfo = [...this.childObjectsInfo]
         }
     }

     ionViewWillLeave() {
         this.drawerComponentCurrentState = DrawerState.Bottom
     }


     addButton_elementId_Onclick() {
         if (this.selectedObjectName === "pfm144373") {
             if (this.childObjectsInfo && this.childObjectsInfo.length >= 1) {
                 alert("You cannot add more than one additional info...");
                 return;
             }
         }
         const options = {
             animate: false
         }
         const addNavigationParams = {
             action: "Add",
             parentId: this.headerDocItem["id"],
             parentFieldLabel: this.parentObjLabel,
             parentFieldValue: this.parentObjValue,
             parentPage: this,
             parentName: this.selectedObjectParentName
         }
         this.navigateObjectBaseEntryPage(addNavigationParams, options)
     }
     undefined
     onItemTap(childItem) {
         const options = {
             animate: false
         }
         const itemTapNavigationParams = {
             //  parentObj: JSON.stringify(this.headerDocItem),
             parentFieldLabel: this.parentObjLabel,
             parentFieldValue: this.parentObjValue,
             parentName: this.selectedObjectParentName,
             id: childItem["id"],
             parentTitle: this.parentTitle,
             prominientObjectInfo: this.prominentDataArray
         }
         this.navigateObjectBaseDetailPage(itemTapNavigationParams, options)
     }
     drawerButtonOnclick() {
         if (this.drawerComponentCurrentState == DrawerState.Bottom) {

             this.drawerComponentCurrentState = DrawerState.Docked;
             this.drawerComponentPreviousState = DrawerState.Bottom;
             for (let i = 0; i < document.getElementsByClassName('cs-divider-lv').length; i++) {
                 document.getElementsByClassName('cs-divider-lv')[i].classList.remove('bottom');
                 document.getElementsByClassName('cs-divider-lv')[i].classList.remove('top');
                 document.getElementsByClassName('cs-divider-lv')[i].classList.add('center');
             }

         } else if (this.drawerComponentCurrentState == DrawerState.Docked) {

             if (this.drawerComponentPreviousState == DrawerState.Bottom) {
                 this.drawerComponentCurrentState = DrawerState.Top;
                 for (let i = 0; i < document.getElementsByClassName('cs-divider-lv').length; i++) {
                     document.getElementsByClassName('cs-divider-lv')[i].classList.remove('bottom');
                     document.getElementsByClassName('cs-divider-lv')[i].classList.remove('center');
                     document.getElementsByClassName('cs-divider-lv')[i].classList.add('top');
                 }


             } else {
                 this.drawerComponentCurrentState = DrawerState.Bottom;
                 for (let i = 0; i < document.getElementsByClassName('cs-divider-lv').length; i++) {
                     document.getElementsByClassName('cs-divider-lv')[i].classList.remove('center');
                     document.getElementsByClassName('cs-divider-lv')[i].classList.remove('top');
                     document.getElementsByClassName('cs-divider-lv')[i].classList.add('bottom');
                 }


             }
             this.drawerComponentPreviousState = DrawerState.Docked;

         } else if (this.drawerComponentCurrentState == DrawerState.Top) {

             this.drawerComponentCurrentState = DrawerState.Docked;
             this.drawerComponentPreviousState = DrawerState.Top;
             for (let i = 0; i < document.getElementsByClassName('cs-divider-lv').length; i++) {
                 document.getElementsByClassName('cs-divider-lv')[i].classList.remove('bottom');
                 document.getElementsByClassName('cs-divider-lv')[i].classList.remove('top');
                 document.getElementsByClassName('cs-divider-lv')[i].classList.add('center');
             }


         }
     }

     backButtonOnclick() {
         this.router.navigate([this.redirectUrl], {
             skipLocationChange: true
         });
     }

     async onScroll(event) {
         const offsetY = event.offsetY;
         // total height of all rows in the viewport
         const offsetHeight = document.getElementById("table").offsetHeight; // this.el.nativeElement.getBoundingClientRect().height - this.headerHeight + 0;
         const viewHeight = offsetHeight - this.headerHeight;

         // check if we scrolled to the end of the viewport
         if (!this.isLoading && offsetY + viewHeight >= this.childObjectsInfo.length * this.rowHeight) {
             this.fetchSelectedObject();
         }
     }

     onTableEventChanged(event) {
         if (event["type"] === "click") {
             this.onItemTap(event["row"]);
         }
     }
 }