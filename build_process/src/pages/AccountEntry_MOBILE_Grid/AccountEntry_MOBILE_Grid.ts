 import {
     Component,
     ViewChild,
     OnInit,
     ApplicationRef,
     ChangeDetectorRef,
     HostListener,
     NgZone
 } from '@angular/core';
 import {
     dbProvider
 } from 'src/core/db/dbProvider';
 import {
     offlineDbIndexCreation
 } from 'src/core/utils/offlineDbIndexCreation';
 import {
     dataProvider
 } from 'src/core/utils/dataProvider';
 import {
     appConstant
 } from 'src/core/utils/appConstant';

 import * as lodash from 'lodash';
 import {
     ToastController,
     ModalController,
     AlertController,
     Platform,
     IonContent,
     LoadingController
 } from '@ionic/angular';
 import {
     lookuppage
 } from 'src/core/pages/lookuppage/lookuppage';
 import {
     FormBuilder,
     Validators,
     FormGroup
 } from '@angular/forms';
 import {
     appUtility
 } from 'src/core/utils/appUtility';
 import * as moment from 'moment';
 import 'moment-timezone';
 import {
     Router,
     ActivatedRoute
 } from '@angular/router';
 import {
     lookupFieldMapping
 } from 'src/core/pfmmapping/lookupFieldMapping';
 import {
     objectTableMapping
 } from 'src/core/pfmmapping/objectTableMapping';
 import * as _ from 'underscore';
 import * as Quill from 'quill';
 import {
     cspfmFieldTrackingMapping
 } from 'src/core/pfmmapping/cspfmFieldTrackingMapping';
 import {
     Http,
     Headers
 } from '@angular/http';
 import {
     dbConfiguration
 } from 'src/core/db/dbConfiguration';
 @Component({
     selector: 'AccountEntry_MOBILE_Grid',
     templateUrl: './AccountEntry_MOBILE_Grid.html'
 }) export class AccountEntry_MOBILE_Grid implements OnInit {
     @ViewChild(IonContent) contentArea: IonContent;
     private obj_pfm147213: any = {};
     public ionDateTimeDisplayValue = {};
     private obj_pfm147213_Temp: any = {};
     public formGroup: FormGroup;
     private customAlert;
     public action = 'Add';
     private id: any = {};
     private tableName_pfm147213 = 'pfm147213';
     public isSaveActionTriggered: Boolean = false;
     private isValidFrom: Boolean = true;
     public dbServiceProvider = appConstant.pouchDBStaticName;
     public selectedDataObject: any = {};
     public formulaObject = {};
     public isFieldTrackingEnable: Boolean = false;
     private showAlert: boolean = false;
     infoMessage = '';
     aniClassAddorRemove: boolean = false;
     private workFlowInitiateList = {};
     public savedSuccessMessage = 'data saved sucessfully';
     public updatedSuccessMessage = 'data updated sucessfully';
     public savedErrorMessage = 'Error saving record';
     public fetchErrorMessage = 'Fetching Failed';
     private dependentNumberCount = {};
     public partiallySavedSuccessMessage = 'data partially saved';
     private backButtonSubscribeRef;
     private unRegisterBackButtonAction: Function;
     public parentObjLabel = '';
     public parentObjValue = '';
     private errorMessageToDisplay: string = 'No Records';
     private parentName = '';
     private parentId = '';
     public isParentObjectShow = false;
     public isSkeletonLoading = true;
     isViewRunning = false;
     loading;
     private objectHierarchyJSON = {
         "objectName": "customermaster",
         "objectType": "PRIMARY",
         "relationShipType": null,
         "fieldId": "0",
         "objectId": "147213",
         "childObject": []
     };
     public isFromMenu = false;
     private redirectUrl = '/';
     private dependentFieldTriggerList = {};
     private geoLocationdependentField = {};
     public pickListValues = {};
     public parentObject: any = {};
     public formulaConfigJSON = {};
     public formulafields = {};
     childObjectList = [];
     objResultMap = new Map < string, any > ();
     objDisplayName = {
         'pfm147213': 'customermaster',
     };
     recordEntryValidation() {
         const formGroupObjectValue = Object.assign({}, this.formGroup.value);
         this.obj_pfm147213 = Object.assign(this.obj_pfm147213, formGroupObjectValue.pfm147213);
         if (
             this.obj_pfm147213["taxregistrationnumber"] || this.obj_pfm147213["paymentterms"] || this.obj_pfm147213["accounttype"] || this.obj_pfm147213["creditlimitrequested"] || this.obj_pfm147213[""] || this.obj_pfm147213["accountname"] || this.obj_pfm147213["region"] || this.obj_pfm147213[""] || this.obj_pfm147213["status"] || this.obj_pfm147213["dunsnumber"] || this.obj_pfm147213["accountcurrency"] || this.obj_pfm147213["businessunits"] || this.obj_pfm147213["accountdescription"] || this.obj_pfm147213["taxexempted"] || this.obj_pfm147213["accountno"] || this.obj_pfm147213["accountno"]) {
             return true;
         } else {
             return false;
         }
     }
     constructor(public loadingCtrl: LoadingController, public dbConfiguration: dbConfiguration, public http: Http, public router: Router, public activatRoute: ActivatedRoute, public applicationRef: ApplicationRef, public alerCtrl: AlertController, public modalCtrl: ModalController, public dbService: dbProvider, public dataProvider: dataProvider, public formBuilder: FormBuilder, public appUtility: appUtility, public objectTableMapping: objectTableMapping, public lookupFieldMapping: lookupFieldMapping, private toastCtrl: ToastController, public platform: Platform, public fieldTrackMapping: cspfmFieldTrackingMapping, private ngZone: NgZone) {
         this.activatRoute.queryParams.subscribe(params => {
             if (Object.keys(params).length == 0 && params.constructor === Object) {
                 console.log("list query params skipped");
                 return
             }
             if (params["isFromMenu"]) {
                 this.isFromMenu = params["isFromMenu"];
             }
             if (params["redirectUrl"]) {
                 this.redirectUrl = params["redirectUrl"]
             }
             this.action = params['action'];
             this.parentId = params['parentId'];
             this.id = params['id'];
             this.parentName = params['parentName'];
             if (this.parentId) {
                 this.isParentObjectShow = true;
                 this.parentObjLabel = params['parentFieldLabel'];
                 this.parentObjValue = params['parentFieldValue'];
             }
             this.initializeObjects(dataProvider.tableStructure());
             if (this.action == 'Edit') {
                 this.id = params['id'];
                 this.fetchRecordAgainstSelectedObject();
             } else {
                 this.isSkeletonLoading = false;
                 this.checkboxInitialization();
             }
         });
         this.createFormGroup();
         this.hardWareBackButtonAction();
     }
     initializeObjects(tableStructure) {
         this.obj_pfm147213 = JSON.parse(JSON.stringify(tableStructure.pfm147213));
     }
     fetchRecordAgainstSelectedObject() {
         const additionalObjectdata = {};
         additionalObjectdata['id'] = this.id;
         const fetchParams = {
             'objectHierarchyJSON': this.objectHierarchyJSON,
             'additionalInfo': additionalObjectdata,
             'dataSource': appConstant.pouchDBStaticName
         };
         this.dataProvider.querySingleDoc(fetchParams).then(res => {
             this.isSkeletonLoading = false;
             if (res['status'] !== 'SUCCESS') {
                 this.errorMessageToDisplay = res['message'];
                 if (this.errorMessageToDisplay == 'No internet') {
                     this.presentNoInternetToast();
                 }
                 return;
             }
             if (res['records'].length < 0) {
                 console.log('FetchRecordAgainstSelectedObject No Records');
                 return;
             }
             let dataObj = res['records'][0];
             this.obj_pfm147213_Temp = lodash.extend({}, this.obj_pfm147213, dataObj);
             this.selectedDataObject['pfm147213'] = JSON.stringify(this.obj_pfm147213_Temp);
             this.obj_pfm147213 = lodash.extend({}, this.obj_pfm147213, dataObj);
             this.formGroup.patchValue({
                 pfm147213: this.obj_pfm147213
             });
             this.applicationRef.tick();
         }).catch(error => {
             this.isSkeletonLoading = false;
             this.showInfoAlert(this.fetchErrorMessage)
         })
     }
     updateGeoLocationFlag(objectName, dataObj, existingDataObj) {
         Object.keys(this.geoLocationdependentField[objectName]).forEach(element => {
             const depFlds = this.geoLocationdependentField[objectName][element]['dependentFields'];
             depFlds.forEach(fieldName => {
                 if (this.action == 'Edit') {
                     if (existingDataObj[fieldName] != dataObj[fieldName]) {
                         dataObj[this.geoLocationdependentField[objectName][element]['dependentFieldName']] = 'Y';
                         return;
                     }
                 } else {
                     if (dataObj[fieldName] != '') {
                         dataObj[this.geoLocationdependentField[objectName][element]['dependentFieldName']] = 'Y';
                         return;
                     }
                 }
             })
         });
     }
     async presentNoInternetToast() {
         const toast = await this.toastCtrl.create({
             message: "No internet connection. Please check your internet connection and try again.",
             showCloseButton: true,
             closeButtonText: "retry"
         });
         toast.onDidDismiss().then(() => {
             toast.dismiss();
             this.refreshData();
         });
         toast.present();
     }

     refreshData() {
         this.fetchRecordAgainstSelectedObject();
     }
     async saveButtonOnclick() {
         this.formGroup.patchValue({
             pfm147213: {
                 taxregistrationnumber: this.obj_pfm147213['taxregistrationnumber'],
                 paymentterms: this.obj_pfm147213['paymentterms'],
                 accounttype: this.obj_pfm147213['accounttype'],
                 creditlimitrequested: this.obj_pfm147213['creditlimitrequested'],
                 accountname: this.obj_pfm147213['accountname'],
                 region: this.obj_pfm147213['region'],
                 status: this.obj_pfm147213['status'],
                 dunsnumber: this.obj_pfm147213['dunsnumber'],
                 accountcurrency: this.obj_pfm147213['accountcurrency'],
                 businessunits: this.obj_pfm147213['businessunits'],
                 accountdescription: this.obj_pfm147213['accountdescription'],
                 taxexempted: this.obj_pfm147213['taxexempted'],
                 accountno: this.obj_pfm147213['accountno']
             }
         });
         if (this.formGroup.valid) {
             this.formGroup.patchValue({});
             this.isValidFrom = true;
             if (this.obj_pfm147213['creditlimitrequested'] !== null) {
                 this.obj_pfm147213['creditlimitrequested'] = Number(this.obj_pfm147213['creditlimitrequested']);
             }
             const fieldTrackObject = this.fieldTrackMapping.mappingDetail[this.tableName_pfm147213]
             if (fieldTrackObject) {
                 this.isFieldTrackingEnable = true;
             } else {
                 this.isFieldTrackingEnable = false;
             }
             let previousParentObject
             if (this.action === "Edit") {
                 previousParentObject = this.selectedDataObject[this.tableName_pfm147213];
                 console.log("previousParentObject = ", previousParentObject);
             } else {
                 previousParentObject = undefined
             }
             if (this.parentId) {
                 this.obj_pfm147213[this.parentName] = this.parentId;
             };
             this.dataProvider.save(this.tableName_pfm147213, this.obj_pfm147213, appConstant.pouchDBStaticName, previousParentObject, this.isFieldTrackingEnable)
                 .then(result => {
                     if (result['status'] != 'SUCCESS') {
                         this.showInfoAlert(result['message']);
                         return;
                     }
                     if (this.childObjectList.length == 0) {
                         this.presentToast(this.savedSuccessMessage);
                         let opts = {
                             animate: false
                         };
                         this.navigatePopUpAction();
                         return
                     };

                 })
                 .catch(error => {
                     this.showInfoAlert(this.savedErrorMessage);
                     console.log(error)
                 });
         } else {
             this.formGroup.patchValue({});
             this.isValidFrom = false;
             this.scrollToValidationFailedField();
         }
         var errorValue = document.querySelector('.entry-page-content');
         errorValue.setAttribute('class', 'entry-page-content entryErrorMessage hydrated');
     };
     scrollToValidationFailedField() {
         let formControls = this.formGroup.controls.pfm147213;
         let formGroupKeys = Object.keys(formControls["controls"]);
         let isValidationSucceed = true;
         formGroupKeys.every(element => {
             if (formControls["controls"][element].status == "INVALID") {
                 let yOffset = document.getElementById("pfm147213_" + element).offsetTop;
                 this.contentArea.scrollToPoint(0, yOffset, 1000);
                 isValidationSucceed = false;
                 return false;
             } else {
                 return true;
             }
         })
     };
     createFormGroup() {
         this.formGroup = this.formBuilder.group({
             pfm147213: this.formBuilder.group({
                 taxregistrationnumber: [null, Validators.compose([])],
                 paymentterms: [null, Validators.compose([])],
                 accounttype: [null, Validators.compose([])],
                 creditlimitrequested: [null, Validators.compose([])],
                 accountname: [null, Validators.compose([])],
                 region: [null, Validators.compose([])],
                 status: [null, Validators.compose([])],
                 dunsnumber: [null, Validators.compose([])],
                 accountcurrency: [null, Validators.compose([])],
                 businessunits: [null, Validators.compose([])],
                 accountdescription: [null, Validators.compose([])],
                 taxexempted: [null, Validators.compose([])]
             })
         });
     }
     lookupResponse(objectname, selectedValue) {}
     loadCheckboxEditValues(fieldName, values) {}
     loadDefaultValues() {}
     resetChildDependentInfo(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName) {}
     ionViewDidEnter() {}
     checkboxInitialization() {};
     backButtonOnclick() {
         if (this.recordEntryValidation()) {
             this.recordDiscardConfirmAlert();
         } else {
             this.navigatePopUpAction();
         }
     };
     navigatePopUpAction() {
         this.router.navigateByUrl(this.redirectUrl, {
             skipLocationChange: true
         });
     };
     hardWareBackButtonAction() {
         this.backButtonSubscribeRef = this.platform.backButton.subscribeWithPriority(999999, () => {
             if (this.customAlert) {
                 return;
             }
             this.backButtonOnclick();
         });
     }
     lookupClearAction(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName) {
         dataObj[dataObjectFieldName] = null;
         delete looklUpObj["id"];
         delete looklUpObj[lookupObjectFieldName];
         this.formGroup.value.formControlerName = null;
         this.resetChildDependentInfo(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName)
     }
     async recordDiscardConfirmAlert() {
         this.customAlert = await this.alerCtrl.create({
             backdropDismiss: false,
             message: 'Are you sure want to leave this page?',
             buttons: [{
                 text: 'Cancel',
                 cssClass: 'method-color',
                 handler: () => {
                     this.customAlert = null;
                     console.log('Individual clicked');
                 }
             }, {
                 text: 'Yes',
                 cssClass: 'method-color',
                 handler: () => {
                     this.navigatePopUpAction();
                 }
             }]
         });
         this.customAlert.present();
     }
     async presentToast(message) {
         const toast = await this.toastCtrl.create({
             message: message,
             duration: 2000,
             position: 'bottom'
         });
         toast.onDidDismiss().then((res) => {
             this.refreshButtonPressed();
         });
         toast.present();
     }
     refreshButtonPressed() {
         this.fetchRecordAgainstSelectedObject();
     }
     async showInfoAlert(info) {
         this.alerCtrl.create({
             message: info,
             subHeader: '',
             buttons: [{
                 text: 'Ok',
                 handler: () => {
                     console.log('Confirm Okay');
                 }
             }]
         }).then(alert => alert.present());
     }
     closeInfoAlert() {
         this.aniClassAddorRemove = false;
         setTimeout(() => {
             this.showAlert = false;
             this.infoMessage = '';
         }, 500);
     };
     onQuillSelectionChanged() {
         var Link = Quill.import('formats/link');
         Link.sanitize = function(url) {
             let protocol = url.slice(0, url.indexOf(':'));
             if (this.PROTOCOL_WHITELIST.indexOf(protocol) === -1) {
                 url = 'http://' + url;
             }
             let anchor = document.createElement('a');
             anchor.href = url;
             protocol = anchor.href.slice(0, anchor.href.indexOf(':'));
             return (this.PROTOCOL_WHITELIST.indexOf(protocol) > -1) ? url : this.SANITIZED_URL;
         }
         Quill.register(Link, true);
     }
     ionViewWillLeave() {
         this.backButtonSubscribeRef.unsubscribe();
     }
     ngOnInit() {}
 }