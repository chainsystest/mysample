 import {
     Component,
     ViewChild,
     OnInit,
     ApplicationRef,
     ChangeDetectorRef,
     HostListener,
     NgZone
 } from '@angular/core';
 import {
     dbProvider
 } from 'src/core/db/dbProvider';
 import {
     offlineDbIndexCreation
 } from 'src/core/utils/offlineDbIndexCreation';
 import {
     dataProvider
 } from 'src/core/utils/dataProvider';
 import {
     appConstant
 } from 'src/core/utils/appConstant';

 import * as lodash from 'lodash';
 import {
     ToastController,
     ModalController,
     AlertController,
     Platform,
     IonContent,
     LoadingController
 } from '@ionic/angular';
 import {
     lookuppage
 } from 'src/core/pages/lookuppage/lookuppage';
 import {
     FormBuilder,
     Validators,
     FormGroup
 } from '@angular/forms';
 import {
     appUtility
 } from 'src/core/utils/appUtility';
 import * as moment from 'moment';
 import 'moment-timezone';
 import {
     Router,
     ActivatedRoute
 } from '@angular/router';
 import {
     lookupFieldMapping
 } from 'src/core/pfmmapping/lookupFieldMapping';
 import {
     objectTableMapping
 } from 'src/core/pfmmapping/objectTableMapping';
 import * as _ from 'underscore';
 import * as Quill from 'quill';
 import {
     cspfmFieldTrackingMapping
 } from 'src/core/pfmmapping/cspfmFieldTrackingMapping';
 import {
     Http,
     Headers
 } from '@angular/http';
 import {
     dbConfiguration
 } from 'src/core/db/dbConfiguration';
 @Component({
     selector: 'crmcustomer_d_m_entry',
     templateUrl: './crmcustomer_d_m_entry.html'
 }) export class crmcustomer_d_m_entry implements OnInit {
     @ViewChild(IonContent) contentArea: IonContent;
     private obj_pfm141093: any = {};
     public ionDateTimeDisplayValue = {};
     private obj_pfm141093_Temp: any = {};
     public formGroup: FormGroup;
     private customAlert;
     public action = 'Add';
     private id: any = {};
     private tableName_pfm141093 = 'pfm141093';
     public isSaveActionTriggered: Boolean = false;
     private isValidFrom: Boolean = true;
     public dbServiceProvider = appConstant.pouchDBStaticName;
     public selectedDataObject: any = {};
     public formulaObject = {};
     public isFieldTrackingEnable: Boolean = false;
     private showAlert: boolean = false;
     infoMessage = '';
     aniClassAddorRemove: boolean = false;
     private workFlowInitiateList = {};
     public savedSuccessMessage = 'data saved sucessfully';
     public updatedSuccessMessage = 'data updated sucessfully';
     public savedErrorMessage = 'Error saving record';
     public fetchErrorMessage = 'Fetching Failed';
     private dependentNumberCount = {};
     public partiallySavedSuccessMessage = 'data partially saved';
     private backButtonSubscribeRef;
     private unRegisterBackButtonAction: Function;
     public parentObjLabel = '';
     public parentObjValue = '';
     private errorMessageToDisplay: string = 'No Records';
     private parentName = '';
     private parentId = '';
     public isParentObjectShow = false;
     public isSkeletonLoading = true;
     isViewRunning = false;
     loading;
     private objectHierarchyJSON = {
         "objectName": "crmcustomer",
         "objectType": "PRIMARY",
         "relationShipType": "",
         "fieldId": "0",
         "objectId": "141093",
         "childObject": []
     };
     public isFromMenu = false;
     private redirectUrl = '/';
     private dependentFieldTriggerList = {};
     private geoLocationdependentField = {};
     public pickListValues = {};
     public parentObject: any = {};
     public formulaConfigJSON = {};
     public formulafields = {};
     childObjectList = [];
     objResultMap = new Map < string, any > ();
     objDisplayName = {
         'pfm141093': 'crmcustomer',
     };
     recordEntryValidation() {
         const formGroupObjectValue = Object.assign({}, this.formGroup.value);
         this.obj_pfm141093 = Object.assign(this.obj_pfm141093, formGroupObjectValue.pfm141093);
         if (
             this.obj_pfm141093["customerstatus"] || this.obj_pfm141093["customerrisktype"] || this.obj_pfm141093["billtolocation"] || this.obj_pfm141093["website"] || this.obj_pfm141093["phone"] || this.obj_pfm141093["name"] || this.obj_pfm141093["shiptolocation"] || this.obj_pfm141093["customername"] || this.obj_pfm141093["annualrevenue"] || this.obj_pfm141093["email"] || this.obj_pfm141093["customercontact"] || this.obj_pfm141093["customernumber"] || this.obj_pfm141093["userid"] || this.obj_pfm141093["userid"]) {
             return true;
         } else {
             return false;
         }
     }
     constructor(public loadingCtrl: LoadingController, public dbConfiguration: dbConfiguration, public http: Http, public router: Router, public activatRoute: ActivatedRoute, public applicationRef: ApplicationRef, public alerCtrl: AlertController, public modalCtrl: ModalController, public dbService: dbProvider, public dataProvider: dataProvider, public formBuilder: FormBuilder, public appUtility: appUtility, public objectTableMapping: objectTableMapping, public lookupFieldMapping: lookupFieldMapping, private toastCtrl: ToastController, public platform: Platform, public fieldTrackMapping: cspfmFieldTrackingMapping, private ngZone: NgZone) {
         this.activatRoute.queryParams.subscribe(params => {
             if (Object.keys(params).length == 0 && params.constructor === Object) {
                 console.log("list query params skipped");
                 return
             }
             if (params["isFromMenu"]) {
                 this.isFromMenu = params["isFromMenu"];
             }
             if (params["redirectUrl"]) {
                 this.redirectUrl = params["redirectUrl"]
             }
             this.action = params['action'];
             this.parentId = params['parentId'];
             this.id = params['id'];
             this.parentName = params['parentName'];
             if (this.parentId) {
                 this.isParentObjectShow = true;
                 this.parentObjLabel = params['parentFieldLabel'];
                 this.parentObjValue = params['parentFieldValue'];
             }
             this.initializeObjects(dataProvider.tableStructure());
             if (this.action == 'Edit') {
                 this.id = params['id'];
                 this.fetchRecordAgainstSelectedObject();
             } else {
                 this.isSkeletonLoading = false;
                 this.checkboxInitialization();
             }
         });
         this.createFormGroup();
         this.hardWareBackButtonAction();
     }
     initializeObjects(tableStructure) {
         this.obj_pfm141093 = JSON.parse(JSON.stringify(tableStructure.pfm141093));
     }
     fetchRecordAgainstSelectedObject() {
         const additionalObjectdata = {};
         additionalObjectdata['id'] = this.id;
         const fetchParams = {
             'objectHierarchyJSON': this.objectHierarchyJSON,
             'additionalInfo': additionalObjectdata,
             'dataSource': appConstant.pouchDBStaticName
         };
         this.dataProvider.querySingleDoc(fetchParams).then(res => {
             this.isSkeletonLoading = false;
             if (res['status'] !== 'SUCCESS') {
                 this.errorMessageToDisplay = res['message'];
                 if (this.errorMessageToDisplay == 'No internet') {
                     this.presentNoInternetToast();
                 }
                 return;
             }
             if (res['records'].length < 0) {
                 console.log('FetchRecordAgainstSelectedObject No Records');
                 return;
             }
             let dataObj = res['records'][0];
             this.obj_pfm141093_Temp = lodash.extend({}, this.obj_pfm141093, dataObj);
             this.selectedDataObject['pfm141093'] = JSON.stringify(this.obj_pfm141093_Temp);
             this.obj_pfm141093 = lodash.extend({}, this.obj_pfm141093, dataObj);
             this.formGroup.patchValue({
                 pfm141093: this.obj_pfm141093
             });
             this.applicationRef.tick();
         }).catch(error => {
             this.isSkeletonLoading = false;
             this.showInfoAlert(this.fetchErrorMessage)
         })
     }
     updateGeoLocationFlag(objectName, dataObj, existingDataObj) {
         Object.keys(this.geoLocationdependentField[objectName]).forEach(element => {
             const depFlds = this.geoLocationdependentField[objectName][element]['dependentFields'];
             depFlds.forEach(fieldName => {
                 if (this.action == 'Edit') {
                     if (existingDataObj[fieldName] != dataObj[fieldName]) {
                         dataObj[this.geoLocationdependentField[objectName][element]['dependentFieldName']] = 'Y';
                         return;
                     }
                 } else {
                     if (dataObj[fieldName] != '') {
                         dataObj[this.geoLocationdependentField[objectName][element]['dependentFieldName']] = 'Y';
                         return;
                     }
                 }
             })
         });
     }
     async presentNoInternetToast() {
         const toast = await this.toastCtrl.create({
             message: "No internet connection. Please check your internet connection and try again.",
             showCloseButton: true,
             closeButtonText: "retry"
         });
         toast.onDidDismiss().then(() => {
             toast.dismiss();
             this.refreshData();
         });
         toast.present();
     }

     refreshData() {
         this.fetchRecordAgainstSelectedObject();
     }
     async saveButtonOnclick() {
         this.formGroup.patchValue({
             pfm141093: {
                 customerstatus: this.obj_pfm141093['customerstatus'],
                 customerrisktype: this.obj_pfm141093['customerrisktype'],
                 billtolocation: this.obj_pfm141093['billtolocation'],
                 website: this.obj_pfm141093['website'],
                 phone: this.obj_pfm141093['phone'],
                 name: this.obj_pfm141093['name'],
                 shiptolocation: this.obj_pfm141093['shiptolocation'],
                 customername: this.obj_pfm141093['customername'],
                 annualrevenue: this.obj_pfm141093['annualrevenue'],
                 email: this.obj_pfm141093['email'],
                 customercontact: this.obj_pfm141093['customercontact'],
                 customernumber: this.obj_pfm141093['customernumber'],
                 userid: this.obj_pfm141093['userid']
             }
         });
         if (this.formGroup.valid) {
             this.formGroup.patchValue({});
             this.isValidFrom = true;
             if (this.obj_pfm141093['annualrevenue'] !== null) {
                 this.obj_pfm141093['annualrevenue'] = Number(this.obj_pfm141093['annualrevenue']);
             }
             if (this.obj_pfm141093['userid'] !== null) {
                 this.obj_pfm141093['userid'] = Number(this.obj_pfm141093['userid']);
             }
             const fieldTrackObject = this.fieldTrackMapping.mappingDetail[this.tableName_pfm141093]
             if (fieldTrackObject) {
                 this.isFieldTrackingEnable = true;
             } else {
                 this.isFieldTrackingEnable = false;
             }
             let previousParentObject
             if (this.action === "Edit") {
                 previousParentObject = this.selectedDataObject[this.tableName_pfm141093];
                 console.log("previousParentObject = ", previousParentObject);
             } else {
                 previousParentObject = undefined
             }
             if (this.parentId) {
                 this.obj_pfm141093[this.parentName] = this.parentId;
             };
             this.dataProvider.save(this.tableName_pfm141093, this.obj_pfm141093, appConstant.pouchDBStaticName, previousParentObject, this.isFieldTrackingEnable)
                 .then(result => {
                     if (result['status'] != 'SUCCESS') {
                         this.showInfoAlert(result['message']);
                         return;
                     }
                     if (this.childObjectList.length == 0) {
                         this.presentToast(this.savedSuccessMessage);
                         let opts = {
                             animate: false
                         };
                         const itemSaveNavigationParams = {
                             id: result["id"]
                         };
                         if (!this.appUtility.checkPageAlreadyInStack("/menu/crmcustomer_d_m_entry")) {
                             itemSaveNavigationParams['redirectUrl'] = "/menu/crmcustomer_d_m_entry"
                         }
                         this.router.navigate(["/menu/crmcustomer_d_m_entry"], {
                             queryParams: itemSaveNavigationParams,
                             skipLocationChange: true
                         });
                     };

                 })
                 .catch(error => {
                     this.showInfoAlert(this.savedErrorMessage);
                     console.log(error)
                 });
         } else {
             this.formGroup.patchValue({});
             this.isValidFrom = false;
             this.scrollToValidationFailedField();
         }
         var errorValue = document.querySelector('.entry-page-content');
         errorValue.setAttribute('class', 'entry-page-content entryErrorMessage hydrated');
     };
     scrollToValidationFailedField() {
         let formControls = this.formGroup.controls.pfm141093;
         let formGroupKeys = Object.keys(formControls["controls"]);
         let isValidationSucceed = true;
         formGroupKeys.every(element => {
             if (formControls["controls"][element].status == "INVALID") {
                 let yOffset = document.getElementById("pfm141093_" + element).offsetTop;
                 this.contentArea.scrollToPoint(0, yOffset, 1000);
                 isValidationSucceed = false;
                 return false;
             } else {
                 return true;
             }
         })
     };
     createFormGroup() {
         this.formGroup = this.formBuilder.group({
             pfm141093: this.formBuilder.group({
                 customerstatus: [null, Validators.compose([])],
                 customerrisktype: [null, Validators.compose([])],
                 billtolocation: [null, Validators.compose([])],
                 website: [null, Validators.compose([Validators.required, Validators.maxLength(200), Validators.pattern("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?")])],
                 phone: [null, Validators.compose([])],
                 shiptolocation: [null, Validators.compose([])],
                 customername: [null, Validators.compose([Validators.required])],
                 annualrevenue: [null, Validators.compose([])],
                 email: [null, Validators.compose([, Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')])],
                 customercontact: [null, Validators.compose([])],
                 customernumber: [null, Validators.compose([Validators.required])],
                 userid: [null, Validators.compose([])]
             })
         });
     }
     lookupResponse(objectname, selectedValue) {}
     loadCheckboxEditValues(fieldName, values) {}
     loadDefaultValues() {}
     resetChildDependentInfo(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName) {}
     ionViewDidEnter() {}
     checkboxInitialization() {};
     backButtonOnclick() {
         if (this.recordEntryValidation()) {
             this.recordDiscardConfirmAlert();
         } else {
             this.navigatePopUpAction();
         }
     };
     navigatePopUpAction() {
         this.router.navigateByUrl(this.redirectUrl, {
             skipLocationChange: true
         });
     };
     hardWareBackButtonAction() {
         this.backButtonSubscribeRef = this.platform.backButton.subscribeWithPriority(999999, () => {
             if (this.customAlert) {
                 return;
             }
             this.backButtonOnclick();
         });
     }
     lookupClearAction(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName) {
         dataObj[dataObjectFieldName] = null;
         delete looklUpObj["id"];
         delete looklUpObj[lookupObjectFieldName];
         this.formGroup.value.formControlerName = null;
         this.resetChildDependentInfo(dataObj, dataObjectFieldName, looklUpObj, lookupObjectFieldName, formControlerName)
     }
     async recordDiscardConfirmAlert() {
         this.customAlert = await this.alerCtrl.create({
             backdropDismiss: false,
             message: 'Are you sure want to leave this page?',
             buttons: [{
                 text: 'Cancel',
                 cssClass: 'method-color',
                 handler: () => {
                     this.customAlert = null;
                     console.log('Individual clicked');
                 }
             }, {
                 text: 'Yes',
                 cssClass: 'method-color',
                 handler: () => {
                     this.navigatePopUpAction();
                 }
             }]
         });
         this.customAlert.present();
     }
     async presentToast(message) {
         const toast = await this.toastCtrl.create({
             message: message,
             duration: 2000,
             position: 'bottom'
         });
         toast.onDidDismiss().then((res) => {
             this.refreshButtonPressed();
         });
         toast.present();
     }
     refreshButtonPressed() {
         this.fetchRecordAgainstSelectedObject();
     }
     async showInfoAlert(info) {
         this.alerCtrl.create({
             message: info,
             subHeader: '',
             buttons: [{
                 text: 'Ok',
                 handler: () => {
                     console.log('Confirm Okay');
                 }
             }]
         }).then(alert => alert.present());
     }
     closeInfoAlert() {
         this.aniClassAddorRemove = false;
         setTimeout(() => {
             this.showAlert = false;
             this.infoMessage = '';
         }, 500);
     };
     onQuillSelectionChanged() {
         var Link = Quill.import('formats/link');
         Link.sanitize = function(url) {
             let protocol = url.slice(0, url.indexOf(':'));
             if (this.PROTOCOL_WHITELIST.indexOf(protocol) === -1) {
                 url = 'http://' + url;
             }
             let anchor = document.createElement('a');
             anchor.href = url;
             protocol = anchor.href.slice(0, anchor.href.indexOf(':'));
             return (this.PROTOCOL_WHITELIST.indexOf(protocol) > -1) ? url : this.SANITIZED_URL;
         }
         Quill.register(Link, true);
     }
     ionViewWillLeave() {
         this.backButtonSubscribeRef.unsubscribe();
     }
     ngOnInit() {}
 }