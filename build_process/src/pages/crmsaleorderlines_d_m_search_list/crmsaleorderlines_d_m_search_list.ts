 import {
     Component,
     ViewChild,
     OnInit,
     ApplicationRef
 } from '@angular/core';
 import {
     LoadingController,
     Events,
     ToastController,
     ModalController,
     IonVirtualScroll
 } from '@ionic/angular';
 import {
     EmailComposer
 } from '@ionic-native/email-composer/ngx';
 import {
     SocialSharing
 } from '@ionic-native/social-sharing/ngx';
 import {
     SMS
 } from '@ionic-native/sms/ngx';
 import {
     CallNumber
 } from '@ionic-native/call-number/ngx';
 import * as lodash from 'lodash';
 import {
     Router,
     ActivatedRoute
 } from '@angular/router';
 import {
     appUtility
 } from 'src/core/utils/appUtility';
 import {
     dataProvider
 } from 'src/core/utils/dataProvider';
 import {
     appConstant
 } from 'src/core/utils/appConstant';
 import {
     Platform
 } from '@ionic/angular';
 import {
     dbProvider
 } from 'src/core/db/dbProvider';
 import {
     offlineDbIndexCreation
 } from 'src/core/utils/offlineDbIndexCreation';
 import {
     crmsaleorderlines_d_m_search_list_Filter
 } from '../crmsaleorderlines_d_m_search_list_Filter/crmsaleorderlines_d_m_search_list_Filter';
 import {
     ColumnMode
 } from "@swimlane/ngx-datatable";
 import {
     FieldInfo
 } from "src/core/pipes/cspfm_data_display";
 @Component({
     selector: 'crmsaleorderlines_d_m_search_list',
     templateUrl: 'crmsaleorderlines_d_m_search_list.html'
 }) export class crmsaleorderlines_d_m_search_list implements OnInit {
     @ViewChild(IonVirtualScroll) virtualScroll: IonVirtualScroll;
     public redirectUrl = "/";
     public isFromMenu = false;
     constructor(public events: Events, public dataProvider: dataProvider, public socialShare: SocialSharing, public loadingCtrl: LoadingController, public modalCtrl: ModalController,
         public callNumber: CallNumber, public emailComposer: EmailComposer, public toastCtrl: ToastController, public sms: SMS, public appUtilityConfig: appUtility, public platform: Platform, public router: Router, public activatRoute: ActivatedRoute, public dbService: dbProvider, public offlineDbIndexCreation: offlineDbIndexCreation) {
         this.filteredResultList = [];
         this.resultList = [];
         this.activatRoute.queryParams.subscribe(params => {
             if (Object.keys(params).length == 0 && params.constructor === Object) {
                 console.log("list query params skipped");
                 return
             }
             if (params["isFromMenu"]) {
                 this.isFromMenu = params["isFromMenu"];
             }
             if (params["redirectUrl"]) {
                 this.redirectUrl = params["redirectUrl"]
             }
         });
         this.fetchAllData(this.objectHierarchyJSON);

         this.appUtilityConfig.setEventSubscriptionlayoutIds(this.tableName_pfm144393, this.layoutId);
         this.events.subscribe(this.layoutId, (modified) => {
             if (modified["dataProvider"] == "PouchDB") {
                 this.dataModifiedEventTrigger(modified);
             }
         });

         this.filterFieldWithoutValues = JSON.parse(JSON.stringify(this.filterFieldWithValues));
         this.filterCustomFieldWithoutValues = JSON.parse(JSON.stringify(this.filterCustomFieldWithValues));


     }

     ngOnDestroy() {
         /*event unsubscriber*/
         console.log('unsubscribe in list')
         if (this.appUtilityConfig.isMobile) {
             this.events.unsubscribe(this.layoutId);
             this.appUtilityConfig.removeEventSubscriptionlayoutIds(this.tableName_pfm144393, this.layoutId);
         }
     }

     dataModifiedEventTrigger(modified) {
         const modifiedData = this.dataProvider.convertRelDocToNormalDoc(modified);
         this.fetchModifiedRec(modifiedData);
     }
     restrictNewRecordsLimit(eventsTriggeredList, limit) {
         if (this.eventsTriggeredList.length > limit) {
             const removedRec = this.eventsTriggeredList.splice(limit, 1)
             Array.prototype.push.apply(this.filteredResultList, removedRec);
             this.filteredResultList = lodash.orderBy(this.filteredResultList, ['name'], ['asc']);
             this.resultList = this.filteredResultList;
         }
     }
     async displayToast(message) {
         const toast = await this.toastCtrl.create({
             message: message,
             duration: 1000,
             position: 'bottom'
         });
         await toast.present();
     }
     checkChildObjectExists(object, childRelationshipJson) {

         if (!this.checkInnerChildObjectExists(object, childRelationshipJson)) {
             return false
         }

         if (childRelationshipJson['innerChild']) {
             if (!this.checkInnerChildObjectExists(object[childRelationshipJson['childId']][0], childRelationshipJson['innerChild'])) {
                 return false
             }

             return this.checkChildObjectExists(object[childRelationshipJson['childId']][0], childRelationshipJson['innerChild'])

         } else {
             if (childRelationshipJson['lookupDetails']) {
                 if (object[childRelationshipJson['childId']][0][childRelationshipJson['lookupDetails']['lookupId']] && object[childRelationshipJson['childId']][0][childRelationshipJson['lookupDetails']['lookupId']] != "") {
                     return true
                 }
                 return false
             }
             return true
         }
     }

     checkInnerChildObjectExists(object, childRelationshipJson) {
         let childKey = childRelationshipJson['childId']
         if (object[childKey] && object[childKey].length > 0) {
             return true
         } else {
             return false
         }
     }

     getChildFieldValue(object, childRelationshipJson) {
         if (!childRelationshipJson['innerChild']) {
             return this.getInnerChildFieldValue(object, childRelationshipJson)
         }

         return this.getChildFieldValue(object[childRelationshipJson['childId']][0], childRelationshipJson['innerChild'])
     }

     getInnerChildFieldValue(object, childRelationshipJson) {
         if (childRelationshipJson['lookupDetails']) {
             let lookupId = childRelationshipJson['lookupDetails']['lookupId']
             return object[childRelationshipJson['childId']][0][lookupId][childRelationshipJson['lookupDetails']['fieldName']]
         }
         return object[childRelationshipJson['childId']][0][childRelationshipJson['fieldName']]
     }

     async fetchModifiedRec(modifiedData) {

         const additionalObjectdata = {};
         // var updatedHierarchyJSON = JSON.parse(JSON.stringify(this.objectHierarchyJSON));
         // if(updatedHierarchyJSON['options']){
         //     updatedHierarchyJSON['options']['selector']['_id'] = "pfm" + updatedHierarchyJSON['objectId'] + "_2_" + modifiedData["id"];
         // }else{
         //     additionalObjectdata['id'] =  modifiedData['id']
         // }

         additionalObjectdata['id'] = modifiedData['id']
         const objHierarchyJSON = JSON.parse(JSON.stringify(this.objectHierarchyJSON))
         if (objHierarchyJSON['options']) {
             delete objHierarchyJSON['options']
         }

         const fetchParams = {
             'objectHierarchyJSON': objHierarchyJSON,
             // 'objectHierarchyJSON' : updatedHierarchyJSON,
             'dataSource': this.dataSource,
             'additionalInfo': additionalObjectdata
         }


         this.dataProvider.querySingleDoc(fetchParams)
             .then(result => {

                 if (result["status"] !== "SUCCESS") {
                     this.errorMessageToDisplay = result["message"];
                     return;
                 }

                 const index = this.getChangedObjectIndex(
                     this.resultList,
                     modifiedData,
                     "id"
                 );
                 if (index > -1) {
                     //already exist
                     this.resultList.splice(index, 1);
                     this.resultList = [...this.resultList];
                     const filteredindex = this.getChangedObjectIndex(
                         this.filteredResultList,
                         modifiedData,
                         "id"
                     );
                     if (filteredindex > -1) {
                         this.filteredResultList.splice(filteredindex, 1);
                         this.filteredResultList = [...this.filteredResultList];
                     }
                 }
                 const modifiedRec = result["records"][0];
                 const eventsTriggeredindex = this.getChangedObjectIndex(this.eventsTriggeredList, modifiedData, "id");
                 if (eventsTriggeredindex > -1) {
                     this.eventsTriggeredList.splice(eventsTriggeredindex, 1);
                 }

                 if (modifiedRec) {
                     this.eventsTriggeredList.splice(0, 0, modifiedRec);
                     if (this.appUtilityConfig.isMobileResolution === false) {
                         this.filteredResultList.splice(index, 0, modifiedRec);
                         this.resultList.splice(index, 0, modifiedRec);
                     }
                 }
                 if (this.appUtilityConfig.isMobile) {
                     this.restrictNewRecordsLimit(this.eventsTriggeredList, 2)
                 } else {
                     this.restrictNewRecordsLimit(this.eventsTriggeredList, 3)
                 }
                 this.recentListRefresh();
                 this.getSearchedItems(this.searchTerm);

             })
             .catch(error => {
                 console.log(error);
             });
     }
     recentListRefresh() {
         if (this.virtualScroll) {
             this.virtualScroll.checkRange(0)
         }
     }

     getChangedObjectIndex(array, modifiedData, key) {
         return lodash.findIndex(array, item => {
             return item[key] === modifiedData[key];
         }, 0)
     }


     public dataSource = 'PouchDB';
     public searchQueryForDesignDoc = "";
     public devWidth = this.platform.width();
     resultList: Array < any > = [];
     public filteredResultList;
     // private layoutId  ;
     public filteredEventTriggeredList = [];
     public isSkeletonLoading = true;

     private tableName_pfm144393 = 'pfm144393';
     public errorMessageToDisplay: string = "No Records";
     public eventsTriggeredList: Array < any > = [];
     public searchTerm: any = "";
     public filterApplied = false;
     public objectHierarchyJSON = {
         "objectName": "crmsaleorderlines",
         "objectType": "PRIMARY",
         "relationShipType": "",
         "fieldId": "0",
         "objectId": "144393",
         "childObject": [{
             "objectName": "crmproduct",
             "objectType": "LOOKUP",
             "relationShipType": "",
             "fieldId": "289353",
             "objectId": "142973",
             "childObject": []
         }]
     };

     public filterFieldWithoutValues = {}
     public filterFieldWithValues = {
         "pfm144393": {
             "product": {
                 "betweenflag": false,
                 "fieldType": "LOOKUP",
                 "fieldValue": ""
             },
             "discountpercentage": {
                 "betweenflag": true,
                 "discountpercentage_to": "",
                 "discountpercentage_from": "",
                 "fieldType": "NUMBER",
                 "fieldValue": "",
                 "betweenfields": ["discountpercentage_from", "discountpercentage_to"]
             },
             "quantity": {
                 "quantity_to": "",
                 "betweenflag": true,
                 "quantity_from": "",
                 "fieldType": "NUMBER",
                 "fieldValue": "",
                 "betweenfields": ["quantity_from", "quantity_to"]
             },
             "listprice": {
                 "betweenflag": true,
                 "listprice_to": "",
                 "fieldType": "CURRENCY",
                 "fieldValue": "",
                 "betweenfields": ["listprice_from", "listprice_to"],
                 "listprice_from": ""
             },
             "productdescription": {
                 "betweenflag": false,
                 "fieldType": "TEXTAREA",
                 "fieldValue": ""
             },
             "totalamount": {
                 "betweenflag": false,
                 "fieldType": "FORMULA",
                 "fieldValue": ""
             },
             "olineid": {
                 "betweenflag": false,
                 "fieldType": "AUTONUMBER",
                 "fieldValue": ""
             },
             "taxpercentage": {
                 "taxpercentage_from": "",
                 "betweenflag": true,
                 "taxpercentage_to": "",
                 "fieldType": "NUMBER",
                 "fieldValue": "",
                 "betweenfields": ["taxpercentage_from", "taxpercentage_to"]
             },
             "partno": {
                 "betweenflag": false,
                 "fieldType": "TEXT",
                 "fieldValue": ""
             }
         },
         "pfm142973": {
             "listprice": {
                 "betweenflag": true,
                 "listprice_to": "",
                 "fieldType": "CURRENCY",
                 "fieldValue": "",
                 "betweenfields": ["listprice_from", "listprice_to"],
                 "listprice_from": ""
             }
         }
     };
     public filterReverseHierarchyJSON = [{
         "objectLayoutLinkId": 237223,
         "objectId": "142973",
         "objectName": "crmproduct",
         "layoutId": 57270,
         "objectType": "LOOKUP",
         "relationshipType": "",
         "lookupId": 40322,
         "fieldId": "289353",
         "referenceObjectId": 144393,
         "childObject": [{
             "objectLayoutLinkId": 237222,
             "objectId": "144393",
             "objectName": "crmsaleorderlines",
             "layoutId": 57270,
             "objectType": "primary",
             "relationshipType": "",
             "lookupId": 0,
             "fieldId": "0",
             "referenceObjectId": 0,
             "childObject": []
         }]
     }];
     public layoutDataRestrictionSet = [];
     public layoutId = "57270";
     public newlyAddedRecordCount = 0;




     public listTableFieldInfoArray: Array < FieldInfo > = [{
         "label": "crmsaleorderlines_d_m_search_list.Element.crmsaleorderlines.olineid",
         "fieldName": "olineid",
         "prop": "olineid",
         "fieldType": "AUTONUMBER",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }, {
         "label": "crmsaleorderlines_d_m_search_list.Element.crmsaleorderlines.product",
         "prop": "pfm142973_289353.product",
         "fieldName": "pfm142973_289353",
         "fieldType": "LOOKUP",
         "child": {
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": "",
             "label": "name",
             "prop": "name",
             "fieldName": "name",
             "fieldType": "AUTONUMBER"
         },
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }, {
         "label": "crmsaleorderlines_d_m_search_list.Element.crmsaleorderlines.productdescription",
         "fieldName": "productdescription",
         "prop": "productdescription",
         "fieldType": "TEXTAREA",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }];
     public gridTableFieldInfoArray: Array < FieldInfo > = [{
         "label": "crmsaleorderlines_d_m_search_list.Element.crmsaleorderlines.olineid",
         "fieldName": "olineid",
         "prop": "olineid",
         "fieldType": "AUTONUMBER",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }, {
         "label": "crmsaleorderlines_d_m_search_list.Element.crmsaleorderlines.product",
         "prop": "pfm142973_289353.product",
         "fieldName": "pfm142973_289353",
         "fieldType": "LOOKUP",
         "child": {
             "child": "",
             "dateFormat": "",
             "mappingDetails": "",
             "currencyDetails": "",
             "label": "name",
             "prop": "name",
             "fieldName": "name",
             "fieldType": "AUTONUMBER"
         },
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }, {
         "label": "crmsaleorderlines_d_m_search_list.Element.crmsaleorderlines.productdescription",
         "fieldName": "productdescription",
         "prop": "productdescription",
         "fieldType": "TEXTAREA",
         "child": "",
         "dateFormat": "",
         "mappingDetails": "",
         "currencyDetails": ""
     }];
     async fetchAllData(objectHierarchyJSON, searchQuery ? ) {
         this.isLoading = true;

         //this.objectHierarchyJSON = this.appUtility.setDataRestrictionByUsers(this.layoutDataRestrictionSet, this.objectHierarchyJSON);
         const fetchParams = {
             'objectHierarchyJSON': objectHierarchyJSON,
             'layoutDataRestrictionSet': this.layoutDataRestrictionSet,
             'dataSource': this.dataSource
         }

         this.dataProvider.fetchDataFromDataSource(fetchParams).then(res => {
             this.isSkeletonLoading = false;
             this.isLoading = false;
             if (res['status'] === 'SUCCESS') {
                 if (res['records'].length > 0) {
                     const data = lodash.orderBy(res['records'], ['name'], ['asc']);
                     this.resultList = data;
                     this.filteredEventTriggeredList = [];
                     this.eventsTriggeredList = []
                     this.filteredResultList = [];
                     this.filteredResultList = [...this.resultList];
                 } else {
                     this.filteredEventTriggeredList = []
                     this.eventsTriggeredList = []
                     this.filteredResultList = [];
                     this.resultList = [];
                     this.errorMessageToDisplay = 'No Records';
                 }
             } else {
                 this.filteredEventTriggeredList = []
                 this.eventsTriggeredList = []
                 this.filteredResultList = [];
                 this.resultList = [];
                 this.errorMessageToDisplay = res['message'];

                 if (this.errorMessageToDisplay == "No internet") {
                     this.presentNoInternetToast();
                 }
             }
         }).catch(error => {
             this.isSkeletonLoading = false;
         });
     }
     async presentNoInternetToast() {
         const toast = await this.toastCtrl.create({
             message: "No internet connection. Please check your internet connection and try again.",
             showCloseButton: true,
             closeButtonText: "retry"
         });
         toast.onDidDismiss().then(() => {
             toast.dismiss();
             this.refreshDataInretry();
         });
         toast.present();
     }

     backButtonOnclick() {
         this.router.navigate([this.redirectUrl], {
             skipLocationChange: true
         });
     }

     refreshDataInretry() {
         this.fetchAllData(this.objectHierarchyJSON);
     }
     getSearchedItems(searchText) {
         this.searchTerm = searchText;
         var queryFields = [{
             "fieldName": "olineid",
             "child": [],
             "mappingDetails": "",
             "fieldType": "AUTONUMBER"
         }, {
             "fieldName": "pfm142973_289353",
             "child": [{
                 "fieldName": "name",
                 "child": [],
                 "mappingDetails": "",
                 "fieldType": "AUTONUMBER"
             }],
             "mappingDetails": "",
             "fieldType": "LOOKUP"
         }, {
             "fieldName": "productdescription",
             "child": [],
             "mappingDetails": "",
             "fieldType": "TEXTAREA"
         }];
         if (searchText === "") {
             this.filteredResultList = [...this.resultList];
             this.filteredEventTriggeredList = [...this.eventsTriggeredList];
             return;
         }
         this.filteredResultList = this.resultList.filter((item) => {
             return this.isSearchMatched(item, queryFields, searchText);
         });
         if (this.appUtilityConfig.isMobileResolution === false) {
             this.resultList = [...this.resultList]
             this.filteredResultList = [...this.filteredResultList]
         }

         this.filteredEventTriggeredList = this.eventsTriggeredList.filter((item) => {
             return this.isSearchMatched(item, queryFields, searchText);
         });

     }
     isSearchMatched(item, queryFields, searchText) {
         for (const queryField of queryFields) {
             if (queryField['fieldType'] == 'STRING' || queryField['fieldType'] == 'TEXT' || queryField['fieldType'] == 'TEXTAREA' || queryField['fieldType'] == 'EMAIL' || queryField['fieldType'] == 'NUMBER' || queryField['fieldType'] == 'URL' || queryField['fieldType'] == 'DECIMAL' || queryField['fieldType'] == 'AUTONUMBER' || queryField['fieldType'] == 'BOOLEAN') {
                 if (item[queryField['fieldName']] && item[queryField['fieldName']].toString().toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
                     return true;
                 }
             } else if (queryField['fieldType'] == 'MULTISELECT' || queryField['fieldType'] == 'CHECKBOX') {
                 if (item[queryField['fieldName']] !== undefined && item[queryField['fieldName']] !== "") {
                     const stateTypeList = item[queryField['fieldName']];
                     for (const element of stateTypeList) {
                         if (queryField['mappingDetails'][element] &&
                             queryField['mappingDetails'][element].toString().toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
                             return true;
                         }
                     }
                 }
             } else if (queryField['fieldType'] == 'LOOKUP') {
                 if (item[queryField['fieldName']]) {
                     if (this.isSearchMatched(item[queryField['fieldName']], queryField['child'], searchText)) {
                         return true
                     }
                 }
             } else if (queryField['fieldType'] == 'one_to_one') {
                 if (item[queryField['fieldName']] && item[queryField['fieldName']].length > 0) {
                     if (this.isSearchMatched(item[queryField['fieldName']][0], queryField['child'], searchText)) {
                         return true
                     }
                 }
             } else if (queryField['fieldType'] == 'RADIO' || queryField['fieldType'] == 'DROPDOWN') {
                 if (item[queryField['fieldName']] && item[queryField['fieldName']] !== "") {
                     if (queryField['mappingDetails'][item[queryField['fieldName']]] &&
                         queryField['mappingDetails'][item[queryField['fieldName']]].toString().toLowerCase().indexOf(searchText.toLowerCase()) > -1) {
                         return true;
                     }
                 }
             }
         }
     }


     private filterCustomFieldWithoutValues = {}
     private filterCustomFieldWithValues = {
         "pfm144393": {
             "pfm142973_289353": {
                 "id": "",
                 "label": "",
                 "object": "pfm142973_289353",
                 "searchKey": ""
             }
         }
     }
     async filterAction() {
         const filterModal = await this.modalCtrl.create({
             component: crmsaleorderlines_d_m_search_list_Filter,
             componentProps: {
                 filterCustomFieldWithValues: this.filterCustomFieldWithValues,
                 filterCustomFieldWithoutValues: this.filterCustomFieldWithoutValues,
                 filterFieldWithValues: this.filterFieldWithValues,
                 filterFieldWithoutValues: this.filterFieldWithoutValues,
                 parentPage: this,
                 dbProvider: this.dbService,
                 dataSource: this.dataSource
             }
         });

         await filterModal.present();
     }

     filterResponse(querySelector ? ) {
         this.isSkeletonLoading = true;
         let msg = this.dataFetchNewMethod('Data_Fetch');

         if (msg === 'success') {
             return;
         } else {
             if (querySelector) {
                 this.filterApplied = true;
                 if (this.dataSource == appConstant.pouchDBStaticName) {
                     this.objectHierarchyJSON['options'] = querySelector;
                 } else {
                     this.searchQueryForDesignDoc = querySelector
                 }
                 this.fetchAllData(this.objectHierarchyJSON, querySelector);
             } else {
                 this.filterApplied = false;
                 if (this.objectHierarchyJSON['options']) {
                     delete this.objectHierarchyJSON['options'];
                 }
                 if (this.dataSource == appConstant.pouchDBStaticName) {
                     this.fetchAllData(this.objectHierarchyJSON);
                 } else {
                     let query = "type:" + this.tableName_pfm144393
                     this.searchQueryForDesignDoc = query;
                     this.fetchAllData(this.objectHierarchyJSON, query);
                 }
             }
         }
     }



     filterParamReset(filterFieldWithValues, filterCustomFieldWithValues) {
         this.filterFieldWithValues = filterFieldWithValues
         this.filterCustomFieldWithValues = filterCustomFieldWithValues
     }
     async onScroll(event) {
         const offsetY = event.offsetY;
         // total height of all rows in the viewport
         const offsetHeight = document.getElementById("table").offsetHeight; // this.el.nativeElement.getBoundingClientRect().height - this.headerHeight + 0;
         const viewHeight = offsetHeight - this.headerHeight;

         // check if we scrolled to the end of the viewport
         if (!this.isLoading && offsetY + viewHeight >= this.filteredResultList.length * this.rowHeight) {
             this.fetchAllData(this.objectHierarchyJSON, this.searchQueryForDesignDoc);
         }
     }

     onTableEventChanged(event) {
         if (event["type"] === "click") {
             this.onItemTap(event["row"]);
         }
     };
     makeFilterPrimaryId(optionArray) {
         this.isSkeletonLoading = true;
         var taskList = [];
         var childObjectArray = [];
         let filterReverseHierarchyJSONArray = [];
         let jsonMappingObj = {};
         jsonMappingObj['144393'] = "Added";

         for (let i = 0; i < this.filterReverseHierarchyJSON.length; i++) {
             const reverseHierarchyJsonObject = this.filterReverseHierarchyJSON[i];
             let optionsSelectorKeys = Object.keys(optionArray);
             console.log(optionsSelectorKeys);

             let filteredReverseHierarchyJSON = this.makeValidReverseHierarchyJSONObject(optionsSelectorKeys, reverseHierarchyJsonObject);
             if (filteredReverseHierarchyJSON != '') {
                 var objectMappingStr = filteredReverseHierarchyJSON.objectId;

                 if (filteredReverseHierarchyJSON.childObject.length > 0) {
                     objectMappingStr = this.makeMappingValueForObjects(objectMappingStr, filteredReverseHierarchyJSON.childObject[0]);
                 }

                 if (!jsonMappingObj[objectMappingStr]) {
                     filterReverseHierarchyJSONArray.push(filteredReverseHierarchyJSON);
                     jsonMappingObj[objectMappingStr] = "Added";
                 }
             }
         }


         for (let i = 0; i < filterReverseHierarchyJSONArray.length; i++) {
             const reverseHierarchyJsonObject = filterReverseHierarchyJSONArray[i];
             taskList.push(this.dbService.fetchDataWithReferenceReverse(reverseHierarchyJsonObject, optionArray).then((res) => {
                 if (res['status'] === 'SUCCESS') {
                     if (res['ids'].length > 0) {
                         console.log("res = ", res);
                         childObjectArray.push(res['ids']);
                     }
                 }
             }))
         }

         var parentObjectIdArray = [];
         Promise.all(taskList).then(res => {
             for (let i = 0; i < childObjectArray.length; i++) {
                 const childObjectInnerArray = childObjectArray[i];
                 for (let j = 0; j < childObjectInnerArray.length; j++) {
                     const primaryObjectId = childObjectInnerArray[j][this.tableName_pfm144393];
                     const primaryId = this.tableName_pfm144393 + "_2_" + primaryObjectId;
                     parentObjectIdArray.push(primaryId);
                 }
             }
             const uniqueparentObjectIdArray = Array.from(new Set(parentObjectIdArray))
             if (uniqueparentObjectIdArray.length > 0) {
                 let parentObjectFilter = {}
                 if (optionArray[this.tableName_pfm144393]) {
                     parentObjectFilter = optionArray[this.tableName_pfm144393];
                     parentObjectFilter["selector"]["_id"] = {
                         "$in": uniqueparentObjectIdArray
                     }
                     this.filterResponse(parentObjectFilter);
                 } else {
                     parentObjectFilter = {
                         "selector": {
                             "data.type": this.tableName_pfm144393,
                             "_id": {}
                         }
                     }
                     parentObjectFilter["selector"]["_id"] = {
                         "$in": uniqueparentObjectIdArray
                     }
                     this.filterResponse(parentObjectFilter);
                 }
             } else {
                 let parentObjectFilter = {};
                 if (optionArray[this.tableName_pfm144393]) {
                     parentObjectFilter = optionArray[this.tableName_pfm144393];
                     this.filterResponse(parentObjectFilter);
                 } else {
                     this.filterResponse();
                 }
             }
         })
     }

     makeValidReverseHierarchyJSONObject(optionsSelectorKeys, hierarchyJSON) {
         if (optionsSelectorKeys.indexOf("pfm" + hierarchyJSON.objectId) > -1) {
             return hierarchyJSON
         } else {
             if (hierarchyJSON.childObject.length > 0) {
                 let tempReverseHierarchyJson = hierarchyJSON.childObject[0]
                 return this.makeValidReverseHierarchyJSONObject(optionsSelectorKeys, tempReverseHierarchyJson)
             } else {
                 return ''
             }
         }
     }

     makeMappingValueForObjects(mappingStr, reverseHierarchyJSON) {
         mappingStr = mappingStr + ',' + reverseHierarchyJSON.objectId
         if (reverseHierarchyJSON.childObject.length > 0) {
             return this.makeMappingValueForObjects(mappingStr, reverseHierarchyJSON.childObject[0])
         } else {
             return mappingStr
         }
     }



     onCancel() {

     }

     ngOnInit() {}
     ionViewDidEnter() {
         if (this.appUtilityConfig.isMobileResolution === false) {
             this.resultList = [...this.resultList]
             this.filteredResultList = [...this.filteredResultList]
         }
     }


     readonly headerHeight = 50;
     readonly rowHeight = 50;
     columnMode = ColumnMode;
     public isLoading;
     addButton_1571942_Onclick() {
         const queryParamsRouting = {
             action: 'Add'
         };
         if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/crmsaleorderlines_d_m_entry")) {
             queryParamsRouting['redirectUrl'] = "/menu/crmsaleorderlines_d_m_search_list"
         }
         this.router.navigate(['/menu/crmsaleorderlines_d_m_entry'], {
             queryParams: queryParamsRouting,
             skipLocationChange: true
         });
     }
     onItemTap(selectedObj) {
         const actionInfo_View = []
         const queryParamsRouting = {
             id: selectedObj["id"],
             viewFetchActionInfo: JSON.stringify(actionInfo_View)
         };
         if (!this.appUtilityConfig.checkPageAlreadyInStack("/menu/crmsaleorderlines_d_m_hl_detail_view")) {
             queryParamsRouting['redirectUrl'] = "/menu/crmsaleorderlines_d_m_search_list"
         }
         this.router.navigate(["/menu/crmsaleorderlines_d_m_hl_detail_view"], {
             queryParams: queryParamsRouting,
             skipLocationChange: true
         });
     }
     dataFetchNewMethod(info) {
         if (info === "") {

             return 'success';
         } else {
             return 'failed';
         }
     }
 }