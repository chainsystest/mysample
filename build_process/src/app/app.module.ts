import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { Broadcaster } from '@ionic-native/broadcaster/ngx';
import { SMS } from '@ionic-native/sms/ngx';
import { File } from '@ionic-native/file/ngx';
import { Network } from '@ionic-native/network/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { HttpModule } from '@angular/http';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import { AppComponent } from './app.component';
import { appRoutingModule } from './app-routing.module';
import { dbConfiguration } from '../core/db/dbConfiguration';
import { dbProvider } from '../core/db/dbProvider';
import { couchdbProvider } from '../core/db/couchdbProvider';
import { MenuComponent } from 'src/core/menu/menu.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Zip } from '@ionic-native/zip/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

// import { DatetimePickerModule } from 'ion-datetime-picker';
import { objectTableMapping } from 'src/core/pfmmapping/objectTableMapping';
import { lookupFieldMapping } from 'src/core/pfmmapping/lookupFieldMapping';
import { themeChanger } from 'src/theme/themeChanger';
import { appConfiguration } from 'src/core/utils/appConfiguration';
import { metaDataDbProvider } from 'src/core/db/metaDataDbProvider';
import { metaDbConfiguration } from 'src/core/db/metaDbConfiguration';
import { metaDbValidation } from 'src/core/utils/metaDbValidation';
import { appUtility } from 'src/core/utils/appUtility';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { calendarBridge } from 'src/core/nativebridges/calendarBridge';
import { mapBridge } from 'src/core/nativebridges/mapBridge';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { approotpageModule } from 'src/core/pages/approotpage/approotpage.module';
import { syncpageModule } from 'src/core/pages/syncpage/syncpage.module';
import { homepageModule } from 'src/core/pages/homepage/homepage.module';
import { lookuppageModule } from 'src/core/pages/lookuppage/lookuppage.module';
import { jsondbProvider } from 'src/core/db/jsondbProvider';
import { Camera } from '@ionic-native/camera/ngx';
import { attachmentlistModule } from 'src/core/pages/attachmentlist/attachmentlist.module';
import { attachmentCouchDbProvider } from 'src/core/db/attachmentCouchDbProvider';
import { attachmentDbConfiguration } from 'src/core/db/attachmentDbConfiguration';
import { attachmentDbProvider } from 'src/core/db/attachmentDbProvider';
import { popoverpageModule } from 'src/core/pages/popoverpage/popoverpage.module';
import { IonicStorageModule } from '@ionic/storage';
import { themepageModule } from 'src/core/pages/themepage/themepage.module';
import { authGuard } from 'src/authentication/authGuard';
import { sessionValidator } from 'src/authentication/sessionValidator';
import { cs_status_workflowmodule } from 'src/core/components/cs_status_workflow/cs_status_workflow.module';
import { cs_status_workflow_popovermodule } from 'src/core/components/cs_status_workflow_popover/cs_status_workflow_popover.module';
import { initialSyncProcess } from 'src/core/db/initialSyncProcess';
import { cs_whocolumn_popovermodule } from 'src/core/components/cs_whocolumn_popover/cs_whocolumn_popover.module';
import { cs_whocolumn_iconmodule } from 'src/core/components/cs_whocolumn_icon/cs_whocolumn_icon.module';
import { networkHandler } from 'src/core/utils/networkHandler';
import { cspfmExecutionPouchDbConfiguration } from 'src/core/db/cspfmExecutionPouchDbConfiguration';
import { cspfmExecutionPouchDbProvider } from 'src/core/db/cspfmExecutionPouchDbProvider';
import { cspfmAuditDbProvider } from 'src/core/db/cspfmAuditDbProvider';
import { cspfmFieldTrackingMapping } from 'src/core/pfmmapping/cspfmFieldTrackingMapping';
import { cspfmAttachmentUploadModule } from 'src/core/pages/cspfmAttachmentUpload/cspfmAttachmentUpload.module';
import { cspfmAttachementUploadDbProvider } from 'src/core/db/cspfmAttachementUploadDbProvider';
import { cspfmFormulamodule } from 'src/core/components/cspfmFormula/cspfmFormula.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { cspfmActionDataServices } from 'src/core/utils/cspfmActionDataServices';
import { AngularSlickgridModule } from 'angular-slickgrid';
import { cspfm_data_display } from 'src/core/pipes/cspfm_data_display';
import { cspfmSettings } from 'src/core/pages/cspfmSettings/cspfmSettings';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

export function createTranslateLoader(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent, MenuComponent,cspfmSettings],
  entryComponents: [cspfmSettings],
  imports: [
    // DatetimePickerModule,
    BrowserModule,BrowserAnimationsModule,
    CalendarModule.forRoot({provide: DateAdapter, useFactory: adapterFactory}),
    IonicModule.forRoot({
      scrollAssist: false
    }),
    NgxDatatableModule.forRoot({
      messages: {
        emptyMessage: 'No data to display', // Message to show when array is presented, but contains no values
        totalMessage: 'total', // Footer total message
        selectedMessage: 'selected' // Footer selected message
      }
    }),
    AngularSlickgridModule.forRoot(),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    appRoutingModule,
    approotpageModule,
    popoverpageModule,
    syncpageModule,
    homepageModule,
    lookuppageModule,Saleorderlist_MOBILE_Grid_with_List_Filtermodule, 

    HttpModule,
    FormsModule, attachmentlistModule, cspfmAttachmentUploadModule,
    ReactiveFormsModule,
    themepageModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    cs_status_workflowmodule,
    cs_status_workflow_popovermodule,
    cs_whocolumn_popovermodule,
    cs_whocolumn_iconmodule,
    cspfmFormulamodule
],
  providers: [
    CallNumber,
    EmailComposer, Camera, authGuard, sessionValidator, initialSyncProcess,
    StatusBar, ScreenOrientation, attachmentCouchDbProvider, attachmentDbProvider, attachmentDbConfiguration,
    SplashScreen, AppPreferences, objectTableMapping, lookupFieldMapping, Broadcaster,
    dbConfiguration, themeChanger, HttpModule, appConfiguration, metaDataDbProvider, metaDbConfiguration, metaDbValidation, SMS, File,
    FileOpener, Zip, LocalNotifications, cspfmActionDataServices,
    appUtility, DatePipe, Network, SocialSharing, jsondbProvider,
    dbProvider, couchdbProvider, calendarBridge, networkHandler, cspfmExecutionPouchDbConfiguration, cspfmExecutionPouchDbProvider, cspfmAuditDbProvider,
    cspfmFieldTrackingMapping, cspfmAttachementUploadDbProvider, CurrencyPipe, cspfm_data_display,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }import { Saleorderlist_MOBILE_Grid_with_List_Filtermodule } from '../pages/Saleorderlist_MOBILE_Grid_with_List_Filter/Saleorderlist_MOBILE_Grid_with_List_Filter.module';
