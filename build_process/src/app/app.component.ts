import { Component } from '@angular/core';
import { Platform, Events, MenuController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { themeChanger } from 'src/theme/themeChanger';
import { fetch as fetchPolyfill } from 'whatwg-fetch';
import { Broadcaster } from '@ionic-native/broadcaster/ngx';
import { appUtility } from 'src/core/utils/appUtility';
import { Router, ActivatedRoute } from '@angular/router';
import { appConfiguration } from 'src/core/utils/appConfiguration';
import { metaDataDbProvider } from 'src/core/db/metaDataDbProvider';
import { metaDbConfiguration } from 'src/core/db/metaDbConfiguration';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  selectedTheme;
  public isValidateOption: Boolean = false;
  announcementList = [];
  public announcementList_length = 0;
  callSyncActionButton: string;
  layoutName = '';
  layoutParams = '';
  menuact = false;
  public userInfoActive: Boolean = false;
  public currentObject;
  public loggedUserDetail = {};
  public appIcon = 'appicon.png';
  public profilePicSrc: string;
  public imagesrc = 'assets/img/app-visualize.svg'
  constructor(public platform: Platform, statusBar: StatusBar, public theme: themeChanger, splashScreen: SplashScreen,
    public events: Events, private broadcaster: Broadcaster, public alertCtrl: AlertController,
    public appUtils: appUtility, private router: Router, public menuctrl: MenuController, public appConfig: appConfiguration,
    public metaDbProvider: metaDataDbProvider, public metaDbConfig: metaDbConfiguration, public appPref: AppPreferences) {


    if (this.appUtils.isMobile) {
      this.callSyncActionButton = "Synchronization";
    }
    else {
      this.callSyncActionButton = "Goto AppList"
    }

    let searchParams = (new URL(window.location.href)).searchParams;
    if (searchParams.get("layoutname")) {
      this.layoutName = searchParams.get("layoutname");
      this.appUtils.enableMenu = false;
      if (searchParams.get("payload")) {
        this.layoutParams = searchParams.get("payload");
      }
    }
    platform.ready().then(() => {
                localStorage.setItem('userId', '35123');
                localStorage.setItem('orgId','3');
                localStorage.setItem('roleId','1');
                localStorage.setItem('assignedApps', JSON.stringify([{ "assignedMenuGroups": [2351], "appName": "Customer and Sales order Management", "appDisplayName": "Customer and Sales order Management", "appId": 39113, "versionNumber":1 }]));
                localStorage.setItem('userTimeZone', 'Asia/Kolkata');
                localStorage.setItem('userDateFormat', 'dd/MM/yyyy');
                localStorage.setItem('appBuilderURL', "https://dev.chainsys.com/apps");
      window.fetch = fetchPolyfill;

      statusBar.backgroundColorByHexString('#ffffff');
      statusBar.styleDefault();

      this.metaValidationAnnouncementEvent()
      if (this.platform.is('ios')) {
        statusBar.overlaysWebView(false);
      }
      splashScreen.hide();
      //   this.theme.setActiveTheme('yellow-theme');
      if (localStorage.getItem('userProfilePicture') != "null") {
        this.profilePicSrc = localStorage.getItem('userProfilePicture');
      } else {
        this.profilePicSrc = "assets/img/user_icon.png";
      }
      this.theme.getActiveTheme().subscribe(val => this.selectedTheme = val);

      if (searchParams.get("layoutname")) {
        this.router.navigate(["/approotpage"],
          { queryParams: { "menu": this.layoutName, "params": this.layoutParams } });

        //this.router.navigate([`/approotpage/${this.layoutName}`])
      }
    });
  }
  AnouncementAction() {
    this.isValidateOption = !this.isValidateOption
  }
  AnnouncementCloseButton() {
    this.isValidateOption = false;
  }
  metaValidationAnnouncementEvent() {
    this.events.subscribe('metaValidationAnnouncement', (modified) => {

      var unChangedWarningTypeList = [];
      if (this.announcementList_length > 0 && modified['warningSet'] && modified['warningSet'].length > 0) {

        if (modified['warningTypes'].length == 2)
          this.announcementList = modified['warningSet']
        else {
          unChangedWarningTypeList = this.announcementList.filter(item => item.annoucementType !== modified['warningTypes'][0])

          if (unChangedWarningTypeList.length > 0) {
            this.announcementList = unChangedWarningTypeList.concat(modified['warningSet']);
          }
          else {
            this.announcementList = modified['warningSet']
          }
        }

      }
      else {
        this.announcementList = modified['warningSet']
      }


      this.announcementList_length = this.announcementList.length
      if (this.appUtils.isMobile) {
        this.callSyncActionButton = "Synchronization";
      }
      else {
        this.callSyncActionButton = "Goto AppList"
      }

    });
  }
  callSyncAction() {
    this.isValidateOption = false;
    this.announcementList = [];
    if (this.appUtils.isMobile) {
      this.broadcaster.fireNativeEvent('ionicNativeBroadcast', { action: 'forceSync' });
    }
    else {
      window.location.replace('/apps/applist?metaFailureAction=Goto AppList');
    }
  }
  triggerMenuEvents() {
    this.userInfoActive = false;
    if (this.appConfig.configuration.isGridMenuEnabled) {
      this.menuact = !this.menuact;
      var element = document.getElementById("cs-box-menu");
      if (this.menuact) {
        element.classList.add("cs-box-menu-active");
      } else {
        element.classList.remove("cs-box-menu-active");
      }
    } else {
      this.menuctrl.toggle();
    }
  }
  public corUsersObjectHierarchyJSON = {
    "objectId": this.metaDbConfig.corUsersObject,
    "objectName": this.metaDbConfig.corUsersObject,
    "fieldId": 0,
    "objectType": "PRIMARY",
    "relationShipType": null,
    "childObject": [

    ]
  };

  getLoggedUserDetail() {
    const options = {};
    const selector = {}
    selector['data.type'] = this.metaDbConfig.corUsersObject;
    selector['data.user_id'] = Number(this.appUtils.userId);
    options['selector'] = selector;
    this.corUsersObjectHierarchyJSON['options'] = options;

    return this.metaDbProvider.fetchDataWithReference(this.corUsersObjectHierarchyJSON).then(corUserResult => {
      if (corUserResult.status !== 'SUCCESS') {

      }
      if (corUserResult['records'].length == 0) {

      }

      const userInfo = JSON.parse(JSON.stringify(corUserResult['records']))
      this.loggedUserDetail = userInfo[0];
      this.userInfoActive = !this.userInfoActive;
      // this.appUtils.presentPopover(event, this.loggedUserDetail)
      console.log("appComponent", this.loggedUserDetail)

    }).catch(err => {
      console.log('Menu Componenet - Exception Received in core user fetching method')
    });

  }


  openSettingPage() {
    this.userInfoActive = false;
    this.router.navigate(["menu/cspfmSettings"], { skipLocationChange: true });

    if (this.appConfig.configuration.isGridMenuEnabled) {
      var element = document.getElementById("cs-box-menu");
      element.classList.remove("cs-box-menu-active");
    }
  }

  logoutAction() {
    this.userInfoActive = false;
    this.showLogoutAlertView();
  }

  async showLogoutAlertView() {
    const confirm = await this.alertCtrl.create({
      header: 'Logout',
      message: 'Are you sure want to logout ?',
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'OK',
          handler: () => {

            window.location.replace('/apps/applist?metaFailureAction=Logout');

          }
        }
      ]
    });
    confirm.present();
  }

  async showExitAlertView() {
    const confirm = await this.alertCtrl.create({
      header: 'Exit',
      message: 'Are you sure want to exit ?',
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'OK',
          handler: () => {

            window.location.replace('/apps/applist');

          }
        }
      ]
    });
    confirm.present();
  }
  async userInfoClick(event) {

    if (Object.keys(this.loggedUserDetail).length == 0)
      this.getLoggedUserDetail()
    else
      this.userInfoActive = !this.userInfoActive;

  }
  appExitAction() {
    this.userInfoActive = false;
    this.showExitAlertView();

    if (this.appConfig.configuration.isGridMenuEnabled) {
      var element = document.getElementById("cs-box-menu");
      element.classList.remove("cs-box-menu-active");
    }
  }
  setApplicationDefaultImage(event) {
    let img = event.srcElement.shadowRoot.children[1];
    img.onerror = () => { img.src = 'assets/img/default_app.png'; };

  }
}
